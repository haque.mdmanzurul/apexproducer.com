﻿using System;
using HOD.Entities;
using HOD.Managers;
using HOD.Managers.Exceptions;
using HOD.Managers.Utilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace HOD.Tests.Managers
{
    [TestClass]
    public class MessageManagerTests
    {
        [TestMethod]
        public void Test_GoodPayload_CorrectLength_Success_ReturnMessage()
        {
            // Arrange
            //
            string phoneNo = "16179458676";
            string message = "HD000001";

            var managerUnderTest = new MessageManager();
            var mockDbManager = new Mock<IDbManager>();
            var mockMessageManagerUtitlity = new Mock<IMessageManagerUtility>();

            mockMessageManagerUtitlity.Setup(x => x.IsValidSmsMessage(phoneNo, message)).Returns(true);

            //mockDbManager.Setup(x => x.SaveSmsRequest(phoneNo, message));
            mockMessageManagerUtitlity.Setup(y => y.ParseRequest(message)).Returns(new ListingRequest() {ListingId = 1});

            var theListing = new Listing(){
                ClientUrl = "http://www.home.com/0001", PhoneNo = "99999999",PropertyDetails = "Apartment"
            };

            mockDbManager.Setup(z => z.GetListingDetail(1)).Returns(theListing);

            mockMessageManagerUtitlity.Setup(a => a.GenerateReturnMessage(theListing, ""))
                .Returns("The apartment is wonderful");

            //managerUnderTest.DbManager = mockDbManager.Object;
            //managerUnderTest.MessageManagerUtility = mockMessageManagerUtitlity.Object;
            //string result = managerUnderTest.ProcessMessage(phoneNo, message);

            mockDbManager.VerifyAll();
            mockMessageManagerUtitlity.VerifyAll();

            // Assert
           //StringAssert.Contains(result, "apartment");

        }
    }
}
