﻿using System;
using HOD.Entities;
using HOD.Managers;
using HOD.Managers.Exceptions;
using HOD.Managers.Utilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace HOD.Tests.Managers
{
    [TestClass]
    public class MessageManagerUtilityTest
    {

        string goodPhoneNo = "16179458676";
        string shortPhoneNo = "123456";
        string emptyMessage = string.Empty;
        private string okMessage = "HD0001";

        [TestInitialize]
        public void InitializeTests(){
        }

        [TestMethod]
        public void TestIsValid_SmsMessage_InvalidTelephoneNo_ReturnFalse()
        {
            var mmUtility = new MessageManagerUtility();

            Assert.AreEqual(false, mmUtility.IsValidSmsMessage(shortPhoneNo, okMessage));
        }

        [TestMethod]
        public void TestIsValid_SmsMessage_Valid_TelephoneNo_ValidMessage_Return_True()
        {
            var mmUtility = new MessageManagerUtility();

            Assert.AreEqual(true, mmUtility.IsValidSmsMessage(goodPhoneNo, okMessage));
        }

        [TestMethod]
        public void TestIsValid_SmsMessage_Valid_TelephoneNo_InvalidMessage_Return_True()
        {
            var mmUtility = new MessageManagerUtility();

            Assert.AreEqual(false, mmUtility.IsValidSmsMessage(goodPhoneNo, emptyMessage));
        }

        [TestMethod]
        public void TestParseMessage_ValidData_ReturnsListingId(){

            var mmUtility = new MessageManagerUtility();

            ListingRequest listingRequest = mmUtility.ParseRequest(okMessage);

            Assert.AreEqual(1, listingRequest.ListingId);

        }

        [TestMethod]
        public void TestParseMessage_ValidData_LargeListingId_ReturnsListingId()
        {

            var mmUtility = new MessageManagerUtility();
            string message = "HD999999999";
            ListingRequest listingRequest = mmUtility.ParseRequest(message);

            Assert.AreEqual(999999999, listingRequest.ListingId);
        }

        [TestMethod]
        [ExpectedException(typeof(MessageManagerExceptions.InvalidMessageRequestException))]
        public void TestParseMessage_InvalidPrefixData_LargeListingId_ThrowsInvalidMessageException()
        {

            var mmUtility = new MessageManagerUtility();
            string message = "AD999999999";
            ListingRequest listingRequest = mmUtility.ParseRequest(message);

            Assert.AreEqual(999999999, listingRequest.ListingId);
        }


        [TestMethod]
        public void TestGenerateReturnMessage_ValidData_ReturnsCorrectString()
        {
            var mmUtility = new MessageManagerUtility();

            var listing = new Listing()
            {ClientUrl = "http://www.home.com/JHAsd", PhoneNo = "99999999", PropertyDetails = "Apartment", ListingId = 1};

            string listingReturnMessage = mmUtility.GenerateReturnMessage(listing, "");

            StringAssert.Contains(listingReturnMessage.ToLower(), "apartment");
        }
        
    }
}
