﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

using System.Web.Routing;
using Moq;

namespace HOD.Tests.Controllers
{
    public class ContextMocks
    {

        public Mock<HttpContextBase> HttpContext { get; set; }
        public Mock<HttpRequestBase> Request { get; set; }
        public RouteData RouteData { get; set; }


        public ContextMocks(ApiController controller)
        {
            HttpContext = new Mock<HttpContextBase>();
            HttpContext.Setup(x => x.Request).Returns(Request.Object);
        }

    }
}
