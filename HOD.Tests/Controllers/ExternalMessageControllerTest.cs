﻿using System.Collections.Specialized;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using HOD.Managers;
using HOD.Managers.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using HOD.Controllers;
using HOD.Tests;
using Moq;
using System.Web.Mvc;

namespace HOD.Tests.Controllers
{
    [TestClass]
    public class ExternalMessageControllerTest
    {
        private string messageBody = "HD0001";
        private string telephoneNo = "16179999999";
        private string returnMessage = "HD0001-Response";

        [TestInitialize]
        public void SetupVariables()
        {
            
        }

        [TestMethod]
        public void SendValidRequest_ProcessSuccessful_ResponseHttpStatusOk()
        {
            // Arrange
            //
            var controller = new ExternalMessageController();

            var mockMessageMgr = new Mock<IMessageManager>();
            //controller.MessageManager = mockMessageMgr.Object;
            //mockMessageMgr.Setup(x => x.ProcessMessage(telephoneNo,messageBody)).Returns(returnMessage);

            HttpRequestMessage request = new HttpRequestMessage(){
                Content = new StringContent(messageBody, Encoding.UTF8, "text/html")
            };

            controller.Request = request;

            HttpConfiguration configuration = new HttpConfiguration();
            controller.Request.Properties["MS_HttpConfiguration"] = configuration;

            var mockReq = new Mock<HttpRequestBase>();

            var requestMessage = new HttpRequestMessage(HttpMethod.Get,
                "phoneNo=" + telephoneNo + "&message=" + messageBody);

            controller.Request = requestMessage;

            // Act
            HttpResponseMessage actualResponse = controller.Send(requestMessage,telephoneNo,messageBody);

            mockMessageMgr.Verify();
            // Assert
            Assert.AreEqual(HttpStatusCode.OK , actualResponse.StatusCode);
            Assert.AreEqual(returnMessage, actualResponse.Content.ReadAsStringAsync().Result);
        }


        [TestMethod]
        public void SendInValidRequest_ContentSpecifiedInPayload_AssumingExceptionOccured_AtManager_ResponseHttpStatus_BadRequest()
        {
            // Arrange
            var controller = new ExternalMessageController();

            var mockMessageMgr = new Mock<IMessageManager>();
            //controller.MessageManager = mockMessageMgr.Object;
            //mockMessageMgr.Setup(x => x.ProcessMessage(telephoneNo, messageBody)).Throws<MessageManagerExceptions.InvalidMessageRequestException>();

            HttpRequestMessage request = new HttpRequestMessage(){
                Content = new StringContent(messageBody, Encoding.UTF8, "text/html")
            };

            controller.Request = request;

            HttpConfiguration configuration = new HttpConfiguration();
            controller.Request.Properties["MS_HttpConfiguration"] = configuration;
            var requestMessage = new HttpRequestMessage(HttpMethod.Get,
                "phoneNo=" + telephoneNo + "&message=" + messageBody);

            controller.Request = requestMessage;

            // Act
            HttpResponseMessage actualResponse = controller.Send(requestMessage, telephoneNo, messageBody);

            // Assert
            Assert.AreEqual(HttpStatusCode.BadRequest, actualResponse.StatusCode);
        }

        [TestMethod]
        public void SendValidRequest_PayLoadIsInvalid_ManagerThrowsException_Response_HttpStatus_BadRequest()
        {
            // Arrange
            //
            messageBody = string.Empty;

            var controller = new ExternalMessageController();

            HttpRequestMessage request = new HttpRequestMessage()
            {
                Content = new StringContent(messageBody, Encoding.UTF8, "text/html")
            };

            controller.Request = request;

            HttpConfiguration configuration = new HttpConfiguration();
            controller.Request.Properties["MS_HttpConfiguration"] = configuration;

            // Act
            var requestMessage = new HttpRequestMessage(HttpMethod.Get,
                "phoneNo=" + telephoneNo + "&message=" + messageBody);

            controller.Request = requestMessage;

            // Act
            HttpResponseMessage actualResponse = controller.Send(requestMessage, telephoneNo, messageBody);


            // Assert
            Assert.AreEqual(HttpStatusCode.BadRequest, actualResponse.StatusCode);
        }



    }
}
