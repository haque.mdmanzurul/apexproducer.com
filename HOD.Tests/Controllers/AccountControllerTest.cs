﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using HOD.Controllers;
using System.Web.Mvc;
using HOD.Payments.StripeChargingGatewayManager.Contracts.ServiceContracts;
using HOD.Payments.StripeChargingGatewayManager.Operations;

namespace HOD.Tests.Controllers
{
    /// <summary>
    /// Summary description for AccountControllerTest
    /// </summary>
    [TestClass]
    public class AccountControllerTest
    {
        public AccountControllerTest()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void Register()
        {
            AccountController controller = new AccountController();
            controller.ControllerContext = new ControllerContext();

            FormCollection fromCollection = new FormCollection();

            CreateStipeTokenRequest createTokenRequest = new CreateStipeTokenRequest()
            {
                CardAddressCountry = "US",
                CardAddressLine1 = "18 th North West Street",
                CardAddressLine2 = "West Bromdale",
                CardAddressCity = "Ft. Lauderdale",
                CardAddressState = "FL",
                CardAddressZip = "33313",
                CardCvc = "123",
                CardExpirationMonth = "11",
                CardExpirationYear = "2014",
                CardName = "Howard McCarthy",
                CardNumber = "4242424242424242",
            };

            StripeOperation operation = new StripeOperation();
            CreateStipeTokenResponse createTokenResponse = operation.CreateStipeToken(createTokenRequest);

            fromCollection.Add("stripeToken", createTokenResponse.Token.Id);
            fromCollection.Add("stripeEmail", "howardmccarthy@gmail.com");
            fromCollection.Add("stripeBillingName", "Howard");
            fromCollection.Add("stripeBillingAddressLine1", "30 Dade Drive");
            fromCollection.Add("stripeBillingAddressZip", "89999");
            fromCollection.Add("stripeBillingAddressState", "St. Catherine");
            fromCollection.Add("stripeBillingAddressCity", "Portmore");
            fromCollection.Add("stripeBillingAddressCountry", "Jamaica");

            fromCollection.Add("UserName", "howardmccarthy@gmail.com");
            fromCollection.Add("FirstName", "Howard");
            fromCollection.Add("LastName", "McCarthy");
            fromCollection.Add("PhoneNumberCell", "9089909");
            fromCollection.Add("PhoneNumberLandLine", "4536666");
            fromCollection.Add("MobileCarrier", "Lime");

            fromCollection.Add("Password", "Password");
            fromCollection.Add("confirmPassword", "Password");

            //ActionResult result = controller.Register(fromCollection);
        }
    }
}
