﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HOD.Entities;
using HOD.Managers.Exceptions;
using HOD.Managers.Utilities;
using Listing = HOD.Data.Entities.Listing;

namespace HOD.Managers
{
    public interface IMenu
    {
        List<MenuItems> GetMenu();
    }
    public interface IListingManager
    {
        int CreateVirtualSign(string propertyType, string listingPerson, string contactPerson, bool PrivateSellerOption, string clientUrl,
            string phoneNo, bool dontShowPhoneNo, decimal listPrice, bool dontShowListPrice, decimal squareFootage,
            bool dontShowSquareFootage, decimal noOfBathrooms, decimal noOfBedrooms, string mainSellingDesc, string locationDesc,
            string address, string city, string province, string postalCode, string neighbourhood, List<HOD.Entities.ListingImage> images, int userId);

        int AddListingDescription(int listingId, string desc, string category, List<HOD.Entities.ListingImage> listingImages);

        void UpdateVirtualSign(HOD.Entities.Listing model, List<HOD.Entities.ListingImage> images);

        int CreateVirtualSign(Listing listing);

        HOD.Entities.ListingImageCategory GetListingImageCategory(int listingCategoryId);

        HOD.Entities.ListingImageCategory GetListingImageCategory(string listingCategoryName);

        List<HOD.Entities.ListingImageCategory> GetListingImageCategories();

        HOD.Entities.ListingImage GetListingImage(int listingImageId);

        List<HOD.Entities.ListingImage> GetListingImageByListing(int listingId);

        List<HOD.Entities.ListingImage> GetListingImages();
        IDbManager DbManager { get; set; }
        IMessageManagerUtility MessageManagerUtility { get; set; }

        List<MenuItems> GetMenu();
        Entities.Listing GetListingData(int listingId);
        List<HOD.Entities.Listing> GetListingsByUserId(int userId);
        List<int> GetListingIdsByUserId(int userId);

        //Listing Section Methods
        void AddListingSection(int listingId, string listingCategory, bool value, int userId);
        HOD.Entities.ListingCategorySection GetListingSection(int listingId, string listingCategory);

        void UpdateMainSummarySection(HOD.Entities.Listing model,  List<HOD.Entities.ListingImage> images);
    }

    public class ListingManager : IListingManager, IMenu
    {
        public IDbManager DbManager { get; set; }
        public IMessageManagerUtility MessageManagerUtility { get; set; }

        /*public MessageManager(IDbManager dbManager, IMessageManagerUtility messageManagerUtility)
        {
            DbManager = dbManager;
            MessageManagerUtility = messageManagerUtility;
        }*/

        public ListingManager()
        {
            DbManager = new DbManager();
            MessageManagerUtility = new MessageManagerUtility();
        }

        public int CreateVirtualSign(Listing listing)
        {
            return DbManager.CreateListing(listing);
        }

        public void UpdateMainSummarySection(HOD.Entities.Listing model, List<HOD.Entities.ListingImage> images)        
        {
            DbManager.UpdateMainSummarySection(model, images);
        }

        /*public int CreateVirtualSign(string propertyType, string listingPerson, string contactPerson,bool PrivateSellerOp, string clientUrl,
            string phoneNo, bool dontShowPhoneNo, decimal listPrice, bool dontShowListPrice, decimal squareFootage,
            bool dontShowSquareFootage, decimal noOfBathrooms, decimal noOfBedrooms, string mainSellingDesc, 
            string image1FilePath, string image2FilePath)
        {
            return DbManager.CreateListing(propertyType, listingPerson,
                contactPerson, PrivateSellerOp, clientUrl, phoneNo, dontShowPhoneNo,
                listPrice, dontShowListPrice, squareFootage, dontShowSquareFootage,
                noOfBathrooms, noOfBedrooms,mainSellingDesc, image1FilePath, image2FilePath);
        }*/

        public int CreateVirtualSign(string propertyType, string listingPerson, string contactPerson,bool PrivateSellerOption, string clientUrl,
            string phoneNo, bool dontShowPhoneNo, decimal listPrice, bool dontShowListPrice, decimal squareFootage,
            bool dontShowSquareFootage, decimal noOfBathrooms, decimal noOfBedrooms, string mainSellingDesc, string locationDesc,
            string address, string city, string province, string postalCode, string neighbourhood, List<HOD.Entities.ListingImage> images, int userId)
        {
            return DbManager.CreateListing(propertyType, listingPerson,
                contactPerson, PrivateSellerOption, clientUrl, phoneNo, dontShowPhoneNo,
                listPrice, dontShowListPrice, squareFootage, dontShowSquareFootage,
                noOfBathrooms, noOfBedrooms, mainSellingDesc, locationDesc,
                address, city, province, postalCode, neighbourhood, images, userId);
        }


        public void UpdateVirtualSign(HOD.Entities.Listing model, List<HOD.Entities.ListingImage> images)
        {
            DbManager.UpdateVirtualSign(model, images);
        }

        public List<MenuItems> GetMenu()
        {
            return DbManager.GetResourcesMenu();
        }

        public int AddListingDescription(int listingId, string desc, string category, List<HOD.Entities.ListingImage> listingImages)
        {
            return DbManager.AddListingDescription(listingId, desc, category, listingImages);
        }

        public Entities.Listing GetListingData(int listingId)
        {
            return DbManager.GetListingDetail(listingId);
        }

        public HOD.Entities.ListingImageCategory GetListingImageCategory(int listingCategoryId)
        {
            return DbManager.GetListingImageCategory(listingCategoryId);
        }

        public HOD.Entities.ListingImageCategory GetListingImageCategory(string listingCategoryName)
        {
            return DbManager.GetListingImageCategory(listingCategoryName);
        }

        public List<HOD.Entities.ListingImageCategory> GetListingImageCategories()
        {
            return DbManager.GetListingImageCategories();
        }

        public HOD.Entities.ListingImage GetListingImage(int listingImageId)
        {
            return DbManager.GetListingImage(listingImageId);
        }

        public List<HOD.Entities.ListingImage> GetListingImageByListing(int listingId)
        {
            return DbManager.GetListingImageByListing(listingId);
        }

        public List<HOD.Entities.ListingImage> GetListingImages()
        {
            return DbManager.GetListingImages();
        }


        public List<HOD.Entities.Listing> GetListingsByUserId(int userId)
        {
            return DbManager.GetListingsByUserId(userId);
        }

        public List<int> GetListingIdsByUserId(int userId)
        {
            return DbManager.GetListingIdsByUserId(userId);
        }

        public void AddListingSection(int listingId, string listingCategory, bool value, int userId)
        {
            DbManager.AddListingSection(listingId, listingCategory, value, userId);
        }

        public HOD.Entities.ListingCategorySection GetListingSection(int listingId, string listingCategory)
        {
            return DbManager.GetListingSection(listingId, listingCategory);
        }

    }
}
