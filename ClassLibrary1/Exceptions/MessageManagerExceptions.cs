﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HOD.Managers.Exceptions
{
    public class MessageManagerExceptions: Exception
    {

        public class InvalidPhoneNumberException : Exception
        {
            public InvalidPhoneNumberException()
            {
            }

            public InvalidPhoneNumberException(string message)
                : base(message)
            {
            }

            public InvalidPhoneNumberException(string message, Exception inner)
                : base(message, inner)
            {
            }
        }

        public class GloballyOptedOutPhoneNumberException : Exception
        {
            public GloballyOptedOutPhoneNumberException()
            {
            }

            public GloballyOptedOutPhoneNumberException(string message)
                : base(message)
            {
            }

            public GloballyOptedOutPhoneNumberException(string message, Exception inner)
                : base(message, inner)
            {
            }
        }

        public class LocalOptOutPhoneNumberException : Exception
        {
            public LocalOptOutPhoneNumberException()
            {
            }

            public LocalOptOutPhoneNumberException(string message)
                : base(message)
            {
            }

            public LocalOptOutPhoneNumberException(string message, Exception inner)
                : base(message, inner)
            {
            }
        }
        
        public class InvalidMessageRequestException: Exception
        {
            public InvalidMessageRequestException()
            {
            }

            public InvalidMessageRequestException(string message)
                : base(message){
            }

            public InvalidMessageRequestException(string message, Exception inner)
                : base(message, inner){
            }
        }

        public class InvalidListingRequestException : Exception
        {
            public InvalidListingRequestException()
            {
            }

            public InvalidListingRequestException(string message)
                : base(message)
            {
            }

            public InvalidListingRequestException(string message, Exception inner)
                : base(message, inner)
            {
            }
        }

        public class InvalidListingImageRequestException : Exception
        {
            public InvalidListingImageRequestException()
            {
            }

            public InvalidListingImageRequestException(string message)
                : base(message)
            {
            }

            public InvalidListingImageRequestException(string message, Exception inner)
                : base(message, inner)
            {
            }
        }

        public class InvalidListingImageCategoryRequestException : Exception
        {
            public InvalidListingImageCategoryRequestException()
            {
            }

            public InvalidListingImageCategoryRequestException(string message)
                : base(message)
            {
            }

            public InvalidListingImageCategoryRequestException(string message, Exception inner)
                : base(message, inner)
            {
            }
        }
    }
}
