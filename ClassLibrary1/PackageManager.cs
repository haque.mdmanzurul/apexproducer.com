﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HOD.Entities;

namespace HOD.Managers
{
    public interface IPackageManager
    {
        PackageChargeFrequency GetPackageChargefrequencyByName(string packageChargefrequencyName);
        PackageChargeFrequency GetPackageChargefrequencyById(int id);
        PackageFeatureChargeFrequency GetPackageFeatureChargeFrequencyByName(string packageFeatureChargefrequencyName);
        PackageFeatureChargeFrequency GetPackageFeatureChargeFrequencyById(int id);
        PackageFeature GetPackageFeatureByName(string packageFeatureName);
        PackageType GetPackageTypeByName(string packageTypeName);
        PackageType GetPackageTypeById(int id);
        PackageFeature GetPackageFeatureById(int id);
        List<PackageFeatureAssociation> GetPackageFeatureAssociationByPackageId(int id);
        List<PackageFeature> GetPackageFeaturesByPackageId(int id);
        UserPackage GetPackageForUser(int userId);
        UserPackage SaveUserPackage(UserPackage userPackage, List<PackageFeature> features);

        StripePayment CreateStripePayment(StripePayment stripePayment);
    }
    public class PackageManager : IPackageManager
    {
        public IDbManager DbManager { get; set; }

        public PackageManager()
        {
            DbManager = new DbManager();
        }

        public PackageChargeFrequency GetPackageChargefrequencyByName(string packageChargefrequencyName)
        {
            return DbManager.GetPackageChargefrequencyByName(packageChargefrequencyName);
        }

        public PackageChargeFrequency GetPackageChargefrequencyById(int id)
        {
            return DbManager.GetPackageChargefrequencyById(id);
        }

        public PackageFeatureChargeFrequency GetPackageFeatureChargeFrequencyByName(string packageFeatureChargefrequencyName)
        {
            return DbManager.GetPackageFeatureChargeFrequencyByName(packageFeatureChargefrequencyName);
        }

        public PackageFeatureChargeFrequency GetPackageFeatureChargeFrequencyById(int id)
        {
            return DbManager.GetPackageFeatureChargeFrequencyById(id);
        }

        public PackageFeature GetPackageFeatureByName(string packageFeatureName)
        {
            return DbManager.GetPackageFeatureByName(packageFeatureName);
        }

        public PackageType GetPackageTypeByName(string packageTypeName)
        {
            return DbManager.GetPackageTypeByName(packageTypeName);
        }

        public PackageType GetPackageTypeById(int id)
        {
            return DbManager.GetPackageTypeById(id);
        }

        public PackageFeature GetPackageFeatureById(int id)
        {
            return DbManager.GetPackageFeatureById(id);
        }

        public List<PackageFeatureAssociation> GetPackageFeatureAssociationByPackageId(int id)
        {
            return DbManager.GetPackageFeatureAssociationByPackageId(id);
        }

        public List<PackageFeature> GetPackageFeaturesByPackageId(int id)
        {
            return DbManager.GetPackageFeaturesByPackageId(id);
        }

        public UserPackage GetPackageForUser(int userId)
        {
            return DbManager.GetPackageForUser(userId);
        }

        public UserPackage SaveUserPackage(UserPackage userPackage, List<PackageFeature> features)
        {
            return DbManager.SaveUserPackage(userPackage, features);
        }


        public StripePayment CreateStripePayment(StripePayment stripePayment)
        {
            return DbManager.CreateStripePayment(stripePayment);
        }
    }
}
