﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HOD.Managers.Exceptions;
using HOD.Managers.Utilities;

using Listing = HOD.Data.Entities.Listing;
using HOD.Entities;

namespace HOD.Managers
{
    public interface IMessageManager
    {
        string ProcessMessage(string phoneNumber, string messageBody, MessageDirection direction);
        void SendSMS(string phoneNumber, string messageBody);

        Message GetMessagesById(int messageId);
        bool ReadMessage(int messageId);
        Message ReadReturnMessage(int messageId);
        Message SaveUserMessage(Message userMessage);
        List<Message> GetMessagesByUser(int fromUser, bool? read);
        List<Message> GetMessagesForUser(int toUser, bool? read);
        List<Message> GetMessagesByConversation(int listingID, bool? read, int toUser, int fromUser, bool isPostBack);
        List<Message> ReadReturnMessagesByConversation(int listingID, bool? read, int toUser, int fromUser);
        List<Message> ReadReturnMessagesByConversation(int forUser, int listingID, bool? read, int toUser, int fromUser);

        List<Guid> GetConversationsForUser(int toUser, bool? read);
        int GetListingIDForUser(int toUser, bool? read);
        MessageType GetMessageTypeById(int messageTypeId);
        MessageType GetMessageTypeByName(string messageTypeName);
        List<MessageType> GetMessageTypes();
    }

    public class MessageManager :IMessageManager
    {
        private IDbManager DbManager = new DbManager();
        private IMessageManagerUtility MessageManagerUtility = new MessageManagerUtility();

        public MessageManager()
        {
            
        }             

        const string InvalidMessageRequestDesc = "The SMS message is invalid";

        public string ProcessMessage(string phoneNo, string message, MessageDirection direction)
        {
            string tollFreeNumber = string.Empty;

            if (!(MessageManagerUtility.IsValidSmsMessage(phoneNo, message))){
                throw new MessageManagerExceptions.InvalidMessageRequestException(InvalidMessageRequestDesc);
            }

            DbManager.SaveSmsRequest(phoneNo,message,direction);

            ListingRequest listingRequest = MessageManagerUtility.ParseRequest(message);
            Entities.Listing foundListing = null;

            foundListing = DbManager.GetListingDetail(listingRequest.ListingId);

            if (foundListing.isPrivateSeller)
                tollFreeNumber = DbManager.GetConfigurationByName("TOLLFREENUMBER").CongfigValue;

            return MessageManagerUtility.GenerateReturnMessage(foundListing,tollFreeNumber);
        }

        public void SendSMS(string phoneNumber, string messageBody){

            try
            {
                string listingText = this.ProcessMessage(phoneNumber, messageBody, MessageDirection.OutGoing);
                MessageManagerUtility.PushMessage(phoneNumber, listingText);
            }
            catch (MessageManagerExceptions.InvalidMessageRequestException exception)
            {
                throw exception;
            }
        }

        public Message SaveUserMessage(Message userMessage)
        {
            return DbManager.SaveUserMessage(userMessage);
        }

        public Message GetMessagesById(int messageId)
        {
            return DbManager.GetMessagesById(messageId);
        }

        public List<Message> GetMessagesByUser(int fromUser, bool? read)
        {
            return DbManager.GetMessagesByUser(fromUser, read);
        }

        public List<Message> GetMessagesForUser(int toUser, bool? read)
        {
            return DbManager.GetMessagesForUser(toUser, read);
        }

        //public List<Message> GetMessagesByConversation(Guid conversationId, bool? read)
        //{
        //    return DbManager.GetMessagesByConversation(conversationId, read);
        //}
        public List<Message> GetMessagesByConversation(int ListingID, bool? read, int toUser, int fromUser, bool isPostBack)
        {
            return DbManager.GetMessagesByConversation(ListingID, read, toUser, fromUser, isPostBack);
        }

        public List<Message> ReadReturnMessagesByConversation(int listingID, bool? read, int toUser, int fromUser)
        {
            return DbManager.ReadReturnMessagesByConversation(listingID, read, toUser, fromUser);
        }

        public List<Message> ReadReturnMessagesByConversation(int forUser, int listingID, bool? read, int toUser, int fromUser)
        {
            return DbManager.ReadReturnMessagesByConversation(forUser, listingID, read, toUser, fromUser);
        }

        public List<Guid> GetConversationsForUser(int toUser, bool? read)
        {
            return DbManager.GetConversationsForUser(toUser, read);
        }

        public int GetListingIDForUser(int toUser, bool? read)
        {
            return DbManager.GetListingIDForUser(toUser, read);
        }

        public MessageType GetMessageTypeById(int messageTypeId)
        {
            return DbManager.GetMessageTypeById(messageTypeId);
        }

        public MessageType GetMessageTypeByName(string messageTypeName)
        {
            return DbManager.GetMessageTypeByName(messageTypeName);
        }

        public List<MessageType> GetMessageTypes()
        {
            return DbManager.GetMessageTypes();
        }

        public bool ReadMessage(int messageId)
        {
            return DbManager.ReadMessage(messageId);
        }

        public Message ReadReturnMessage(int messageId)
        {
            return DbManager.ReadReturnMessage(messageId);
        }
    }
}
