﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HOD.Managers.Translators
{

    public static class PackageFeatureTranslator
    {
        public static HOD.Entities.PackageFeature Translate(HOD.Data.Entities.PackageFeature translate)
        {
            IDbManager dbManager = new DbManager();

            HOD.Entities.PackageFeature translatedPackageFeature = new HOD.Entities.PackageFeature();
            translatedPackageFeature.Description = translate.Description;
            translatedPackageFeature.FeatureChargeFrequencyId = translate.FeatureChargeFrequencyId;
            translatedPackageFeature.Cost = translate.Cost;
            translatedPackageFeature.ShippingCost = translate.ShippingCost;
            translatedPackageFeature.PackageFeatureId = translate.PackageFeatureId;
            translatedPackageFeature.Name = translate.Name;

            return translatedPackageFeature;
        }

        public static HOD.Data.Entities.PackageFeature Translate(HOD.Entities.PackageFeature translate)
        {
            IDbManager dbManager = new DbManager();

            HOD.Data.Entities.PackageFeature translatedPackageFeature = new HOD.Data.Entities.PackageFeature();
            translatedPackageFeature.Description = translate.Description;
            translatedPackageFeature.FeatureChargeFrequencyId = translate.FeatureChargeFrequencyId;
            translatedPackageFeature.Name = translate.Name;
            translatedPackageFeature.Cost = translate.Cost;
            translatedPackageFeature.ShippingCost = translate.ShippingCost;
            translatedPackageFeature.PackageFeatureId = translate.PackageFeatureId;

            return translatedPackageFeature;
        }

        public static List<HOD.Data.Entities.PackageFeature> Translate(List<HOD.Entities.PackageFeature> translateList)
        {
            List<HOD.Data.Entities.PackageFeature> translatedErrorList = new List<HOD.Data.Entities.PackageFeature>();

            foreach (HOD.Entities.PackageFeature translate in translateList)
                translatedErrorList.Add(Translate(translate));

            return translatedErrorList;
        }

        public static List<HOD.Entities.PackageFeature> Translate(List<HOD.Data.Entities.PackageFeature> translateList)
        {
            List<HOD.Entities.PackageFeature> translatedErrorList = new List<HOD.Entities.PackageFeature>();

            foreach (HOD.Data.Entities.PackageFeature translate in translateList)
                translatedErrorList.Add(Translate(translate));

            return translatedErrorList;
        }
    }
}
