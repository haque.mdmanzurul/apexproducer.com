﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HOD.Managers.Translators
{

    public static class PackageFeatureAssociationTranslator
    {
        public static HOD.Entities.PackageFeatureAssociation Translate(HOD.Data.Entities.PackageFeatureAssociation translate)
        {
            IDbManager dbManager = new DbManager();

            HOD.Entities.PackageFeatureAssociation translatedPackageFeatureAssociation = new HOD.Entities.PackageFeatureAssociation();
            translatedPackageFeatureAssociation.Optional = translate.Optional;
            translatedPackageFeatureAssociation.PackagefeatureAssociationId = translate.PackagefeatureAssociationId;
            translatedPackageFeatureAssociation.PackageFeatureId = translate.PackageFeatureId;
            translatedPackageFeatureAssociation.PackageId = translate.PackageFeatureId;
            translatedPackageFeatureAssociation.Shipping = translate.Shipping;

            return translatedPackageFeatureAssociation;
        }

        public static HOD.Data.Entities.PackageFeatureAssociation Translate(HOD.Entities.PackageFeatureAssociation translate)
        {
            IDbManager dbManager = new DbManager();

            HOD.Data.Entities.PackageFeatureAssociation translatedPackageFeatureAssociation = new HOD.Data.Entities.PackageFeatureAssociation();
            translatedPackageFeatureAssociation.Optional = translate.Optional;
            translatedPackageFeatureAssociation.PackagefeatureAssociationId = translate.PackagefeatureAssociationId;
            translatedPackageFeatureAssociation.PackageFeatureId = translate.PackageFeatureId;
            translatedPackageFeatureAssociation.PackageId = translate.PackageFeatureId;
            translatedPackageFeatureAssociation.Shipping = translate.Shipping;

            return translatedPackageFeatureAssociation;
        }

        public static List<HOD.Data.Entities.PackageFeatureAssociation> Translate(List<HOD.Entities.PackageFeatureAssociation> translateList)
        {
            List<HOD.Data.Entities.PackageFeatureAssociation> translatedErrorList = new List<HOD.Data.Entities.PackageFeatureAssociation>();

            foreach (HOD.Entities.PackageFeatureAssociation translate in translateList)
                translatedErrorList.Add(Translate(translate));

            return translatedErrorList;
        }

        public static List<HOD.Entities.PackageFeatureAssociation> Translate(List<HOD.Data.Entities.PackageFeatureAssociation> translateList)
        {
            List<HOD.Entities.PackageFeatureAssociation> translatedErrorList = new List<HOD.Entities.PackageFeatureAssociation>();

            foreach (HOD.Data.Entities.PackageFeatureAssociation translate in translateList)
                translatedErrorList.Add(Translate(translate));

            return translatedErrorList;
        }
    }
}
