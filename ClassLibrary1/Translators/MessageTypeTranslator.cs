﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HOD.Managers.Translators
{
    public static class MessageTypeTranslator
    {
        public static HOD.Entities.MessageType Translate(HOD.Data.Entities.MessageType translate)
        {
            IDbManager dbManager = new DbManager();

            HOD.Entities.MessageType translatedMessage = new HOD.Entities.MessageType();
            translatedMessage.Description = translate.Description;
            translatedMessage.MessageTypeId = translate.MessageTypeId;
            translatedMessage.Name = translate.Name;

            return translatedMessage;
        }

        public static HOD.Data.Entities.MessageType Translate(HOD.Entities.MessageType translate)
        {
            IDbManager dbManager = new DbManager();

            HOD.Data.Entities.MessageType translatedMessage = new HOD.Data.Entities.MessageType();
            translatedMessage.Description = translate.Description;
            translatedMessage.MessageTypeId = translate.MessageTypeId;
            translatedMessage.Name = translate.Name;

            return translatedMessage;
        }

        public static List<HOD.Data.Entities.MessageType> Translate(List<HOD.Entities.MessageType> translateList)
        {
            List<HOD.Data.Entities.MessageType> translatedErrorList = new List<HOD.Data.Entities.MessageType>();

            foreach (HOD.Entities.MessageType translate in translateList)
                translatedErrorList.Add(Translate(translate));

            return translatedErrorList;
        }

        public static List<HOD.Entities.MessageType> Translate(List<HOD.Data.Entities.MessageType> translateList)
        {
            List<HOD.Entities.MessageType> translatedErrorList = new List<HOD.Entities.MessageType>();

            foreach (HOD.Data.Entities.MessageType translate in translateList)
                translatedErrorList.Add(Translate(translate));

            return translatedErrorList;
        }
    }
}
