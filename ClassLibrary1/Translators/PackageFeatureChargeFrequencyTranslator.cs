﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HOD.Managers.Translators
{

    public static class PackageFeatureChargeFrequencyTranslator
    {
        public static HOD.Entities.PackageFeatureChargeFrequency Translate(HOD.Data.Entities.PackageFeatureChargeFrequency translate)
        {
            IDbManager dbManager = new DbManager();

            HOD.Entities.PackageFeatureChargeFrequency translatedPackageFeatureChargeFrequency = new HOD.Entities.PackageFeatureChargeFrequency();
            translatedPackageFeatureChargeFrequency.Description = translate.Description;
            translatedPackageFeatureChargeFrequency.FeatureChargeFrequencyId = translate.FeatureChargeFrequencyId;
            translatedPackageFeatureChargeFrequency.Name = translate.Name;

            return translatedPackageFeatureChargeFrequency;
        }

        public static HOD.Data.Entities.PackageFeatureChargeFrequency Translate(HOD.Entities.PackageFeatureChargeFrequency translate)
        {
            IDbManager dbManager = new DbManager();

            HOD.Data.Entities.PackageFeatureChargeFrequency translatedPackageFeatureChargeFrequency = new HOD.Data.Entities.PackageFeatureChargeFrequency();
            translatedPackageFeatureChargeFrequency.Description = translate.Description;
            translatedPackageFeatureChargeFrequency.FeatureChargeFrequencyId = translate.FeatureChargeFrequencyId;
            translatedPackageFeatureChargeFrequency.Name = translate.Name;

            return translatedPackageFeatureChargeFrequency;
        }

        public static List<HOD.Data.Entities.PackageFeatureChargeFrequency> Translate(List<HOD.Entities.PackageFeatureChargeFrequency> translateList)
        {
            List<HOD.Data.Entities.PackageFeatureChargeFrequency> translatedErrorList = new List<HOD.Data.Entities.PackageFeatureChargeFrequency>();

            foreach (HOD.Entities.PackageFeatureChargeFrequency translate in translateList)
                translatedErrorList.Add(Translate(translate));

            return translatedErrorList;
        }

        public static List<HOD.Entities.PackageFeatureChargeFrequency> Translate(List<HOD.Data.Entities.PackageFeatureChargeFrequency> translateList)
        {
            List<HOD.Entities.PackageFeatureChargeFrequency> translatedErrorList = new List<HOD.Entities.PackageFeatureChargeFrequency>();

            foreach (HOD.Data.Entities.PackageFeatureChargeFrequency translate in translateList)
                translatedErrorList.Add(Translate(translate));

            return translatedErrorList;
        }
    }
}
