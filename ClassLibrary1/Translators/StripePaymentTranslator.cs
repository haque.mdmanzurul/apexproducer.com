﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HOD.Entities;

namespace HOD.Managers.Translators
{

    public static class StripePaymentTranslator
    {
        public static HOD.Entities.StripePayment Translate(HOD.Data.Entities.StripePayment translate)
        {
            IDbManager dbManager = new DbManager();

            HOD.Entities.StripePayment translatedStripePayment = new HOD.Entities.StripePayment();
            translatedStripePayment.StripeChargeId = translate.StripeChargeId;
            translatedStripePayment.StripeCustomerId = translate.StripeCustomerId;
            translatedStripePayment.StripePaymentId = translate.StripePaymentId;
            translatedStripePayment.UserId = translate.UserId;
            translatedStripePayment.Amount = translate.Amount;

            return translatedStripePayment;
        }

        public static HOD.Data.Entities.StripePayment Translate(HOD.Entities.StripePayment translate)
        {
            IDbManager dbManager = new DbManager();

            HOD.Data.Entities.StripePayment translatedStripePayment = new HOD.Data.Entities.StripePayment();
            translatedStripePayment.StripeChargeId = translate.StripeChargeId;
            translatedStripePayment.StripeCustomerId = translate.StripeCustomerId;
            translatedStripePayment.StripePaymentId = translate.StripePaymentId;
            translatedStripePayment.UserId = translate.UserId;
            translatedStripePayment.Amount = translate.Amount;

            return translatedStripePayment;
        }

        public static List<HOD.Data.Entities.StripePayment> Translate(List<HOD.Entities.StripePayment> translateList)
        {
            List<HOD.Data.Entities.StripePayment> translatedErrorList = new List<HOD.Data.Entities.StripePayment>();

            foreach (HOD.Entities.StripePayment translate in translateList)
                translatedErrorList.Add(Translate(translate));

            return translatedErrorList;
        }

        public static List<HOD.Entities.StripePayment> Translate(List<HOD.Data.Entities.StripePayment> translateList)
        {
            List<HOD.Entities.StripePayment> translatedErrorList = new List<HOD.Entities.StripePayment>();

            foreach (HOD.Data.Entities.StripePayment translate in translateList)
                translatedErrorList.Add(Translate(translate));

            return translatedErrorList;
        }
    }
}
