﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HOD.Managers.Translators
{
    public static class UserTranslator
    {
        public static HOD.Entities.User Translate(HOD.Data.Entities.UserProfile translate)
        {
            IDbManager dbManager = new DbManager();

            HOD.Entities.User translatedUser = new HOD.Entities.User();
            translatedUser.FirstName = translate.FirstName;
            translatedUser.LastName = translate.LastName;
            translatedUser.UserId = translate.UserId;
            translatedUser.UserName = translate.UserName;
            translatedUser.UserTypeId = translate.UserTypeId;
           // translatedUser.PrivateMessageFeature = translate.PrivateMessageFeature.HasValue ? translate.PrivateMessageFeature.Value : false;

            return translatedUser;
        }

        public static HOD.Data.Entities.UserProfile Translate(HOD.Entities.User translate)
        {
            IDbManager dbManager = new DbManager();

            HOD.Data.Entities.UserProfile translatedUser = new HOD.Data.Entities.UserProfile();
            translatedUser.FirstName = translate.FirstName;
            translatedUser.LastName = translate.LastName;
            translatedUser.UserId = translate.UserId;
            translatedUser.UserName = translate.UserName;
            translatedUser.UserTypeId = translate.UserTypeId;
            //translatedUser.PrivateMessageFeature = translate.PrivateMessageFeature;

            return translatedUser;
        }

        public static List<HOD.Data.Entities.UserProfile> Translate(List<HOD.Entities.User> translateList)
        {
            List<HOD.Data.Entities.UserProfile> translatedErrorList = new List<HOD.Data.Entities.UserProfile>();

            foreach (HOD.Entities.User translate in translateList)
                translatedErrorList.Add(Translate(translate));

            return translatedErrorList;
        }

        public static List<HOD.Entities.User> Translate(List<HOD.Data.Entities.UserProfile> translateList)
        {
            List<HOD.Entities.User> translatedErrorList = new List<HOD.Entities.User>();

            foreach (HOD.Data.Entities.UserProfile translate in translateList)
                translatedErrorList.Add(Translate(translate));

            return translatedErrorList;
        }
    }
}
