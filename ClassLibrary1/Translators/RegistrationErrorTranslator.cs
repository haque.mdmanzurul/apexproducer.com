﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HOD.Managers.Translators
{
    public static class RegistrationErrorTranslator
    {
        public static HOD.Entities.RegistrationError Translate(HOD.Data.Entities.RegistrationError translate)
        {
            IDbManager dbManager = new DbManager();

            HOD.Entities.RegistrationError translatedError = new HOD.Entities.RegistrationError();
            translatedError.Charge = translate.Charge;
            translatedError.Code = translate.Code;
            translatedError.ErrorType = translate.ErrorType;
            translatedError.Message = translate.Message;
            translatedError.Parameter = translate.Parameter;
            translatedError.RegistrationErrorId = translate.RegistrationErrorId;
            translatedError.StripeError = translate.StripeError;

            return translatedError;
        }

        public static HOD.Data.Entities.RegistrationError Translate(HOD.Entities.RegistrationError translate)
        {
            IDbManager dbManager = new DbManager();

            HOD.Data.Entities.RegistrationError translatedError = new HOD.Data.Entities.RegistrationError();
            translatedError.Charge = translate.Charge;
            translatedError.Code = translate.Code;
            translatedError.ErrorType = translate.ErrorType;
            translatedError.Message = translate.Message;
            translatedError.Parameter = translate.Parameter;
            translatedError.RegistrationErrorId = translate.RegistrationErrorId;
            translatedError.StripeError = translate.StripeError;

            return translatedError;
        }

        public static List<HOD.Data.Entities.RegistrationError> Translate(List<HOD.Entities.RegistrationError> translateList)
        {
            List<HOD.Data.Entities.RegistrationError> translatedErrorList = new List<HOD.Data.Entities.RegistrationError>();

            foreach (HOD.Entities.RegistrationError translate in translateList)
                translatedErrorList.Add(Translate(translate));

            return translatedErrorList;
        }

        public static List<HOD.Entities.RegistrationError> Translate(List<HOD.Data.Entities.RegistrationError> translateList)
        {
            List<HOD.Entities.RegistrationError> translatedErrorList = new List<HOD.Entities.RegistrationError>();

            foreach (HOD.Data.Entities.RegistrationError translate in translateList)
                translatedErrorList.Add(Translate(translate));

            return translatedErrorList;
        }
    }
}
