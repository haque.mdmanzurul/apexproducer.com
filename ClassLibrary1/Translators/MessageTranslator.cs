﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HOD.Managers.Translators
{
    public static class MessageTranslator
    {
        public static HOD.Entities.Message Translate(HOD.Data.Entities.Message translate)
        {
            IDbManager dbManager = new DbManager();

            HOD.Entities.Message translatedMessage = new HOD.Entities.Message();
            translatedMessage.Body = translate.Body;
            translatedMessage.ConversationId = translate.ConversationId;
            translatedMessage.FromUser = translate.FromUser;
            translatedMessage.MessageDate = translate.MessageDate;
            translatedMessage.MessageId = translate.MessageId;
            translatedMessage.MessageTypeId = translate.MessageTypeId;
            translatedMessage.MessageTypeName = dbManager.GetMessageTypeById(translate.MessageTypeId).Name;
            translatedMessage.MessageRead = translate.MessageRead;
            translatedMessage.Subject = translate.Subject;
            translatedMessage.ToUser = translate.ToUser;
            translatedMessage.ListingID = translate.ListingID;
            return translatedMessage;
        }

        public static HOD.Data.Entities.Message Translate(HOD.Entities.Message translate)
        {
            IDbManager dbManager = new DbManager();

            HOD.Data.Entities.Message translatedMessage = new HOD.Data.Entities.Message();
            translatedMessage.Body = translate.Body;
            translatedMessage.ConversationId = translate.ConversationId;
            translatedMessage.FromUser = translate.FromUser;
            translatedMessage.MessageDate = translate.MessageDate;
            translatedMessage.MessageId = translate.MessageId;
            translatedMessage.MessageTypeId = translate.MessageTypeId;
            translatedMessage.MessageRead = translate.MessageRead;
            translatedMessage.Subject = translate.Subject;
            translatedMessage.ToUser = translate.ToUser;
            translatedMessage.ListingID = translate.ListingID;
            return translatedMessage;
        }

        public static List<HOD.Data.Entities.Message> Translate(List<HOD.Entities.Message> translateList)
        {
            List<HOD.Data.Entities.Message> translatedErrorList = new List<HOD.Data.Entities.Message>();

            foreach (HOD.Entities.Message translate in translateList)
                translatedErrorList.Add(Translate(translate));

            return translatedErrorList;
        }

        public static List<HOD.Entities.Message> Translate(List<HOD.Data.Entities.Message> translateList)
        {
            List<HOD.Entities.Message> translatedErrorList = new List<HOD.Entities.Message>();

            foreach (HOD.Data.Entities.Message translate in translateList)
                translatedErrorList.Add(Translate(translate));

            return translatedErrorList;
        }
    }
}
