﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HOD.Managers.Translators
{

    public static class PackageTypeTranslator
    {
        public static HOD.Entities.PackageType Translate(HOD.Data.Entities.PackageType translate)
        {
            IDbManager dbManager = new DbManager();

            HOD.Entities.PackageType translatedPackageType = new HOD.Entities.PackageType();
            translatedPackageType.Description = translate.Description;
            translatedPackageType.PackageTypeId = translate.PackageTypeId;
            translatedPackageType.Name = translate.Name;
            translatedPackageType.PackageChargeFrequencyId = translate.PackageChargeFrequencyId;
            translatedPackageType.BaseCost = translate.BaseCost;

            return translatedPackageType;
        }

        public static HOD.Data.Entities.PackageType Translate(HOD.Entities.PackageType translate)
        {
            IDbManager dbManager = new DbManager();

            HOD.Data.Entities.PackageType translatedPackageType = new HOD.Data.Entities.PackageType();
            translatedPackageType.Description = translate.Description;
            translatedPackageType.PackageTypeId = translate.PackageTypeId;
            translatedPackageType.Name = translate.Name;
            translatedPackageType.PackageChargeFrequencyId = translate.PackageChargeFrequencyId;
            translatedPackageType.BaseCost = translate.BaseCost;

            return translatedPackageType;
        }

        public static List<HOD.Data.Entities.PackageType> Translate(List<HOD.Entities.PackageType> translateList)
        {
            List<HOD.Data.Entities.PackageType> translatedErrorList = new List<HOD.Data.Entities.PackageType>();

            foreach (HOD.Entities.PackageType translate in translateList)
                translatedErrorList.Add(Translate(translate));

            return translatedErrorList;
        }

        public static List<HOD.Entities.PackageType> Translate(List<HOD.Data.Entities.PackageType> translateList)
        {
            List<HOD.Entities.PackageType> translatedErrorList = new List<HOD.Entities.PackageType>();

            foreach (HOD.Data.Entities.PackageType translate in translateList)
                translatedErrorList.Add(Translate(translate));

            return translatedErrorList;
        }
    }
}
