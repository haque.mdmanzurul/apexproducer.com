﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HOD.Managers.Translators
{
    public static class ListingImageCategoryTranslator
    {
        public static HOD.Entities.ListingImageCategory Translate(HOD.Data.Entities.ListingImageCategory translate)
        {
            HOD.Entities.ListingImageCategory translatedImageCategory = new HOD.Entities.ListingImageCategory();
            translatedImageCategory.ListingImageCategoryId = translate.ListingImageCategoryId;
            translatedImageCategory.Name= translate.Name;
            translatedImageCategory.Description = translate.Description;

            return translatedImageCategory;
        }

        public static HOD.Data.Entities.ListingImageCategory Translate(HOD.Entities.ListingImageCategory translate)
        {
            HOD.Data.Entities.ListingImageCategory translatedImageCategory = new HOD.Data.Entities.ListingImageCategory();
            translatedImageCategory.ListingImageCategoryId = translate.ListingImageCategoryId;
            translatedImageCategory.Name = translate.Name;
            translatedImageCategory.Description = translate.Description;

            return translatedImageCategory;
        }

        public static List<HOD.Data.Entities.ListingImageCategory> Translate(List<HOD.Entities.ListingImageCategory> translateList)
        {
            List<HOD.Data.Entities.ListingImageCategory> translatedImageCategoryList = new List<HOD.Data.Entities.ListingImageCategory>();

            foreach (HOD.Entities.ListingImageCategory translate in translateList)
                translatedImageCategoryList.Add(Translate(translate));

            return translatedImageCategoryList;
        }

        public static List<HOD.Entities.ListingImageCategory> Translate(List<HOD.Data.Entities.ListingImageCategory> translateList)
        {
            List<HOD.Entities.ListingImageCategory> translatedImageCategoryList = new List<HOD.Entities.ListingImageCategory>();

            foreach (HOD.Data.Entities.ListingImageCategory translate in translateList)
                translatedImageCategoryList.Add(Translate(translate));

            return translatedImageCategoryList;
        }
        public static List<HOD.Entities.MenuItems> TranslateMenuItems(List<HOD.Data.Entities.MenuItem> DBMenu)
        {
            List<HOD.Entities.MenuItems> translatedMenuList = new List<HOD.Entities.MenuItems>();
            List<Entities.MenuLinks> MenuLinksList = new List<Entities.MenuLinks>();

            HOD.Entities.MenuItems NewMenuItem = new Entities.MenuItems(); 
            HOD.Entities.MenuLinks MenuLinks= new Entities.MenuLinks();

            foreach (HOD.Data.Entities.MenuItem _menuItem in DBMenu)
            {
                NewMenuItem.menuHeading = _menuItem.MenuHeading;
                NewMenuItem.menuId = (int)_menuItem.MenuId;
                NewMenuItem.menuRole = _menuItem.MenuRole;

                foreach(var item in _menuItem.MenuLinks)
                {
                    MenuLinks.Position = (int)item.Position;
                    MenuLinks.Name = item.LinkName;
                    MenuLinks.Hyperlink = item.Hyperlink;

                    MenuLinksList.Add(MenuLinks);
                    MenuLinks = new Entities.MenuLinks();
                }
                NewMenuItem.Links = MenuLinksList;

                translatedMenuList.Add(NewMenuItem);
                NewMenuItem = new Entities.MenuItems();
                MenuLinksList = new List<Entities.MenuLinks>();
            }

            return translatedMenuList;
        }
    }
}
