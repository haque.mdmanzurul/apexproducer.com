﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HOD.Managers.Translators
{

    public static class PackageChargeFrequencyTranslator
    {
        public static HOD.Entities.PackageChargeFrequency Translate(HOD.Data.Entities.PackageChargeFrequency translate)
        {
            IDbManager dbManager = new DbManager();

            HOD.Entities.PackageChargeFrequency translatedMessage = new HOD.Entities.PackageChargeFrequency();
            translatedMessage.Description = translate.Description;
            translatedMessage.PackageChargeFreqencyId = translate.PackageChargeFreqencyId;
            translatedMessage.Name = translate.Name;

            return translatedMessage;
        }

        public static HOD.Data.Entities.PackageChargeFrequency Translate(HOD.Entities.PackageChargeFrequency translate)
        {
            IDbManager dbManager = new DbManager();

            HOD.Data.Entities.PackageChargeFrequency translatedMessage = new HOD.Data.Entities.PackageChargeFrequency();
            translatedMessage.Description = translate.Description;
            translatedMessage.PackageChargeFreqencyId = translate.PackageChargeFreqencyId;
            translatedMessage.Name = translate.Name;

            return translatedMessage;
        }

        public static List<HOD.Data.Entities.PackageChargeFrequency> Translate(List<HOD.Entities.PackageChargeFrequency> translateList)
        {
            List<HOD.Data.Entities.PackageChargeFrequency> translatedErrorList = new List<HOD.Data.Entities.PackageChargeFrequency>();

            foreach (HOD.Entities.PackageChargeFrequency translate in translateList)
                translatedErrorList.Add(Translate(translate));

            return translatedErrorList;
        }

        public static List<HOD.Entities.PackageChargeFrequency> Translate(List<HOD.Data.Entities.PackageChargeFrequency> translateList)
        {
            List<HOD.Entities.PackageChargeFrequency> translatedErrorList = new List<HOD.Entities.PackageChargeFrequency>();

            foreach (HOD.Data.Entities.PackageChargeFrequency translate in translateList)
                translatedErrorList.Add(Translate(translate));

            return translatedErrorList;
        }
    }
}
