﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HOD.Managers.Translators
{
    public static class ListingImageTranslator
    {
        public static HOD.Entities.ListingImage Translate(HOD.Data.Entities.ListingImage translate)
        {
            IDbManager dbManager = new DbManager();

            HOD.Entities.ListingImage translatedImage = new HOD.Entities.ListingImage();
            translatedImage.Image = translate.Image;
            translatedImage.ListingId = translate.ListingId;
            translatedImage.ListingImageCategory = dbManager.GetListingImageCategory(translate.ListingImageCategoryId).Name;
            translatedImage.ListingImageId = translate.ListingImageId;

            return translatedImage;
        }

        public static HOD.Data.Entities.ListingImage Translate(HOD.Entities.ListingImage translate)
        {
            IDbManager dbManager = new DbManager();

            HOD.Data.Entities.ListingImage translatedImage = new HOD.Data.Entities.ListingImage();
            translatedImage.Image = translate.Image;
            translatedImage.ListingId = translate.ListingId;
            translatedImage.ListingImageCategoryId = dbManager.GetListingImageCategory(translate.ListingImageCategory).ListingImageCategoryId;
            translatedImage.ListingImageId = translate.ListingImageId;

            return translatedImage;
        }

        public static List<HOD.Data.Entities.ListingImage> Translate(List<HOD.Entities.ListingImage> translateList)
        {
            List<HOD.Data.Entities.ListingImage> translatedImageCategoryList = new List<HOD.Data.Entities.ListingImage>();

            foreach (HOD.Entities.ListingImage translate in translateList)
                translatedImageCategoryList.Add(Translate(translate));

            return translatedImageCategoryList;
        }

        public static List<HOD.Entities.ListingImage> Translate(List<HOD.Data.Entities.ListingImage> translateList)
        {
            List<HOD.Entities.ListingImage> translatedImageCategoryList = new List<HOD.Entities.ListingImage>();

            foreach (HOD.Data.Entities.ListingImage translate in translateList)
                translatedImageCategoryList.Add(Translate(translate));

            return translatedImageCategoryList;
        }
    }
}
