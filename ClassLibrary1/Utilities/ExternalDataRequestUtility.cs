﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HOD.Entities;
using HOD.Managers.Exceptions;
using System.Net;
using System.IO;

namespace HOD.Managers.Utilities
{
    public interface IExternalDataRequestUtility
    {
        string SendHttpRequest(string url);
    }

    public class ExternalDataRequestUtility : IExternalDataRequestUtility
    {
        public string SendHttpRequest(string url)
        {
            WebRequest request = WebRequest.Create(url);

            request.Credentials = CredentialCache.DefaultCredentials;
            WebResponse response = request.GetResponse();

            Stream dataStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);

            string responseFromServer = reader.ReadToEnd();

            reader.Close();
            response.Close();

            return responseFromServer;
        }
    }
}
