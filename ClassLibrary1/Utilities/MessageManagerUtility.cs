﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HOD.Entities;
using HOD.Managers.Exceptions;
using System.Net;

namespace HOD.Managers.Utilities
{
    public interface IMessageManagerUtility
    {
        bool IsValidSmsMessage (string phone, string messageBody);
        ListingRequest ParseRequest(string message);
        string GenerateReturnMessage(HOD.Entities.Listing listingRequest, string tollFreeNumber );
        void PushMessage(string phoneNo, string message);
        void SendEmailMessage(EmailMessage messageToSend);
    }

    public class MessageManagerUtility : IMessageManagerUtility
    {
        const string messagePrefix = "VUE";

        public bool IsValidSmsMessage(string phone, string messageBody)
        {
            if (phone.Length < 10)
                return false;

            if (messageBody == string.Empty)
                return false;

            return true;
        }

        public ListingRequest ParseRequest(string message)
        {
           
            if (!message.ToLower().Contains(messagePrefix.ToLower())){
                throw new MessageManagerExceptions.InvalidMessageRequestException(string.Format("Prefix of message does not match {0}", messagePrefix));
            }

            int startPoint = message.IndexOf(messagePrefix, StringComparison.OrdinalIgnoreCase);
            string messageId = message.Substring(startPoint + messagePrefix.Length, message.Length - (startPoint + messagePrefix.Length));

            int listingId;
            int.TryParse(messageId, out listingId);

            return new ListingRequest() { ListingId = listingId};
        }

        public string GenerateReturnMessage(HOD.Entities.Listing listing, string tollFreeNumber )
        {
            const string sms_template = "{0}. Contact: {1} Type: {2} PhoneNo: {3} Url: {4}";
            const string sms_private_template = "{0}. Name: Private Type : {1}. Call Toll Free: {2} Url: {3}";

            string businessName =RetrieveBusinessName(listing.UserId.Value);

            HOD.Data.Entities.AgentInformation agentInfo = RetrieveAgentInfoByListingId(listing.ListingId);

            if (listing.isPrivateSeller)
                return string.Format(sms_private_template, businessName, tollFreeNumber, listing.ClientUrl);

            return string.Format(sms_template,businessName, listing.ContactPerson,
                    listing.PhoneNo, listing.ClientUrl);
        }

        private string RetrieveBusinessName(int userId)
        {
            return new DbManager().GetBusinessNameByUserId(userId);  
        }

        private HOD.Data.Entities.AgentInformation RetrieveAgentInfoByListingId(int listingId)
        {
            return new DbManager().RetrieveAgentInfoByListingId(listingId);
        }


        public void PushMessage(string phoneNo, string messageBody){

            IExternalDataRequestUtility dataUtil = new ExternalDataRequestUtility();            
            string response = dataUtil.SendHttpRequest(string.Format("https://app.eztexting.com/api/sending/?user=sostester&pass=ppsos1213&phonenumber={0}&subject={1}&message={2}&express=1",phoneNo, "From CasaVue", messageBody));

            parseResponse(response);

            dataUtil = null;
        }

        public void parseResponse(string response){
           
           int responseCode = 0;

           int.TryParse(response, out responseCode);

           switch (responseCode)
           {
               case 1:
                   return;
               case -5:
                   throw new MessageManagerExceptions.LocalOptOutPhoneNumberException("Apparently in the past you have opted out to receive messages from shortcode 393939");
               case -104:
                   throw new MessageManagerExceptions.GloballyOptedOutPhoneNumberException("Apparently in the past you have opted out to receive messages from shortcode 393939");
               case -106:
                   throw new MessageManagerExceptions.InvalidPhoneNumberException("Invalid phone number - phone number must be 10 characters long.");
               default:
                   throw new ApplicationException("An unexpected result was returned from EzTexting");
           } 
        }

        public void SendEmailMessage(EmailMessage messageToSend)
        {
            try
            {
                string to = string.Join(",", messageToSend.recipients.ToArray());
                //string to = "michellejones@misamjo.com";
                //string to = "michelle+123@particularpresence.com";
                //string from = "michelle@particularpresence.com";
                string from = ApplicationUtility.EmailSettings.DefaultSenderEmailAddress;
                System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage(from, to);

                message.Subject = messageToSend.subject;
                message.Body = messageToSend.body;
                message.ReplyToList.Add(ApplicationUtility.EmailSettings.DefaultSenderEmailAddress);
                message.IsBodyHtml = true;

                System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient(ApplicationUtility.EmailSettings.SMTPAddress, ApplicationUtility.EmailSettings.SMTPPort);

                client.Credentials = new System.Net.NetworkCredential(ApplicationUtility.EmailSettings.UserName , ApplicationUtility.EmailSettings.SMTPPassword);

                client.EnableSsl = true;

                client.Send(message);
            }
            catch
            {
                throw new ApplicationException("An error occurred while sending email. Email not sent.");
            }

        }


    }
}
