﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HOD.Managers.Utilities
{
    public static class ApplicationUtility
    {
        #region Email Settings
        public static class EmailSettings
        {

            public static string SMTPAddress
            {
                get
                {
                    return System.Configuration.ConfigurationManager.AppSettings["SMTPAddress"];
                }
            }

            public static int SMTPPort { 
                get {
                    return int.Parse(System.Configuration.ConfigurationManager.AppSettings["SMTPPort"]);                
                 }
            }

            public static string UserName { 
                get {
                    return System.Configuration.ConfigurationManager.AppSettings["SMTPUserName"];
                }
            }

            public static string SMTPPassword { 

                  get {
                    return System.Configuration.ConfigurationManager.AppSettings["SMTPPassword"];
                }
            }

            public static string DefaultSenderEmailAddress
            {

                get
                {
                    return System.Configuration.ConfigurationManager.AppSettings["DefaultSenderEmailAddress"];
                }
            }

       }

        #endregion

        public static string DefaultNoImageFileName
        {
            get
            {
                return System.Configuration.ConfigurationManager.AppSettings["NoHouseImageName"];
            }
        }
        public static string ImageStoreURL {
            get {
                return System.Configuration.ConfigurationManager.AppSettings["AWSS3BucketURL"];
            }
        }

        public static string VideoStoreURL
        {
            get
            {
                return System.Configuration.ConfigurationManager.AppSettings["AWSS3VideoBucketURL"];
            }
        }

        public static string AWSVideoStoreName
        {
            get
            {
                return System.Configuration.ConfigurationManager.AppSettings["AWSS3VideoBucket"];
            }
        }

        public static string AWSBucketName {
            get
            {
                return System.Configuration.ConfigurationManager.AppSettings["AWSS3BucketName"];
            }
        }

        public static string AWSAccessKey {
            get
            {
                return System.Configuration.ConfigurationManager.AppSettings["AWSAccessKey"];
            }            
        }

        public static string AWSSecretKey
        {
            get
            {
                return System.Configuration.ConfigurationManager.AppSettings["AWSSecretKey"];
            }
        }


   }
}
