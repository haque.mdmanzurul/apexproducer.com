﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HOD.Data.Entities;
using HOD.Managers.Constants;
using HOD.Managers.Exceptions;
using HOD.Managers.Translators;
using busEntities = HOD.Entities;

namespace HOD.Managers
{
    public interface IDbManager
    {
        void SaveSmsRequest(string phoneNo, string messageBody, busEntities.MessageDirection direction);
        busEntities.Listing GetListingDetail(int listingId);

        //int CreateListing(string propertyType, string listingPerson, string contactPerson,bool PrivateSellerOp, string clientUrl,
        //    string phoneNo, bool dontShowPhoneNo, decimal listPrice, bool dontShowListPrice, decimal squareFootage,
        //    bool dontShowSquareFootage, decimal noOfBathrooms, decimal noOfBedrooms, string mainSellingDesc,
        //    string image1FilePath, string image2FilePath);

        int CreateListing(string propertyType, string listingPerson, string contactPerson, bool PrivateSellerOp, string clientUrl,
            string phoneNo, bool dontShowPhoneNo, decimal listPrice, bool dontShowListPrice, decimal squareFootage,
            bool dontShowSquareFootage, decimal noOfBathrooms, decimal noOfBedrooms, string mainSellingDesc, string locationDesc,
            string address, string city, string province, string postalCode, string neighbourhood, List<busEntities.ListingImage> listingImages, int userId);

        int AddListingDescription(int listingId, string desc, string imageCategory, List<busEntities.ListingImage> listingImages);

        int CreateListing(Listing listing);

        busEntities.ListingImageCategory GetListingImageCategory(int listingCategoryId);

        busEntities.ListingImageCategory GetListingImageCategory(string listingCategoryName);

        List<busEntities.ListingImageCategory> GetListingImageCategories();

        busEntities.ListingImage GetListingImage(int listingImageId);
        //Boolean removeListingImage(int listingId);

        List<busEntities.ListingImage> GetListingImageByListing(int listingId);

        List<busEntities.ListingImage> GetListingImages();

        int saveListingImage(ListingImage image);

        int CreateRegistrationError(busEntities.RegistrationError registrationError);

        List<busEntities.Configuration> GetAllConfiguration();

        HOD.Entities.Configuration GetConfigurationById(int Id);
        HOD.Entities.Configuration GetConfigurationByName(string configName);

        void UpdateVirtualSign(HOD.Entities.Listing model,List<busEntities.ListingImage> images);
        void UpdateMainSummarySection(HOD.Entities.Listing model, List<busEntities.ListingImage> images);

        List<busEntities.Listing> GetListingsByUserId(int userId);
        List<int> GetListingIdsByUserId(int userId);

        void AddListingSection(int listingId, string listingCategory, bool value, int userId);
        HOD.Entities.ListingCategorySection GetListingSection(int listingId, string listingCategory);

        busEntities.Message GetMessagesById(int messageId);
        bool ReadMessage(int messageId);
        busEntities.Message ReadReturnMessage(int messageId);
        busEntities.Message SaveUserMessage(busEntities.Message userMessage);
        List<busEntities.Message> GetMessagesByUser(int fromUser, bool? read);
        List<busEntities.Message> GetMessagesForUser(int toUser, bool? read);
        //List<busEntities.Message> GetMessagesByConversation(Guid conversationId, bool? read);
        List<busEntities.Message> GetMessagesByConversation(int listingID, bool? read, int toUser, int fromUser, bool isPostBack);
        List<busEntities.Message> ReadReturnMessagesByConversation(int listingID, bool? read, int toUser, int fromUser);
        List<busEntities.Message> ReadReturnMessagesByConversation(int forUser, int listingID, bool? read, int toUser, int fromUser);

        List<Guid> GetConversationsForUser(int toUser, bool? read);
        int GetListingIDForUser(int toUser, bool? read);

        List<busEntities.User> GetUserByUserType(int userType);
        busEntities.User GetUserByUserId(int userId);

        busEntities.User GetUserByUserName(string userName);

        busEntities.MessageType GetMessageTypeById(int messageTypeId);
        busEntities.MessageType GetMessageTypeByName(string messageTypeName);
        List<busEntities.MessageType> GetMessageTypes();


         busEntities.PackageChargeFrequency GetPackageChargefrequencyByName(string packageChargefrequencyName);
         busEntities.PackageChargeFrequency GetPackageChargefrequencyById(int id);
         busEntities.PackageFeatureChargeFrequency GetPackageFeatureChargeFrequencyByName(string packageFeatureChargefrequencyName);
         busEntities.PackageFeatureChargeFrequency GetPackageFeatureChargeFrequencyById(int id);
         busEntities.PackageFeature GetPackageFeatureByName(string packageFeatureName);
         busEntities.PackageType GetPackageTypeByName(string packageTypeName);
         busEntities.PackageType GetPackageTypeById(int id);
         busEntities.PackageFeature GetPackageFeatureById(int id);
         List<busEntities.PackageFeatureAssociation> GetPackageFeatureAssociationByPackageId(int id);
         List<busEntities.PackageFeature> GetPackageFeaturesByPackageId(int id);
         busEntities.UserPackage GetPackageForUser(int userId);
         busEntities.UserPackage SaveUserPackage(busEntities.UserPackage userPackage, List<busEntities.PackageFeature> features);
         List<busEntities.MenuItems> GetResourcesMenu();
         busEntities.StripePayment CreateStripePayment(busEntities.StripePayment stripePayment);    }

    public interface IDbAgent
    {
        int GetAgentIDByURL(string URL);
    }



    public class DbManager : IDbManager,IDbAgent
    {
        const string messageDirectionIncoming = "I";
        const string messageDirectionOutgoing = "O";
        public const String VIRTUALSIGN = "VirtualSign";
        public const String LOCATION = "Location";
        public const String KITCHEN = "Kitchen";
        public const String BATHROOM = "Bathroom";
        public const String OBEDROOM = "Other Bedroom";
        public const String BASEMENT = "Basement";
        public const String FAMILYROOM = "Family Room";
        public const String DININGROOM = "Dining Room";

        //Add Listing IMage
        public int saveListingImage(ListingImage image)
        {
            using (var db = new HODContext())
            {
                var listingImage = new ListingImage()
                {
                    Image = image.Image,
                    ListingId = image.ListingId,
                    ListingImageCategoryId = image.ListingImageCategoryId
                    //MessageDirection = messageDirection                    
                };

                db.ListingImages.Add(listingImage);
                db.SaveChanges();

                return listingImage.ListingImageId;
            }
        }


        //Remove Listing IMage

        //public Boolean removeListingImage(ListingImage image)
        //{

        //    using (var db = new HODContext())
        //    {
        //    //var image = db.ListingImages.Where(x => x.ListingImageId == listingImageId);
        //        db.ListingImages.Remove(image);
        //        db.SaveChanges();

        //        if (image == null)
        //            return true;
        //        else
        //            return false;
        //    }

        //}


        public void SaveSmsRequest(string phoneNo, string messageBody, busEntities.MessageDirection direction)
        {
            string messageDirection;

            if (direction == busEntities.MessageDirection.Incoming){
                messageDirection = messageDirectionIncoming;
            } else {
                messageDirection = messageDirectionOutgoing;
            }

            using (var db = new HODContext())
            {
                var smsMessage = new smsMessage()
                {
                    phoneNo = phoneNo,
                    message = messageBody,
                    messageDate = DateTime.Now,
                    messageGuid = Guid.NewGuid().ToString(),
                    //MessageDirection = messageDirection                    
                };

                db.smsMessages.Add(smsMessage);
                db.SaveChanges();
            }
        }

        public busEntities.Message SaveUserMessage(busEntities.Message userMessage)
        {
            using (var db = new HODContext())
            {
                var message = new Message()
                {
                    MessageTypeId = userMessage.MessageTypeId,
                    ConversationId = userMessage.ConversationId,
                    FromUser = userMessage.FromUser,
                    ToUser = userMessage.ToUser,
                    Subject = userMessage.Subject,
                    Body = userMessage.Body,
                    MessageDate = userMessage.MessageDate,
                    MessageRead = userMessage.MessageRead,
                    ListingID = userMessage.ListingID
                };

                db.Messages.Add(message);
                db.SaveChanges();

                return MessageTranslator.Translate(message);
            }
        }

        public bool ReadMessage(int messageId)
        {
            using (var db = new HODContext())
            {
                Message message = db.Messages.Where(m => m.MessageId == messageId).SingleOrDefault<Message>();

                if (message != null)
                {
                    message.MessageRead = true;

                    db.SaveChanges();
                }

                return message.MessageRead;
            }
        }

        public busEntities.Message ReadReturnMessage(int messageId)
        {
            busEntities.Message userMessage = null;
            using (var db = new HODContext())
            {
                
                Message message = db.Messages.Where(m => m.MessageId == messageId).SingleOrDefault<Message>();
                message.MessageRead = true;
                db.SaveChanges();

                if (message != null)
                {
                    userMessage = MessageTranslator.Translate(message);
                }
            }

            return userMessage;
        }

        public busEntities.Message GetMessagesById(int messageId)
        {
            busEntities.Message userMessage = null;
            using (var db = new HODContext())
            {

                    Message message = db.Messages.Where(m => m.MessageId == messageId).SingleOrDefault<Message>();


                    if (message != null)
                {
                    userMessage = MessageTranslator.Translate(message);
                }
            }

            return userMessage;
        }



        public List<busEntities.Message> GetMessagesByUser(int fromUser, bool? read)
        {
            List<busEntities.Message> userMessages = null;
            using (var db = new HODContext())
            {
                IQueryable<Message> messages = null;

                if(read.HasValue)
                    messages = db.Messages.Where(m => m.FromUser == fromUser && m.MessageRead == read).OrderBy(m => m.MessageId);
                else
                    messages = db.Messages.Where(m => m.FromUser == fromUser).OrderBy(m => m.MessageId);

                if (messages != null)
                {
                    userMessages = MessageTranslator.Translate(messages.ToList<Message>());
                }
            }

            return userMessages;
        }

        public List<busEntities.Message> GetMessagesForUser(int toUser, bool? read)
        {
            List<busEntities.Message> userMessages = null;
            using (var db = new HODContext())
            {
                IQueryable<Message> messages = null;

                if (read.HasValue)
                    messages = db.Messages.Where(m => m.ToUser == toUser && m.MessageRead == read).OrderBy(m => m.MessageId);
                else
                    messages = db.Messages.Where(m => m.ToUser == toUser).OrderBy(m => m.MessageId);

                if (messages != null)
                {
                    userMessages = MessageTranslator.Translate(messages.ToList<Message>());
                }
            }

            return userMessages;
        }

        public List<busEntities.Message> GetMessagesByConversation(int listingID, bool? read, int toUser, int fromUser, bool isPostBack)
        {
            List<busEntities.Message> userMessages = null;

            using (var db = new HODContext())
            {
                IQueryable<Message> messages = null;
                List<Message> tempList = new List<Message>();
                Message tempVar = new Message();

                if (read.HasValue)
                {
                    messages = db.Messages.Where(m => m.ListingID == listingID && m.MessageRead == read).OrderBy(m => m.MessageId);
                    messages = messages.Where(m => m.FromUser == fromUser && m.ToUser == toUser || m.FromUser == toUser && m.ToUser == fromUser && m.ListingID == listingID).OrderBy(m => m.MessageId);
                }
                else
                {
                    if (!isPostBack)
                    {
                        messages = db.Messages.Where(m => m.ListingID == listingID).OrderBy(m => m.MessageId);
                        messages = messages.Where(m => m.FromUser == fromUser && m.ToUser == toUser || m.FromUser == toUser && m.ToUser == fromUser && m.ListingID == listingID).OrderBy(m => m.MessageId);
                    }
                    else
                    {
                        messages = db.Messages.Where(m => m.ListingID == listingID).OrderBy(m => m.MessageId);
                        var conversationIDs =  (from u in messages group u by u.ConversationId into grp where grp.Count() > 0 select grp.Key).ToList();

                        foreach (Guid g in conversationIDs)
                        {
                            tempVar = (from m in messages where m.ConversationId == g select m).First();
                            tempList.Add(tempVar);
                        }

                        messages = (from t in tempList.ToList() select t).AsQueryable();
                    }
                }

                if (messages != null)
                {
                    userMessages = MessageTranslator.Translate(messages.ToList<Message>());
                }
            }

            return userMessages;
        }

        public List<busEntities.Message> ReadReturnMessagesByConversation(int listingID, bool? read, int toUser, int fromUser)
        {
            List<busEntities.Message> userMessages = null;
            using (var db = new HODContext())
            {
                IQueryable<Message> messages = null;

                if (read.HasValue)
                {
                    messages = db.Messages.Where(m => m.ListingID == listingID && m.MessageRead == read).OrderBy(m => m.MessageId);
                    messages = messages.Where(m => m.FromUser == fromUser && m.ToUser == toUser || m.FromUser == toUser && m.ToUser == fromUser && m.ListingID == listingID).OrderBy(m => m.MessageId);
                }
                else
                {
                    messages = db.Messages.Where(m => m.ListingID == listingID).OrderBy(m => m.MessageId);
                    messages = messages.Where(m => m.FromUser == fromUser && m.ToUser == toUser || m.FromUser == toUser && m.ToUser == fromUser && m.ListingID == listingID).OrderBy(m => m.MessageId);
                }

                bool updateRead = false;
                if (messages != null)
                {
                    foreach (Message message in messages)
                    {
                        if (!message.MessageRead)
                        {
                            updateRead = true;
                            message.MessageRead = true;
                        }
                    }
                    if (updateRead)
                        db.SaveChanges();

                    userMessages = MessageTranslator.Translate(messages.ToList<Message>());
                }
            }

            return userMessages;
        }

        public List<busEntities.Message> ReadReturnMessagesByConversation(int forUser, int listingID, bool? read, int toUser, int fromUser)
        {
            List<busEntities.Message> userMessages = null;
            using (var db = new HODContext())
            {
                IQueryable<Message> messages = null;

                if (read.HasValue)
                {
                    messages = db.Messages.Where(m => m.ListingID == listingID && m.MessageRead == read).OrderBy(m => m.MessageId);
                    messages = messages.Where(m => m.FromUser == fromUser && m.ToUser == toUser || m.FromUser == toUser && m.ToUser == fromUser && m.ListingID == listingID).OrderBy(m => m.MessageId);
                }
                else
                {
                    messages = db.Messages.Where(m => m.ListingID == listingID).OrderBy(m => m.MessageId);
                    messages = messages.Where(m => m.FromUser == fromUser && m.ToUser == toUser || m.FromUser == toUser && m.ToUser == fromUser && m.ListingID == listingID).OrderBy(m => m.MessageId);
                }

                bool updateRead = false;
                if (messages != null)
                {
                    foreach (Message message in messages)
                    {
                        if (!message.MessageRead && (message.ToUser == forUser))
                        {
                            updateRead = true;
                            message.MessageRead = true;
                        }
                    }
                    if (updateRead)
                        db.SaveChanges();

                    userMessages = MessageTranslator.Translate(messages.ToList<Message>());
                }
            }

            return userMessages;
        }

        public List<Guid> GetConversationsForUser(int toUser, bool? read)
        {
            List<busEntities.Message> userMessages = null;
            List<Guid> userConversations = null;
            using (var db = new HODContext())
            {
                IQueryable<Message> messages = null;

                if (read.HasValue)
                    messages = db.Messages.Where(m => m.ToUser == toUser && m.MessageRead == read).OrderBy(m => m.MessageId);
                else
                    messages = db.Messages.Where(m => m.ToUser == toUser).OrderBy(m => m.MessageId);

                if (messages != null)
                {
                    userMessages = MessageTranslator.Translate(messages.ToList<Message>());
                    userConversations = userMessages.Select(d => d.ConversationId).Distinct<Guid>().ToList();
                }
            }

            return userConversations;
        }

        public int GetListingIDForUser(int toUser, bool? read)
        {
            List<busEntities.Message> userMessages = null;
            int listingID = 0;
            using (var db = new HODContext())
            {
                IQueryable<Message> messages = null;

                if (read.HasValue)
                    messages = db.Messages.Where(m => m.ToUser == toUser && m.MessageRead == read).OrderBy(m => m.MessageId);
                else
                    messages = db.Messages.Where(m => m.ToUser == toUser).OrderBy(m => m.MessageId);

                if (messages != null)
                {
                    userMessages = MessageTranslator.Translate(messages.ToList<Message>());
                    if (userMessages.Count > 0)
                        listingID = userMessages.Select(d => d.ListingID).First();
                }
            }

            return listingID;
        }

        public busEntities.MessageType GetMessageTypeByName(string messageTypeName)
        {
            busEntities.MessageType userMessageType = null;
            using (var db = new HODContext())
            {
                MessageType messageType = null;

                messageType = db.MessageTypes.Where(m => m.Name == messageTypeName).SingleOrDefault();

                if (messageType != null)
                {
                    userMessageType = MessageTypeTranslator.Translate(messageType);
                }
            }

            return userMessageType;
        }

        public busEntities.MessageType GetMessageTypeById(int messageTypeId)
        {
            busEntities.MessageType userMessageType = null;
            using (var db = new HODContext())
            {
                MessageType messageType = null;

                messageType = db.MessageTypes.Where(m => m.MessageTypeId == messageTypeId).SingleOrDefault();

                if (messageType != null)
                {
                    userMessageType = MessageTypeTranslator.Translate(messageType);
                }
            }

            return userMessageType;
        }

        public List<busEntities.MessageType> GetMessageTypes()
        {
            List<busEntities.MessageType> userMessageTypes = null;
            using (var db = new HODContext())
            {
                IQueryable<MessageType> messageTypes = null;

                messageTypes = db.MessageTypes;

                if (messageTypes != null)
                {
                    userMessageTypes = MessageTypeTranslator.Translate(messageTypes.ToList<MessageType>());
                }
            }

            return userMessageTypes;
        }

        public List<busEntities.User> GetUserByUserType(int userType)
        {
            List<busEntities.User> users = null;
            using (var db = new HODContext())
            {
                var usersRetrieved = db.UserProfiles.Where(u => u.UserTypeId == userType).OrderBy(u => u.FirstName);

                if (usersRetrieved != null)
                {
                    users = UserTranslator.Translate(usersRetrieved.ToList<UserProfile>());
                }
            }

            return users;
        }

        public busEntities.User GetUserByUserId(int userId)
        {
            busEntities.User user = null;
            using (var db = new HODContext())
            {
                var userRetrieved = db.UserProfiles.FirstOrDefault<UserProfile>(u => u.UserId == userId);

                if (userRetrieved != null)
                {
                    user = UserTranslator.Translate(userRetrieved);
                }
            }

            return user;
        }

        public busEntities.User GetUserByUserName(string userName)
        {
            busEntities.User user = null;
            using (var db = new HODContext())
            {
                var userRetrieved = db.UserProfiles.FirstOrDefault<UserProfile>(u => u.UserName == userName);

                if (userRetrieved != null)
                {
                    user = UserTranslator.Translate(userRetrieved);
                }
            }

            return user;
        }

        
        public int CreateListing(string propertyType, string listingPerson, string contactPerson,bool PrivateSellerOp, string clientUrl,
            string phoneNo, bool dontShowPhoneNo, decimal listPrice, bool dontShowListPrice, decimal squareFootage,
            bool dontShowSquareFootage, decimal noOfBathrooms, decimal noOfBedrooms, string mainSellingDesc, string locationDesc,
            string address, string city, string province, string postalCode, string neighbourHood, List<busEntities.ListingImage> listingImages, int userId){

            int listingId;

            try
            {
                using (var db = new HODContext())
                {
                    var listing = new Listing()
                    {
                        PropertyType = propertyType,
                        ContactPerson = contactPerson,
                        IsPrivateSeller = PrivateSellerOp,
                        ListingPerson = 0,
                        PhoneNo = phoneNo,
                        DontShowPhoneNo = dontShowPhoneNo,
                        DontShowListPrice = dontShowListPrice,
                        DontShowSquareFootage = dontShowSquareFootage,
                        MainSellingDesc = mainSellingDesc,
                        ListPrice = listPrice,
                        SquareFootage = squareFootage,
                        NoOfBathrooms = noOfBathrooms,
                        NoOfBedrooms = noOfBedrooms,
                        clientUrl = string.Empty,
                        LocationDesc = locationDesc,
                        Address = address,
                        City = city,
                        Province = province,
                        PostalCode = postalCode,
                        Neighbourhood = neighbourHood,
                        UserId = userId,
                        ListingCategory = "NEW"
                    };

                    db.Listings.Add(listing);

                    db.SaveChanges();

                    listingId = listing.ListingId;

                    //foreach (busEntities.ListingImage listingImage in listingImages)
                    //{
                    //    listingImage.ListingId = listingId;
                    //    db.ListingImages.Add(ListingImageTranslator.Translate(listingImage));
                    //}


                    /*** Create images for all categories ***/
                    //IQueryable<ListingImageCategory> categories = db.ListingImageCategories;

                    //foreach (ListingImageCategory category in categories) {                 
     
                    //    if (category.Name != VIRTUALSIGN){                            
                    //      List<ListingImage> createdImages  = CreateDefaultImagesForSections(category, listingId);
                    //      foreach (ListingImage image in createdImages){
                    //          db.ListingImages.Add(image);
                    //      }
                    //    }

                    //}


                    var currentListing = db.Listings.Find(listingId);
                    currentListing.clientUrl = generateUrl(listingId);
                    db.SaveChanges();

                }
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }

            return listingId;
        }

        private List<ListingImage> CreateDefaultImagesForSections(ListingImageCategory imageCategory, int listingId){
            const int MAXIMUM_NO_IMAGES = 6;

            String fileName = "nohouse.png";
            List<ListingImage> listOfImages = new List<ListingImage>();

            for (int i = 0; i < MAXIMUM_NO_IMAGES; i++){
                ListingImage theImage = new ListingImage();
                theImage.ListingImageCategory = imageCategory;
                theImage.Image = fileName;
                theImage.ListingId = listingId;                
                listOfImages.Add(theImage);
            }

            return listOfImages;
        }

        public void UpdateMainSummarySection(HOD.Entities.Listing model, List<busEntities.ListingImage> images)
        {
            var db = new HODContext();

            try
            {
                ListingImage TempListItem = new ListingImage();


                Listing ListingModel = new Listing();


                ListingModel = (from c in db.Listings
                                where c.ListingId == model.ListingId
                                select c).SingleOrDefault();

                ListingModel.MainSellingDesc = model.MainSellingDesc;
                ListingModel.ListPrice = model.ListPrice;
                ListingModel.NoOfBathrooms = model.NoOfBathrooms;
                ListingModel.NoOfBedrooms = model.NoOfBedrooms;
                ListingModel.PropertyType = model.PropertyType;
                ListingModel.SquareFootage = model.SquareFootage;
                ListingModel.AgeOfHome = model.AgeOfHome;
                ListingModel.NoOfFloors = model.NoOfFloors;
                ListingModel.hasBasement = model.hasBasement;
                ListingModel.hasFinishedBasement = model.hasFinishedBasement;
                ListingModel.hasGarage = model.hasGarage;
                ListingModel.hasAC = model.hasAC;
                ListingModel.heatType = model.HeatType;
                ListingModel.AnnualTaxes = model.AnnualTaxes;

                db.SaveChanges();
                /*
                List<ListingImage> ListingImagesModel = (from c in db.ListingImages
                                                         where c.ListingId == model.ListingId && c.ListingImageCategory.Name == "VirtualSign"
                                                         select c).ToList();

                ListingImagesModel[0].Image = images[0].Image;
                ListingImagesModel[1].Image = images[1].Image;
                ListingImagesModel[2].Image = images[2].Image;
                ListingImagesModel[3].Image = images[3].Image;

                if (images.Count > 4){
                    ListingImagesModel[4].Image = images[4].Image;
                    ListingImagesModel[5].Image = images[5].Image;
                }


                db.SaveChanges();*/
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }

        }

        public void UpdateVirtualSign(HOD.Entities.Listing model,List<busEntities.ListingImage> images)
        {
           var db = new HODContext();

            try{
                ListingImage TempListItem = new ListingImage();
                

               Listing ListingModel = new Listing();


             ListingModel =(from c in db.Listings
                         where c.ListingId == model.ListingId
                         select c).SingleOrDefault();

            ListingModel.Address =model.Address;
            ListingModel.City = model.City;
            ListingModel.clientUrl = model.ClientUrl;
            ListingModel.ContactPerson =model.ContactPerson;
            ListingModel.DontShowListPrice = model.DontShowListPrice;
            ListingModel.DontShowPhoneNo = model.DontShowPhoneNo;
            ListingModel.DontShowSquareFootage = model.DontShowSquareFootage;
            ListingModel.IsPrivateSeller = model.isPrivateSeller;
            ListingModel.KitchenDesc= model.KitchenDesc;
            ListingModel.ListingPerson = Convert.ToInt32(model.ListingPerson);
            ListingModel.ListPrice = model.ListPrice;
            ListingModel.MainSellingDesc= model.MainSellingDesc;
            ListingModel.NoOfBathrooms= model.NoOfBathrooms;
            ListingModel.NoOfBedrooms = model.NoOfBedrooms;
            ListingModel.PhoneNo = model.PhoneNo;
            ListingModel.PostalCode = model.PostalCode;
            ListingModel.Neighbourhood = model.NeighbourHood;
            ListingModel.Province = model.Province;
            ListingModel.SquareFootage =model.SquareFootage;
            ListingModel.KitchenDesc = model.KitchenDesc;
            ListingModel.PropertyType = model.PropertyType;
            ListingModel.PropertyDetails = model.PropertyDetails;
            ListingModel.PropertyStatus = model.PropertyStatus;

            db.SaveChanges();

            //List <ListingImage> ListingImagesModel = (from c in db.ListingImages
            //                         where c.ListingId ==model.ListingId && c.ListingImageCategory.Name == "VirtualSign" 
            //                         select c).ToList();

            //var submittedImages = (from img in images
            //                       where img.Image != null
            //                       select img).ToList();


            //ListingImagesModel[0].Image = images[0].Image;
            //ListingImagesModel[1].Image = images[1].Image;
            //ListingImagesModel[2].Image = images[2].Image;
            //ListingImagesModel[3].Image = images[3].Image;

            //db.SaveChanges();
                                          }
            catch(DbEntityValidationException e)
            { 
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
            
        }

        public int AddListingDescription(int listingId, string desc, string imageCategory, List<busEntities.ListingImage> listingImages)
        {
            try
            {
                using (var db = new HODContext())
                {
                    var listing = db.Listings.Find(listingId);

                    if (listing != null)
                    {
                        if (imageCategory == ImageCategoryName.LOCATION)
                            listing.LocationDesc = desc;
                        else if (imageCategory == ImageCategoryName.KITCHEN)
                            listing.KitchenDesc = desc;
                        else if (imageCategory == ImageCategoryName.OBEDROOM)
                            listing.BedroomDesc = desc;
                        else if (imageCategory == ImageCategoryName.BASEMENT)
                            listing.BasementDesc = desc;
                        else if (imageCategory == ImageCategoryName.DININGROOM)
                            listing.DiningRoomDesc = desc;
                        else if (imageCategory == ImageCategoryName.FAMILYROOM)
                            listing.FamilyRoomDesc = desc;
                        else if (imageCategory == ImageCategoryName.BATHROOM)
                            listing.BathroomDesc = desc;
                        else if (imageCategory == ImageCategoryName.VIDEOUPLOAD)
                            listing.VideoDesc = desc;
                        else if (imageCategory == ImageCategoryName.OTHERSECTION1)
                            listing.OtherSection1Desc = desc;
                        else if (imageCategory == ImageCategoryName.OTHERSECTION2)
                            listing.OtherSection2Desc = desc;
                        else if (imageCategory == ImageCategoryName.OTHERSECTION3)
                            listing.OtherSection3Desc = desc;
                        else if (imageCategory == ImageCategoryName.OTHERSECTION4)
                            listing.OtherSection4Desc = desc;                        //else if(imageCategory == ImageCategoryName.VIRTUALSIGN)
                        //    listing.MainSellingDesc = desc;
                    }

                    db.SaveChanges();

                    
                    listingId = listing.ListingId;

                    IQueryable<ListingImage> prevListingImages = db.ListingImages.Where(x => x.ListingId == listingId && x.ListingImageCategory.Name == imageCategory);

                    foreach (ListingImage image in prevListingImages) {
                        db.ListingImages.Remove(image);
                    }

                    foreach (busEntities.ListingImage listingImage in listingImages)
                    {
                        listingImage.ListingId = listingId;
                        db.ListingImages.Add(ListingImageTranslator.Translate(listingImage));
                    }
                    db.SaveChanges();
                }
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }

            return listingId;
        }


        private string generateUrl(int listingId)
        {
            AgentInformation agentInfo = RetrieveAgentInfoByListingId(listingId);
            string url = "{0}/VUE{1}";
            string finalUrl;

            if (agentInfo == null)
            {
                finalUrl = string.Format(url, "http://www.apexproducer.com", listingId.ToString("D6"));
            }
            else
            {
                finalUrl = string.Format(url, agentInfo.AgentUrl, listingId.ToString("D6"));
            }
            return finalUrl;

        /*    return System.Configuration.ConfigurationSettings.AppSettings["applicationRoot"].ToString() + "VUE" +
                  listingId.ToString("D6");*/
        }

        public int CreateListing(Listing listing)
        {

            int listingId = 0;
            using (var db = new HODContext())
            {
                db.Listings.Add(listing);
                db.SaveChanges();
                listingId = listing.ListingId;
            }

            return listingId;
        }


        public busEntities.Listing GetListingDetail(int listingId)
        {
            var listing = new HOD.Data.Entities.Listing();
            List <HOD.Data.Entities.ListingCategorySection> isSectionVisible=new List<HOD.Data.Entities.ListingCategorySection>();
            using (var db = new HODContext())
            {
                db.Configuration.AutoDetectChangesEnabled = true;
                //listing = db.Listings.Find(listingId);
                listing = db.Listings.Where(x => x.ListingId == listingId).FirstOrDefault();
                isSectionVisible = db.ListingCategorySections.AsNoTracking().Where(m => m.ListingId == listingId).ToList();
                db.Configuration.AutoDetectChangesEnabled = true;

            }

            if (listing == null){
                throw new ApplicationException(string.Format("There is no listing with that request id VUE{0}", listingId.ToString()));
            }


            int ageOfHome = listing.AgeOfHome.HasValue ? listing.AgeOfHome.Value : 0;
            decimal annualTaxes = listing.AnnualTaxes.HasValue ? listing.AnnualTaxes.Value : 0;
            bool hasAC = listing.hasAC.HasValue ? listing.hasAC.Value : false;
            bool hasGarage = listing.hasGarage.HasValue ? listing.hasGarage.Value : false;
            bool hasFinBasement = listing.hasFinishedBasement.HasValue ? listing.hasFinishedBasement.Value : false;
            bool hasBasement = listing.hasBasement.HasValue ? listing.hasBasement.Value : false;
            int noOfFloors = listing.NoOfFloors.HasValue ? listing.NoOfFloors.Value : 0;
            string heatType = listing.heatType;
            decimal listPrice = listing.ListPrice.Value;

            var localListing = new busEntities.Listing(){
                ListingId = listingId,
                PropertyType = listing.PropertyType,
                PropertyStatus = listing.PropertyStatus,
                UserId = listing.UserId,
                ClientUrl = listing.clientUrl,
                ContactPerson = listing.ContactPerson,
                PhoneNo = listing.PhoneNo,
                DontShowPhoneNo = listing.DontShowPhoneNo.HasValue ? listing.DontShowPhoneNo.Value : default(bool),
                ListPrice = listPrice,
                DontShowListPrice = listing.DontShowListPrice.HasValue ? listing.DontShowListPrice.Value : default(bool),
                SquareFootage = listing.SquareFootage.HasValue ? listing.SquareFootage.Value : default(decimal),
                DontShowSquareFootage = listing.DontShowSquareFootage.HasValue ? listing.DontShowSquareFootage.Value : default(bool),
                NoOfBathrooms = listing.NoOfBathrooms.HasValue ? listing.NoOfBathrooms.Value : default(decimal),
                NoOfBedrooms = listing.NoOfBedrooms.HasValue ? listing.NoOfBedrooms.Value : default(decimal),
                MainSellingDesc = listing.MainSellingDesc,
                LocationDesc = listing.LocationDesc,
                KitchenDesc = listing.KitchenDesc,
                BathroomDesc = listing.BathroomDesc,
                Address = listing.Address,
                City = listing.City,
                Province = listing.Province,
                PostalCode = listing.PostalCode,
                NeighbourHood = listing.Neighbourhood,
                isPrivateSeller = (bool)listing.IsPrivateSeller,
                AgeOfHome = ageOfHome,
                AnnualTaxes = annualTaxes,
                hasAC = hasAC,
                hasGarage = hasGarage,
                hasFinishedBasement = hasFinBasement,
                hasBasement = hasBasement,
                NoOfFloors = noOfFloors,
                HeatType = heatType,
                images = GetListingImageByListing(listing.ListingId),
                BasementDesc =  listing.BasementDesc,
                BedroomDesc = listing.BedroomDesc,
                DiningRoomDesc = listing.DiningRoomDesc,
                FamilyRoomDesc = listing.FamilyRoomDesc,  
                VideoDesc=listing.VideoDesc,
                OtherSection1Desc = listing.OtherSection1Desc,
                OtherSection2Desc = listing.OtherSection2Desc,
                OtherSection3Desc = listing.OtherSection3Desc,
                OtherSection4Desc = listing.OtherSection4Desc                
            };


           /* foreach (var section in isSectionVisible)
            {
                switch (section.ListingImageCategoryId)
                {
                    case 1: ;
                        break;
                    case 2: localListing.isLocationPublic=section.VisibleSetting;
                        break;
                    case 3: localListing.isKitchenPublic=section.VisibleSetting;
                        break;
                    case 4: localListing.isBathroomPublic=section.VisibleSetting;
                        break;
                    case 5: localListing.isBasementPublic=section.VisibleSetting;
                        break;
                    case 6: localListing.isBedroomPublic=section.VisibleSetting;
                        break;
                    case 7: localListing.isDiningPublic=section.VisibleSetting;
                        break;
                    case 8: localListing.isFamilyRoomPublic=section.VisibleSetting;
                        break;
                    case 9: localListing.isVideoPublic=section.VisibleSetting ;
                        break;
                    case 10: localListing.isOtherSection1Public = section.VisibleSetting;
                        break;
                    case 11: localListing.isOtherSection2Public = section.VisibleSetting;
                        break;
                    case 12: localListing.isOtherSection3Public = section.VisibleSetting;
                        break;
                    case 13: localListing.isOtherSection4Public = section.VisibleSetting;
                        break;
                }
            }*/

            //int imgCount = localListing.images.Count;

            //for (int i = 1; i <= (6 - imgCount); i++){
            //    localListing.images.Add(new busEntities.ListingImage(){ Image="no_houseicon.png"});
            //}

            return localListing;
        }

        public List<int> GetListingIdsByUserId(int userId)
        {
            List<int> listingIds = null;

            List<HOD.Data.Entities.Listing> returnedListings = null;
            using (var db = new HODContext())
            {
                returnedListings = (from listing in db.Listings
                                    where (listing.UserId == userId)//UserId is not in listing
                                    select listing).ToList();
            }

            if (returnedListings != null && returnedListings.Count > 0)
            {
                foreach (Listing listing in returnedListings)
                {
                    if (listingIds != null)
                        listingIds.Add(listing.ListingId);
                    else
                    {
                        listingIds = new List<int>();
                        listingIds.Add(listing.ListingId);
                    }
                }
            }
            return listingIds;
        }

        public List<busEntities.Listing> GetListingsByUserId(int userId)
        {

            List<busEntities.Listing> listings = null;

            List<HOD.Data.Entities.Listing> returnedListings = null;
            using (var db = new HODContext())
            {
                returnedListings = (from listing in db.Listings
                                    where (listing.UserId == userId)
                                    select listing).ToList();
            }

            if (returnedListings != null && returnedListings.Count > 0)
            {
                foreach (Listing listing in returnedListings)
                {
                    if (listings != null)
                    {
                        var localListing = new busEntities.Listing()
                        {
                            PropertyType = listing.PropertyType,
                            ClientUrl = listing.clientUrl,
                            ContactPerson = listing.ContactPerson,
                            PhoneNo = listing.PhoneNo,
                            DontShowPhoneNo = listing.DontShowPhoneNo.HasValue ? listing.DontShowPhoneNo.Value : default(bool),
                            ListPrice = listing.ListPrice.HasValue ? listing.ListPrice.Value : default(decimal),
                            DontShowListPrice = listing.DontShowListPrice.HasValue ? listing.DontShowListPrice.Value : default(bool),
                            SquareFootage = listing.SquareFootage.HasValue ? listing.SquareFootage.Value : default(decimal),
                            DontShowSquareFootage = listing.DontShowSquareFootage.HasValue ? listing.DontShowSquareFootage.Value : default(bool),
                            NoOfBathrooms = listing.NoOfBathrooms.HasValue ? listing.NoOfBathrooms.Value : default(decimal),
                            NoOfBedrooms = listing.NoOfBedrooms.HasValue ? listing.NoOfBedrooms.Value : default(decimal),
                            MainSellingDesc = listing.MainSellingDesc,
                            LocationDesc = listing.LocationDesc,
                            KitchenDesc = listing.KitchenDesc,
                            Address = listing.Address,
                            City = listing.City,
                            Province = listing.Province,
                            PostalCode = listing.PostalCode,
                            isPrivateSeller = (bool)listing.IsPrivateSeller,

                            images = GetListingImageByListing(listing.ListingId)
                        };

                        listings.Add(localListing);
                    }
                    else
                    {
                        var localListing = new busEntities.Listing()
                        {
                            PropertyType = listing.PropertyType,
                            ClientUrl = listing.clientUrl,
                            ContactPerson = listing.ContactPerson,
                            PhoneNo = listing.PhoneNo,
                            DontShowPhoneNo = listing.DontShowPhoneNo.HasValue ? listing.DontShowPhoneNo.Value : default(bool),
                            ListPrice = listing.ListPrice.HasValue ? listing.ListPrice.Value : default(decimal),
                            DontShowListPrice = listing.DontShowListPrice.HasValue ? listing.DontShowListPrice.Value : default(bool),
                            SquareFootage = listing.SquareFootage.HasValue ? listing.SquareFootage.Value : default(decimal),
                            DontShowSquareFootage = listing.DontShowSquareFootage.HasValue ? listing.DontShowSquareFootage.Value : default(bool),
                            NoOfBathrooms = listing.NoOfBathrooms.HasValue ? listing.NoOfBathrooms.Value : default(decimal),
                            NoOfBedrooms = listing.NoOfBedrooms.HasValue ? listing.NoOfBedrooms.Value : default(decimal),
                            MainSellingDesc = listing.MainSellingDesc,
                            LocationDesc = listing.LocationDesc,
                            KitchenDesc = listing.KitchenDesc,
                            Address = listing.Address,
                            City = listing.City,
                            Province = listing.Province,
                            PostalCode = listing.PostalCode,
                            isPrivateSeller = (bool)listing.IsPrivateSeller,

                            images = GetListingImageByListing(listing.ListingId)
                        };
                        listings = new List<busEntities.Listing>();
                        listings.Add(localListing);
                    }
                }
            }
            return listings;
        }

        public busEntities.ListingImageCategory GetListingImageCategory(int listingCategoryId)
        {
            var listingCategory = new HOD.Data.Entities.ListingImageCategory();

            using (var db = new HODContext())
            {
                db.Configuration.AutoDetectChangesEnabled = false;
                listingCategory = db.ListingImageCategories.Find(listingCategoryId);
                db.Configuration.AutoDetectChangesEnabled = true;
            }

            if (listingCategory == null)
            {
                throw new MessageManagerExceptions.InvalidListingImageCategoryRequestException();
            }

            var localListingCategory = new busEntities.ListingImageCategory()
            {
                ListingImageCategoryId = listingCategory.ListingImageCategoryId,
                Name = listingCategory.Name,
                Description = listingCategory.Description,
            };

            return localListingCategory;
        }

        public busEntities.ListingImageCategory GetListingImageCategory(string listingCategoryName)
        {
            var listingCategory = new HOD.Data.Entities.ListingImageCategory();

            using (var db = new HODContext())
            {
                listingCategory = (from listing_Category in db.ListingImageCategories
                                   where (listing_Category.Name == listingCategoryName)
                                   select listing_Category).FirstOrDefault<HOD.Data.Entities.ListingImageCategory>();
            }

            if (listingCategory == null)
            {
                throw new MessageManagerExceptions.InvalidListingImageCategoryRequestException();
            }

            var localListingCategory = new busEntities.ListingImageCategory()
            {
                ListingImageCategoryId = listingCategory.ListingImageCategoryId,
                Name = listingCategory.Name,
                Description = listingCategory.Description,
            };

            return localListingCategory;
        }

        public List<busEntities.MenuItems> GetResourcesMenu()
        {
            var menuItems = new List<HOD.Data.Entities.MenuItem>();
            List<busEntities.MenuItems> MenuList;

            using (var db = new HODContext())
            {
                menuItems = db.MenuItems.ToList<HOD.Data.Entities.MenuItem>();
                MenuList = ListingImageCategoryTranslator.TranslateMenuItems(menuItems);
            }
            return MenuList;
        }

        public List<busEntities.ListingImageCategory> GetListingImageCategories()
        {
            var listingCategories = new List<HOD.Data.Entities.ListingImageCategory>();

            using (var db = new HODContext())
            {
                listingCategories = db.ListingImageCategories.ToList<HOD.Data.Entities.ListingImageCategory>();
            }

            if (listingCategories == null)
            {
                throw new MessageManagerExceptions.InvalidListingImageCategoryRequestException();
            }

            var localListingCategories = ListingImageCategoryTranslator.Translate(listingCategories);

            return localListingCategories;
        }

        public busEntities.ListingImage GetListingImage(int listingImageId)
        {
            var listingImage = new HOD.Data.Entities.ListingImage();

            using (var db = new HODContext())
            {
                listingImage = db.ListingImages.Find(listingImageId);
            }

            if (listingImage == null)
            {
                throw new MessageManagerExceptions.InvalidListingImageRequestException();
            }

            var localListingImages = new busEntities.ListingImage()
            {
                ListingImageId = listingImage.ListingImageId,
                ListingId = listingImage.ListingId,
                ListingImageCategory = listingImage.ListingImageCategory.Name,
                Image = listingImage.Image
            };

            return localListingImages;
        }

        public List<busEntities.ListingImage> GetListingImageByListing(int listingId)
        {
            var listingCategories = new List<HOD.Data.Entities.ListingImage>();

            using (var db = new HODContext())
            {
                listingCategories = (from listing_Image in db.ListingImages
                                   where (listing_Image.ListingId == listingId)
                                   select listing_Image).ToList<HOD.Data.Entities.ListingImage>();
            }

            if (listingCategories == null)
            {
                throw new MessageManagerExceptions.InvalidListingImageRequestException();
            }

            var localListingCategories = ListingImageTranslator.Translate(listingCategories);

            return localListingCategories;
        }

        public List<busEntities.ListingImage> GetListingImages()
        {
            var listingCategories = new List<HOD.Data.Entities.ListingImage>();

            using (var db = new HODContext())
            {
                db.Configuration.AutoDetectChangesEnabled = false;
                listingCategories = db.ListingImages.ToList<HOD.Data.Entities.ListingImage>();
                db.Configuration.AutoDetectChangesEnabled = true;
            }

            if (listingCategories == null)
            {
                throw new MessageManagerExceptions.InvalidListingImageRequestException();
            }

            var localListingCategories = ListingImageTranslator.Translate(listingCategories);

            return localListingCategories;
        }

        public Listing GetListingDetail(string listingId)
        {
            return new Listing();
        }

        public int CreateRegistrationError(busEntities.RegistrationError registrationError)
        {
            int registrationErrorId = 0;
            using (var db = new HODContext())
            {
                db.RegistrationErrors.Add(RegistrationErrorTranslator.Translate(registrationError));
                db.SaveChanges();
                registrationErrorId = registrationError.RegistrationErrorId;
            }

            return registrationErrorId;
        }

        public List<busEntities.Configuration> GetAllConfiguration()
        {
            var db = new HODContext();
            List<busEntities.Configuration> Config = new List<busEntities.Configuration> { };

            foreach (var p in db.Configs.ToList())
            {
                Config.Add(new busEntities.Configuration
                {
                    ConfigId = p.Id,
                    ConfigName = p.ConfigName,
                    CongfigValue = p.Value
                });
            }

            return Config;
        }

        public HOD.Entities.Configuration GetConfigurationById(int Id)
        {
            return this.GetAllConfiguration().Where(x => x.ConfigId == Id).SingleOrDefault();
        }

        public HOD.Entities.Configuration GetConfigurationByName(string configName)
        {
            return this.GetAllConfiguration().Where(x => x.ConfigName.Equals(configName)).SingleOrDefault();
        }

        public void AddListingSection(int listingId, string listingCategory, bool value, int userId)
        {
            var db = new HODContext();
            int listCatId = db.ListingImageCategories.Where(x=>x.Name==listingCategory).Select(y=>y.ListingImageCategoryId).SingleOrDefault();

            var result = (from c in db.ListingCategorySections
                         where c.ListingId == listingId && c.ListingImageCategoryId == listCatId
                         select c).SingleOrDefault();

            if (result != null)
            {
                result.VisibleSetting = value;
                db.SaveChanges();
            }
            else
            {
                
                ListingCategorySection listCatSec = new ListingCategorySection() { ListingId =listingId, ListingImageCategoryId = listCatId,VisibleSetting =value, UserId=userId};
                db.ListingCategorySections.Add(listCatSec);
                db.SaveChanges();
            }
        }

        public busEntities.ListingCategorySection GetListingSection(int listingId, string listingCategory)
        {
            
            var db = new HODContext();
            int listCatId = db.ListingImageCategories.Where(x=>x.Name==listingCategory).Select(y=>y.ListingImageCategoryId).SingleOrDefault();
            busEntities.ListingCategorySection listingSection = new busEntities.ListingCategorySection();
            var result =db.ListingCategorySections.Where(x => x.ListingId == listingId && x.ListingImageCategoryId == listCatId).SingleOrDefault();

            if (result != null)
            {
                  
                
                    listingSection.ListingCategoryId = result.ListingImageCategoryId;
                    listingSection.ListingId = result.ListingId;
                    listingSection.UserId = result.UserId;
                    listingSection.Value = result.VisibleSetting;
               
            }
            else
            {
               
                    listingSection.ListingCategoryId = listCatId;
                    listingSection.ListingId = listingId;
                    listingSection.Value = false;
               
            }

            return listingSection;
        }

        public busEntities.PackageChargeFrequency GetPackageChargefrequencyByName(string packageChargefrequencyName)
        {
            busEntities.PackageChargeFrequency frequency = null;
            using (var db = new HODContext())
            {
                PackageChargeFrequency packageChargefrequency = null;

                packageChargefrequency = db.PackageChargeFrequencies.Where(m => m.Name == packageChargefrequencyName).SingleOrDefault();

                if (packageChargefrequency != null)
                {
                    frequency = PackageChargeFrequencyTranslator.Translate(packageChargefrequency);
                }
            }

            return frequency;
        }

        public busEntities.PackageChargeFrequency GetPackageChargefrequencyById(int id)
        {
            busEntities.PackageChargeFrequency frequency = null;
            using (var db = new HODContext())
            {
                PackageChargeFrequency packageChargefrequency = null;

                packageChargefrequency = db.PackageChargeFrequencies.Where(m => m.PackageChargeFreqencyId == id).SingleOrDefault();

                if (packageChargefrequency != null)
                {
                    frequency = PackageChargeFrequencyTranslator.Translate(packageChargefrequency);
                }
            }

            return frequency;
        }

        public busEntities.PackageFeatureChargeFrequency GetPackageFeatureChargeFrequencyByName(string packageFeatureChargefrequencyName)
        {
            busEntities.PackageFeatureChargeFrequency frequency = null;
            using (var db = new HODContext())
            {
                PackageFeatureChargeFrequency packageChargefrequency = null;

                packageChargefrequency = db.PackageFeatureChargeFrequencies.Where(m => m.Name == packageFeatureChargefrequencyName).SingleOrDefault();

                if (packageChargefrequency != null)
                {
                    frequency = PackageFeatureChargeFrequencyTranslator.Translate(packageChargefrequency);
                }
            }

            return frequency;
        }

        public busEntities.PackageFeatureChargeFrequency GetPackageFeatureChargeFrequencyById(int id)
        {
            busEntities.PackageFeatureChargeFrequency frequency = null;
            using (var db = new HODContext())
            {
                PackageFeatureChargeFrequency packageChargefrequency = null;

                packageChargefrequency = db.PackageFeatureChargeFrequencies.Where(m => m.FeatureChargeFrequencyId == id).SingleOrDefault();

                if (packageChargefrequency != null)
                {
                    frequency = PackageFeatureChargeFrequencyTranslator.Translate(packageChargefrequency);
                }
            }

            return frequency;
        }

        public busEntities.PackageFeature GetPackageFeatureByName(string packageFeatureName)
        {
            busEntities.PackageFeature feature = null;
            using (var db = new HODContext())
            {
                PackageFeature packageFeature = null;

                packageFeature = db.PackageFeatures.Where(m => m.Name == packageFeatureName).SingleOrDefault();

                if (packageFeature != null)
                {
                    feature = PackageFeatureTranslator.Translate(packageFeature);
                }
            }

            return feature;
        }

        public busEntities.PackageType GetPackageTypeByName(string packageTypeName)
        {
            busEntities.PackageType type = null;
            using (var db = new HODContext())
            {
                PackageType packageType = null;

                packageType = db.PackageTypes.Where(m => m.Name == packageTypeName).SingleOrDefault();

                if (packageType != null)
                {
                    type = PackageTypeTranslator.Translate(packageType);
                }
            }

            return type;
        }

        public busEntities.PackageType GetPackageTypeById(int id)
        {
            busEntities.PackageType type = null;
            using (var db = new HODContext())
            {
                PackageType packageType = null;

                packageType = db.PackageTypes.Where(m => m.PackageTypeId == id).SingleOrDefault();

                if (packageType != null)
                {
                    type = PackageTypeTranslator.Translate(packageType);
                }
            }

            return type;
        }

        public busEntities.PackageFeature GetPackageFeatureById(int id)
        {
            busEntities.PackageFeature feature = null;
            using (var db = new HODContext())
            {
                PackageFeature packageFeature = null;

                packageFeature = db.PackageFeatures.Where(m => m.PackageFeatureId == id).SingleOrDefault();

                if (packageFeature != null)
                {
                    feature = PackageFeatureTranslator.Translate(packageFeature);
                }
            }

            return feature;
        }

        public List<busEntities.PackageFeatureAssociation> GetPackageFeatureAssociationByPackageId(int id)
        {
            List<busEntities.PackageFeatureAssociation> associations = null;
            using (var db = new HODContext())
            {
                var associationsRetrieved = db.PackageFeatureAssociations.Where(p => p.PackageId == id).OrderBy(p => p.PackageFeatureId);

                if (associationsRetrieved != null)
                {
                    associations = PackageFeatureAssociationTranslator.Translate(associationsRetrieved.ToList<PackageFeatureAssociation>());
                }
            }

            return associations;
        }

        public List<busEntities.PackageFeature> GetPackageFeaturesByPackageId(int id)
        {
            List<busEntities.PackageFeature> features = null;
            using (var db = new HODContext())
            {
                var associationsRetrieved = db.PackageFeatureAssociations.Where(p => p.PackageId == id).OrderBy(p => p.PackageFeatureId);

                if (associationsRetrieved != null)
                {
                    foreach (var association in associationsRetrieved)
                    {
                        features = new List<busEntities.PackageFeature>();

                        PackageFeature packageFeature = db.PackageFeatures.Where(m => m.PackageFeatureId == association.PackageFeatureId).SingleOrDefault();

                        busEntities.PackageFeature featureToAdd = PackageFeatureTranslator.Translate(packageFeature);
                        featureToAdd.Optional = association.Optional;
                        featureToAdd.Shipping = association.Shipping;
                        featureToAdd.FeatureChargeFrequency = packageFeature.PackageFeatureChargeFrequency.Name;

                        features.Add(featureToAdd);
                    }
                }
            }

            return features;
        }

        public busEntities.UserPackage GetPackageForUser(int userId)
        {
            busEntities.UserPackage package = null;
            using (var db = new HODContext())
            {
                UserPackage userPackage = null;

                userPackage = db.UserPackages.Where(m => m.UserId == userId).SingleOrDefault();

                if (userPackage != null)
                {
                    package = new busEntities.UserPackage();

                    package.PackageId = userPackage.PackageId;
                    package.UserId = userPackage.UserId;
                    package.PackageTypeId = userPackage.PackageType.PackageTypeId;
                    package.Name = userPackage.PackageType.Name;
                    package.Description = userPackage.PackageType.Description;
                    package.BaseCost = userPackage.PackageType.BaseCost;
                    package.PackageChargeFrequencyId = userPackage.PackageType.PackageChargeFrequency.PackageChargeFreqencyId;
                    package.PackageChargeFrequency = userPackage.PackageType.PackageChargeFrequency.Name;
                }
            }

            return package;
        }

        public busEntities.UserPackage SaveUserPackage(busEntities.UserPackage userPackage, List<busEntities.PackageFeature> features)
        {
            using (var db = new HODContext())
            {
                UserPackage package = new UserPackage()
                {
                    UserId = userPackage.UserId,
                    PackageTypeId = userPackage.PackageTypeId
                };

                db.UserPackages.Add(package);
                db.SaveChanges();

                foreach (busEntities.PackageFeature packagefeature in features)
                {
                    PackageFeatureAssociation packageAssociation = new PackageFeatureAssociation()
                    {
                        PackageId = package.PackageId,
                        PackageFeatureId = packagefeature.PackageFeatureId,
                        Optional = packagefeature.Optional,
                        Shipping = packagefeature.Shipping
                    };

                    db.PackageFeatureAssociations.Add(packageAssociation);
                }

                db.SaveChanges();

                userPackage.PackageId = package.PackageId;
            }

            return userPackage;
        }

        public busEntities.StripePayment CreateStripePayment(busEntities.StripePayment stripePayment)
        {
            using (var db = new HODContext())
            {
                //Check if payment already exists
                if (!db.StripePayments.Any(m => m.StripeChargeId == stripePayment.StripeChargeId))
                {

                    StripePayment payment = new StripePayment()
                    {
                        UserId = stripePayment.UserId,
                        StripeCustomerId = stripePayment.StripeCustomerId,
                        StripeChargeId = stripePayment.StripeChargeId,
                        Amount = stripePayment.Amount,
                        PaidByAgent = stripePayment.PaidByAgent,
                    };

                    db.StripePayments.Add(payment);
                    db.SaveChanges();

                    stripePayment.StripePaymentId = payment.StripePaymentId;
                }
            }

            return stripePayment;
        }
        public int GetAgentIDByURL(string URL)
        {
            int UserID=0;
            using (var db = new HODContext()){
                if (db.UserProfiles.Where(x => x.AgentURL == URL).FirstOrDefault() != null){
                    UserID = db.UserProfiles.First(m => m.AgentURL == URL).UserId;
                }
            }

            return UserID;
        }

        public string GetBusinessNameByUserId(int userId)
        {
            string businessName = string.Empty;
            UserProfile customerUser;
            AgentInformation agentInfo;
            using (var db = new HODContext())
            {
                customerUser = db.UserProfiles.Where(x => x.UserId == userId).FirstOrDefault();
                agentInfo = db.AgentInformations.Where(y => y.AgentId == customerUser.AgentID).FirstOrDefault();
                businessName = agentInfo.BusinessName;
            }

            return businessName;
        }

        public AgentInformation RetrieveAgentInfoByListingId(int listingId)
        {
            UserProfile customerUser;
            AgentInformation agentInfo;
            Listing currentListing;
            using (var db = new HODContext())
            {
                currentListing = db.Listings.Where(x => x.ListingId == listingId).FirstOrDefault();
                customerUser = db.UserProfiles.Where(x => x.UserId == currentListing.UserId).FirstOrDefault();
                agentInfo = db.AgentInformations.Where(y => y.AgentId == customerUser.AgentID).FirstOrDefault();
            }

            return agentInfo;
        }





        //public void removeListingImage(busEntities.ListingImage image)
        //{
        //    throw new NotImplementedException();
        //}
    }
}
