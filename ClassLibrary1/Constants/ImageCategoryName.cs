﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HOD.Managers.Constants
{
    public static class ImageCategoryName
    {
        public const String VIRTUALSIGN = "VirtualSign";
        public const String LOCATION = "Location";
        public const String KITCHEN = "Kitchen";
        public const String BATHROOM = "Bathroom";
        public const String OBEDROOM = "Other Bedroom";
        public const String BASEMENT = "Basement";
        public const String FAMILYROOM = "Family Room";
        public const String DININGROOM = "Dining Room";
        public const string VIDEOUPLOAD = "Video Upload";
        public const string OTHERSECTION1 = "OtherSection1";
        public const string OTHERSECTION2 = "OtherSection2";
        public const string OTHERSECTION3 = "OtherSection3";
        public const string OTHERSECTION4 = "OtherSection4";

    }

    
}
