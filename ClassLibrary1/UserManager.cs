﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HOD.Entities;

namespace HOD.Managers
{
    public interface IUserNamager
    {
        List<User> GetUserByUserType(int userType);
        User GetUserByUserId(int userId);
    }
    
    public class UserManager : IUserNamager
    {
        public IDbManager DbManager { get; set; }

        public UserManager()
        {
            DbManager = new DbManager();
        }

        public List<User> GetUserByUserType(int userType)
        {
            return DbManager.GetUserByUserType(userType);
        }
        public User GetUserByUserId(int userId)
        {
            return DbManager.GetUserByUserId(userId);
        }
    }
}
