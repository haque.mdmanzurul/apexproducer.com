
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, and Azure
-- --------------------------------------------------
-- Date Created: 01/23/2014 16:32:55
-- Generated from EDMX file: C:\Projects\Synchro\HOD\HomeOwnerDirect\HOD.Data.Entities\HODModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [HOD];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_ListingImages_Listings]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ListingImages] DROP CONSTRAINT [FK_ListingImages_Listings];
GO
IF OBJECT_ID(N'[dbo].[FK_Listings_PropertyTypes]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Listings] DROP CONSTRAINT [FK_Listings_PropertyTypes];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[ListingImageCategories]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ListingImageCategories];
GO
IF OBJECT_ID(N'[dbo].[ListingImages]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ListingImages];
GO
IF OBJECT_ID(N'[dbo].[Listings]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Listings];
GO
IF OBJECT_ID(N'[dbo].[PropertyTypes]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PropertyTypes];
GO
IF OBJECT_ID(N'[dbo].[RegistrationErrors]', 'U') IS NOT NULL
    DROP TABLE [dbo].[RegistrationErrors];
GO
IF OBJECT_ID(N'[dbo].[smsMessages]', 'U') IS NOT NULL
    DROP TABLE [dbo].[smsMessages];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'smsMessages'
CREATE TABLE [dbo].[smsMessages] (
    [id] int  NOT NULL,
    [messageGuid] varchar(100)  NULL,
    [messageDate] datetime  NULL,
    [phoneNo] varchar(50)  NULL,
    [message] varchar(200)  NULL,
    [MessageDirection] char(1)  NULL
);
GO

-- Creating table 'PropertyTypes'
CREATE TABLE [dbo].[PropertyTypes] (
    [PropertyTypeId] varchar(5)  NOT NULL,
    [Name] varchar(50)  NULL,
    [Description] varchar(100)  NULL
);
GO

-- Creating table 'sysdiagrams'
CREATE TABLE [dbo].[sysdiagrams] (
    [name] nvarchar(128)  NOT NULL,
    [principal_id] int  NOT NULL,
    [diagram_id] int IDENTITY(1,1) NOT NULL,
    [version] int  NULL,
    [definition] varbinary(max)  NULL
);
GO

-- Creating table 'ListingImageCategories'
CREATE TABLE [dbo].[ListingImageCategories] (
    [ListingImageCategoryId] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(50)  NOT NULL,
    [Description] nvarchar(200)  NULL
);
GO

-- Creating table 'ListingImages'
CREATE TABLE [dbo].[ListingImages] (
    [ListingImageId] int IDENTITY(1,1) NOT NULL,
    [ListingId] int  NOT NULL,
    [ListingImageCategoryId] int  NOT NULL,
    [Image] varchar(max)  NULL
);
GO

-- Creating table 'RegistrationErrors'
CREATE TABLE [dbo].[RegistrationErrors] (
    [RegistrationErrorId] int IDENTITY(1,1) NOT NULL,
    [UserEmail] nvarchar(56)  NULL,
    [ErrorType] nvarchar(50)  NULL,
    [Message] nvarchar(200)  NULL,
    [Code] nvarchar(50)  NULL,
    [Parameter] nvarchar(50)  NULL,
    [Charge] nvarchar(50)  NULL,
    [StripeError] nvarchar(200)  NULL,
    [FirstName] nvarchar(56)  NULL,
    [LastName] nvarchar(56)  NULL,
    [Address] nvarchar(200)  NULL,
    [City] nvarchar(56)  NULL,
    [Province] nvarchar(56)  NULL,
    [PostalCode] nvarchar(20)  NULL,
    [PhoneNumberCell] nvarchar(10)  NULL,
    [PhoneNumberLandLine] nvarchar(10)  NULL,
    [Carrier] nvarchar(56)  NULL,
    [BillingName] nvarchar(120)  NULL,
    [BillingAddress] nvarchar(200)  NULL,
    [BillingCity] nvarchar(56)  NULL,
    [BillingProvince] nvarchar(56)  NULL,
    [BillingPostalCode] nvarchar(20)  NULL,
    [CreatedDateTime] datetime  NOT NULL
);
GO

-- Creating table 'Listings'
CREATE TABLE [dbo].[Listings] (
    [ListingId] int IDENTITY(1,1) NOT NULL,
    [PropertyType] varchar(5)  NOT NULL,
    [ListingPerson] int  NOT NULL,
    [ContactPerson] varchar(20)  NOT NULL,
    [clientUrl] varchar(200)  NOT NULL,
    [PhoneNo] varchar(20)  NULL,
    [DontShowPhoneNo] bit  NULL,
    [ListPrice] decimal(18,2)  NULL,
    [DontShowListPrice] bit  NULL,
    [SquareFootage] decimal(18,4)  NULL,
    [DontShowSquareFootage] bit  NULL,
    [NoOfBathrooms] decimal(18,2)  NULL,
    [NoOfBedrooms] decimal(18,2)  NULL,
    [MainSellingDesc] varchar(500)  NULL,
    [LocationDesc] varchar(500)  NULL,
    [KitchenDesc] varchar(500)  NULL,
    [Address] nvarchar(200)  NULL,
    [City] nvarchar(56)  NULL,
    [Province] nvarchar(56)  NULL,
    [PostalCode] nvarchar(20)  NULL,
    [isPrivateSeller] bit  NULL
);
GO

-- Creating table 'Config'
CREATE TABLE [dbo].[Config] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [ConfigName] varchar(200)  NOT NULL,
    [Value] varchar(300)  NOT NULL,
    [Description] varchar(max)  NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [id] in table 'smsMessages'
ALTER TABLE [dbo].[smsMessages]
ADD CONSTRAINT [PK_smsMessages]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [PropertyTypeId] in table 'PropertyTypes'
ALTER TABLE [dbo].[PropertyTypes]
ADD CONSTRAINT [PK_PropertyTypes]
    PRIMARY KEY CLUSTERED ([PropertyTypeId] ASC);
GO

-- Creating primary key on [diagram_id] in table 'sysdiagrams'
ALTER TABLE [dbo].[sysdiagrams]
ADD CONSTRAINT [PK_sysdiagrams]
    PRIMARY KEY CLUSTERED ([diagram_id] ASC);
GO

-- Creating primary key on [ListingImageCategoryId] in table 'ListingImageCategories'
ALTER TABLE [dbo].[ListingImageCategories]
ADD CONSTRAINT [PK_ListingImageCategories]
    PRIMARY KEY CLUSTERED ([ListingImageCategoryId] ASC);
GO

-- Creating primary key on [ListingImageId] in table 'ListingImages'
ALTER TABLE [dbo].[ListingImages]
ADD CONSTRAINT [PK_ListingImages]
    PRIMARY KEY CLUSTERED ([ListingImageId] ASC);
GO

-- Creating primary key on [RegistrationErrorId] in table 'RegistrationErrors'
ALTER TABLE [dbo].[RegistrationErrors]
ADD CONSTRAINT [PK_RegistrationErrors]
    PRIMARY KEY CLUSTERED ([RegistrationErrorId] ASC);
GO

-- Creating primary key on [ListingId] in table 'Listings'
ALTER TABLE [dbo].[Listings]
ADD CONSTRAINT [PK_Listings]
    PRIMARY KEY CLUSTERED ([ListingId] ASC);
GO

-- Creating primary key on [Id] in table 'Config'
ALTER TABLE [dbo].[Config]
ADD CONSTRAINT [PK_Config]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [ListingImageCategoryId] in table 'ListingImages'
ALTER TABLE [dbo].[ListingImages]
ADD CONSTRAINT [FK_ListingImages_ListingImageCategories]
    FOREIGN KEY ([ListingImageCategoryId])
    REFERENCES [dbo].[ListingImageCategories]
        ([ListingImageCategoryId])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ListingImages_ListingImageCategories'
CREATE INDEX [IX_FK_ListingImages_ListingImageCategories]
ON [dbo].[ListingImages]
    ([ListingImageCategoryId]);
GO

-- Creating foreign key on [ListingId] in table 'ListingImages'
ALTER TABLE [dbo].[ListingImages]
ADD CONSTRAINT [FK_ListingImages_Listings]
    FOREIGN KEY ([ListingId])
    REFERENCES [dbo].[Listings]
        ([ListingId])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ListingImages_Listings'
CREATE INDEX [IX_FK_ListingImages_Listings]
ON [dbo].[ListingImages]
    ([ListingId]);
GO

-- Creating foreign key on [PropertyType] in table 'Listings'
ALTER TABLE [dbo].[Listings]
ADD CONSTRAINT [FK_Listings_PropertyTypes]
    FOREIGN KEY ([PropertyType])
    REFERENCES [dbo].[PropertyTypes]
        ([PropertyTypeId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Listings_PropertyTypes'
CREATE INDEX [IX_FK_Listings_PropertyTypes]
ON [dbo].[Listings]
    ([PropertyType]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------