﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HOD.Entities
{
    public class Listing
    {
        public Listing(){
         isLocationPublic = true;
         isKitchenPublic = true;
         isBathroomPublic = true;
         isBedroomPublic = true;
         isBasementPublic = true;
         isDiningPublic = true;
         isFamilyRoomPublic = true;
         isVideoPublic = true;
         isOtherSection1Public = true;
         isOtherSection2Public = true;
         isOtherSection3Public = true;
         isOtherSection4Public = true;

        }

        public int ListingId { get; set; }
        public int? UserId { get; set; }
        public string PropertyType { get; set; } 

        public string PhoneNo { get; set; }
        public string ClientUrl { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ListingPerson { get; set; }
        public string ContactPerson { get; set; }
        public bool isPrivateSeller { get; set; }
        public bool DontShowPhoneNo { get; set; }
        public decimal ListPrice { get; set; }
        public bool DontShowListPrice { get; set; }
        public decimal SquareFootage { get; set; }
        public bool DontShowSquareFootage { get; set; }
        public decimal NoOfBathrooms { get; set; }
        public decimal NoOfBedrooms { get; set; }
        public string MainSellingDesc { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Province { get; set; }
        public string PostalCode { get; set; }
        public string NeighbourHood { get; set; }

        public string LocationDesc { get; set; }
        public string KitchenDesc { get; set; }
        public string DiningRoomDesc { get; set; }
        public string BasementDesc { get; set; }
        public string BedroomDesc { get; set; }
        public string BathroomDesc { get; set; }
        public string FamilyRoomDesc { get; set; }
        public string VideoDesc { get; set; }
        public string OtherSection1Desc { get; set; }
        public string OtherSection2Desc { get; set; }
        public string OtherSection3Desc { get; set; }
        public string OtherSection4Desc { get; set; }

        public int NoOfFloors { get; set; }
        public bool hasBasement { get; set; }
        public bool hasFinishedBasement { get; set; }
        public bool hasGarage { get; set; }
        public bool hasAC { get; set; }
        public decimal AnnualTaxes { get; set; }
        public List<ListingImage> images { get; set; }
        public string HeatType { get; set; }
        public int AgeOfHome { get; set; }
        public List<ListingImage> BasementImages { get; set; }

        public bool isLocationPublic { get; set; }
        public bool isKitchenPublic { get; set; }
        public bool isBathroomPublic { get; set; }
        public bool isBedroomPublic { get; set; }
        public bool isBasementPublic { get; set; }
        public bool isDiningPublic { get; set; }
        public bool isFamilyRoomPublic { get; set; }
        public bool isVideoPublic { get; set; }
        public bool isOtherSection1Public { get; set; }
        public bool isOtherSection2Public { get; set; }
        public bool isOtherSection3Public { get; set; }
        public bool isOtherSection4Public { get; set; }

        public string PropertyStatus { get; set; }

        public string PropertyDetails { get; set; }
    }

    public class ListingRequest
    {
        public int ListingId { get; set; }
        public string Message { get; set; }
    }

    public class ListingImage
    {
        public int ListingImageId { get; set; }
        public int ListingId { get; set; }
        public string ListingImageCategory { get; set; }
        public string Image { get; set; }
    }

    public class ListingImageCategory
    {
        public int ListingImageCategoryId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

    }

    public class RegistrationError
    {
        public int RegistrationErrorId { get; set; }
        public string ErrorType { get; set; }
        public string Message { get; set; }
        public string Code { get; set; }
        public string Parameter { get; set; }
        public string Charge { get; set; }
        public string StripeError { get; set; }
    }

    public class Configuration
    {
        public int ConfigId { get; set; }
        public string ConfigName { get; set; }
        public string CongfigValue { get; set; }
    }

    public enum MessageDirection
    {
        OutGoing, Incoming
    }

    public class SmsResponse
    {
        public int code;
        public string desc;
    }

    public class ListingCategorySection
    {
        public int ListingId { get; set; }
        public int ListingCategoryId { get; set; }
        public bool Value { get; set; }
        public int UserId { get; set; }
    }

    public class Message
    {
        public long MessageId { get; set; }
        public int MessageTypeId { get; set; }
        public string MessageTypeName { get; set; }
        public Guid ConversationId { get; set; }
        public int FromUser { get; set; }
        public int ToUser { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public DateTime MessageDate { get; set; }
        public bool MessageRead { get; set; }
        public int ListingID { get; set; }
    }

    public class MessageType
    {
        public int MessageTypeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }

    public class User
    {
        public int UserId { get; set; }
        public int UserTypeId { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }

    public class EmailMessage
    {
        public EmailMessage()
        {
            recipients = new List<string>();
        }
        public string sender { get; set; }
        public List<string> recipients { get; set; }
        public string subject { get; set; }
        public string body { get; set; }

    }



    public  class PackageChargeFrequency
    {
        public int PackageChargeFreqencyId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }

    public  class PackageFeature
    {
        public int PackageFeatureId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int FeatureChargeFrequencyId { get; set; }
        public decimal Cost { get; set; }
        public decimal ShippingCost { get; set; }

        public bool Optional { get; set; }
        public bool Shipping { get; set; }
        public string FeatureChargeFrequency { get; set; }
    }

    public class PackageFeatureAssociation
    {
        public int PackagefeatureAssociationId { get; set; }
        public int PackageId { get; set; }
        public int PackageFeatureId { get; set; }
        public bool Optional { get; set; }
        public bool Shipping { get; set; }
    }

    public  class PackageFeatureChargeFrequency
    {
        public int FeatureChargeFrequencyId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }

    public partial class PackageType
    {
        public int PackageTypeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int PackageChargeFrequencyId { get; set; }
        public decimal BaseCost { get; set; }
    }

    public partial class UserPackage
    {
        public int PackageId { get; set; }
        public int UserId { get; set; }
        public int PackageTypeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int PackageChargeFrequencyId { get; set; }
        public string PackageChargeFrequency { get; set; }
        public decimal BaseCost { get; set; }
    }
    public class MenuItems
    {
        public string menuHeading { get; set; }
        public int menuId { get; set; }
        public List<MenuLinks> Links;
        public string menuRole { get; set; }
        public MenuItems()
        {
            menuHeading= "";
            menuId= 0;
            List<MenuLinks> Links= new List<MenuLinks>();
            menuRole = "";
        }
    }

    public struct MenuLinks
    {
        public string Name;
        public string Hyperlink;
        public int Position;
    }
    public partial class StripePayment
    {
        public long StripePaymentId { get; set; }
        public int UserId { get; set; }
        public string StripeCustomerId { get; set; }
        public string StripeChargeId { get; set; }
        public decimal Amount { get; set; }
        public bool PaidByAgent { get; set; }
    }
}
