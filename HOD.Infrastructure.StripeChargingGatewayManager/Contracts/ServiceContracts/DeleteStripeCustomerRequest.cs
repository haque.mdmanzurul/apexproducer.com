﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HOD.Payments.StripeChargingGatewayManager.Contracts.ServiceContracts
{
    public sealed class DeleteStripeCustomerRequest
    {
         public DeleteStripeCustomerRequest()
        {
        }
         DeleteStripeCustomerRequest(String customerId)
        {
            CustomerId = customerId;
        }

        public String CustomerId { get; set; }
    }
}
