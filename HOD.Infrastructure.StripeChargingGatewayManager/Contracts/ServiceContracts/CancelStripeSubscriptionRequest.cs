﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace HOD.Payments.StripeChargingGatewayManager.Contracts.ServiceContracts
{
    public sealed class CancelStripeSubscriptionRequest
    {
        public string CustomerId { get; set; }
        public string SubscriptionId { get; set; }

        [JsonProperty("at_period_end")]
        public bool? AtPeriodEnd { get; set; }
    }
}
