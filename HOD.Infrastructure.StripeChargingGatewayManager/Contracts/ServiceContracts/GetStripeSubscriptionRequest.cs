﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HOD.Payments.StripeChargingGatewayManager.Contracts.ServiceContracts
{
    public sealed class GetStripeSubscriptionRequest
    {
        public string CustomerId { get; set; }
        public string SubscriptionId { get; set; }
    }
}
