﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace HOD.Payments.StripeChargingGatewayManager.Contracts.ServiceContracts
{

    public sealed class UpdateStripeCustomerRequest
    {
        public string CustomerId { get; set; }

        [JsonProperty("plan")]
        public string PlanId { get; set; }

        [JsonProperty("coupon")]
        public string CouponId { get; set; }

        [JsonProperty("prorate")]
        public bool? Prorate { get; set; }

        public DateTime? TrialEnd { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("quantity")]
        public int? Quantity { get; set; }

        [JsonProperty("trial_end")]
        internal int? TrialEndInternal
        {
            get
            {
                if (!TrialEnd.HasValue) return null;

                var diff = TrialEnd.Value - new DateTime(1970, 1, 1);

                return (int)Math.Floor(diff.TotalSeconds);
            }
        }
    }
}
