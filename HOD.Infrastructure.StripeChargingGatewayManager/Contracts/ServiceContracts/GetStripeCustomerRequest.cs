﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HOD.Payments.StripeChargingGatewayManager.Contracts.ServiceContracts
{

    public sealed class GetStripeCustomerRequest
    {
        public GetStripeCustomerRequest()
        {
        }

        GetStripeCustomerRequest(String customerId)
        {
            CustomerId = customerId;
        }

        public String CustomerId { get; set; }
    }
}
