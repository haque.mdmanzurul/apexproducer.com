﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HOD.Payments.StripeChargingGatewayManager.Contracts.DataContracts;

namespace HOD.Payments.StripeChargingGatewayManager.Contracts.ServiceContracts
{
    public sealed class GetStripeSubscriptionsResponse
    {
        public List<Subscription> Subscriptions { get; set; }
    }
}
