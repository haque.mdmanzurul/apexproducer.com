﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HOD.Payments.StripeChargingGatewayManager.Contracts.DataContracts;
using HOD.Payments.StripeChargingGatewayManager.DataContracts;

namespace HOD.Payments.StripeChargingGatewayManager.Contracts.ServiceContracts
{
    public sealed class GetStripeChargesResponse
    {
        public List<Charge> Charges { get; set; }
    }
}
