﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HOD.Payments.StripeChargingGatewayManager.Contracts.ServiceContracts
{

    public sealed class GetStripePlanRequest
    {
        public GetStripePlanRequest()
        {
        }

        GetStripePlanRequest(String planId)
        {
            PlanId = planId;
        }

        public String PlanId { get; set; }
    }
}
