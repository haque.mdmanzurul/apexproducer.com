﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace HOD.Payments.StripeChargingGatewayManager.Contracts.ServiceContracts
{
    public sealed class GetStripeInvoiceItemsRequest
    {
        public GetStripeInvoiceItemsRequest()
        {
        }

        [JsonProperty("customer")]
        public string CustomerId { get; set; }

        [JsonProperty("count")]
        public int? Count { get; set; }

        [JsonProperty("offset")]
        public int? OffSet { get; set; }
    }
}
