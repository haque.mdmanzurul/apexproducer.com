﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HOD.Payments.StripeChargingGatewayManager.Contracts.ServiceContracts
{

    public sealed class GetStripeInvoiceItemRequest
    {
        public GetStripeInvoiceItemRequest()
        {
        }

        GetStripeInvoiceItemRequest(String invoiceItemId)
        {
            InvoiceItemId = invoiceItemId;
        }

        public String InvoiceItemId { get; set; }
    }
}
