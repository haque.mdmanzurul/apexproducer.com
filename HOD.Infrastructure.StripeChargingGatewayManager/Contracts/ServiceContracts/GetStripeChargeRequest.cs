﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HOD.Payments.StripeChargingGatewayManager.Contracts.ServiceContracts
{
    public sealed class GetStripeChargeRequest
    {
        public GetStripeChargeRequest()
        {
        }
        GetStripeChargeRequest(String chargeId)
        {
            ChargeId = chargeId;
        }

        public String ChargeId { get; set; }
    }
}
