﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HOD.Payments.StripeChargingGatewayManager.Contracts.ServiceContracts
{
    public sealed class GetStripeTokenRequest
    {
        public GetStripeTokenRequest()
        {
        }
        GetStripeTokenRequest(String tokenId)
        {
            TokenId = tokenId;
        }

        public String TokenId { get; set; }
    }
}
