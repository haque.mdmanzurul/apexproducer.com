﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace HOD.Payments.StripeChargingGatewayManager.Contracts.ServiceContracts
{
    public sealed class CreateStripeCustomerRequest
    {
        [JsonProperty("coupon")]
        public string CouponId { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("account_balance")]
        public int? AccountBalance { get; set; }

        [JsonProperty("plan")]
        public string PlanId { get; set; }

        public DateTime? TrialEnd { get; set; }

        [JsonProperty("quantity")]
        public int? Quantity { get; set; }

        [JsonProperty("trial_end")]
        internal int? TrialEndInternal
        {
            get
            {
                if (!TrialEnd.HasValue) return null;

                var diff = TrialEnd.Value - new DateTime(1970, 1, 1);

                return (int)Math.Floor(diff.TotalSeconds);
            }
        }

        [JsonProperty("card[number]")]
        public string CardNumber { get; set; }

        [JsonProperty("card[exp_month]")]
        public string CardExpirationMonth { get; set; }

        [JsonProperty("card[exp_year]")]
        public string CardExpirationYear { get; set; }

        [JsonProperty("card[cvc]")]
        public string CardCvc { get; set; }

        [JsonProperty("card[name]")]
        public string CardName { get; set; }

        [JsonProperty("card[address_line1]")]
        public string CardAddressLine1 { get; set; }

        [JsonProperty("card[address_line2]")]
        public string CardAddressLine2 { get; set; }

        [JsonProperty("card[address_zip]")]
        public string CardAddressZip { get; set; }

        [JsonProperty("card[address_city]")]
        public string CardAddressCity { get; set; }

        [JsonProperty("card[address_state]")]
        public string CardAddressState { get; set; }

        [JsonProperty("card[address_country]")]
        public string CardAddressCountry { get; set; }

        [JsonProperty("card")]
        public string TokenId { get; set; }
    }
}
