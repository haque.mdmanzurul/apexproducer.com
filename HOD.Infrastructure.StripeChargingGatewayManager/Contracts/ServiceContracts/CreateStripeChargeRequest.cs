﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace HOD.Payments.StripeChargingGatewayManager.Contracts.ServiceContracts
{
    public sealed class CreateStripeChargeRequest
    {
        [JsonProperty("amount")]
        public int? AmountInCents { get; set; }

        [JsonProperty("application_fee")]
        public int? ApplicationFeeInCents { get; set; }

        [JsonProperty("currency")]
        public string Currency { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("customer")]
        public string CustomerId { get; set; }

        [JsonProperty("capture")]
        public bool? Capture { get; set; }

        [JsonProperty("card")]
        public string Card { get; set; }

        [JsonProperty("card[number]")]
        public string CardNumber { get; set; }

        [JsonProperty("card[exp_month]")]
        public string CardExpirationMonth { get; set; }

        [JsonProperty("card[exp_year]")]
        public string CardExpirationYear { get; set; }

        [JsonProperty("card[cvc]")]
        public string CardCvc { get; set; }

        [JsonProperty("card[name]")]
        public string CardName { get; set; }

        [JsonProperty("card[address_line1]")]
        public string CardAddressLine1 { get; set; }

        [JsonProperty("card[address_line2]")]
        public string CardAddressLine2 { get; set; }

        [JsonProperty("card[address_zip]")]
        public string CardAddressZip { get; set; }

        [JsonProperty("card[address_city]")]
        public string CardAddressCity { get; set; }

        [JsonProperty("card[address_state]")]
        public string CardAddressState { get; set; }

        [JsonProperty("card[address_country]")]
        public string CardAddressCountry { get; set; }

    }
}
