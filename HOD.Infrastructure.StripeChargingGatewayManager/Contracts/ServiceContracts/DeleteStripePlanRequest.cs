﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HOD.Payments.StripeChargingGatewayManager.Contracts.ServiceContracts
{
    public sealed class DeleteStripePlanRequest
    {
         public DeleteStripePlanRequest()
        {
        }
         DeleteStripePlanRequest(String planId)
        {
            PlanId = planId;
        }

        public String PlanId { get; set; }
    }
}
