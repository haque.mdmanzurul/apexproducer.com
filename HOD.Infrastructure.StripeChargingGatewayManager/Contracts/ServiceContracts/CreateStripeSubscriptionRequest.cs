﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HOD.Payments.StripeChargingGatewayManager.Helpers;
using Newtonsoft.Json;

namespace HOD.Payments.StripeChargingGatewayManager.Contracts.ServiceContracts
{
    public sealed class CreateStripeSubscriptionRequest
    {
        public string CustomerId { get; set; }

        [JsonProperty("plan")]
        public string Plan { get; set; }

        [JsonProperty("coupon")]
        public string Coupon { get; set; }

        [JsonProperty("trial_end")]
        public int? TrialEnd { get; set; }

        [JsonProperty("card")]
        public String Card { get; set; }

        [JsonProperty("quantity")]
        public decimal? Quantity { get; set; }

        [JsonProperty("application_fee_percent")]
        public decimal? ApplicationFeePercent { get; set; }
    }
}
