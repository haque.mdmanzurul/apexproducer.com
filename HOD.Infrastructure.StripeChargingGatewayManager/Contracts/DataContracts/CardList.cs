﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HOD.Payments.StripeChargingGatewayManager.DataContracts;
using Newtonsoft.Json;

namespace HOD.Payments.StripeChargingGatewayManager.Contracts.DataContracts
{
    public sealed class CardList
    {
            [JsonProperty("data")]
            public List<Card> Cards { get; set; }
    }
}
