﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HOD.Payments.StripeChargingGatewayManager.Helpers;
using Newtonsoft.Json;

namespace HOD.Payments.StripeChargingGatewayManager.DataContracts
{
    public sealed class Charge
    {
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("id")]
        public String Id { get; set; }

        /// <summary>
        /// value is "charge"
        /// </summary>
        [JsonProperty("object")]
        public String Object { get { return "charge"; } }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("created")]
        [JsonConverter(typeof(DateTimeSerializer))]
        public DateTime Created { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("livemode")]
        public Boolean? LiveMode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("paid")]
        public Boolean? Paid { get; set; }

        /// <summary>
        /// Amount charged in cents
        /// </summary>
        [JsonProperty("amount")]
        public Int32? AmountInCents { get; set; }

        /// <summary>
        /// Three-letter ISO currency code representing the currency in which the charge was made.
        /// </summary>
        [JsonProperty("currency")]
        public String Currency { get; set; }

        /// <summary>
        /// Whether or not the charge has been fully refunded. If the charge is only partially refunded, this attribute will still be false.
        /// </summary>
        [JsonProperty("refunded")]
        public Boolean? Refunded { get; set; }

        /// <summary>
        /// hash A list of refunds that have been applied to the charge.
        /// </summary>
        [JsonProperty("refunds")]
        public Refund[] Refunds { get; set; }

        /// <summary>
        /// Hash describing the card used to make the charge
        /// </summary>
        [JsonProperty("card")]
        public Card StripeCard { get; set; }

        /// <summary>
        /// If the charge was created without capturing, this boolean represents whether or not it is still uncaptured or has since been captured.
        /// </summary>
        [JsonProperty("captured")]
        public Boolean? Captured { get; set; }

        /// <summary>
        /// Balance transaction that describes the impact of this charge on your account balance (not including refunds or disputes).
        /// </summary>
        [JsonProperty("balance_transaction")]
        public String BalanceTransaction { get; set; }

        /// <summary>
        /// Message to user further explaining reason for charge failure if available
        /// </summary>
        [JsonProperty("failure_message")]
        public String FailureMessage { get; set; }

        /// <summary>
        /// Error code explaining reason for charge failure if available (see https://stripe.com/docs/api#errors for a list of codes)
        /// </summary>
        [JsonProperty("failure_code")]
        public String FailureCode { get; set; }

        /// <summary>
        /// Amount in cents refunded (can be less than the amount attribute on the charge if a partial refund was issued)
        /// </summary>
        [JsonProperty("amount_refunded")]
        public Int32? AmountInCentsRefunded { get; set; }

        /// <summary>
        /// ID of the customer this charge is for if one exists
        /// </summary>
        [JsonProperty("customer")]
        public String Customer { get; set; }

        /// <summary>
        /// ID of the invoice this charge is for if one exists
        /// </summary>
        [JsonProperty("invoice")]
        public String Invoice { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("description")]
        public String Description { get; set; }

    }
}
