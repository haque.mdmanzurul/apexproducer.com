﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HOD.Payments.StripeChargingGatewayManager.Helpers;
using Newtonsoft.Json;

namespace HOD.Payments.StripeChargingGatewayManager.DataContracts
{
    public sealed class Refund
    {
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("object")]
        public string Object { get { return "refund"; } }

        /// <summary>
        /// Amount refunded, in cents.
        /// </summary>
        [JsonProperty("amount")]
        public Int32? Amount { get ; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("created")]
        [JsonConverter(typeof(DateTimeSerializer))]
        public string Created { get; set; }

        /// <summary>
        /// Three-letter ISO code representing the currency of the refund.
        /// </summary>
        [JsonProperty("currency")]
        public string Currency { get; set; }

        /// <summary>
        /// Balance transaction that describes the impact of this refund on your account balance.
        /// </summary>
        [JsonProperty("balance_transaction")]
        public string BalanceTransaction { get; set; }

    }
}
