﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HOD.Payments.StripeChargingGatewayManager.Helpers;
using Newtonsoft.Json;

namespace HOD.Payments.StripeChargingGatewayManager.Contracts.DataContracts
{
    public sealed class Subscription
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("customer")]
        public string CustomerId { get; set; }

        [JsonProperty("current_period_start")]
        [JsonConverter(typeof(DateTimeSerializer))]
        public DateTime? PeriodStart { get; set; }

        [JsonProperty("current_period_end")]
        [JsonConverter(typeof(DateTimeSerializer))]
        public DateTime? PeriodEnd { get; set; }

        [JsonProperty("start")]
        [JsonConverter(typeof(DateTimeSerializer))]
        public DateTime? Start { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("application_fee_percent")]
        public decimal? ApplicationFeePercent { get; set; }

        [JsonProperty("canceled_at")]
        [JsonConverter(typeof(DateTimeSerializer))]
        public DateTime? CanceledAt { get; set; }

        [JsonProperty("ended_at")]
        [JsonConverter(typeof(DateTimeSerializer))]
        public DateTime? EndedAt { get; set; }

        [JsonProperty("trial_start")]
        [JsonConverter(typeof(DateTimeSerializer))]
        public DateTime? TrialStart { get; set; }

        [JsonProperty("trial_end")]
        [JsonConverter(typeof(DateTimeSerializer))]
        public DateTime? TrialEnd { get; set; }

        [JsonProperty("quantity")]
        public int Quantity { get; set; }

        [JsonProperty("plan")]
        public Plan StripePlan { get; set; }

        [JsonProperty("cancel_at_period_end")]
        public bool CancelAtPeriodEnd { get; set; }
    }
}
