﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HOD.Payments.StripeChargingGatewayManager.Helpers;
using Newtonsoft.Json;

namespace HOD.Payments.StripeChargingGatewayManager.Contracts.DataContracts
{
    public class Discount
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("start")]
        [JsonConverter(typeof(DateTimeSerializer))]
        public DateTime? Start { get; set; }

        [JsonProperty("end")]
        [JsonConverter(typeof(DateTimeSerializer))]
        public DateTime? End { get; set; }

        [JsonProperty("coupon")]
        public Coupon StripeCoupon { get; set; }
        public Discount()
        {
            Id = "";
            StripeCoupon = new Coupon();
        }
    }
}
