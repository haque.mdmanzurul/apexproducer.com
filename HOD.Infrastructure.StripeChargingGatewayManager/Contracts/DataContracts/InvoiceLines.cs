﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace HOD.Payments.StripeChargingGatewayManager.Contracts.DataContracts
{
    public sealed class InvoiceLines
    {
        [JsonProperty("data")]
        public List<InvoiceItem> InvoiceItems { get; set; }
    }
}
