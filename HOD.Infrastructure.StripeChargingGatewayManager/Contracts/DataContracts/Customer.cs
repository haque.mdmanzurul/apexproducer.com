﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HOD.Payments.StripeChargingGatewayManager.Helpers;
using Newtonsoft.Json;

namespace HOD.Payments.StripeChargingGatewayManager.Contracts.DataContracts
{
    public sealed class Customer
    {
        
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("account_balance")]
        public int? AccountBalance { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("livemode")]
        public bool? LiveMode { get; set; }

        [JsonProperty("created")]
        [JsonConverter(typeof(DateTimeSerializer))]
        public DateTime Created { get; set; }

        [JsonProperty("deleted")]
        public bool? Deleted { get; set; }

        /// <summary>
        /// Whether or not the latest charge for the customer's latest invoice has failed.
        /// </summary>
        [JsonProperty("delinquent")]
        public bool? Delinquent { get; set; }

        /// <summary>
        /// Describes the current discount active on the customer, if there is one.
        /// </summary>
        [JsonProperty("discount")]
        public Discount StripeDiscount { get; set; }

        /// <summary>
        /// Hash describing the current subscription on the customer, if there is one. If the
        /// customer has no current subscription, this will be null.
        /// </summary>
        [JsonProperty("subscription")]
        public Subscription StripeSubscription { get; set; }

        /// <summary>
        /// ID of the default credit card attached to the customer.
        /// </summary>
        [JsonProperty("default_card")]
        public string StripeDefaultCardId { get; set; }

        [JsonProperty("cards")]
        public CardList StripeCardList { get; set; }

        public Customer()
        {
            Id="DefaultID";
            Email="";
            Description="";
            Created=new DateTime();
            StripeDiscount=new Discount();
            StripeSubscription = new Subscription();
            StripeDefaultCardId="";
            StripeCardList = new CardList();
        }
    }
}
