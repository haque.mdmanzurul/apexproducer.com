﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HOD.Payments.StripeChargingGatewayManager.Helpers
{
    public static class HTTPStatusCodes
    {
        public static String Message(Int32 code)
        {
            if (code == 200)
                return "OK - Everything worked as expected.";
            else if (code == 400)
                return "Bad Request - Often missing a required parameter.";
            else if (code == 401)
                return "Unauthorized - No valid API key provided.";
            else if (code == 402)
                return "Request Failed - Parameters were valid but request failed.";
            else if (code == 404)
                return "Not Found - The requested item doesn't exist.";
            else
                return "Server errors - something went wrong on Stripe's end.";
        }
    }
}
