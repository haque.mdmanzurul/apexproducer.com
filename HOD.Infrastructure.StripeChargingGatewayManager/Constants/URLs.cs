﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HOD.Payments.StripeChargingGatewayManager.Constants
{
    public static class URLs
    {
        private static String baseUrl = "https://api.stripe.com/" + GetStripeVersion();
        private static String version;

        public static String CHARGES = baseUrl + "/charges";
        public static String CHARGES_ID = baseUrl + "/charges/{0}";
        public static String COUPONS = baseUrl + "/coupons";
        public static String COUPONS_ID = baseUrl + "/coupons/{0}";
        public static String CUSTOMERS = baseUrl + "/customers";
        public static String CUSTOMERS_ID = baseUrl + "/customers/{0}";
        public static String CUSTOMERS_ID_SUBSCRIPTIONS = baseUrl + "/customers/{0}/subscriptions";
        public static String CUSTOMERS_ID_SUBSCRIPTIONS_ID = baseUrl + "/customers/{0}/subscriptions/{1}";
        public static String INVOICES = baseUrl + "/invoices";
        public static String INVOICES_ID_PAY = baseUrl + "/invoices/{0}" + "/pay";
        public static String INVOICES_ID = baseUrl + "/invoices/{0}";
        public static String INVOICES_ID_LINES = baseUrl + "/invoices/{0}/lines";
        public static String INVOICE_ITEMS = baseUrl + "/invoiceitems";
        public static String INVOICE_ITEMS_ID = baseUrl + "/invoiceitems/{0}";
        public static String PLANS = baseUrl + "/plans";
        public static String PLANS_ID = baseUrl + "/plans/{0}";
        public static String TOKENS = baseUrl + "/tokens";
        public static String TOKENS_ID = baseUrl + "/tokens/{0}";
        public static String EVENTS = baseUrl + "/events";
        public static String EVENTS_ID = baseUrl + "/events/{0}";


        internal static string GetStripeVersion()
        {
            if (String.IsNullOrEmpty(version))
                version = ConfigurationManager.AppSettings["Stripe_Version"];

            return version;
        }


    }
}
