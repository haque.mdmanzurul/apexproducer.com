﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HOD.Payments.StripeChargingGatewayManager.Constants
{
    public static class ErrorCodes
    {
        /// <summary>
        /// The card number is incorrect.
        /// </summary>
        public static String INCORRECT_NUMBER = "incorrect_number";
        /// <summary>
        /// The card number is not a valid credit card number.
        /// </summary>
        public static String INVALID_NUMBER = "invalid_number";
        /// <summary>
        /// The card's expiration month is invalid.
        /// </summary>
        public static String INVALID_EXPIRATION_MONTH = "invalid_expiry_month";
        /// <summary>
        /// The card's expiration year is invalid.
        /// </summary>
        public static String INVALID_EXPIRATION_YEAR = "invalid_expiry_year";
        /// <summary>
        /// The card's security code is invalid.
        /// </summary>
        public static String INVALID_CVC = "invalid_cvc";
        /// <summary>
        /// The card has expired.
        /// </summary>
        public static String EXPIRED_CARD = "expired_card";
        /// <summary>
        /// The card's security code is incorrect.
        /// </summary>
        public static String INCORRECT_CVC = "incorrect_cvc";
        /// <summary>
        /// The card's zip code failed validation.
        /// </summary>
        public static String INCORRECT_ZIP = "incorrect_zip";
        /// <summary>
        /// The card was declined.
        /// </summary>
        public static String CARD_DECLINED = "card_declined";
        /// <summary>
        /// There is no card on a customer that is being charged.
        /// </summary>
        public static String MISSING = "missing";
        /// <summary>
        /// An error occurred while processing the card.
        /// </summary>
        public static String PROCESSING_ERROR = "processing_error";
    }
}
