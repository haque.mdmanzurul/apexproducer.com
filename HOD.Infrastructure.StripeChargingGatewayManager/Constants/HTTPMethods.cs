﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HOD.Payments.StripeChargingGatewayManager.Constants
{
    public static class HTTPMethods
    {
        public static String POST = "POST";
        public static String GET = "GET";
        public static String PUT = "PUT";
        public static String DELETE = "DELETE";
    }
}
