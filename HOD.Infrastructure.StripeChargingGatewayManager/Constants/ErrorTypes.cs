﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HOD.Payments.StripeChargingGatewayManager.Constants
{
    public static class ErrorTypes
    {
        /// <summary>
        /// Invalid request errors arise when your request has invalid parameters.
        /// </summary>
        public static String INVALID_REQUEST_ERROR = "invalid_request_error";
        /// <summary>
        /// API errors cover any other type of problem (e.g. a temporary problem with Stripe's servers) and should turn up only very infrequently.
        /// </summary>
        public static String API_ERROR = "api_error";
        /// <summary>
        /// Card errors are the most common type of error you should expect to handle. They result when the user enters a card that can't be charged for some reason.
        /// </summary>
        public static String ERROR_CARD = "card_error";
    }
}
