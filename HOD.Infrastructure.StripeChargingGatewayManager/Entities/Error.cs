﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace HOD.Payments.StripeChargingGatewayManager.Entities
{
    public sealed class Error
    {
        /// <summary>
        /// The type of error returned. Can be invalid_request_error, api_error, or card_error.
        /// </summary>
        [JsonProperty("type")]
        public string ErrorType { get; set; }

        /// <summary>
        /// A human-readable message giving more details about the error. For card errors, these messages can be shown to your users.
        /// </summary>
        [JsonProperty("message")]
        public string Message { get; set; }

        /// <summary>
        /// optional
        /// For card errors, a short string from amongst those listed on the right describing the kind of card error that occurred.
        /// </summary>
        [JsonProperty("code")]
        public string Code { get; set; }

        /// <summary>
        /// optional
        /// The parameter the error relates to if the error is parameter-specific. You can use this to display a message near the correct form field, for example.
        /// </summary>
        [JsonProperty("param")]
        public string Parameter { get; set; }


        [JsonProperty("charge")]
        public string Charge { get; set; }


        [JsonProperty("error")]
        public string StripeError { get; set; }
    }
}

