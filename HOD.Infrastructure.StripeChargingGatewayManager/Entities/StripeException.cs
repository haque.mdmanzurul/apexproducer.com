﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace HOD.Payments.StripeChargingGatewayManager.Entities
{
     [JsonObject(MemberSerialization.OptIn)]
    public class StripeException : Exception
    {
        public HttpStatusCode HttpStatusCode { get; set; }

        
        public Error Error { get; set; }

        [JsonConstructor]
        public StripeException()
        {
        }

        public StripeException(HttpStatusCode httpStatusCode, Error error, string message)
            : base(message)
        {
            HttpStatusCode = httpStatusCode;
            Error = error;
        }
    }
}
