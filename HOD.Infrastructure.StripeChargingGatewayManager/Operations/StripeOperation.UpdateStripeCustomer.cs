﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HOD.Payments.StripeChargingGatewayManager.Constants;
using HOD.Payments.StripeChargingGatewayManager.Contracts.DataContracts;
using HOD.Payments.StripeChargingGatewayManager.Contracts.ServiceContracts;
using Newtonsoft.Json;

namespace HOD.Payments.StripeChargingGatewayManager.Operations
{
    public sealed partial class StripeOperation
    {
        public UpdateStripeCustomerResponse UpdateStripeCustomer(UpdateStripeCustomerRequest request)
        {
            UpdateStripeCustomerResponse response = new UpdateStripeCustomerResponse();

            String stripeResponse = ProcessRequest(String.Format(URLs.CUSTOMERS_ID, request.CustomerId), HTTPMethods.POST, BuildBody(request));

            response.Customer = JsonConvert.DeserializeObject<Customer>(stripeResponse);

            return response;
        }
    }
}
