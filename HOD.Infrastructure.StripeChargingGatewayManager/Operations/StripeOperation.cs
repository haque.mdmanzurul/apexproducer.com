﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using HOD.Payments.StripeChargingGatewayManager.Entities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace HOD.Payments.StripeChargingGatewayManager.Operations
{
    public sealed partial class StripeOperation
    {
        private WebRequest SetupRequest(string method, string url)
        {
            WebRequest webRequest = (WebRequest)WebRequest.Create(url);
            webRequest.Method = method;
            if (webRequest is HttpWebRequest)
            {
                ((HttpWebRequest)webRequest).UserAgent = "HOD Payments - Stripe.Net";
            }
            webRequest.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["Stripe_API_KEY"], ""); ;
            webRequest.PreAuthenticate = true;
            webRequest.Timeout = 60 * 1000;
            if (method == "POST")
                webRequest.ContentType = "application/x-www-form-urlencoded";
            return webRequest;
        }

        private string ProcessRequest(string endpoint, string method, string body)
        {
            string result = null;
            WebRequest webRequest = SetupRequest(method, endpoint);
            if (body != null)
            {
                byte[] bytes = Encoding.UTF8.GetBytes(body.ToString());
                webRequest.ContentLength = bytes.Length;
                using (Stream st = webRequest.GetRequestStream())
                {
                    st.Write(bytes, 0, bytes.Length);
                }
            }

            try
            {
                using (WebResponse response = (WebResponse)webRequest.GetResponse())
                {
                    using (StreamReader sr = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
                    {
                        result = sr.ReadToEnd();
                    }
                }
            }
            catch (WebException webException)
            {
                if (webException.Response != null)
                {
                    string json_error = "";
                    using (StreamReader sr = new StreamReader(webException.Response.GetResponseStream(), Encoding.UTF8))
                    {
                        json_error = sr.ReadToEnd();
                    }
                    HttpStatusCode status_code = HttpStatusCode.BadRequest;
                    HttpWebResponse errorResponse = webException.Response as HttpWebResponse;
                    if (errorResponse != null)
                        status_code = errorResponse.StatusCode;

                    if ((int)status_code <= 500)
                    {
                        Error error = default(Error);

                        JToken jTokenError = JObject.Parse(json_error).SelectToken("error");

                        if (jTokenError != null)
                        {
                            json_error = jTokenError.ToString();
                            error = JsonConvert.DeserializeObject<Error>(json_error);
                        }
                        else
                            error = JsonConvert.DeserializeObject<Error>(json_error);

                        throw new StripeException(status_code, error, error.Message);
                    }
                }
                throw;
            }
            return result;
        }

        private string BuildBody(Object obj)
		{
			if (obj == null) return "";

			var parameterBody = "";

			foreach (PropertyInfo property in obj.GetType().GetProperties(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance))
			{
				foreach (Object attribute in property.GetCustomAttributes(false))
				{
					if (attribute.GetType() == typeof(JsonPropertyAttribute))
                    {
					    JsonPropertyAttribute jsonPropertyAttribute = attribute as JsonPropertyAttribute;

                        if(jsonPropertyAttribute != null)
                        {
					        Object value = property.GetValue(obj, null);

					        if (value != null)
                             parameterBody = String.Format("{0}{1}={2}{3}", parameterBody, jsonPropertyAttribute.PropertyName, HttpUtility.UrlEncode(value.ToString()), "&");
                            //parameterBody = String.Format("{0}{1}={2}", parameterBody, jsonPropertyAttribute.PropertyName, HttpUtility.UrlEncode(value.ToString()));
                        }
                    }
				}
			}

			return parameterBody;
		}

        private string BuildBody(Object obj, bool stripeCreateInvoice)
        {
            if (obj == null) return "";

            var parameterBody = "";

            foreach (PropertyInfo property in obj.GetType().GetProperties(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance))
            {
                foreach (Object attribute in property.GetCustomAttributes(false))
                {
                    if (attribute.GetType() == typeof(JsonPropertyAttribute))
                    {
                        JsonPropertyAttribute jsonPropertyAttribute = attribute as JsonPropertyAttribute;

                        if (jsonPropertyAttribute != null)
                        {
                            Object value = property.GetValue(obj, null);

                            if (value != null)
                                //parameterBody = String.Format("{0}{1}={2}{3}", parameterBody, jsonPropertyAttribute.PropertyName, HttpUtility.UrlEncode(value.ToString()), "&");
                                parameterBody = String.Format("{0}{1}={2}", parameterBody, jsonPropertyAttribute.PropertyName, HttpUtility.UrlEncode(value.ToString()));
                        }
                    }
                }
            }

            return parameterBody;
        }



	}
}
