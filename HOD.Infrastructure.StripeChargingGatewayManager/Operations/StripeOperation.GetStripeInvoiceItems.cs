﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HOD.Payments.StripeChargingGatewayManager.Constants;
using HOD.Payments.StripeChargingGatewayManager.Contracts.DataContracts;
using HOD.Payments.StripeChargingGatewayManager.Contracts.ServiceContracts;
using HOD.Payments.StripeChargingGatewayManager.Helpers;
using Newtonsoft.Json;

namespace HOD.Payments.StripeChargingGatewayManager.Operations
{
    public sealed partial class StripeOperation
    {
        public GetStripeInvoiceItemsResponse GetStripeInvoiceItems(GetStripeInvoiceItemsRequest request)
        {
            GetStripeInvoiceItemsResponse response = new GetStripeInvoiceItemsResponse();

            String stripeResponse = ProcessRequest(URLs.INVOICE_ITEMS, HTTPMethods.GET, null);

            response.InvoiceItems = Mapper<InvoiceItem>.MapCollectionFromJson(stripeResponse);

            return response;
        }
    }
}
