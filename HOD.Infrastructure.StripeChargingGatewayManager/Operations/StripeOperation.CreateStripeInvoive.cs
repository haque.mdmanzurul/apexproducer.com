﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HOD.Payments.StripeChargingGatewayManager.Constants;
using HOD.Payments.StripeChargingGatewayManager.Contracts.DataContracts;
using HOD.Payments.StripeChargingGatewayManager.Contracts.ServiceContracts;
using Newtonsoft.Json;

namespace HOD.Payments.StripeChargingGatewayManager.Operations
{
    public sealed partial class StripeOperation
    {
        public CreateStripeInvoiceResponse CreateStripeInvoice(CreateStripeInvoiceRequest request)
        {
            CreateStripeInvoiceResponse response = new CreateStripeInvoiceResponse();

            String stripeResponse = ProcessRequest(URLs.INVOICES, HTTPMethods.POST, BuildBody(request));

            response.Invoice = JsonConvert.DeserializeObject<Invoice>(stripeResponse);

            return response;
        }
    }
}
