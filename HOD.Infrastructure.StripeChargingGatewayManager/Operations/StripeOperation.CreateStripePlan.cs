﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HOD.Payments.StripeChargingGatewayManager.Constants;
using HOD.Payments.StripeChargingGatewayManager.Contracts.DataContracts;
using HOD.Payments.StripeChargingGatewayManager.Contracts.ServiceContracts;
using Newtonsoft.Json;

namespace HOD.Payments.StripeChargingGatewayManager.Operations
{
    public sealed partial class StripeOperation
    {
        public CreateStripePlanResponse CreateStripePlan(CreateStripePlanRequest request)
        {
            CreateStripePlanResponse response = new CreateStripePlanResponse();

            String stripeResponse = ProcessRequest(URLs.PLANS, HTTPMethods.POST, BuildBody(request));

            response.Plan = JsonConvert.DeserializeObject<Plan>(stripeResponse);

            return response;
        }
    }
}
