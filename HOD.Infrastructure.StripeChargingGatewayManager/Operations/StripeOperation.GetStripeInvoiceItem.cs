﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HOD.Payments.StripeChargingGatewayManager.Constants;
using HOD.Payments.StripeChargingGatewayManager.Contracts.DataContracts;
using HOD.Payments.StripeChargingGatewayManager.Contracts.ServiceContracts;
using Newtonsoft.Json;

namespace HOD.Payments.StripeChargingGatewayManager.Operations
{
    public sealed partial class StripeOperation
    {
        public GetStripeInvoiceItemResponse GetStripeInvoiceItem(GetStripeInvoiceItemRequest request)
        {
            GetStripeInvoiceItemResponse response = new GetStripeInvoiceItemResponse();

            String stripeResponse = ProcessRequest(String.Format(URLs.INVOICE_ITEMS_ID, request.InvoiceItemId), HTTPMethods.GET, null);

            response.InvoiceItem = JsonConvert.DeserializeObject<InvoiceItem>(stripeResponse);

            return response;
        }
    }
}
