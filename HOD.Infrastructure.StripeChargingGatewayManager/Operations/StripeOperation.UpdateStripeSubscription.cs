﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HOD.Payments.StripeChargingGatewayManager.Constants;
using HOD.Payments.StripeChargingGatewayManager.Contracts.DataContracts;
using HOD.Payments.StripeChargingGatewayManager.Contracts.ServiceContracts;
using Newtonsoft.Json;

namespace HOD.Payments.StripeChargingGatewayManager.Operations
{
    public sealed partial class StripeOperation
    {
        public UpdateStripeSubscriptionResponse UpdateStripeSubscription(UpdateStripeSubscriptionRequest request)
        {
            UpdateStripeSubscriptionResponse response = new UpdateStripeSubscriptionResponse();

            String stripeResponse = ProcessRequest(String.Format(URLs.CUSTOMERS_ID_SUBSCRIPTIONS_ID, request.CustomerId, request.SubscriptionId), HTTPMethods.POST, BuildBody(request));

            response.Subscription = JsonConvert.DeserializeObject<Subscription>(stripeResponse);

            return response;
        }
    }
}
