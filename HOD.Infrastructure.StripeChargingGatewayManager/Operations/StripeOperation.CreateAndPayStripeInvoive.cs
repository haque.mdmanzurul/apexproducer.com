﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HOD.Payments.StripeChargingGatewayManager.Constants;
using HOD.Payments.StripeChargingGatewayManager.Contracts.DataContracts;
using HOD.Payments.StripeChargingGatewayManager.Contracts.ServiceContracts;
using Newtonsoft.Json;

namespace HOD.Payments.StripeChargingGatewayManager.Operations
{
    public sealed partial class StripeOperation
    {
        public CreateAndPayStripeInvoiceResponse CreateAndPayStripeInvoice(CreateAndPayStripeInvoiceRequest request)
        {
            CreateAndPayStripeInvoiceResponse response = new CreateAndPayStripeInvoiceResponse();

            String stripeCreateResponse = ProcessRequest(URLs.INVOICES, HTTPMethods.PUT, BuildBody(request, true));

            //response.Invoice = JsonConvert.DeserializeObject<InvoiceName>(stripeCreateResponse);
            InvoiceName inv = JsonConvert.DeserializeObject<InvoiceName>(stripeCreateResponse);

            if (response.Invoice != null)
            {
                String stripePayResponse = ProcessRequest(String.Format(URLs.INVOICES_ID_PAY, response.Invoice.Id), HTTPMethods.PUT, null);

                response.Invoice = JsonConvert.DeserializeObject<Invoice>(stripePayResponse);
            }

            return response;
        }
    }
}
