﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HOD.Payments.StripeChargingGatewayManager.Constants;
using HOD.Payments.StripeChargingGatewayManager.Contracts.DataContracts;
using HOD.Payments.StripeChargingGatewayManager.Contracts.ServiceContracts;
using Newtonsoft.Json;

namespace HOD.Payments.StripeChargingGatewayManager.Operations
{
    public sealed partial class StripeOperation
    {
        public GetStripePlanResponse GetStripePlan(GetStripePlanRequest request)
        {
            GetStripePlanResponse response = new GetStripePlanResponse();

            String stripeResponse = ProcessRequest(String.Format(URLs.PLANS_ID, request.PlanId), HTTPMethods.GET, null);

            response.Plan = JsonConvert.DeserializeObject<Plan>(stripeResponse);

            return response;
        }
    }
}
