﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HOD.Payments.StripeChargingGatewayManager.Constants;
using HOD.Payments.StripeChargingGatewayManager.Contracts.ServiceContracts;
using Newtonsoft.Json;

namespace HOD.Payments.StripeChargingGatewayManager.Operations
{
    public sealed partial class StripeOperation
    {
        public DeleteStripeCustomerResponse DeleteStripeCustomer(DeleteStripeCustomerRequest request)
        {
            DeleteStripeCustomerResponse response = new DeleteStripeCustomerResponse();

            String stripeResponse = ProcessRequest(String.Format(URLs.CUSTOMERS_ID, request.CustomerId), HTTPMethods.DELETE, null);

            response = JsonConvert.DeserializeObject<DeleteStripeCustomerResponse>(stripeResponse);

            return response;
        }
    }
}
