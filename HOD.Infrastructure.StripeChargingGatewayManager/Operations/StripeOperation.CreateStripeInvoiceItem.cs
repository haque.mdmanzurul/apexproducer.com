﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HOD.Payments.StripeChargingGatewayManager.Constants;
using HOD.Payments.StripeChargingGatewayManager.Contracts.DataContracts;
using HOD.Payments.StripeChargingGatewayManager.Contracts.ServiceContracts;
using Newtonsoft.Json;

namespace HOD.Payments.StripeChargingGatewayManager.Operations
{
    public sealed partial class StripeOperation
    {
        public CreateStripeInvoiceItemResponse CreateStripeInvoiceItem(CreateStripeInvoiceItemRequest request)
        {
            CreateStripeInvoiceItemResponse response = new CreateStripeInvoiceItemResponse();

            String stripeResponse = ProcessRequest(URLs.INVOICE_ITEMS, HTTPMethods.POST, BuildBody(request));

            response.InvoiceItem = JsonConvert.DeserializeObject<InvoiceItem>(stripeResponse);

            return response;
        }
    }
}
