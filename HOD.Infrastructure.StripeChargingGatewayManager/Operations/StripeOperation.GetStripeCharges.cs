﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HOD.Payments.StripeChargingGatewayManager.Constants;
using HOD.Payments.StripeChargingGatewayManager.Contracts.DataContracts;
using HOD.Payments.StripeChargingGatewayManager.Contracts.ServiceContracts;
using HOD.Payments.StripeChargingGatewayManager.DataContracts;
using HOD.Payments.StripeChargingGatewayManager.Helpers;

namespace HOD.Payments.StripeChargingGatewayManager.Operations
{
    public sealed partial class StripeOperation
    {
        public GetStripeChargesResponse GetStripeCharges(GetStripeChargesRequest request)
        {
            GetStripeChargesResponse response = new GetStripeChargesResponse();

            String stripeResponse = ProcessRequest(String.Format(URLs.CHARGES + "?customer={0}", request.CustomerId), HTTPMethods.GET, null);

            response.Charges = Mapper<Charge>.MapCollectionFromJson(stripeResponse);

            return response;
        }
    }
}
