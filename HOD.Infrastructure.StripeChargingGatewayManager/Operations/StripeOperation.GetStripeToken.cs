﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HOD.Payments.StripeChargingGatewayManager.Constants;
using HOD.Payments.StripeChargingGatewayManager.Contracts.DataContracts;
using HOD.Payments.StripeChargingGatewayManager.Contracts.ServiceContracts;
using Newtonsoft.Json;

namespace HOD.Payments.StripeChargingGatewayManager.Operations
{
    public sealed partial class StripeOperation
    {
        public GetStripeTokenResponse GetStripeToken(GetStripeTokenRequest request)
        {
            GetStripeTokenResponse response = new GetStripeTokenResponse();

            String stripeResponse = ProcessRequest(String.Format(URLs.TOKENS_ID, request.TokenId), HTTPMethods.GET, null);

            response.Token = JsonConvert.DeserializeObject<Token>(stripeResponse);

            return response;
        }
    }
}
