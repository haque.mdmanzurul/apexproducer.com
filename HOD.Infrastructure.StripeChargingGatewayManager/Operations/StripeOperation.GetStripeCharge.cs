﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HOD.Payments.StripeChargingGatewayManager.Constants;
using HOD.Payments.StripeChargingGatewayManager.Contracts.ServiceContracts;
using HOD.Payments.StripeChargingGatewayManager.DataContracts;
using Newtonsoft.Json;

namespace HOD.Payments.StripeChargingGatewayManager.Operations
{
    public sealed partial class StripeOperation
    {
        public GetStripeChargeResponse GetStripeCharge(GetStripeChargeRequest request)
        {
            GetStripeChargeResponse response = new GetStripeChargeResponse();

            String stripeResponse = ProcessRequest(String.Format(URLs.CHARGES_ID, request.ChargeId), HTTPMethods.GET, null);

            response.Charge = JsonConvert.DeserializeObject<Charge>(stripeResponse);

            return response;
        }
    }
}
