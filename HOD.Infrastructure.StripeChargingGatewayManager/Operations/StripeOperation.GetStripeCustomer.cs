﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HOD.Payments.StripeChargingGatewayManager.Constants;
using HOD.Payments.StripeChargingGatewayManager.Contracts.DataContracts;
using HOD.Payments.StripeChargingGatewayManager.Contracts.ServiceContracts;
using Newtonsoft.Json;

namespace HOD.Payments.StripeChargingGatewayManager.Operations
{
    public sealed partial class StripeOperation
    {
        public GetStripeCustomerResponse GetStripeCustomer(GetStripeCustomerRequest request)
        {
            GetStripeCustomerResponse response = new GetStripeCustomerResponse();

            String stripeResponse = ProcessRequest(String.Format(URLs.CUSTOMERS_ID, request.CustomerId), HTTPMethods.GET, null);

            response.Customer = JsonConvert.DeserializeObject<Customer>(stripeResponse);

            return response;
        }
    }
}
