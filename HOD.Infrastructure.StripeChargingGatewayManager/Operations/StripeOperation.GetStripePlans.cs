﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HOD.Payments.StripeChargingGatewayManager.Constants;
using HOD.Payments.StripeChargingGatewayManager.Contracts.DataContracts;
using HOD.Payments.StripeChargingGatewayManager.Contracts.ServiceContracts;
using HOD.Payments.StripeChargingGatewayManager.Helpers;
using Newtonsoft.Json;

namespace HOD.Payments.StripeChargingGatewayManager.Operations
{
    public sealed partial class StripeOperation
    {
        public GetStripePlansResponse GetStripePlans(GetStripePlansRequest request)
        {
            GetStripePlansResponse response = new GetStripePlansResponse();

            String stripeResponse = ProcessRequest(URLs.PLANS, HTTPMethods.GET, null);

            response.Plans = Mapper<Plan>.MapCollectionFromJson(stripeResponse);

            return response;
        }
    }
}
