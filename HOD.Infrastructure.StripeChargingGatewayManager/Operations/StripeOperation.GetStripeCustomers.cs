﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HOD.Payments.StripeChargingGatewayManager.Constants;
using HOD.Payments.StripeChargingGatewayManager.Contracts.DataContracts;
using HOD.Payments.StripeChargingGatewayManager.Contracts.ServiceContracts;
using HOD.Payments.StripeChargingGatewayManager.Helpers;
using Newtonsoft.Json;

namespace HOD.Payments.StripeChargingGatewayManager.Operations
{
    public sealed partial class StripeOperation
    {
        public GetStripeCustomersResponse GetStripeCustomers(GetStripeCustomersRequest request)
        {
            GetStripeCustomersResponse response = new GetStripeCustomersResponse();

            String stripeResponse = ProcessRequest(URLs.CUSTOMERS, HTTPMethods.GET, null);

            response.Customers = Mapper<Customer>.MapCollectionFromJson(stripeResponse);

            return response;
        }
    }
}
