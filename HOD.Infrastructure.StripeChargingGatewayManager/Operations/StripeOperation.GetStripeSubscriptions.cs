﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HOD.Payments.StripeChargingGatewayManager.Constants;
using HOD.Payments.StripeChargingGatewayManager.Contracts.DataContracts;
using HOD.Payments.StripeChargingGatewayManager.Contracts.ServiceContracts;
using HOD.Payments.StripeChargingGatewayManager.Helpers;

namespace HOD.Payments.StripeChargingGatewayManager.Operations
{
    public sealed partial class StripeOperation
    {
        public GetStripeSubscriptionsResponse GetStripeSubscriptions(GetStripeSubscriptionsRequest request)
        {
            GetStripeSubscriptionsResponse response = new GetStripeSubscriptionsResponse();

            String stripeResponse = ProcessRequest(String.Format(URLs.CUSTOMERS_ID_SUBSCRIPTIONS, request.CustomerId), HTTPMethods.GET, null);

            response.Subscriptions = Mapper<Subscription>.MapCollectionFromJson(stripeResponse);

            return response;
        }
    }
}
