﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HOD.Payments.StripeChargingGatewayManager.Constants;
using HOD.Payments.StripeChargingGatewayManager.Contracts.DataContracts;
using HOD.Payments.StripeChargingGatewayManager.Contracts.ServiceContracts;
using Newtonsoft.Json;

namespace HOD.Payments.StripeChargingGatewayManager.Operations
{
    public sealed partial class StripeOperation
    {
        public PayStripeInvoiceResponse PayStripeInvoice(PayStripeInvoiceRequest request)
        {
            PayStripeInvoiceResponse response = new PayStripeInvoiceResponse();

            String stripeResponse = ProcessRequest(String.Format(URLs.INVOICES_ID_PAY, request.InvoiceId), HTTPMethods.POST, null);

            response.Invoice = JsonConvert.DeserializeObject<Invoice>(stripeResponse);

            return response;
        }
    }
}
