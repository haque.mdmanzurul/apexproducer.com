﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HOD.Payments.StripeChargingGatewayManager.Constants;
using HOD.Payments.StripeChargingGatewayManager.Contracts.ServiceContracts;
using Newtonsoft.Json;

namespace HOD.Payments.StripeChargingGatewayManager.Operations
{
    public sealed partial class StripeOperation
    {
        public DeleteStripePlanResponse DeleteStripePlan(DeleteStripePlanRequest request)
        {
            DeleteStripePlanResponse response = new DeleteStripePlanResponse();

            String stripeResponse = ProcessRequest(String.Format(URLs.PLANS_ID, request.PlanId), HTTPMethods.DELETE, null);

            response = JsonConvert.DeserializeObject<DeleteStripePlanResponse>(stripeResponse);

            return response;
        }
    }
}
