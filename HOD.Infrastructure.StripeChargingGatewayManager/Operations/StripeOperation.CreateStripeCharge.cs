﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HOD.Payments.StripeChargingGatewayManager.Constants;
using HOD.Payments.StripeChargingGatewayManager.Contracts.ServiceContracts;
using HOD.Payments.StripeChargingGatewayManager.DataContracts;
using Newtonsoft.Json;

namespace HOD.Payments.StripeChargingGatewayManager.Operations
{
    public sealed partial class StripeOperation
    {
        public CreateStripeChargeResponse CreateStripeCharge(CreateStripeChargeRequest request)
        {
            CreateStripeChargeResponse response = new CreateStripeChargeResponse();

            String stripeResponse = ProcessRequest(URLs.CHARGES, HTTPMethods.POST, BuildBody(request));

            response.Charge = JsonConvert.DeserializeObject<Charge>(stripeResponse);

            return response;
        }
    }
}
