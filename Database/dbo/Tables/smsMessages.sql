﻿CREATE TABLE [dbo].[smsMessages] (
    [id]               INT           IDENTITY (1, 1) NOT NULL,
    [messageGuid]      VARCHAR (100) NULL,
    [messageDate]      DATETIME      NULL,
    [phoneNo]          VARCHAR (50)  NULL,
    [message]          VARCHAR (200) NULL,
    [MessageDirection] CHAR (1)      NULL,
    CONSTRAINT [PK_smsMessages] PRIMARY KEY CLUSTERED ([id] ASC)
);

