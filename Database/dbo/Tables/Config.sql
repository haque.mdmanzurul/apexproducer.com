﻿CREATE TABLE [dbo].[Config] (
    [Id]          INT           IDENTITY (1, 1) NOT NULL,
    [ConfigName]  VARCHAR (200) NOT NULL,
    [Value]       VARCHAR (300) NOT NULL,
    [Description] VARCHAR (MAX) NULL
);

