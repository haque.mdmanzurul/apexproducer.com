﻿Create table ListingLayout
(
 [ListingLayoutId] [int] IDENTITY(1,1) PRIMARY KEY ,
    [ListingId] [int] ,
 [HeaderImage] [varchar](500),
 [SkinColor] [varchar](100),
 [BackgroundImage] [varchar](500),
 [BackgroundColor] [varchar](100)
)
