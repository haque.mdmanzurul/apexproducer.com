﻿Create table PackageList
(
 [PackageId] [int] IDENTITY(1,1) PRIMARY KEY , 
 [PackageName] [varchar](500),
 [PackageDescription] [varchar](500),
 [PackageAmount] [int],
 [PackageTypeId] [int],
 CONSTRAINT [FK_PackageList] PRIMARY KEY CLUSTERED ([PackageTypeId] ASC)
)
