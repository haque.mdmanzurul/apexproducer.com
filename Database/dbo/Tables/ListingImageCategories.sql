﻿CREATE TABLE [dbo].[ListingImageCategories] (
    [ListingImageCategoryId] INT            IDENTITY (1, 1) NOT NULL,
    [Name]                   NVARCHAR (50)  NOT NULL,
    [Description]            NVARCHAR (200) NULL,
    CONSTRAINT [PK_ListingImageCategories] PRIMARY KEY CLUSTERED ([ListingImageCategoryId] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_ListingImageCategories]
    ON [dbo].[ListingImageCategories]([Name] ASC);

