﻿CREATE TABLE [dbo].[ListingImages] (
    [ListingImageId]         INT           IDENTITY (1, 1) NOT NULL,
    [ListingId]              INT           NOT NULL,
    [ListingImageCategoryId] INT           NOT NULL,
    [Image]                  VARCHAR (MAX) NULL,
    CONSTRAINT [PK_ListingImages] PRIMARY KEY CLUSTERED ([ListingImageId] ASC),
    CONSTRAINT [FK_ListingImages_ListingImageCategories] FOREIGN KEY ([ListingImageCategoryId]) REFERENCES [dbo].[ListingImageCategories] ([ListingImageCategoryId]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_ListingImages_Listings] FOREIGN KEY ([ListingId]) REFERENCES [dbo].[Listings] ([ListingId]) ON DELETE CASCADE ON UPDATE CASCADE
);

