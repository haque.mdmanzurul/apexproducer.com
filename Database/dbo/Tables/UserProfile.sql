﻿CREATE TABLE [dbo].[UserProfile] (
    [UserId]              INT            IDENTITY (1, 1) NOT NULL,
    [UserName]            NVARCHAR (56)  NOT NULL,
    [FirstName]           NVARCHAR (56)  NULL,
    [LastName]            NVARCHAR (56)  NULL,
    [Address]             NVARCHAR (200) NULL,
    [City]                NVARCHAR (56)  NULL,
    [Province]            NVARCHAR (56)  NULL,
    [PostalCode]          NVARCHAR (20)  NULL,
    [PhoneNumberCell]     NVARCHAR (10)  NULL,
    [PhoneNumberLandLine] NVARCHAR (10)  NULL,
    [Carrier]             NVARCHAR (56)  NULL,
    [BillingName]         NVARCHAR (120) NULL,
    [BillingAddress]      NVARCHAR (200) NULL,
    [BillingCity]         NVARCHAR (56)  NULL,
    [BillingProvince]     NVARCHAR (56)  NULL,
    [BillingPostalCode]   NVARCHAR (20)  NULL,
    [StripeChargeId]      NVARCHAR (56)  NULL,
	[MaxUploadCount]      NVARCHAR (56)  NULL,
    CONSTRAINT [PK__UserProf__1788CC4C30F848ED] PRIMARY KEY CLUSTERED ([UserId] ASC),
    CONSTRAINT [UQ__UserProf__C9F2845633D4B598] UNIQUE NONCLUSTERED ([UserName] ASC)
);

