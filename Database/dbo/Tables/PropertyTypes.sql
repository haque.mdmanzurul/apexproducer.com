﻿CREATE TABLE [dbo].[PropertyTypes] (
    [PropertyTypeId] VARCHAR (5)   NOT NULL,
    [Name]           VARCHAR (50)  NULL,
    [Description]    VARCHAR (100) NULL,
    CONSTRAINT [PK_PropertyTypes] PRIMARY KEY CLUSTERED ([PropertyTypeId] ASC)
);

