﻿using System;
using HOD.Payments.StripeChargingGatewayManager.Constants;
using HOD.Payments.StripeChargingGatewayManager.Contracts.ServiceContracts;
using HOD.Payments.StripeChargingGatewayManager.Entities;
using HOD.Payments.StripeChargingGatewayManager.Operations;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace HOD.Payments.Tests
{
    [TestClass]
    public class ChargeUnitTest
    {
        private static String invalid_expiry_month = "13";
        private static String invalid_expiry_year = "1970";
        private static String invalid_cvc = "99";
        private static String FAIL = "fail";
        private static String UNCHECKED = "unchecked";

        private static class CardNumbers
        {
            public static String Visa1 = "4242424242424242";
            public static String Visa2 = "4012888888881881";
            public static String MasterCard1 = "5555555555554444";
            public static String MasterCard2 = "5105105105105100";
            public static String AmericanExpress1 = "378282246310005";
            public static String AmericanExpress2 = "371449635398431";
            public static String Discover1 = "6011111111111117";
            public static String Discover2 = "6011000990139424";
            public static String DinersClub1 = "30569309025904";
            public static String DinersClub2 = "38520000023237";
            public static String JCB1 = "3530111333300000";
            public static String JCB2 = "3566002020360505";

            public static String FAIL_address_line1_check_and_address_zip_check = "4000000000000010";
            public static String FAIL_address_line1_check = "4000000000000028";
            public static String FAIL_address_zip_check = "4000000000000036";
            public static String FAIL_address_zip_check_and_address_line1_check = "4000000000000044";
            public static String FAIL_cvc_check = "4000000000000101";

            public static String FAIL_charge_the_card = "4000000000000341";
            public static String FAIL_card_declined = "4000000000000002";
            public static String FAIL_incorrect_cvc = "4000000000000127";
            public static String FAIL_expired_card = "4000000000000069";
            public static String FAIL_incorrect_number = "4242424242424241";

            public static String FAIL_processing_error = "4000000000000119";
        }

        private static class CardTypes
        {
            public static String Visa = "Visa";
            public static String MasterCard = "MasterCard";
            public static String AmericanExpress = "American Express";
            public static String Discover = "Discover";
            public static String DinersClub = "Diners Club";
            public static String JCB = "JCB";
        }


        [TestMethod]
        public void TestChargeOnMultipleCards()
        {
            //TestChargeOnCardType(CardNumbers.Visa1, CardTypes.Visa, new Random().Next(5000, 100000));
            //TestChargeOnCardType(CardNumbers.Visa2, CardTypes.Visa, new Random().Next(5000, 100000));
            //TestChargeOnCardType(CardNumbers.AmericanExpress1, CardTypes.AmericanExpress, new Random().Next(5000, 100000));
            //TestChargeOnCardType(CardNumbers.AmericanExpress2, CardTypes.AmericanExpress, new Random().Next(5000, 100000));
            //TestChargeOnCardType(CardNumbers.DinersClub1, CardTypes.DinersClub, new Random().Next(5000, 100000));
            //TestChargeOnCardType(CardNumbers.DinersClub2, CardTypes.DinersClub, new Random().Next(5000, 100000));
            //TestChargeOnCardType(CardNumbers.Discover1, CardTypes.Discover, new Random().Next(5000, 100000));
            //TestChargeOnCardType(CardNumbers.Discover2, CardTypes.Discover, new Random().Next(5000, 100000));
            //TestChargeOnCardType(CardNumbers.MasterCard1, CardTypes.MasterCard, new Random().Next(5000, 100000));
            //TestChargeOnCardType(CardNumbers.MasterCard2, CardTypes.MasterCard, new Random().Next(5000, 100000));
            //TestChargeOnCardType(CardNumbers.JCB1, CardTypes.JCB, new Random().Next(5000, 100000));
            //TestChargeOnCardType(CardNumbers.JCB2, CardTypes.JCB, new Random().Next(5000, 100000));

            //TestChargeINVALID_MONTH(CardNumbers.Visa1, new Random().Next(5000, 100000), invalid_expiry_month, ErrorCodes.INVALID_EXPIRATION_MONTH, ErrorTypes.ERROR_CARD);
            //TestChargeINVALID_YEAR(CardNumbers.Visa1, new Random().Next(5000, 100000), invalid_expiry_year, ErrorCodes.INVALID_EXPIRATION_YEAR, ErrorTypes.ERROR_CARD);
            //TestChargeINVALID_CVC(CardNumbers.Visa1, new Random().Next(5000, 100000), invalid_cvc, ErrorCodes.INVALID_CVC, ErrorTypes.ERROR_CARD);

            //TestChargeFAIL(CardNumbers.FAIL_address_line1_check_and_address_zip_check, new Random().Next(5000, 100000), FAIL);
            //TestChargeFAIL(CardNumbers.FAIL_address_line1_check, new Random().Next(5000, 100000), FAIL);
            //TestChargeFAIL(CardNumbers.FAIL_address_zip_check, new Random().Next(5000, 100000), FAIL);
            //TestChargeFAIL(CardNumbers.FAIL_address_zip_check_and_address_line1_check, new Random().Next(5000, 100000), UNCHECKED);
            //TestChargeFAIL(CardNumbers.FAIL_cvc_check, new Random().Next(5000, 100000), FAIL);

            //TestChargeERROR(CardNumbers.FAIL_charge_the_card, new Random().Next(5000, 100000), ErrorCodes.CARD_DECLINED, ErrorTypes.ERROR_CARD);
            //TestChargeERROR(CardNumbers.FAIL_card_declined, new Random().Next(5000, 100000), ErrorCodes.CARD_DECLINED, ErrorTypes.ERROR_CARD);
            //TestChargeERROR(CardNumbers.FAIL_incorrect_cvc, new Random().Next(5000, 100000), ErrorCodes.INCORRECT_CVC, ErrorTypes.ERROR_CARD);
            //TestChargeERROR(CardNumbers.FAIL_expired_card, new Random().Next(5000, 100000), ErrorCodes.EXPIRED_CARD, ErrorTypes.ERROR_CARD);
            //TestChargeERROR(CardNumbers.FAIL_incorrect_number, new Random().Next(5000, 100000), ErrorCodes.INCORRECT_NUMBER, ErrorTypes.ERROR_CARD);

            TestChargeERROR(CardNumbers.FAIL_processing_error, new Random().Next(5000, 100000), ErrorCodes.PROCESSING_ERROR, ErrorTypes.ERROR_CARD);

        }
        [TestMethod]
        private void TestChargeOnCardType(String cardNumber, String cardType, Int32 amount)
        {
            StripeOperation operation = new StripeOperation();

            CreateStripeChargeRequest createChargeRequest = new CreateStripeChargeRequest()
            {
                CardAddressCountry = "US",
                CardAddressLine1 = "18 th North West Street",
                CardAddressLine2 = "West Bromdale",
                CardAddressCity = "Ft. Lauderdale",
                CardAddressState = "FL",
                CardAddressZip = "33313",
                CardCvc = "123",
                CardExpirationMonth = "11",
                CardExpirationYear = "2014",
                CardName = "Howard McCarthy",
                CardNumber = cardNumber,
                Description = "Test Charge",
                AmountInCents = amount,
                Currency = "usd"
            };

            CreateStripeChargeResponse createChargeResponse = operation.CreateStripeCharge(createChargeRequest);

            Assert.IsNotNull(createChargeResponse);
            Assert.IsNotNull(createChargeResponse.Charge);
            Assert.IsNotNull(createChargeResponse.Charge.StripeCard);

            Assert.AreEqual(createChargeResponse.Charge.AmountInCents, createChargeRequest.AmountInCents);
            Assert.AreEqual(createChargeResponse.Charge.Currency, createChargeRequest.Currency);

            Assert.IsTrue(createChargeResponse.Charge.StripeCard.Type == cardType);
            Assert.AreEqual(createChargeResponse.Charge.StripeCard.ExpirationMonth, createChargeRequest.CardExpirationMonth);
            Assert.AreEqual(createChargeResponse.Charge.StripeCard.ExpirationYear, createChargeRequest.CardExpirationYear);
            Assert.AreEqual(createChargeResponse.Charge.StripeCard.Name, createChargeRequest.CardName);

            GetStripeChargeRequest getChargeRequest = new GetStripeChargeRequest()
            {
                ChargeId = createChargeResponse.Charge.Id
            };

            GetStripeChargeResponse getChargeResponse = operation.GetStripeCharge(getChargeRequest);

            Assert.IsNotNull(getChargeResponse);
            Assert.IsNotNull(createChargeResponse.Charge);
            Assert.IsNotNull(createChargeResponse.Charge.StripeCard);

            Assert.AreEqual(createChargeResponse.Charge.Id, getChargeResponse.Charge.Id);
            Assert.AreEqual(createChargeResponse.Charge.AmountInCents, getChargeResponse.Charge.AmountInCents);

            Assert.IsTrue(createChargeResponse.Charge.StripeCard.Type == cardType);
            Assert.AreEqual(createChargeResponse.Charge.StripeCard.Id, getChargeResponse.Charge.StripeCard.Id);
            Assert.AreEqual(createChargeResponse.Charge.StripeCard.Name, getChargeResponse.Charge.StripeCard.Name);
            Assert.AreEqual(createChargeResponse.Charge.StripeCard.Fingerprint, getChargeResponse.Charge.StripeCard.Fingerprint);
        }

        [TestMethod]
        private void TestChargeFAIL(String cardNumber, Int32 amount, String failure)
        {
            StripeOperation operation = new StripeOperation();

            CreateStripeChargeRequest createChargeRequest = new CreateStripeChargeRequest()
            {
                CardAddressCountry = "US",
                CardAddressLine1 = "18 th North West Street",
                CardAddressLine2 = "West Bromdale",
                CardAddressCity = "Ft. Lauderdale",
                CardAddressState = "FL",
                CardAddressZip = "33313",
                CardCvc = "123",
                CardExpirationMonth = "11",
                CardExpirationYear = "2014",
                CardName = "Howard McCarthy",
                CardNumber = cardNumber,
                Description = "Test Charge",
                AmountInCents = amount,
                Currency = "usd"
            };

            CreateStripeChargeResponse createChargeResponse = operation.CreateStripeCharge(createChargeRequest);

            Assert.IsNotNull(createChargeResponse);
            Assert.IsNotNull(createChargeResponse.Charge);
            Assert.IsNotNull(createChargeResponse.Charge.StripeCard);

            Assert.AreEqual(createChargeResponse.Charge.AmountInCents, createChargeRequest.AmountInCents);
            Assert.AreEqual(createChargeResponse.Charge.Currency, createChargeRequest.Currency);

            Assert.AreEqual(createChargeResponse.Charge.StripeCard.ExpirationMonth, createChargeRequest.CardExpirationMonth);
            Assert.AreEqual(createChargeResponse.Charge.StripeCard.ExpirationYear, createChargeRequest.CardExpirationYear);
            Assert.AreEqual(createChargeResponse.Charge.StripeCard.Name, createChargeRequest.CardName);

            if (CardNumbers.FAIL_address_line1_check_and_address_zip_check == cardNumber)
            {
                Assert.AreEqual(createChargeResponse.Charge.StripeCard.AddressLine1Check, failure);
                Assert.AreEqual(createChargeResponse.Charge.StripeCard.AddressZipCheck, failure);
            }
            else if (CardNumbers.FAIL_address_zip_check_and_address_line1_check == cardNumber)
            {
                Assert.AreEqual(createChargeResponse.Charge.StripeCard.AddressLine1Check, failure);
                Assert.AreEqual(createChargeResponse.Charge.StripeCard.AddressZipCheck, failure);
            }
            else if (CardNumbers.FAIL_address_line1_check == cardNumber)
            {
                Assert.AreEqual(createChargeResponse.Charge.StripeCard.AddressLine1Check, failure);
            }
            else if (CardNumbers.FAIL_address_zip_check == cardNumber)
            {
                Assert.AreEqual(createChargeResponse.Charge.StripeCard.AddressZipCheck, failure);
            }
            else if (CardNumbers.FAIL_cvc_check == cardNumber)
            {
                Assert.AreEqual(createChargeResponse.Charge.StripeCard.CvcCheck, failure);
            }
        }

        [TestMethod]
        private void TestChargeERROR(String cardNumber, Int32 amount, String errorCode, String errorType)
        {
            StripeOperation operation = new StripeOperation();

            CreateStripeChargeRequest createChargeRequest = new CreateStripeChargeRequest()
            {
                CardAddressCountry = "US",
                CardAddressLine1 = "18 th North West Street",
                CardAddressLine2 = "West Bromdale",
                CardAddressCity = "Ft. Lauderdale",
                CardAddressState = "FL",
                CardAddressZip = "33313",
                CardCvc = "123",
                CardExpirationMonth = "11",
                CardExpirationYear = "2014",
                CardName = "Howard McCarthy",
                CardNumber = cardNumber,
                Description = "Test Charge",
                AmountInCents = amount,
                Currency = "usd"
            };

            try
            {
                CreateStripeChargeResponse createChargeResponse = operation.CreateStripeCharge(createChargeRequest);
            }
            catch (StripeException stripeException)
            {
                Assert.IsNotNull(stripeException);
                Assert.IsNotNull(stripeException.Error);
                Assert.IsNotNull(stripeException.Error.Code);

                Assert.AreEqual(stripeException.Error.Code, errorCode);
                Assert.AreEqual(stripeException.Error.ErrorType, errorType);
            }
        }

        [TestMethod]
        private void TestChargeINVALID_MONTH(String cardNumber, Int32 amount, String invalidMonth, String errorCode, String errorType)
        {
            StripeOperation operation = new StripeOperation();

            CreateStripeChargeRequest createChargeRequest = new CreateStripeChargeRequest()
            {
                CardAddressCountry = "US",
                CardAddressLine1 = "18 th North West Street",
                CardAddressLine2 = "West Bromdale",
                CardAddressCity = "Ft. Lauderdale",
                CardAddressState = "FL",
                CardAddressZip = "33313",
                CardCvc = "123",
                CardExpirationMonth = invalidMonth,
                CardExpirationYear = "2014",
                CardName = "Howard McCarthy",
                CardNumber = cardNumber,
                Description = "Test Charge",
                AmountInCents = amount,
                Currency = "usd"
            };

            try
            {
                CreateStripeChargeResponse createChargeResponse = operation.CreateStripeCharge(createChargeRequest);
            }
            catch (StripeException stripeException)
            {
                Assert.IsNotNull(stripeException);
                Assert.IsNotNull(stripeException.Error);
                Assert.IsNotNull(stripeException.Error.Code);

                Assert.AreEqual(stripeException.Error.Code, errorCode);
                Assert.AreEqual(stripeException.Error.ErrorType, errorType);
            }
        }

        [TestMethod]
        private void TestChargeINVALID_YEAR(String cardNumber, Int32 amount, String invalidYear, String errorCode, String errorType)
        {
            StripeOperation operation = new StripeOperation();

            CreateStripeChargeRequest createChargeRequest = new CreateStripeChargeRequest()
            {
                CardAddressCountry = "US",
                CardAddressLine1 = "18 th North West Street",
                CardAddressLine2 = "West Bromdale",
                CardAddressCity = "Ft. Lauderdale",
                CardAddressState = "FL",
                CardAddressZip = "33313",
                CardCvc = "123",
                CardExpirationMonth = "11",
                CardExpirationYear = invalidYear,
                CardName = "Howard McCarthy",
                CardNumber = cardNumber,
                Description = "Test Charge",
                AmountInCents = amount,
                Currency = "usd"
            };

            try
            {
                CreateStripeChargeResponse createChargeResponse = operation.CreateStripeCharge(createChargeRequest);
            }
            catch (StripeException stripeException)
            {
                Assert.IsNotNull(stripeException);
                Assert.IsNotNull(stripeException.Error);
                Assert.IsNotNull(stripeException.Error.Code);

                Assert.AreEqual(stripeException.Error.Code, errorCode);
                Assert.AreEqual(stripeException.Error.ErrorType, errorType);
            }
        }

        [TestMethod]
        private void TestChargeINVALID_CVC(String cardNumber, Int32 amount, String invalidCvc, String errorCode, String errorType)
        {
            StripeOperation operation = new StripeOperation();

            CreateStripeChargeRequest createChargeRequest = new CreateStripeChargeRequest()
            {
                CardAddressCountry = "US",
                CardAddressLine1 = "18 th North West Street",
                CardAddressLine2 = "West Bromdale",
                CardAddressCity = "Ft. Lauderdale",
                CardAddressState = "FL",
                CardAddressZip = "33313",
                CardCvc = invalidCvc,
                CardExpirationMonth = "11",
                CardExpirationYear = "2014",
                CardName = "Howard McCarthy",
                CardNumber = cardNumber,
                Description = "Test Charge",
                AmountInCents = amount,
                Currency = "usd"
            };

            try
            {
                CreateStripeChargeResponse createChargeResponse = operation.CreateStripeCharge(createChargeRequest);
            }
            catch (StripeException stripeException)
            {
                Assert.IsNotNull(stripeException);
                Assert.IsNotNull(stripeException.Error);
                Assert.IsNotNull(stripeException.Error.Code);

                Assert.AreEqual(stripeException.Error.Code, errorCode);
                Assert.AreEqual(stripeException.Error.ErrorType, errorType);
            }
        }

        [TestMethod]
        public void TestGetPlan()
        {
            StripeOperation operation = new StripeOperation();

            GetStripePlanRequest getStripePlanRequest = new GetStripePlanRequest()
            {
                PlanId = "Plan 2 Test"
            };

            GetStripePlanResponse getStripePlanResponse = operation.GetStripePlan(getStripePlanRequest);

            Assert.IsNotNull(getStripePlanResponse);
            Assert.IsNotNull(getStripePlanResponse.Plan);
            Assert.IsNotNull(getStripePlanResponse.Plan.Id);
        }

        [TestMethod]
        public void TestGetPlans()
        {
            StripeOperation operation = new StripeOperation();

            GetStripePlansRequest getStripePlansRequest = new GetStripePlansRequest()
            {
                Count = 1,
                OffSet = 0
            };

            GetStripePlansResponse getStripePlansResponse = operation.GetStripePlans(getStripePlansRequest);

            Assert.IsNotNull(getStripePlansResponse);
            Assert.IsNotNull(getStripePlansResponse.Plans);
            Assert.IsNotNull(getStripePlansResponse.Plans.Count);
        }

        [TestMethod]
        public void TestCreatePlan()
        {
            StripeOperation operation = new StripeOperation();

            CreateStripePlanRequest createStripeRequest = new CreateStripePlanRequest()
            {
                Id = "Weekly Payment",
                AmountInCents = 10000,
                Currency = "USD",
                Interval = "week",
                IntervalCount = 2,
                Name = "Weekly Payment"
            };

            CreateStripePlanResponse createStripeResponse = operation.CreateStripePlan(createStripeRequest);

            Assert.IsNotNull(createStripeResponse);
            Assert.IsNotNull(createStripeResponse.Plan);
        }

        [TestMethod]
        public void TestDeletePlan()
        {
            StripeOperation operation = new StripeOperation();

            DeleteStripePlanRequest deleteStripeRequest = new DeleteStripePlanRequest()
            {
                PlanId = "Plan 2 Test",
            };

            DeleteStripePlanResponse deleteStripeResponse = operation.DeleteStripePlan(deleteStripeRequest);

            Assert.IsNotNull(deleteStripeResponse);
        }


        [TestMethod]
        public void TestCreateStripeSubscription()
        {
            StripeOperation operation = new StripeOperation();

            CreateStripeSubscriptionRequest stripeSubscriptionRequest = new CreateStripeSubscriptionRequest()
            {
                CustomerId = "cus_3BN2BLHQB42Ro1",
                Plan = "Sellers Privacy Protection"
            };

            CreateStripeSubscriptionResponse stripeSubscriptionResponse = operation.CreateStripeSubscription(stripeSubscriptionRequest);

            Assert.IsNotNull(stripeSubscriptionResponse);
        }

        [TestMethod]
        public void TestGetStripeSubscription()
        {
            StripeOperation operation = new StripeOperation();

            GetStripeSubscriptionRequest stripeSubscriptionRequest = new GetStripeSubscriptionRequest()
            {
                CustomerId = "cus_3BN2BLHQB42Ro1",
                SubscriptionId = "sub_3gn3R7ETNH0qfl"
            };

            GetStripeSubscriptionResponse stripeSubscriptionResponse = operation.GetStripeSubscription(stripeSubscriptionRequest);

            Assert.IsNotNull(stripeSubscriptionResponse);
        }

        [TestMethod]
        public void TestGetStripeSubscriptions()
        {
            StripeOperation operation = new StripeOperation();

            GetStripeSubscriptionsRequest stripeSubscriptionsRequest = new GetStripeSubscriptionsRequest()
            {
                CustomerId = "cus_3BN2BLHQB42Ro1",
                Count = 10,
                OffSet = 0
            };

            GetStripeSubscriptionsResponse stripeSubscriptionsResponse = operation.GetStripeSubscriptions(stripeSubscriptionsRequest);

            Assert.IsNotNull(stripeSubscriptionsResponse);
        }

        [TestMethod]
        public void TestUpdateStripeSubscription()
        {
            StripeOperation operation = new StripeOperation();

            UpdateStripeSubscriptionRequest stripeSubscriptionRequest = new UpdateStripeSubscriptionRequest()
            {
                CustomerId = "cus_3BN2BLHQB42Ro1",
                SubscriptionId = "sub_3gn3R7ETNH0qfl",
                Plan = "Seller Executive Assistant 2 Weeks"
            };

            UpdateStripeSubscriptionResponse stripeSubscriptionResponse = operation.UpdateStripeSubscription(stripeSubscriptionRequest);

            Assert.IsNotNull(stripeSubscriptionResponse);
        }

        [TestMethod]
        public void TestCancelStripeSubscription()
        {
            StripeOperation operation = new StripeOperation();

            CancelStripeSubscriptionRequest stripeSubscriptionRequest = new CancelStripeSubscriptionRequest()
            {
                CustomerId = "cus_3BN2BLHQB42Ro1",
                SubscriptionId = "sub_3goihvLc35rQiT",
            };

            CancelStripeSubscriptionResponse stripeSubscriptionResponse = operation.CancelStripeSubscription(stripeSubscriptionRequest);

            Assert.IsNotNull(stripeSubscriptionResponse);
        }


        [TestMethod]
        public void TestCreateStripeCustomer()
        {
            StripeOperation operation = new StripeOperation();

            CreateStripeCustomerRequest stripeCustomerRequest = new CreateStripeCustomerRequest()
            {
                CardAddressCountry = "US",
                CardAddressLine1 = "234 Bacon St",
                CardAddressLine2 = "Apt 1",
                CardAddressState = "NC",
                CardAddressZip = "27617",
                Email = "howardmccarthy@email.com",
                CardCvc = "1661",
                CardExpirationMonth = "10",
                CardExpirationYear = "2021",
                CardName = "Howard Mac",
                CardNumber = "4242424242424242",
                Description = "Howard McCarthy (howardmccarthy@email.com)",
                AccountBalance = 100,
                PlanId = "Sellers Privacy Protection"
            };

            CreateStripeCustomerResponse stripeCustomerResponse = operation.CreateStripeCustomer(stripeCustomerRequest);

            Assert.IsNotNull(stripeCustomerResponse);
        }

        [TestMethod]
        public void TestGetStripeCustomer()
        {
            StripeOperation operation = new StripeOperation();

            GetStripeCustomerRequest stripeCustomerRequest = new GetStripeCustomerRequest()
            {
                CustomerId = "cus_3BN2BLHQB42Ro1"
            };

            GetStripeCustomerResponse stripeCustomerResponse = operation.GetStripeCustomer(stripeCustomerRequest);

            Assert.IsNotNull(stripeCustomerResponse);
        }

        [TestMethod]
        public void TestGetStripeCustomers()
        {
            StripeOperation operation = new StripeOperation();

            GetStripeCustomersRequest stripeCustomersRequest = new GetStripeCustomersRequest()
            {
                Count = 10,
                OffSet = 0
            };

            GetStripeCustomersResponse stripeCustomersResponse = operation.GetStripeCustomers(stripeCustomersRequest);

            Assert.IsNotNull(stripeCustomersResponse);
        }

        [TestMethod]
        public void TestUpdateStripeCustomer()
        {
            StripeOperation operation = new StripeOperation();

            UpdateStripeCustomerRequest stripeCustomerRequest = new UpdateStripeCustomerRequest()
            {
                CustomerId = "cus_3BN2BLHQB42Ro1",
                Description = "Real Deal (realdeal@email.com)",
            };

            UpdateStripeCustomerResponse stripeCustomerResponse = operation.UpdateStripeCustomer(stripeCustomerRequest);

            Assert.IsNotNull(stripeCustomerResponse);
        }

        [TestMethod]
        public void TestDeleteStripeCustomer()
        {
            StripeOperation operation = new StripeOperation();

            DeleteStripeCustomerRequest stripeCustomerRequest = new DeleteStripeCustomerRequest()
            {
                CustomerId = "cus_3BN7ebDTMrUIwT"
            };

            DeleteStripeCustomerResponse stripeCustomerResponse = operation.DeleteStripeCustomer(stripeCustomerRequest);

            Assert.IsNotNull(stripeCustomerResponse);
        }


        //Stripe InvoiceItems
        [TestMethod]
        public void TestCreateStripeInvoiceItem()
        {
            StripeOperation operation = new StripeOperation();

            CreateStripeInvoiceItemRequest stripeInvoiceItemRequest = new CreateStripeInvoiceItemRequest()
            {
                AmountInCents = 1000,
                Currency = "usd",
                CustomerId = "cus_3gqmV4w4yQ4mYO",
                Description = "Test Invoice Item 2"
            };
                                                                                  
            CreateStripeInvoiceItemResponse stripeInvoiceItemResponse = operation.CreateStripeInvoiceItem(stripeInvoiceItemRequest);

            Assert.IsNotNull(stripeInvoiceItemResponse);
        }

        [TestMethod]
        public void TestGetStripeInvoiceItem()
        {
            StripeOperation operation = new StripeOperation();

            GetStripeInvoiceItemRequest stripeInvoiceItemRequest = new GetStripeInvoiceItemRequest()
            {
                InvoiceItemId = "ii_103hA22kk7h8NI58R2ExRwuq"
            };

            GetStripeInvoiceItemResponse stripeInvoiceItemResponse = operation.GetStripeInvoiceItem(stripeInvoiceItemRequest);

            Assert.IsNotNull(stripeInvoiceItemResponse);
        }

        [TestMethod]
        public void TestGetStripeInvoiceItems()
        {
            StripeOperation operation = new StripeOperation();

            GetStripeInvoiceItemsRequest stripeInvoiceItemsRequest = new GetStripeInvoiceItemsRequest()
            {
                Count = 10,
                OffSet = 0
            };

            GetStripeInvoiceItemsResponse stripeInvoiceItemsResponse = operation.GetStripeInvoiceItems(stripeInvoiceItemsRequest);

            Assert.IsNotNull(stripeInvoiceItemsResponse);
        }

        [TestMethod]
        public void TestUpdateStripeInvoiceItem()
        {
            StripeOperation operation = new StripeOperation();

            UpdateStripeInvoiceItemRequest stripeInvoiceItemRequest = new UpdateStripeInvoiceItemRequest()
            {
                InvoiceItemId = "ii_103hA22kk7h8NI58R2ExRwuq",
                Description = "Updated Fee",
            };

            UpdateStripeInvoiceItemResponse stripeInvoiceItemResponse = operation.UpdateStripeInvoiceItem(stripeInvoiceItemRequest);

            Assert.IsNotNull(stripeInvoiceItemResponse);
        }

        [TestMethod]
        public void TestDeleteStripeInvoiceItem()
        {
            ////StripeOperation operation = new StripeOperation();

            ////DeleteStripeInvoiceItemRequest stripeInvoiceItemRequest = new DeleteStripeInvoiceItemRequest()
            ////{
            ////    InvoiceItemId = "ii_103hA22kk7h8NI58R2ExRwuq"
            ////};

            ////DeleteStripeInvoiceItemResponse stripeInvoiceItemResponse = operation.DeleteStripeInvoiceItem(stripeInvoiceItemRequest);

            ////Assert.IsNotNull(stripeInvoiceItemResponse);

            //StripeOperation operation = new StripeOperation();

            //CreateStripeChargeRequest createChargeRequest = new CreateStripeChargeRequest()
            //{
            //    AmountInCents = 200,
            //    CustomerId = "cus_3gqmV4w4yQ4mYO",
            //    Description = "Test Charge",
            //    Currency = "usd"
            //};

            //CreateStripeChargeResponse createChargeResponse = operation.CreateStripeCharge(createChargeRequest);

            //Assert.IsNotNull(createChargeResponse);

            //StripeOperation operation = new StripeOperation();

            //CreateStripeInvoiceRequest createChargeRequest = new CreateStripeInvoiceRequest()
            //{
            //    CustomerId = "cus_3gqmV4w4yQ4mYO",
            //};

            //CreateStripeInvoiceResponse createInvoiceResponse = operation.CreateStripeInvoice(createChargeRequest);

            //Assert.IsNotNull(createInvoiceResponse);

            StripeOperation operation = new StripeOperation();

            PayStripeInvoiceRequest payChargeRequest = new PayStripeInvoiceRequest()
            {
                InvoiceId = "in_103hD92kk7h8NI5824rvP7Zz",
            };

            PayStripeInvoiceResponse payInvoiceResponse = operation.PayStripeInvoice(payChargeRequest);

            Assert.IsNotNull(payInvoiceResponse);
        }

        [TestMethod]
        private void TestCreateChargeForCustomer()
        {
            StripeOperation operation = new StripeOperation();

            CreateStripeChargeRequest createChargeRequest = new CreateStripeChargeRequest()
            {
                AmountInCents = 6777,
                CustomerId = "cus_3gqmV4w4yQ4mYO",
                Description = "Test Charge",
                Currency = "usd"
            };

            CreateStripeChargeResponse createChargeResponse = operation.CreateStripeCharge(createChargeRequest);

            Assert.IsNotNull(createChargeResponse);
        }

        //[TestMethod]
        //private void TestCreateInvoice()
        //{
        //    StripeOperation operation = new StripeOperation();

        //    CreateStripeInvoiceRequest createChargeRequest = new CreateStripeInvoiceRequest()
        //    {
        //        CustomerId = "cus_3gqmV4w4yQ4mYO",
        //    };

        //    CreateStripeInvoiceResponse createInvoiceResponse = operation.CreateStripeInvoice(createChargeRequest);

        //    Assert.IsNotNull(createInvoiceResponse);
        //}


    }
}
