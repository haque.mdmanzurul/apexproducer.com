﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HOD.Validation
{
    public class ModelClientValidationNotEqualToRule : ModelClientValidationRule
    {
        public ModelClientValidationNotEqualToRule(string errorMessage, string otherProperties)
        {
            ErrorMessage = errorMessage;
            ValidationType = "notequalto";
            ValidationParameters.Add("otherproperties", otherProperties);
        }
    }
}