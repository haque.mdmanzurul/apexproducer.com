﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.IO;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using System.Drawing.Imaging;
using HOD.Entities;
using HOD.Managers.Utilities;
using System.Collections.Specialized;
using System.Text;
using HOD.Managers;

using System.Threading.Tasks;
using System.Net.Http;

using Amazon;
using Amazon.S3;
using Amazon.S3.IO;
using Newtonsoft.Json;
using System.Web.Script.Serialization;


namespace HOD.Controllers
{

    public static class BitmapExtensions
    {
        public static void SaveJPG100(this Bitmap bmp, string filename)
        {
            var encoderParameters = new EncoderParameters(1);
            encoderParameters.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 100L);
            bmp.Save(filename, GetEncoder(ImageFormat.Jpeg), encoderParameters);
        }

        public static void SaveJPG100(this Bitmap bmp, Stream stream)
        {
            var encoderParameters = new EncoderParameters(1);
            encoderParameters.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 100L);
            bmp.Save(stream, GetEncoder(ImageFormat.Jpeg), encoderParameters);
        }

        public static ImageCodecInfo GetEncoder(ImageFormat format)
        {
            var codecs = ImageCodecInfo.GetImageDecoders();

            foreach (var codec in codecs)
            {
                if (codec.FormatID == format.Guid)
                {
                    return codec;
                }
            }

            // Return 
            return null;
        }
    }

    public class LinkedInOAuthResponse
    {
        [JsonProperty("expires_in")]
        public int expiresIn { get; set; }
        [JsonProperty("access_token")]
        public string accessToken { get; set; }
    }


    public class ExternalServiceController : HODBaseController
    {
        //FIXME: externalize these strings
        string appId = "664233603619644";
        string appSecret = "fded38a2fbcb66e37c566c1ee8349d67";

        string linkedInApiKey = "75cwbazjh8dtc3";
        string linkedInSecretKey = "ClcVkEKPWjU8Cklg";
        string linkedInOauthUserToken = "5ca76a8d-4aad-43df-a934-88b2ee9dc71d";
        string linkedInOauthUserSecret = "3c4a3e3e-6f4e-4a56-9612-75ef26d7c2df";

        string postLoginUrl = System.Configuration.ConfigurationManager.AppSettings["applicationRoot"] + "ExternalService/uploadFlyerToSocialMedia"; 
        
        const string linkedInParamKey = "linkedin";
        const string facebookParamKey = "fb";

        public string fileName { get; set; }        
        public string generatedImage { get; set; }

        [HttpGet]
        public ActionResult uploadFlyerToSocialMedia(string id, string sName, string caller){

            const string socialMediaMessageTemplate = "Private Sale: {0}, {1} sq ft, {2} bedroom {3} Bathroom URL {4}";
            string serverMapPath = Server.MapPath("~/ListingImgs");
            string lid = id;

            MessageManagerUtility mmu = new MessageManagerUtility();

            ListingRequest lr;

            string externalServiceSelected = sName;
            string redirectAction = caller;

            if (externalServiceSelected != null){
                Session.Add("sName", sName);
                Session.Add("caller", caller);
            } else {
                externalServiceSelected = Session["sName"].ToString();
                redirectAction = Session["caller"].ToString();
            }

            if (lid.ToLower().Contains("vue")) {
                lr = mmu.ParseRequest(lid);
            }  else {
                lr = new ListingRequest();
                lr.ListingId = int.Parse(lid);
            }

            try {

                postLoginUrl = postLoginUrl + "?id=" + lr.ListingId.ToString() + "&sName=" + Session["sName"].ToString();

                if (Request["code"] == null){

                    string dialogUrl = RetrieveDialogUrl(externalServiceSelected, RetrieveAppId(externalServiceSelected),postLoginUrl);//
                    return Redirect(dialogUrl);

                }  else {

                    ListingManager lm = new ListingManager();
                    Listing theListing = lm.GetListingData(lr.ListingId);

                    string textToPost = string.Format(socialMediaMessageTemplate, theListing.PropertyType, theListing.SquareFootage, theListing.NoOfBedrooms, theListing.NoOfBathrooms, theListing.ClientUrl);

                    Session["sName"] = string.Empty;                                       

                    if (externalServiceSelected == linkedInParamKey){
                        string linkedInTokenUrl = RequestAccessTokenUrl(externalServiceSelected, Request["code"]);
                        string bearerTokenRequestData = SendHttpRequest(linkedInTokenUrl, "POST");
                        LinkedInOAuthResponse theResponse = JsonConvert.DeserializeObject<LinkedInOAuthResponse>(bearerTokenRequestData);
                        string lawnSignImages = generateImage(lr.ListingId, serverMapPath, null);
                        ProcessLinkedInPost("POST", theResponse.accessToken, theListing, textToPost, lawnSignImages);
                    }

                    if (externalServiceSelected == facebookParamKey){
                        string url = "https://graph.facebook.com/oauth/access_token?client_id={0}&redirect_uri={1}&client_secret={2}&code={3}";
                        string fbTokenUrl = SendHttpRequest(string.Format(url, appId, postLoginUrl, appSecret, Request["code"].ToString()), "POST");
                        string graphUrl = string.Format("https://graph.facebook.com/me/photos?{0}", fbTokenUrl);
                        int theId = lr.ListingId;
                        generateImage(theId, serverMapPath, null);

                        postImageToFacebook(generatedImage, graphUrl, textToPost, theListing.ClientUrl);
                    }

                }
            }
            catch (Exception ex)
            {
                this.LogException(ex);
                Response.StatusCode = 500;                                     
            }

            return RedirectToAction(redirectAction, "Listing", routeValues: new { id = lid });

        }

        private void ProcessLinkedInPost(string url, string accessToken,Listing listing, string message, string lawnsignImage)
        {
            url = "https://api.linkedin.com/v1/people/~/shares?oauth2_access_token=" + accessToken;

            var shareMsg = new
            {
                comment = message,
                content = new
                {
                    title = "Selling my house",
                    submitted_url = listing.ClientUrl,
                    submitted_image_url = lawnsignImage
                },
                visibility = new
                {
                    code = "anyone"
                }
            };

            string json = new JavaScriptSerializer().Serialize(shareMsg);
            WebRequest request = WebRequest.Create(url);
            request.Method = "POST";
            request.ContentType ="application/json";

            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {

                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
                request.Credentials = CredentialCache.DefaultCredentials;
                WebResponse response = request.GetResponse();

                Stream dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);

                string responseFromServer = reader.ReadToEnd();

                reader.Close();
                response.Close();
            }
        }

        private string RetrieveAppId(string externalServiceSelected)
        {
            if (externalServiceSelected == facebookParamKey)
            {
                return appId;
            }

            if (externalServiceSelected == "twitter")
            {
                return "";
            }

            if (externalServiceSelected == linkedInParamKey)
            {                
                return linkedInApiKey;
            }

            return "";
        }

        private string RetrieveSecretKey(string externalServiceSelected)
        {
            if (externalServiceSelected == facebookParamKey)
            {
                return  appSecret;
            }

            if (externalServiceSelected == "twitter")
            {
                return "";
            }

            if (externalServiceSelected == linkedInParamKey)
            {
                return linkedInSecretKey;
            }

            return "";

        }

        private string RetrieveDialogUrl(string externalServiceSelected,string clientId, string redirectUrl)
        {
            if (externalServiceSelected == facebookParamKey)
            {
                return string.Format("http://www.facebook.com/dialog/oauth?client_id={0}&redirect_uri={1}&scope=publish_stream,user_photos", clientId,redirectUrl);
            }

            if (externalServiceSelected == "twitter"){
                return "";
            }

            if (externalServiceSelected == linkedInParamKey)
            {
                string randomStringRoot = Guid.NewGuid().ToString();
                string randomString = randomStringRoot.Substring(0, 6);
                return string.Format("https://www.linkedin.com/uas/oauth2/authorization?response_type=code&client_id={0}&state={1}XHDAD65ASD5DSads78D&redirect_uri={2}&scope=r_fullprofile%20r_emailaddress%20r_network%20rw_nus", clientId,randomString, redirectUrl);
                
            }

            return "";
        }

        private string RequestAccessTokenUrl(string serviceName, string code){

            const string linkedInAccessTokenUrl = "https://www.linkedin.com/uas/oauth2/accessToken?grant_type=authorization_code&code={0}&redirect_uri={1}&client_id={2}&client_secret={3}";
            const string facebookAccessTokenUrl = "https://graph.facebook.com/oauth/access_token?client_id={0}&redirect_uri={1}&client_secret={2}&code={3}";

            string accessTokenUrlWithParams = string.Empty;

            if (serviceName == linkedInParamKey)
                accessTokenUrlWithParams = string.Format(linkedInAccessTokenUrl, code, postLoginUrl, RetrieveAppId(serviceName), RetrieveSecretKey(serviceName));

            return accessTokenUrlWithParams;
        }

        public string SendHttpRequest(string url, string method)
        {
            WebRequest request = WebRequest.Create(url);

            request.Method = method;
            request.Credentials = CredentialCache.DefaultCredentials;
            WebResponse response = request.GetResponse();

            Stream dataStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);

            string responseFromServer = reader.ReadToEnd();

            reader.Close();
            response.Close();

            StringReader sr = new StringReader(responseFromServer);
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            return responseFromServer;
        }

        public string generateImage(int listingId, string serverMapPath, string pFileName){

            string imageUrl = System.Configuration.ConfigurationManager.AppSettings["applicationRoot"] + "Listing/Poster/" + listingId.ToString();
            string fileName = string.Empty;

            if (pFileName == null){
                fileName = Guid.NewGuid().ToString() + ".jpg";
                this.fileName = fileName;
            }
            else {
                fileName = pFileName;
                this.fileName = fileName;
            }

            generatedImage =  Path.Combine(serverMapPath, Path.GetFileName(fileName));           
            WebsiteToImage webToImage = new WebsiteToImage(imageUrl, generatedImage);
            try
            {
                Bitmap generatedBMP = webToImage.Generate();
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
            }

            return fileName;
        }

        public void postImageToFacebook(string generatedImage, string fbGraphUrl, string message, string link)
        {
            WebClient wc = new WebClient();
            byte[] bytes = System.IO.File.ReadAllBytes(generatedImage);

            wc.QueryString.Add("source", generatedImage);

         //   byte[] returnedBytes = wc.UploadFile(fbGraphUrl, generatedImage);

            using (var stream3 = System.IO.File.Open(generatedImage, FileMode.Open))
                {
                    var files = new[] 
                    {
                        new UploadFile
                        {
                            Name = "file",
                            Filename = this.fileName,
                            ContentType = "image/jpeg",
                            Stream = stream3
                        },
                    };

                    var values = new NameValueCollection
                    {
                        { "message", message},
                        { "caption", "My house is posted on CasaVue"}
                    };

                    byte[] result = UploadFiles(fbGraphUrl, files, values);
                }            

   
        }


        public class WebsiteToImage
        {
            private Bitmap m_Bitmap;
            private string m_Url;
            private string m_FileName = string.Empty;
            private System.Object lockThis = new System.Object();

            public WebsiteToImage(string url)
            {
                // Without file 
                m_Url = url;
            }

            public WebsiteToImage(string url, string fileName)
            {
                // With file 
                m_Url = url;
                m_FileName = fileName;
            }

            public Bitmap Generate()
            {
                // Thread 

                lock (lockThis)
                {
                    var m_thread = new Thread(_Generate);
                    m_thread.SetApartmentState(ApartmentState.STA);
                    m_thread.Start();
                    m_thread.Join();
                }
                return m_Bitmap;
            }

            private void _Generate()
            {
               // var browser = new WebBrowser { ScrollBarsEnabled = false };
               // browser.ScriptErrorsSuppressed = false;

               //// browser.Scale(new SizeF(300,300));
               // browser.Navigate(m_Url);
               // browser.Refresh(WebBrowserRefreshOption.Completely);
               // browser.DocumentCompleted += WebBrowser_DocumentCompleted;

               // while (browser.ReadyState != WebBrowserReadyState.Complete)
               // {
               //     Application.DoEvents();
               // }

               // if (browser.ReadyState == WebBrowserReadyState.Complete)
               // {
               //     //browser.ClientSize = new Size(browser.Document.Body.ScrollRectangle.Width, browser.Document.Body.ScrollRectangle.Bottom);
               //     browser.ClientSize = new Size(browser.Document.Body.ScrollRectangle.Width, browser.Document.Body.ScrollRectangle.Bottom);
               //     browser.ScrollBarsEnabled = false;
               //     //m_Bitmap = new Bitmap(browser.Document.Body.ScrollRectangle.Width, browser.Document.Body.ScrollRectangle.Bottom);
               //     m_Bitmap = new Bitmap(browser.Document.Body.ScrollRectangle.Width, browser.Document.Body.ScrollRectangle.Bottom);

               //     browser.BringToFront();
               //     browser.DrawToBitmap(m_Bitmap, browser.Bounds);

               //     // Save as file? 
               //     if (m_FileName.Length > 0)
               //     {
               //         // Save 
               //         m_Bitmap.SaveJPG100(m_FileName);
               //     }
               // }

               // browser.Dispose();

                }

            private void WebBrowser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
            {
/*                // Capture 
                var browser = (WebBrowser)sender;
                //browser.ClientSize = new Size(browser.Document.Body.ScrollRectangle.Width, browser.Document.Body.ScrollRectangle.Bottom);
                browser.ClientSize = new Size(960, 1024);
                browser.ScrollBarsEnabled = false;
                //m_Bitmap = new Bitmap(browser.Document.Body.ScrollRectangle.Width, browser.Document.Body.ScrollRectangle.Bottom);
                m_Bitmap = new Bitmap( 960,1024);
                browser.BringToFront();
                browser.DrawToBitmap(m_Bitmap, browser.Bounds);

                // Save as file? 
                if (m_FileName.Length > 0)
                {
                    // Save 
                    m_Bitmap.SaveJPG100(m_FileName);
                }*/
            }
        }

        public byte[] UploadFiles(string address, IEnumerable<UploadFile> files, NameValueCollection values)
        {
            var request = WebRequest.Create(address);
            request.Method = "POST";
            var boundary = "---------------------------" + DateTime.Now.Ticks.ToString("x", System.Globalization.NumberFormatInfo.InvariantInfo);
            request.ContentType = "multipart/form-data; boundary=" + boundary;
            boundary = "--" + boundary;

            using (var requestStream = request.GetRequestStream())
            {
                // Write the values
                foreach (string name in values.Keys)
                {
                    var buffer = Encoding.ASCII.GetBytes(boundary + Environment.NewLine);
                    requestStream.Write(buffer, 0, buffer.Length);
                    buffer = Encoding.ASCII.GetBytes(string.Format("Content-Disposition: form-data; name=\"{0}\"{1}{1}", name, Environment.NewLine));
                    requestStream.Write(buffer, 0, buffer.Length);
                    buffer = Encoding.UTF8.GetBytes(values[name] + Environment.NewLine);
                    requestStream.Write(buffer, 0, buffer.Length);
                }

                // Write the files
                foreach (var file in files)
                {
                    var buffer = Encoding.ASCII.GetBytes(boundary + Environment.NewLine);
                    requestStream.Write(buffer, 0, buffer.Length);
                    buffer = Encoding.UTF8.GetBytes(string.Format("Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}\"{2}", file.Name, file.Filename, Environment.NewLine));
                    requestStream.Write(buffer, 0, buffer.Length);
                    buffer = Encoding.ASCII.GetBytes(string.Format("Content-Type: {0}{1}{1}", file.ContentType, Environment.NewLine));
                    requestStream.Write(buffer, 0, buffer.Length);
                    file.Stream.CopyTo(requestStream);
                    buffer = Encoding.ASCII.GetBytes(Environment.NewLine);
                    requestStream.Write(buffer, 0, buffer.Length);
                }

                var boundaryBuffer = Encoding.ASCII.GetBytes(boundary + "--");
                requestStream.Write(boundaryBuffer, 0, boundaryBuffer.Length);
            }

            using (var response = request.GetResponse())
            using (var responseStream = response.GetResponseStream())
            using (var stream = new MemoryStream())
            {
                responseStream.CopyTo(stream);
                return stream.ToArray();
            }
        }    
        public class UploadFile
        {
            public UploadFile()
            {
                ContentType = "application/octet-stream";
            }
            public string Name { get; set; }
            public string Filename { get; set; }
            public string ContentType { get; set; }
            public Stream Stream { get; set; }
        }
        public class AmazonVideoUploader
        {
            public string UploadFile(HttpPostedFileBase currentFile)
            {
                string guid = Guid.NewGuid().ToString();
                string fileName = guid + currentFile.FileName;
                try
                {
                    string bucketName = ApplicationUtility.AWSBucketName;// AWSVideoStoreName;
                    IAmazonS3 client;

                    using (client = Amazon.AWSClientFactory.CreateAmazonS3Client(RegionEndpoint.USEast1))
                    {
                        Amazon.S3.Model.PutObjectRequest putObj = new Amazon.S3.Model.PutObjectRequest()
                        {
                            BucketName = bucketName,
                            InputStream = currentFile.InputStream,
                            Key = fileName
                        };
                        putObj.CannedACL = S3CannedACL.PublicRead;
                        Amazon.S3.Model.PutObjectResponse putResponse = client.PutObject(putObj);
                    }
                }
                catch (AmazonS3Exception ae)
                {
                    string logFile = "/App_Data/ErrorLog.txt";
                    logFile = System.Web.HttpContext.Current.Server.MapPath(logFile);

                    // Open the log file for append and write the log
                    StreamWriter sw = new StreamWriter(logFile, true);
                    sw.WriteLine("********** {0} **********", DateTime.Now);
                    sw.Write("Execption: {0}", ae.ToString());
                    sw.WriteLine("Request ID: {0}", ae.RequestId);
                    sw.Close();
                }
                return fileName;
            }

          /*  public string UploadFile(string currentFilePath, string fileName, bool diff)
            {
                try
                {
                    string bucketName = ApplicationUtility.AWSBucketName;// AWSVideoStoreName;
                    IAmazonS3 client;

                    string guid = Guid.NewGuid().ToString();
                    string awsAccessKey = ApplicationUtility.AWSAccessKey;
                    string awsSecret = ApplicationUtility.AWSSecretKey;

                    using (client = Amazon.AWSClientFactory.CreateAmazonS3Client(awsAccessKey, awsSecret, RegionEndpoint.USEast1))
                    {
                        Amazon.S3.Model.PutObjectRequest putObj = new Amazon.S3.Model.PutObjectRequest()
                        {
                            BucketName = bucketName,
                            FilePath = currentFilePath,
                            Key = fileName
                        };
                        putObj.CannedACL = S3CannedACL.PublicRead;
                        Amazon.S3.Model.PutObjectResponse putResponse = client.PutObject(putObj);

                    }
                }
                catch (AmazonS3Exception ae)
                {
                    string logFile = "/App_Data/ErrorLog.txt";
                    logFile = System.Web.HttpContext.Current.Server.MapPath(logFile);

                    // Open the log file for append and write the log
                    StreamWriter sw = new StreamWriter(logFile, true);
                    sw.WriteLine("********** {0} **********", DateTime.Now);
                    sw.Write("Execption: {0}", ae.ToString());
                    sw.WriteLine("Request ID: {0}", ae.RequestId);
                    sw.Close();
                }

                return fileName;
            }*/


            public string UploadFile(string currentFilePath, String Name, bool uploadAsync = false)
            {

                string fileName = Name;

                try
                {
                    string bucketName = ApplicationUtility.AWSBucketName;//AWSVideoStoreName;
                    string awsAccessKey = ApplicationUtility.AWSAccessKey;
                    string awsSecret = ApplicationUtility.AWSSecretKey;

                    //Amazon.S3.Transfer.TransferUtility _TransferUtility = new Amazon.S3.Transfer.TransferUtility(Amazon.AWSClientFactory.CreateAmazonS3Client(awsAccessKey, awsSecret, RegionEndpoint.USEast1));
                    Amazon.S3.Transfer.TransferUtility _TransferUtility = new Amazon.S3.Transfer.TransferUtility(Amazon.AWSClientFactory.CreateAmazonS3Client(RegionEndpoint.USEast1));

                    Amazon.S3.Transfer.TransferUtilityUploadRequest fileTransferUtilityRequest = new Amazon.S3.Transfer.TransferUtilityUploadRequest
                    {
                        BucketName = bucketName,
                        FilePath = currentFilePath,
                        StorageClass = S3StorageClass.ReducedRedundancy,
                        PartSize = 6291456, // 6 MB.
                        Key = fileName,
                        CannedACL = S3CannedACL.PublicRead
                    };

                    if (uploadAsync){
                        _TransferUtility.UploadAsync(fileTransferUtilityRequest);
                    } else {
                        _TransferUtility.Upload(fileTransferUtilityRequest);
                    }

                    //System.IO.File.Delete(currentFilePath);
                }

                catch(AmazonS3Exception ae) {
                    string logFile = "/App_Data/ErrorLog.txt";
                    logFile = System.Web.HttpContext.Current.Server.MapPath(logFile);                   

                    // Open the log file for append and write the log
                    StreamWriter sw = new StreamWriter(logFile, true);
                    sw.WriteLine("********** {0} **********", DateTime.Now);
                    sw.Write("Execption: {0}", ae.ToString());
                    sw.WriteLine("Request ID: {0}", ae.RequestId);
                    sw.Close();

                }

                return fileName;
            }

            public bool DeleteFile(string currentFile)
            {
                bool success=false;
                try
                {
                    string bucketName = ApplicationUtility.AWSBucketName;
                    IAmazonS3 client;


                    using (client = Amazon.AWSClientFactory.CreateAmazonS3Client(RegionEndpoint.USEast1))
                    {
                        Amazon.S3.Model.DeleteObjectRequest delObj = new Amazon.S3.Model.DeleteObjectRequest()
                        {
                            BucketName = bucketName,
                            Key = currentFile,
                        };

                        Amazon.S3.Model.DeleteObjectResponse delResponse = client.DeleteObject(delObj);

                    }
                }
                catch (AmazonS3Exception ae)
                {
                    string logFile = "/App_Data/ErrorLog.txt";
                    logFile = System.Web.HttpContext.Current.Server.MapPath(logFile);

                    // Open the log file for append and write the log
                    StreamWriter sw = new StreamWriter(logFile, true);
                    sw.WriteLine("********** {0} **********", DateTime.Now);
                    sw.Write("Execption: {0}", ae.ToString());
                    sw.WriteLine("Request ID: {0}", ae.RequestId);
                    sw.Close();
                }
                return success;
            }
        }
    }
}
