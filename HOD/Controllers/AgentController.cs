﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HOD.Data.Entities;
using System.Transactions;
using System.IO;
using HOD.Managers.Utilities;
using HOD.Managers;
using WebMatrix.WebData;

namespace HOD.Controllers
{   //[Authorize]

    public enum PageTypes{

        AgentHome = 1,
        AboutUs = 2,
        YourOtherPage1 = 3,
        YourOtherPage2 = 4
    }

    public enum PageItemTypesEnum{
        MainTextContent = 1, 
        ProfilePhoto = 2,
        CompanyLogo = 3,
        BackgroundTop = 4
    }

    public class AgentController : HODBaseController
    {
        //
        // GET: /Agent/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Redirect()
        {
            string uRL = Request.UrlReferrer.Host;

            // Get Agent ID by URL
            IDbAgent DB = new DbManager();
            int agentID = DB.GetAgentIDByURL(uRL);
            Session.Add("AGENTID", agentID);

            if (Request.UrlReferrer.Segments.Count() > 1)     {
               //There is a vue code @ the end
                if (Request.UrlReferrer.Segments[1].ToUpper().Contains("VUE"))   {
                    return RedirectToAction("ViewFullListing", "Listing", new { id = Request.UrlReferrer.Segments[1] });
                }
            }                

            return RedirectToAction("AgentDisplay","Agent", new {AgentId =  agentID});
        }

        [Authorize()]
        public ActionResult RegCustomer()
        {
            Session["PayOnUserBehalf"]  = true;
            Session["AgentContext"] = true;
            Session["AGENTID"] = WebSecurity.CurrentUserId.ToString();

            return RedirectToAction("Register", "Account", new { UserType = "Seller", RedirectUrl = String.Empty });
        }

        public ActionResult SignUp()
        {
            Session["PayOnUserBehalf"] = false;
            Session["AgentContext"] = false;
            return RedirectToAction("Register", "Account", new { UserType = "Seller", RedirectUrl = String.Empty });
        }

        [Authorize()]
        public ActionResult ViewListing(int listingId)
        {
            Session["AgentContext"] = true;

            using (var db = new HODContext())
            {
                var userListing = (from listing in db.Listings
                                     join profiles in db.UserProfiles on listing.UserId equals profiles.UserId
                                     where listing.ListingId == listingId
                                      select listing).FirstOrDefault();
                Session["ListingUserId"] = userListing.UserId;
                Session["currListingId"] = userListing.ListingId;
            }
            
            return RedirectToAction("ListingTemplate", "Listing", new { id = listingId.ToString()});
        }

        public ActionResult AgentDisplay(int agentId)
        {
            if (agentId == 0){
                agentId = int.Parse(Session["AGENTID"].ToString());
            } else {
                Session.Add("AGENTID", agentId);
            }


            return View(loadAgentModelData(agentId));
        }

        public ActionResult AboutUs(int agentId)
        {
            return View(loadAgentModelData(agentId));
        }

        public ActionResult OtherPage1(int agentId)
        {
            return View(loadAgentModelData(agentId));
        }

        public ActionResult OtherPage2(int agentId)
        {
            return View(loadAgentModelData(agentId));
        }

        public ActionResult ContactUs(int agentId)
        {
            return View(loadAgentModelData(agentId));
        }


        private HOD.Models.AgentDisplayModel loadAgentModelData(int agentId)
        {
            HOD.Models.AgentDisplayModel model = new HOD.Models.AgentDisplayModel();
            
            //Retrieve resources related to this agent
            using (var db = new HODContext())
            {
                AgentInformation agentInfo = db.AgentInformations.Where(x => x.AgentId == agentId).FirstOrDefault();

                if (agentInfo == null)
                {
                    return new Models.AgentDisplayModel();
                }
                
                model.AgentTopBackground = ApplicationUtility.ImageStoreURL + agentInfo.BackgroundPhoto;
                model.AgentLogo = ApplicationUtility.ImageStoreURL + agentInfo.Logo;
                model.AboutMe = agentInfo.AboutUsContent;
                model.HomePageContent = agentInfo.HomePageContent;
                model.OtherPage1Label = agentInfo.OtherPage1Title;
                model.OtherPage2Label = agentInfo.OtherPage2Title;
                model.OtherPage1Content = agentInfo.OtherPage1Content;
                model.OtherPage2Content = agentInfo.OtherPage2Content;

                model.AgentProfilePic = ApplicationUtility.ImageStoreURL + agentInfo.ProfilePhoto;

                model.AgentId = agentInfo.AgentId;

                model.BusinessName = agentInfo.BusinessName;
                model.BusinessAddress = agentInfo.BusinessAddress;
                model.BusinessCity =  agentInfo.BusinessCity;
                model.PostalCode = agentInfo.PostalCode;
                model.Province =  agentInfo.Province;
                model.Phone1 =  agentInfo.Phone1;
                model.Phone2 = agentInfo.Phone2;

                var agentListings = (from listing in db.Listings
                                     join profiles in db.UserProfiles on listing.UserId equals profiles.UserId
                                     where profiles.AgentID == agentId
                                     select listing).ToList<Listing>();

                var agentUserProfile = (from userProfile in db.UserProfiles
                                        where userProfile.UserId == agentId
                                        select userProfile).FirstOrDefault<UserProfile>();

                List<HOD.Models.ListingImageMetaData> imagesData = new List<Models.ListingImageMetaData>();
                model.listingImages = imagesData;

                List<ListingImage> listingImages;

                foreach (var listing in agentListings)
                {
                    string lawnSignFileNameTemplate = "VUE{0}Lawnsign.jpg";
                    string lawnSignFileName = string.Format(lawnSignFileNameTemplate, listing.ListingId);
                    string imgLoc = string.Format("{0}{1}", System.Configuration.ConfigurationManager.AppSettings["AWSS3BucketURL"], lawnSignFileName);
                    string listingImgUrl = string.Empty;

                    listingImages  = listing.ListingImages.ToList<ListingImage>();
                    model.listingImages.Add(new Models.ListingImageMetaData(){
                         PublicLawnSignImageUrl = string.Format(imgLoc, lawnSignFileName),
                         //ListingImageUrl = listing.ListingImages[0].Image,
                         ListingImageUrl = ProcessListingImages(listingImages),
                         Category = listing.ListingCategory,
                         Address = listing.Address,
                         City = listing.City,
                         Province = listing.Province,
                         Neighbourhood = listing.Neighbourhood,
                         NoOfBathrooms = listing.NoOfBathrooms.Value,
                         NoOfBedrooms = listing.NoOfBedrooms.Value,
                         SquareFootage = listing.SquareFootage.Value,
                         CientUrl = listing.clientUrl,
                         ListPrice = listing.ListPrice.Value,
                         MainSellingDesc = listing.MainSellingDesc,
                         ListingId = listing.ListingId.ToString(),
                         EmailAddress = agentUserProfile.UserName
                         //Category = listingCat
                    });
                }

                Session.Add("AGENTID", agentId);
                ViewBag.AgentEmail = agentUserProfile.UserName;
                if (agentInfo.OtherAgentPhoto10 == null)
                {
                    ViewBag.AgentImage = "#";
                }
                else
                {
                    ViewBag.AgentImage = System.Configuration.ConfigurationManager.AppSettings["AWSS3BucketURL"] + agentInfo.OtherAgentPhoto10;
                }
            }

            return model;
        }

        private string ProcessListingImages(List<ListingImage> images)
        {
            string retVal;
            if (images[0] == null)
            {
                retVal = "#";
            }
            else
            {
               retVal =  System.Configuration.ConfigurationManager.AppSettings["AWSS3BucketURL"] + images[0].Image;
            }

            return retVal;
        }

        [Authorize()]
        [HttpPost]
        public JsonResult UpdateListingCat(string listingId, string categoryVal)
        {

    
           int intListingId = 0;

           if (!int.TryParse(listingId, out intListingId)){
               Response.StatusCode = 400;
               return new JsonResult();
           }

            using (var db = new HODContext())
            {
                Listing listing =  db.Listings.Where(x => x.ListingId == intListingId).First();

                if (db.PageCategories.Where(x => x.Code == categoryVal).Count() > 0)   {
                    listing.ListingCategory = categoryVal;
                }

                db.SaveChanges();
            }

            Response.StatusCode = 200;

            return Json(new object());
        }

        [Authorize]
        public ActionResult CategorizeListing()
        {
            int currentUserId = WebMatrix.WebData.WebSecurity.CurrentUserId;
            HOD.Models.AgentDisplayModel model = loadAgentModelData(currentUserId);
            return View(model);
        }

        [Authorize()]
        public ActionResult Pages()
        {

            int currentUserId = WebMatrix.WebData.WebSecurity.CurrentUserId;

            HOD.Models.AgentSetupModel agentModel;

            AgentInformation agentInfo;
            UserProfile profile;
            using (var db = new HODContext())
            {
                agentInfo = db.AgentInformations.Where(x => x.AgentId == currentUserId).FirstOrDefault();
                profile = db.UserProfiles.Where(x => x.UserId == currentUserId).FirstOrDefault();
            }

            Session.Add("AgentName", profile.FirstName + ' ' + profile.LastName);
            Session.Add("TIER", profile.Tier);
            Session.Add("AGENTID", WebMatrix.WebData.WebSecurity.CurrentUserId);

            if (agentInfo != null)
            {
                agentModel = new Models.AgentSetupModel(){
                    BusinessName = agentInfo.BusinessName,
                    Address = agentInfo.BusinessAddress,
                    City = agentInfo.BusinessCity,
                    Province = agentInfo.BusinessCity,
                    PostalCode = agentInfo.PostalCode,
                    AgentFullName = profile.FirstName + ' ' + profile.LastName,
                    PhoneNumberCell = agentInfo.Phone1,
                    PhoneNumberLandLine = agentInfo.Phone2,
                    AgentUrl = agentInfo.AgentUrl,
                    YourAboutUsContent = agentInfo.AboutUsContent,
                    YourHomeContent = agentInfo.HomePageContent,
                    OtherPage1Text = agentInfo.OtherPage1Content,
                    OtherPage2Text = agentInfo.OtherPage2Content,
                    OtherPage1Label = agentInfo.OtherPage1Title,
                    OtherPage2Label = agentInfo.OtherPage2Title,
                    OtherAgentPhoto1 = GetDisplayImage(agentInfo.OtherAgentPhoto1),
                    OtherAgentPhoto2 = GetDisplayImage(agentInfo.OtherAgentPhoto2),
                    OtherAgentPhoto3 = GetDisplayImage(agentInfo.OtherAgentPhoto3),
                    OtherAgentPhoto4 = GetDisplayImage(agentInfo.OtherAgentPhoto4),
                    OtherAgentPhoto5 = GetDisplayImage(agentInfo.OtherAgentPhoto5),
                    OtherAgentPhoto6 = GetDisplayImage(agentInfo.OtherAgentPhoto6),
                    OtherAgentPhoto7 = GetDisplayImage(agentInfo.OtherAgentPhoto7),
                    OtherAgentPhoto8 = GetDisplayImage(agentInfo.OtherAgentPhoto8),
                    OtherAgentPhoto9 = GetDisplayImage(agentInfo.OtherAgentPhoto9),
                    OtherAgentPhoto10 = GetDisplayImage(agentInfo.OtherAgentPhoto10),
                    AgentLogo =  GetDisplayImage(agentInfo.Logo).ToLower() == "blank-icon.jpg" ? "blank-logo-icon.png" : GetDisplayImage(agentInfo.Logo),
                    AgentBottomBackground = GetDisplayImage(agentInfo.BackgroundPhoto),
                    AgentTopBackground = GetDisplayImage(agentInfo.BackgroundPhoto),
                    MainWebsitePhoto = GetDisplayImage(agentInfo.MainWebsitePhoto),
                    ProfileAgentLogoDisplay = GetDisplayImage(agentInfo.Logo),
                    ProfilePic1 = GetDisplayImage(agentInfo.ProfilePhoto),
                    profileWebsiteTopImage= GetDisplayImage(agentInfo.MainWebsitePhoto)                   
                };

                if (profile.Tier.ToUpper() == "FREE")
                {
                    agentModel.AgentTopBackground = this.ConfigurationSettingValue("ApexBackground");
                    agentModel.MainWebsitePhoto = this.ConfigurationSettingValue("ApexMainWebsiteImage");
                }
            }
            else {
                agentModel = new Models.AgentSetupModel(){
                    Address = profile.Address,
                    City = profile.City,
                    Province = profile.Province,
                    PostalCode = profile.PostalCode,
                    AgentFullName = profile.FirstName + ' ' + profile.LastName,
                    PhoneNumberCell = profile.PhoneNumberCell,
                    PhoneNumberLandLine = profile.PhoneNumberLandLine,
                    AgentUrl = profile.AgentURL,
                    BusinessName = profile.BusinessName,
                    AgentTopBackground =  this.ConfigurationSettingValue("ApexBackground"),
                    MainWebsitePhoto = this.ConfigurationSettingValue("ApexMainWebsiteImage"),
                    AgentLogo = string.Format("{0}{1}", this.ConfigurationSettingValue(this.ImageBucketLocationKeyName), this.BlankLogoIconKeyName),
                    ProfileAgentLogoDisplay = string.Format("{0}{1}", this.ConfigurationSettingValue(this.ImageBucketLocationKeyName), this.BlankLogoIconKeyName)
                };
            }
            //this.

            return View(agentModel);              
        }

        private string GetDisplayImage(string image)
        {
            string noImage = "/imgs/blank-icon.jpg";

            if (image == null || image.Contains("blank-icon.jpg")) {
                return noImage;
            } else {
                return this.ConfigurationSettingValue("AWSS3BucketURL") + image;
            }

        }

        [Authorize()]
        public ActionResult AgentPortal()
        {
            int agentId = 0;

            string applicationRoot = this.ConfigurationSettingValue("applicationRoot");
            string redirectUrl = String.Format("{0}{1}", applicationRoot, "Agent/AgentPortal");

            if (!WebMatrix.WebData.WebSecurity.Initialized){
                return RedirectToAction("Login", "Account", new {@RedirectUrl = redirectUrl });
            }
                
            agentId = WebMatrix.WebData.WebSecurity.CurrentUserId;

            using (var db = new HODContext())
            {
                AgentInformation agentData = db.AgentInformations.Where(x => x.AgentId == agentId).FirstOrDefault();
                UserProfile userData = db.UserProfiles.Where(x => x.UserId == agentId).FirstOrDefault();

                string awsS3BucketUrl = this.ConfigurationSettingValue("AWSS3BucketURL");

                if (agentData != null){
                    TempData["AgentImage"] = awsS3BucketUrl + agentData.MainWebsitePhoto;

                    if (userData.Tier.ToUpper() == "FREE"){
                        TempData["AgentBackground"] = "http://casavue-image-bucket.s3-website-us-east-1.amazonaws.com/bgTopNew.jpg";
                        TempData["AgentName"] = string.Format("{0}", userData.FirstName);// userData.LastName);
                        TempData["AgentImage"] = "http://casavue-image-bucket.s3-website-us-east-1.amazonaws.com/AP_logo.png";

                    } else {
                        TempData["AgentBackground"] = awsS3BucketUrl + agentData.BackgroundPhoto;
                        TempData["AgentName"] = string.Format("{0}", userData.FirstName);// userData.LastName);
                    }

                }
                else
                {
                    TempData["AgentImage"] = "http://casavue-image-bucket.s3-website-us-east-1.amazonaws.com/AP_logo.png";
                    TempData["AgentBackground"] =  "http://casavue-image-bucket.s3-website-us-east-1.amazonaws.com/bgTopNew.jpg";
                    TempData["AgentName"] = string.Format("{0}", userData.FirstName);// userData.LastName);
                }
            }

            Session.Add("AGENTID", WebMatrix.WebData.WebSecurity.CurrentUserId);
            return View();
        }

        [Authorize()]
        public ActionResult ContentPage(int pageTypeId)
        {
                return View();
        }


        [Authorize()]
        [HttpPost]
        public ActionResult ContentPageSave(HOD.Models.AgentSetupModel pageModel)
        {
            return RedirectToAction("AgentPortal", "Agent");
        }

        [Authorize()]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AgentSetup( HOD.Models.AgentSetupModel agentModel)
        {

            List<PageItem> profilePicItems = new List<PageItem>();
            int currentUserId = WebMatrix.WebData.WebSecurity.CurrentUserId;

            string agentName  = string.Empty;

            if (Session["AgentName"] != null){
                agentName = Session["AgentName"].ToString();
            }

            string profilePic1 = DetermineImageName(Request.Files["uplProfilePic1"], agentModel.ProfilePic1, "PPic_" + agentName);
            string mainWebsitePic = DetermineImageName(Request.Files["uplMainWebsitePic"], agentModel.MainWebsitePhoto, "MnWebPic_" + agentName);
            string companyLogo = DetermineImageName(Request.Files["uplCompanyLogo"], agentModel.ProfileAgentLogoDisplay, "CompLogo_" + agentName);
            string agentTopBackground = DetermineImageName(Request.Files["uplAgentTopBackground"], agentModel.AgentTopBackground, "AgntTop_" + agentName);
            string agentOtherPhoto1 = DetermineImageName(Request.Files["uplAgentOtherPhoto1"], agentModel.OtherAgentPhoto1, agentName);
            string agentOtherPhoto2 = DetermineImageName(Request.Files["uplAgentOtherPhoto2"], agentModel.OtherAgentPhoto2, agentName);
            string agentOtherPhoto3 = DetermineImageName(Request.Files["uplAgentOtherPhoto3"], agentModel.OtherAgentPhoto3, agentName);
            string agentOtherPhoto4 = DetermineImageName(Request.Files["uplAgentOtherPhoto4"], agentModel.OtherAgentPhoto4, agentName);
            string agentOtherPhoto5 = DetermineImageName(Request.Files["uplAgentOtherPhoto5"], agentModel.OtherAgentPhoto5, agentName);
            string agentOtherPhoto6 = DetermineImageName(Request.Files["uplAgentOtherPhoto6"], agentModel.OtherAgentPhoto6, agentName);
            string agentOtherPhoto7 = DetermineImageName(Request.Files["uplAgentOtherPhoto7"], agentModel.OtherAgentPhoto7, agentName);
            string agentOtherPhoto8 = DetermineImageName(Request.Files["uplAgentOtherPhoto8"], agentModel.OtherAgentPhoto8, agentName);
            string agentOtherPhoto9 = DetermineImageName(Request.Files["uplAgentOtherPhoto9"], agentModel.OtherAgentPhoto9, agentName);
            string agentOtherPhoto10 = DetermineImageName(Request.Files["uplAgentOtherPhoto10"], agentModel.OtherAgentPhoto10, "RoundPic_" + agentName);

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {

                    saveListingCategoryOrder(agentModel.listingOrderCategoryTop, agentModel.listingOrderCategoryMiddle, agentModel.listingOrderCategoryBottom, currentUserId);
                    saveAgentInfo(agentModel.BusinessName, agentModel.Address, agentModel.City, agentModel.Province, agentModel.PostalCode, agentModel.PhoneNumberCell,
                        agentModel.PhoneNumberLandLine, agentModel.AgentUrl, "Home Page", agentModel.YourHomeContent, "About Us", agentModel.YourAboutUsContent, agentModel.OtherPage1Label,
                        agentModel.OtherPage1Text, agentModel.OtherPage2Label, agentModel.OtherPage2Text,
                        profilePic1, agentTopBackground, mainWebsitePic, companyLogo, agentOtherPhoto1, agentOtherPhoto2, agentOtherPhoto3, agentOtherPhoto4, 
                        agentOtherPhoto5, agentOtherPhoto6,
                        agentOtherPhoto7, agentOtherPhoto8,   agentOtherPhoto9, agentOtherPhoto10);
                }
                catch (TransactionException ex)
                {
                    LogError(ex);
                    ViewBag.ErrorDisplay = "An error occurred while saving your account";
                    return View();
                }
                catch (Exception ex2)
                {
                    LogError(ex2);
                    ViewBag.ErrorDisplay = "An error occurred while saving your account"; 
                    return View();
                }
                scope.Complete();
            }
            Session.Add("AGENTID", currentUserId);
            return RedirectToAction("AgentPortal", "Agent");
        }

        private string DetermineImageName(HttpPostedFileBase uploadedFile, string currentImageName, string agentName)
        {
            if (uploadedFile == null){
                return Path.GetFileName(currentImageName);
            }

            if (string.IsNullOrEmpty(uploadedFile.FileName)){
                return Path.GetFileName(currentImageName);
            }

           string imageName = SaveImage(uploadedFile, agentName);
           
           return imageName;
        }

        private string SaveImage(HttpPostedFileBase file, string fileNamePrefix)
        {
            string fileLocation = Server.MapPath("~/AgentImageResources/");
            string fileUid = Guid.NewGuid().ToString().Substring(0,5);
            string fileName = fileNamePrefix + "_" + fileUid + "_" + file.FileName;
            string fullFilePath = Path.Combine(fileLocation, fileNamePrefix + "_" + fileUid + "_" + file.FileName);
            file.SaveAs(fullFilePath);

            ExternalServiceController.AmazonVideoUploader imageUploader = new ExternalServiceController.AmazonVideoUploader();
            imageUploader.UploadFile(fullFilePath, fileName);

            return fileName;
        }

        private void saveAgentInfo(string businessName, string address, string city, string province, string postalCode, string phoneNumberMobile, string phoneNumberLandline, 
            string agentUrl, string homePageTitle, string homePageContent, string aboutUsTitle, string aboutUsContent, string otherPage1Title,  string otherPage1Content, 
            string otherPage2Title,  string otherPage2Content, string profilePhoto, string backgroundPhoto, string mainWebsitePhoto, string logo,
            string otherAgentPhoto1,string otherAgentPhoto2,string otherAgentPhoto3,string otherAgentPhoto4,string otherAgentPhoto5,string otherAgentPhoto6,
            string otherAgentPhoto7,string otherAgentPhoto8,string otherAgentPhoto9,string otherAgentPhoto10){

            using (var db = new HODContext())
            {
                AgentInformation agentInfo = new AgentInformation()
                {
                    AgentId = WebMatrix.WebData.WebSecurity.CurrentUserId,
                    BusinessName = businessName,
                    BusinessAddress = address,
                    BusinessCity = city, 
                    Province = province,
                    PostalCode = postalCode,
                    Phone1 = phoneNumberMobile,
                    Phone2 = phoneNumberLandline,
                    AgentUrl = agentUrl,
                    OtherPage1Title = otherPage1Title,
                    OtherPage1Content = otherPage1Content,
                    OtherPage2Title = otherPage2Title,
                    OtherPage2Content = otherPage2Content,
                    AboutUsContent = aboutUsContent,
                    HomePageContent = homePageContent,
                    BackgroundPhoto = backgroundPhoto,
                    Logo = logo,
                    MainWebsitePhoto = mainWebsitePhoto,
                    ProfilePhoto = profilePhoto,
                    OtherAgentPhoto1 = otherAgentPhoto1,
                    OtherAgentPhoto2 = otherAgentPhoto2,
                    OtherAgentPhoto3 = otherAgentPhoto3,
                    OtherAgentPhoto4 = otherAgentPhoto4,
                    OtherAgentPhoto5 = otherAgentPhoto5,
                    OtherAgentPhoto6 = otherAgentPhoto6,
                    OtherAgentPhoto7 = otherAgentPhoto7,
                    OtherAgentPhoto8 = otherAgentPhoto8,
                    OtherAgentPhoto9 = otherAgentPhoto9,
                    OtherAgentPhoto10 = otherAgentPhoto10                                                                             
                };

                    AgentInformation agentToRemove = db.AgentInformations.Where(x => x.AgentId == WebMatrix.WebData.WebSecurity.CurrentUserId).FirstOrDefault();

                    if (agentToRemove != null)
                    {
                        db.AgentInformations.Remove(agentToRemove);
                        db.SaveChanges();
                    }
                    if (db.AgentInformations.Where(x => x.AgentId == agentInfo.AgentId).Count() <= 0)
                    {
                        db.AgentInformations.Add(agentInfo);
                        db.SaveChanges();
                    }
                }

                  //  AgentInformation theAgent = db.AgentInformations.Where(x => x.AgentId == agentInfo.AgentId).First();
                
            }
            

        private void LogError(Exception ex)
        {

            string logFile = "/App_Data/ErrorLog.txt";
            logFile = System.Web.HttpContext.Current.Server.MapPath(logFile);

            // Open the log file for append and write the log
            System.IO.StreamWriter sw = new System.IO.StreamWriter(logFile, true);
            sw.WriteLine("********** {0} **********", DateTime.Now);
            sw.Write("Execption: {0}", ex.ToString());
            sw.WriteLine("Request ID: {0}", ex.StackTrace);
            sw.Close();
        }

        private void saveListingCategoryOrder(string top, string middle, string bottom, int userId)
        {
            int topId;
            int middleId;
            int bottomId;

            using (var db = new HODContext())
            {
                topId = db.PageCategories.Where(x => x.Code == top).FirstOrDefault().Id;
                middleId = db.PageCategories.Where(x => x.Code == middle).FirstOrDefault().Id;
                bottomId = db.PageCategories.Where(x => x.Code == bottom).FirstOrDefault().Id;

                UserListingCategory listingCat = db.UserListingCategories.Where(x => x.UserId == userId).FirstOrDefault();

                if (listingCat != null){
                    db.UserListingCategories.Remove(listingCat);
                }

                db.UserListingCategories.Add(new UserListingCategory()
                {
                    TopCategory = topId,
                    MiddleCategory = middleId,
                    BottomCategory = bottomId,
                    UserId = userId
                });

                db.SaveChanges();
            }
        }



        private PageItemType RetrievePageItemType(PageItemTypesEnum pageItemType)
        {
            PageItemType currentPageItemType;
            using (var db = new HODContext())
            {
                currentPageItemType = db.PageItemTypes.Where(x => x.PageItemTypeId == (int)pageItemType).FirstOrDefault();
            }

            return currentPageItemType;
        }



        private void savePageItems(List<PageItem> pageItems)
        {
            using (var db = new HODContext())
            {
                foreach (var item in pageItems)
                {
                    db.PageItems.Add(item);
                }

                db.SaveChanges();
            }
        }


        private PageType RetrievePageType(int pageTypeId)
        {
            PageType currentPageType;
            using (var db = new HODContext())
            {
                currentPageType = db.PageTypes.Where(x => x.PageTypeId == pageTypeId).FirstOrDefault();
            }

            return currentPageType;
        }

        private UserProfile RetrieveUserProfile(int userId)
        {
            UserProfile currentUser;
            using (var db = new HODContext())
            {
               currentUser = db.UserProfiles.Where(x => x.UserId == userId).FirstOrDefault();
            }

            return currentUser;
        }


    }
}
