﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Http.OData.Builder;
using System.Web.Mvc;
using System.Web.Routing;
using HOD.Constants;
using HOD.Entities;
using HOD.Managers;
using HOD.Models;
using HOD.Managers.Exceptions;
using HOD.Managers.Utilities;
using WebMatrix.WebData;
using HOD.Filters;
using Amazon;
using Amazon.S3;
using Amazon.S3.IO;
using System.Net;
using System.Drawing;
using System.Transactions;
using System.Data.SqlClient;
using System.Configuration; 

namespace HOD.Controllers
{
    [InitializeSimpleMembership]
    public class ListingController : HODBaseController
    {
        public IListingManager ListingManager = new ListingManager();
        public IMessageManager messageManager = new MessageManager();
        public IDbManager dbManager = new DbManager();
        IMessageManagerUtility messageManagerUtility = new MessageManagerUtility();


        public ActionResult Index()
        {
            return View();
        }

        [Authorize()]
        public ActionResult Create()
        {
            var virtualListing = new VirtualListing()
            {
                Address = (string)TempData["RegisterAddress"],
                City = (string)TempData["RegisterCity"],
                Province = (string)TempData["RegisterProvince"],
                PostalCode = (string)TempData["RegisterPostalCode"],
                ContactPerson = (string)TempData["RegisterContactPerson"],
                PhoneNo = (string)TempData["RegisterPhone"],
                DontShowListPrice = false,
                DontShowSquareFootage = false,
                DontShowPhoneNo = false

            };

            return View(virtualListing);
        }

        //
        // GET: /Listing/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // POST: /Listing/Create

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SendMessage(ListingMessageModel model)
        {
            try
            {
                if (!Request.IsAjaxRequest())
                    return Json(new { response = "error" });

                UserProfile user = null;
                using (UsersContext db = new UsersContext())
                {
                    user = db.UserProfiles.Find(WebSecurity.CurrentUserId);
                }

                if (user != null)
                {
                    MessageType messageType = messageManager.GetMessageTypeByName(MessageTypes.MESSAGE);
                    if (messageType == null)
                        throw new ArgumentNullException("Message Type does not exist.");

                    Message userMessage = new Message();
                    userMessage.Body = model.Message.Body;
                    userMessage.ConversationId = Guid.NewGuid();
                    userMessage.FromUser = user.UserId;
                    userMessage.MessageDate = DateTime.Now;
                    userMessage.MessageRead = false;
                    userMessage.MessageTypeId = messageType.MessageTypeId;
                    userMessage.Subject = model.Message.Subject;
                    userMessage.ToUser = model.Message.to;

                    if (model.Message.ListingId.ToLower().Contains("vue"))
                    {
                        string[] ID = model.Message.ListingId.Split('E');
                        userMessage.ListingID = int.Parse(ID[1]);
                    }
                    else
                    {
                        userMessage.ListingID = int.Parse(model.Message.ListingId);
                    }

                    UserProfile toUser = null;
                    using (UsersContext db = new UsersContext())
                    {
                        toUser = db.UserProfiles.Find(userMessage.ToUser);
                    }

                    string conversationUrl = System.Configuration.ConfigurationManager.AppSettings["applicationRoot"] + "UserMessages/Conversations/" + userMessage.ToUser;

                    EmailMessage emailMessage = new EmailMessage();
                    emailMessage.subject = "CasaVue Private Message";
                    emailMessage.sender = user.UserName;
                    emailMessage.recipients.Add(toUser.UserName);
                    emailMessage.body = MailMessageHelper.FormattedMail(model.Message.Subject, toUser.FirstName, model.Message.Body, conversationUrl);
                    messageManagerUtility.SendEmailMessage(emailMessage);
                    messageManager.SaveUserMessage(userMessage);

                }

                MessageModel message = new MessageModel()
                {
                    to = model.Message.to,
                    from = model.Message.from,
                    Body = model.Message.Body,
                    Subject = model.Message.Subject
                };

                //VirtualListing listing = new VirtualListing()
                //{
                //    ListingId = model.Message.ListingId,
                //};
                //return Json(message, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                //return Json(new { response = e.Message });
                throw e;
            }

            return RedirectToAction("ViewFullListing", routeValues: new { id = model.Message.ListingId });
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            if (!filterContext.HttpContext.Request.IsAjaxRequest()) return;

            filterContext.Result = AjaxError(filterContext.Exception.Message, filterContext);

            filterContext.ExceptionHandled = true;
        }

        protected JsonResult AjaxError(string message, ExceptionContext filterContext)
        {
            if (String.IsNullOrEmpty(message))
                message = "An error occurred while sending email. Email not sent.";

            filterContext.HttpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;

            filterContext.HttpContext.Response.TrySkipIisCustomErrors = true;

            return new JsonResult
            {
                Data = new { ErrorMessage = message },
                ContentEncoding = System.Text.Encoding.UTF8,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public JsonResult NotifyAgent(string first_name, string last_name, string email, string phone, string contact_preference, string bio, 
            string occupation, string income, string housetype, string affiliations, string comments, string userId)
        {
            
           // MailMessageHelper.AgentMessage("listing@casavue.com", "CasaVue")
/*            var firstName = form["first_name"];
            var lastName = form["last_name"];
            var email = form["email"];
            var phone = form["phone"];
            var contactPref = form["contact_preference"];
            var bio = form["bio"];
            var occupation = form["occupation"];
            var income = form["income"];
            var housetype = form["housetype"];
            var affiliations = form["affiliations"];
            var comments = form["comments"];
            var userId = form["UserId"];*/

            UserProfile user;
            using (UsersContext db = new UsersContext()){
                     user = db.UserProfiles.Find(int.Parse(userId));
            }

            string messageBody = first_name + last_name + email + phone + contact_preference + bio + occupation + income + housetype + affiliations + comments;

            List<string> recipientList = new List<string>();
            recipientList.Add(user.UserName);
            recipientList.Add("dellyjm@gmail.com");
            
            messageManagerUtility.SendEmailMessage(new EmailMessage(){
                 recipients = recipientList,
                 //sender = "listings@casavue.com",
                 sender = "dellyjm@gmail.com",
                 subject = "Listing Interest",
                 body = MailMessageHelper.AgentMessage("Listing Interest", first_name, last_name, email, phone,contact_preference, bio, occupation, income , housetype ,affiliations, comments)
            });

            Response.StatusCode = 200;
            string response = "BLAH";
            return Json(response, JsonRequestBehavior.AllowGet);   
        }



        [HttpPost]
        [Authorize]
        public ActionResult Create(List<HttpPostedFileBase> ImagesVirtualSign, VirtualListing model, FormCollection collection)// FormCollection collection)
        {
            if (ModelState.IsValid == false)
            {
                #region ErrorHandling
                //foreach (ModelState modelState in ViewData.ModelState.Values)
                //{
                //    foreach (ModelError error in modelState.Errors)
                //    {
                //        String errr = error.ErrorMessage;
                //        Exception exxx = error.Exception;
                //    }
                //}
                #endregion
                return View(model);
            }

            // Allow Private Seller to Overseed all other options
            if (model.PrivateSellerOption)
            {
                model.DontShowSquareFootage = true;
                model.DontShowPhoneNo = true;
                model.DontShowListPrice = true;
            }

               //ListingImageCategory imageCategory = ListingManager.GetListingImageCategory(ListingImageCategoryNames.VIRTUALSIGN);

               //List<ListingImage> images = CreateListingUploads(model, imageCategory);
            ListingImageCategory imageCategory = null;
            List<ListingImage> images = null;

               int listingUserId = WebSecurity.CurrentUserId;
               Session["ListingUserId"] = listingUserId;
               bool validUser;

               if (Session["ListingUserId"] != null) {
                   validUser = int.TryParse(Session["ListingUserId"].ToString(), out listingUserId);
               }
               else {
                   validUser = false;
               }

               if (!validUser){
                   ModelState.AddModelError("ContactPerson", "Problem setting up account. Please logout");
                   return View("Create", model);
               }
               
               int listingId = ListingManager.CreateVirtualSign(model.PropertyType, model.ListingPerson,
                    model.ContactPerson, model.PrivateSellerOption, model.CientUrl, model.PhoneNo, model.DontShowPhoneNo,
                    model.ListPrice, model.DontShowListPrice, model.SquareFootage, model.DontShowSquareFootage,
                    model.NoOfBathrooms, model.NoOfBedrooms, model.MainSellingDesc, model.LocationDesc,
                    model.Address, model.City, model.Province, model.PostalCode,model.Neighbourhood,  images, listingUserId);


              var dbconn = new HOD.Managers.DbManager();
              string imageNames = Request["listingImages"].ToString();
              string[] imageNamesCollection = imageNames.Split(',');
              int i = 1; 
              foreach (var image in imageNamesCollection)
              {
                  int category = 1;

                  if(i>4)
                    category = 2;
                  var li = new HOD.Data.Entities.ListingImage()
                  {
                      Image = image,
                      ListingId = listingId,
                      ListingImageCategoryId = category
                      //MessageDirection = messageDirection                    
                  };
                  i = i+1;

                  dbconn.saveListingImage(li);
              }

              return  Redirect("/Account/Dashboard");
                              
              //return RedirectToAction("ListingTemplate", routeValues: new { id = listingId });

     /*       } catch (Exception ex)   
            {
                    throw ex;
                //FIXME: Change to return the error page view.
            }*/
        }

        [HttpPost]
        [Authorize]
        public ActionResult UploadNewListingImages(FormCollection frm)
        {
            bool isSavedSuccessfully = true;
            string fileNameAWS = "";
            string aviaryImagePath = "";
            int listingImageId = Convert.ToInt32(frm["ImageListingId"]);
            //int ImageCategoryId = Convert.ToInt32(frm["ImageCategoryId"]);
            var dbconn = new HOD.Managers.DbManager();
            try
            {
                foreach (string fileName in Request.Files)
                {
                    HttpPostedFileBase file = Request.Files[fileName];
                    //Save file content goes here
                    //fName = file.FileName;
                    if (file != null && file.ContentLength > 0)
                    {

                        var originalDirectory = new DirectoryInfo(string.Format("{0}Images", Server.MapPath(@"\")));

                        string pathString = System.IO.Path.Combine(originalDirectory.ToString(), "ListingImages");

                        var fileName1 = Path.GetFileName(file.FileName);

                        bool isExists = System.IO.Directory.Exists(pathString);

                        if (!isExists)
                            System.IO.Directory.CreateDirectory(pathString);

                        var path = string.Format("{0}\\{1}", pathString, file.FileName);
                        file.SaveAs(path);

                        //Save to amazon repo
                        HOD.Controllers.ExternalServiceController.AmazonVideoUploader listingImageAWS = new ExternalServiceController.AmazonVideoUploader();

                        string CurrentPath = Server.MapPath("~/Images/ListingImages/") + fileName1;

                        fileNameAWS = listingImageAWS.UploadFile(CurrentPath, fileName1);

                        //var li = new HOD.Data.Entities.ListingImage()
                        //{
                        //    Image = fileName1,
                        //    ListingId = Convert.ToInt32(frm["ImageListingId"]),
                        //    ListingImageCategoryId = Convert.ToInt32(frm["ImageCategoryId"])
                        //    //MessageDirection = messageDirection                    
                        //};

                        //listingImageId = dbconn.saveListingImage(li);

                    }

                }

            }
            catch (Exception ex)
            {
                isSavedSuccessfully = false;
            }


            if (isSavedSuccessfully)
            { 
                return Json(new { name = fileNameAWS, imgId = listingImageId });
            }
            else
            {
                return Json(new { Message = "Error in saving file" });
            }
        }

        private List<ListingImage> CreateListingUploads(VirtualListing ImageModel, ListingImageCategory imageCategory)
        {
            string serverPath = ApplicationUtility.ImageStoreURL;
            string filePath = "";
            string name = "";

            VirtualListing vlDefaultValues = new VirtualListing();

            ListingImage defaultImage = new ListingImage()
            {
                Image = vlDefaultValues.Image1Location,
                ListingImageCategory = imageCategory.Name
            };
           
            List<ListingImage> images = new List<ListingImage>();
            if ((ImageModel.Image1VirtualSign != ApplicationUtility.DefaultNoImageFileName) && !(ImageModel.Image1VirtualSign.Contains("http:")))
            {
                filePath=Server.MapPath(ImageModel.Image1VirtualSign);
                name = Path.GetFileName(filePath);
                images.Add(SaveAndCreateVirtualImage(imageCategory, name, filePath));
            }
            else
            {
                defaultImage = new ListingImage();
                if (ImageModel.Image1VirtualSign.Contains("http:"))
                {
                    defaultImage.Image = ImageModel.Image1VirtualSign.Substring(serverPath.Length);
                }
                else
                {
                    defaultImage.Image = vlDefaultValues.Image1Location;
                }
                
                defaultImage.ListingImageCategory = imageCategory.Name;
                images.Add(defaultImage);

            }

            if ((ImageModel.Image2VirtualSign != ApplicationUtility.DefaultNoImageFileName) && !(ImageModel.Image2VirtualSign.Contains("http:")))
            {
                filePath = Server.MapPath(ImageModel.Image2VirtualSign);
                name = Path.GetFileName(filePath);
                images.Add(SaveAndCreateVirtualImage(imageCategory, name, filePath));
            }
            else
            {
                defaultImage = new ListingImage();
                if (ImageModel.Image2VirtualSign.Contains("http:"))
                {
                    defaultImage.Image = ImageModel.Image2VirtualSign.Substring(serverPath.Length);
                }
                else
                {
                    defaultImage.Image = vlDefaultValues.Image1Location;
                }

                defaultImage.ListingImageCategory = imageCategory.Name;
                images.Add(defaultImage);
            }

            if ((ImageModel.Image3VirtualSign != ApplicationUtility.DefaultNoImageFileName) && !(ImageModel.Image3VirtualSign.Contains("http:")))
            {
                filePath = Server.MapPath(ImageModel.Image3VirtualSign);
                name = Path.GetFileName(filePath);
                images.Add(SaveAndCreateVirtualImage(imageCategory, name, filePath));
            }
            else
            {
                defaultImage = new ListingImage();
                if (ImageModel.Image3VirtualSign.Contains("http:"))
                {
                    defaultImage.Image = ImageModel.Image3VirtualSign.Substring(serverPath.Length);
                }
                else
                {
                    defaultImage.Image = vlDefaultValues.Image1Location;
                }

                defaultImage.ListingImageCategory = imageCategory.Name;
                images.Add(defaultImage);
            }
            if ((ImageModel.Image4VirtualSign != ApplicationUtility.DefaultNoImageFileName) && !(ImageModel.Image4VirtualSign.Contains("http:")))
            {
                filePath = Server.MapPath(ImageModel.Image4VirtualSign);
                name = Path.GetFileName(filePath);
                images.Add(SaveAndCreateVirtualImage(imageCategory, name, filePath));
            }
            else
            {
                defaultImage = new ListingImage();
                if (ImageModel.Image4VirtualSign.Contains("http:"))
                {
                    defaultImage.Image = ImageModel.Image4VirtualSign.Substring(serverPath.Length);
                }
                else
                {
                    defaultImage.Image = vlDefaultValues.Image1Location;
                }

                defaultImage.ListingImageCategory = imageCategory.Name;
                images.Add(defaultImage);
            }
            if ((ImageModel.Image5VirtualSign != ApplicationUtility.DefaultNoImageFileName) && !(ImageModel.Image5VirtualSign.Contains("http:")))
            {
                filePath = Server.MapPath(ImageModel.Image5VirtualSign);
                name = Path.GetFileName(filePath);
                images.Add(SaveAndCreateVirtualImage(imageCategory, name, filePath));

            }
            else
            {
                defaultImage = new ListingImage();
                if (ImageModel.Image5VirtualSign.Contains("http:"))
                {
                    defaultImage.Image = ImageModel.Image5VirtualSign.Substring(serverPath.Length);
                }
                else
                {
                    defaultImage.Image = vlDefaultValues.Image1Location;
                }

                defaultImage.ListingImageCategory = imageCategory.Name;
                images.Add(defaultImage);
            }

            if ((ImageModel.Image6VirtualSign != ApplicationUtility.DefaultNoImageFileName) && !(ImageModel.Image6VirtualSign.Contains("http:")))
            {
                filePath = Server.MapPath(ImageModel.Image6VirtualSign);
                name = Path.GetFileName(filePath);
                images.Add(SaveAndCreateVirtualImage(imageCategory, name, filePath));
            }
            else
            {
                defaultImage = new ListingImage();
                if (ImageModel.Image6VirtualSign.Contains("http:"))
                {
                    defaultImage.Image = ImageModel.Image6VirtualSign.Substring(serverPath.Length);
                }
                else
                {
                    defaultImage.Image = vlDefaultValues.Image1Location;
                }

                defaultImage.ListingImageCategory = imageCategory.Name;
                images.Add(defaultImage);
            }

            return images;
        }

        private ListingImage SaveAndCreateVirtualImage(ListingImageCategory imageCategory, string currentImageName, string CurrentPath)
        {
            HOD.Controllers.ExternalServiceController.AmazonVideoUploader videoAndImageUpload = new ExternalServiceController.AmazonVideoUploader();
            string fileName = "";
            if (imageCategory.Name == "Video Upload")
            {
                fileName = videoAndImageUpload.UploadFile(CurrentPath,currentImageName);
            }
            else
            {
                fileName = videoAndImageUpload.UploadFile(CurrentPath, currentImageName);
            }

            ListingImage theImage = new ListingImage();
            theImage.ListingImageCategory = imageCategory.Name;
            theImage.Image = fileName;

            return theImage;
        }

       ///<summary>Accepts uploaded image files, stores them to image repository and returns list of images to be stored (uploaded and automatically created)
       /// </summary>///
        private List<ListingImage> ProcessImageUploads(List<HttpPostedFileBase> ImagesVirtualSign, ListingImageCategory imageCategory)
        {
            HttpPostedFileBase currentImage;
            const int MINIMUM_IMAGE_AMOUNT = 6;

            VirtualListing vlDefaultValues = new VirtualListing();

            ListingImage defaultImage = new ListingImage(){
                Image = vlDefaultValues.Image1Location,
                ListingImageCategory = imageCategory.Name
            };

            List<ListingImage> images = new List<ListingImage>();

            for (int count = 0; count < MINIMUM_IMAGE_AMOUNT; count++)
            {
                if (ImagesVirtualSign.Count >= count + 1){
                    currentImage = ImagesVirtualSign[count];
                } else {
                    currentImage = null;
                }

                if (currentImage != null){
                    images.Add(SaveAndCreateImage(imageCategory, currentImage));
                } else {
                    images.Add(defaultImage);
                }
            }

            return images;
        }

        private ListingImage SaveAndCreateImage(ListingImageCategory imageCategory, HttpPostedFileBase currentImage)
        {
            HOD.Controllers.ExternalServiceController.AmazonVideoUploader videoAndImageUpload = new ExternalServiceController.AmazonVideoUploader();
            string fileName = "";
            if (imageCategory.Name == "Video Upload")
            {
                fileName = videoAndImageUpload.UploadFile(currentImage);
            }
            else
            {
                fileName = videoAndImageUpload.UploadFile(currentImage); //CreateImage(currentImage);
            }
            
            ListingImage theImage = new ListingImage();
            theImage.ListingImageCategory = imageCategory.Name;
            theImage.Image = fileName;
            
            return theImage;
        }

        /***
         * 
         * Creates the image and returns the fileName of the image
         ***/
   /*     private string CreateImage(HttpPostedFileBase currentImage)
        {

            string bucketName = ApplicationUtility.AWSBucketName;
            IAmazonS3 client;

            string guid = Guid.NewGuid().ToString();
            string fileName = guid + currentImage.FileName;

            //TODO: This code needs to be refactored out to one of the Utility classes in the manager project
            using (client = Amazon.AWSClientFactory.CreateAmazonS3Client(RegionEndpoint.USEast1))
            {
                Amazon.S3.Model.PutObjectRequest putObj = new Amazon.S3.Model.PutObjectRequest(){
                    BucketName = bucketName,
                    InputStream = currentImage.InputStream,
                    Key = fileName                     
                };

                putObj.CannedACL = S3CannedACL.PublicRead;
                Amazon.S3.Model.PutObjectResponse putResponse = client.PutObject(putObj);

            }

            return fileName;
        }
*/

        public ActionResult PublicView(string id)
        {
            MessageManagerUtility mmu = new MessageManagerUtility();
            ListingRequest lr = new ListingRequest();
            try
            {
                if (!this.RouteData.Values.ContainsKey("id"))
                    throw new MessageManagerExceptions.InvalidMessageRequestException();

                lr = mmu.ParseRequest(this.RouteData.Values["id"].ToString());
            }
            catch (MessageManagerExceptions.InvalidMessageRequestException mmeX)
            {
                return new HttpNotFoundResult(string.Format("Request resource with id {0} could not be found", lr.ListingId.ToString()));
            }

            Session.Add("guestsession", true);
            return RedirectToAction("ViewFullListing", routeValues: new { id = lr.ListingId });
        }

        public ActionResult ListingTemplate(int? id)
        {

            if (id == null)
            {
                return View("Index");
            }

            VirtualListing virtualListingModel = null;

            if (id.Value > 0)
            {
                Listing returnedListing = null;

                try   {                    

                    returnedListing = ListingManager.GetListingData(id.Value);
                    HttpContext.Session["CurrentListing"] = returnedListing;
                    Session["ListingUserId"] = returnedListing.UserId;

                    if(returnedListing == null)
                        return RedirectToAction("Index", "Home");
                }
                catch (MessageManagerExceptions.InvalidListingRequestException ex)
                {
                    return new HttpNotFoundResult(string.Format("Request resource with id {0} could not be found", id.Value.ToString()));
                }

                var dir = ApplicationUtility.ImageStoreURL;

                string listingId = "VUE" + id.Value.ToString("D5");
                //bool bath  =ListingManager.GetListingSection((int)id,ListingImageCategoryNames.BATHROOM).//
                virtualListingModel = new VirtualListing()
                {
                    UserId = returnedListing.UserId,
                    AgeOfHome = returnedListing.AgeOfHome,
                    annualTaxes = returnedListing.AnnualTaxes,
                     hasAC = returnedListing.hasAC,
                     hasBasement = returnedListing.hasBasement,
                     hasFinishedBasement = returnedListing.hasFinishedBasement,
                     hasGarage = returnedListing.hasGarage,
                     heatType =returnedListing.HeatType,
                     NoOfFloors = returnedListing.NoOfFloors,                      
                    CientUrl = returnedListing.ClientUrl,
                    ContactPerson = returnedListing.ContactPerson,
                    DontShowListPrice = returnedListing.DontShowListPrice,
                    DontShowPhoneNo = returnedListing.DontShowPhoneNo,
                    DontShowSquareFootage = returnedListing.DontShowSquareFootage,
                    ListPrice = returnedListing.ListPrice,
                    PhoneNo = (returnedListing.DontShowPhoneNo == true) || (returnedListing.isPrivateSeller == true) ? "844-CASAVUE" : returnedListing.PhoneNo,//"<Phone number hidden>"
                    SquareFootage = returnedListing.SquareFootage,
                    NoOfBedrooms = returnedListing.NoOfBedrooms,
                    NoOfBathrooms = returnedListing.NoOfBathrooms,
                    PropertyType = returnedListing.PropertyType,
                    PropertyStatus = returnedListing.PropertyStatus,
                    LocationDesc = returnedListing.LocationDesc,
                    KitchenDesc = returnedListing.KitchenDesc,
                    DiningDesc = returnedListing.DiningRoomDesc,
                    BedroomDesc = returnedListing.BedroomDesc,
                    OBedroomDesc = returnedListing.BedroomDesc,
                    FamilyDesc = returnedListing.FamilyRoomDesc,
                    BasementDesc = returnedListing.BasementDesc,
                    BathroomDesc = returnedListing.BathroomDesc,  
                    VideoDesc= returnedListing.VideoDesc,
                    OtherSection1Desc = returnedListing.OtherSection1Desc,
                    OtherSection2Desc = returnedListing.OtherSection2Desc,
                    OtherSection3Desc = returnedListing.OtherSection3Desc,
                    OtherSection4Desc = returnedListing.OtherSection4Desc,
                    TollFreeNumber = dbManager.GetConfigurationByName("TOLLFREENUMBER").CongfigValue,
                    ListingPerson = returnedListing.ContactPerson,//returnedListing.ListingPerson,
                    MainSellingDesc = returnedListing.MainSellingDesc,
                    ListingId = listingId,
                    PrivateSellerOption = returnedListing.isPrivateSeller,

                    Address = returnedListing.Address,
                    City = returnedListing.City,
                    Province = returnedListing.Province,
                    PostalCode = returnedListing.PostalCode,
                    Neighbourhood = returnedListing.NeighbourHood,

                    isBathroomPublic = returnedListing.isBathroomPublic, //ListingManager.GetListingSection((int)id, ListingImageCategoryNames.BATHROOM).Value,
                    isKitchenPublic =  returnedListing.isKitchenPublic, //ListingManager.GetListingSection((int)id, ListingImageCategoryNames.KITCHEN).Value,
                    isLocationPublic = returnedListing.isLocationPublic, //ListingManager.GetListingSection((int)id, ListingImageCategoryNames.LOCATION).Value,
                    isBedroomPublic = returnedListing.isBedroomPublic, //ListingManager.GetListingSection((int)id, ListingImageCategoryNames.OBEDROOM).Value,
                    isFamilyRoomPublic =  returnedListing.isFamilyRoomPublic,         //ListingManager.GetListingSection((int)id, ListingImageCategoryNames.FAMILYROOM).Value,
                    isBasementPublic = returnedListing.isBasementPublic,  //ListingManager.GetListingSection((int)id, ListingImageCategoryNames.BASEMENT).Value
                    isOtherSection1Public = returnedListing.isOtherSection1Public,
                    isOtherSection2Public = returnedListing.isOtherSection2Public,
                    isOtherSection3Public = returnedListing.isOtherSection3Public,
                    isOtherSection4Public = returnedListing.isOtherSection4Public
                };

                Session.Add("currListingId", id);
                if (returnedListing.images != null)
                {
                    List<ListingImage> listingImages = returnedListing.images.FindAll(image => (image.ListingImageCategory == ListingImageCategoryNames.VIRTUALSIGN));

                    if (listingImages != null && listingImages.Count > 0)
                    {
                        if (listingImages[0].Image != null)
                            virtualListingModel.Image1VirtualSign = Path.Combine(dir, listingImages[0].Image);

                        if (listingImages.Count > 1)
                            if (listingImages[1].Image != null)
                                virtualListingModel.Image2VirtualSign = Path.Combine(dir, listingImages[1].Image);

                        if (listingImages.Count > 2)
                            if (listingImages[2].Image != null)
                                virtualListingModel.Image3VirtualSign = Path.Combine(dir, listingImages[2].Image);

                        if (listingImages.Count > 3)
                            if (listingImages[3].Image != null)
                                virtualListingModel.Image4VirtualSign = Path.Combine(dir, listingImages[3].Image);

                        if (listingImages.Count > 4)
                            if (listingImages[4].Image != null)
                                virtualListingModel.Image5VirtualSign = Path.Combine(dir, listingImages[4].Image);

                        if (listingImages.Count > 5)
                            if (listingImages[5].Image != null)
                                virtualListingModel.Image6VirtualSign = Path.Combine(dir, listingImages[5].Image);
                    }

                    List<ListingImage> locationImages = returnedListing.images.FindAll(image => (image.ListingImageCategory == ListingImageCategoryNames.LOCATION));

                    if (locationImages != null && locationImages.Count > 0)
                    {
                        if (locationImages[0] != null)
                            virtualListingModel.Image1Location = Path.Combine(dir, locationImages[0].Image);

                        if (locationImages.Count > 1)
                            if (locationImages[1] != null)
                                virtualListingModel.Image2Location = Path.Combine(dir, locationImages[1].Image);

                        if (locationImages.Count > 2)
                            if (locationImages[2] != null)
                                virtualListingModel.Image3Location = Path.Combine(dir, locationImages[2].Image);

                        if (locationImages.Count > 3)
                            if (locationImages[3] != null)
                                virtualListingModel.Image4Location = Path.Combine(dir, locationImages[3].Image);
                        
                        if (locationImages.Count > 4)
                            if (locationImages[4] != null)
                                virtualListingModel.Image5Location = Path.Combine(dir, locationImages[4].Image);
                        
                        if (locationImages.Count > 5)
                            if (locationImages[5] != null)
                                virtualListingModel.Image6Location = Path.Combine(dir, locationImages[5].Image);
                    }

                    List<ListingImage> kitchenImages = returnedListing.images.FindAll(image => (image.ListingImageCategory == ListingImageCategoryNames.KITCHEN));

                    if (kitchenImages != null && kitchenImages.Count > 0)
                    {
                        if (kitchenImages[0] != null)
                            virtualListingModel.Image1Kitchen = Path.Combine(dir, kitchenImages[0].Image);

                        if (kitchenImages.Count > 1)
                            if (kitchenImages[1] != null)
                                virtualListingModel.Image2Kitchen = Path.Combine(dir, kitchenImages[1].Image);

                        if (kitchenImages.Count > 2)
                            if (kitchenImages[2] != null)
                                virtualListingModel.Image3Kitchen = Path.Combine(dir, kitchenImages[2].Image);

                        if (kitchenImages.Count > 3)
                            if (kitchenImages[3] != null)
                                virtualListingModel.Image4Kitchen = Path.Combine(dir, kitchenImages[3].Image);

                        if (kitchenImages.Count > 4)
                            if (kitchenImages[4] != null)
                                virtualListingModel.Image5Kitchen = Path.Combine(dir, kitchenImages[4].Image);

                        if (kitchenImages.Count > 5)
                            if (kitchenImages[5] != null)
                                virtualListingModel.Image6Kitchen = Path.Combine(dir, kitchenImages[5].Image);
                    }

                    List<ListingImage> bedroomImages = returnedListing.images.FindAll(image => (image.ListingImageCategory == ListingImageCategoryNames.BEDROOM));

                    if (bedroomImages != null && bedroomImages.Count > 0)
                    {
                        if (bedroomImages[0] != null)
                            virtualListingModel.Image1Bedroom = Path.Combine(dir, bedroomImages[0].Image);

                        if (bedroomImages.Count > 1)
                            if (bedroomImages[1] != null)
                                virtualListingModel.Image2Bedroom = Path.Combine(dir, bedroomImages[1].Image);

                        if (bedroomImages.Count > 2)
                            if (bedroomImages[2] != null)
                                virtualListingModel.Image3Bedroom = Path.Combine(dir, bedroomImages[2].Image);

                        if (bedroomImages.Count > 3)
                            if (bedroomImages[3] != null)
                                virtualListingModel.Image4Bedroom = Path.Combine(dir, bedroomImages[3].Image);

                        if (bedroomImages.Count > 4)
                            if (bedroomImages[4] != null)
                                virtualListingModel.Image5Bedroom = Path.Combine(dir, bedroomImages[4].Image);

                        if (bedroomImages.Count > 5)
                            if (bedroomImages[5] != null)
                                virtualListingModel.Image6Bedroom = Path.Combine(dir, bedroomImages[5].Image);
                    }

                    List<ListingImage> ObedroomImages = returnedListing.images.FindAll(image => (image.ListingImageCategory == ListingImageCategoryNames.OBEDROOM));

                    if (ObedroomImages != null && ObedroomImages.Count > 0)
                    {
                        if (ObedroomImages[0] != null)
                            virtualListingModel.Image1OBedroom = Path.Combine(dir, ObedroomImages[0].Image);

                        if (ObedroomImages.Count > 1)
                            if (ObedroomImages[1] != null)
                                virtualListingModel.Image2OBedroom = Path.Combine(dir, ObedroomImages[1].Image);

                        if (ObedroomImages.Count > 2)
                            if (ObedroomImages[2] != null)
                                virtualListingModel.Image3OBedroom = Path.Combine(dir, ObedroomImages[2].Image);

                        if (ObedroomImages.Count > 3)
                            if (ObedroomImages[3] != null)
                                virtualListingModel.Image4OBedroom = Path.Combine(dir, ObedroomImages[3].Image);

                        if (ObedroomImages.Count > 4)
                            if (ObedroomImages[4] != null)
                                virtualListingModel.Image5OBedroom = Path.Combine(dir, ObedroomImages[4].Image);

                        if (ObedroomImages.Count > 5)
                            if (ObedroomImages[5] != null)
                                virtualListingModel.Image6OBedroom = Path.Combine(dir, ObedroomImages[5].Image);
                    }

                    List<ListingImage> basementImages = returnedListing.images.FindAll(image => (image.ListingImageCategory == ListingImageCategoryNames.BASEMENT));

                    if (basementImages != null && basementImages.Count > 0)
                    {
                        if (basementImages[0] != null)
                            virtualListingModel.Image1Basement = Path.Combine(dir, basementImages[0].Image);

                        if (basementImages.Count > 1)
                            if (basementImages[1] != null)
                                virtualListingModel.Image2Basement = Path.Combine(dir, basementImages[1].Image);

                        if (basementImages.Count > 2)
                            if (basementImages[2] != null)
                                virtualListingModel.Image3Basement = Path.Combine(dir, basementImages[2].Image);

                        if (basementImages.Count > 3)
                            if (basementImages[3] != null)
                                virtualListingModel.Image4Basement = Path.Combine(dir, basementImages[3].Image);

                        if (basementImages.Count > 4)
                            if (basementImages[4] != null)
                                virtualListingModel.Image5Basement = Path.Combine(dir, basementImages[4].Image);

                        if (basementImages.Count > 5)
                            if (basementImages[5] != null)
                                virtualListingModel.Image6Basement = Path.Combine(dir, basementImages[5].Image);
                    }

                    List<ListingImage> diningImages = returnedListing.images.FindAll(image => (image.ListingImageCategory == ListingImageCategoryNames.DININGROOM));

                    if (diningImages != null && diningImages.Count > 0)
                    {
                        if (diningImages[0] != null)
                            virtualListingModel.Image1Dining = Path.Combine(dir, diningImages[0].Image);

                        if (diningImages.Count > 1)
                            if (diningImages[1] != null)
                                virtualListingModel.Image2Dining = Path.Combine(dir, diningImages[1].Image);

                        if (diningImages.Count > 2)
                            if (diningImages[2] != null)
                                virtualListingModel.Image3Dining = Path.Combine(dir, diningImages[2].Image);

                        if (diningImages.Count > 3)
                            if (diningImages[3] != null)
                                virtualListingModel.Image4Dining = Path.Combine(dir, diningImages[3].Image);

                        if (diningImages.Count > 4)
                            if (diningImages[4] != null)
                                virtualListingModel.Image5Dining = Path.Combine(dir, diningImages[4].Image);

                        if (diningImages.Count > 5)
                            if (diningImages[5] != null)
                                virtualListingModel.Image6Dining = Path.Combine(dir, diningImages[5].Image);
                    }

                    List<ListingImage> familyImages = returnedListing.images.FindAll(image => (image.ListingImageCategory == ListingImageCategoryNames.FAMILYROOM));

                    if (familyImages != null && familyImages.Count > 0)
                    {
                        if (familyImages[0] != null)
                            virtualListingModel.Image1Family = Path.Combine(dir, familyImages[0].Image);

                        if (familyImages.Count > 1)
                            if (familyImages[1] != null)
                                virtualListingModel.Image2Family = Path.Combine(dir, familyImages[1].Image);

                        if (familyImages.Count > 2)
                            if (familyImages[2] != null)
                                virtualListingModel.Image3Family = Path.Combine(dir, familyImages[2].Image);

                        if (familyImages.Count > 3)
                            if (familyImages[3] != null)
                                virtualListingModel.Image4Family = Path.Combine(dir, familyImages[3].Image);

                        if (familyImages.Count > 4)
                            if (familyImages[4] != null)
                                virtualListingModel.Image5Family = Path.Combine(dir, familyImages[4].Image);

                        if (familyImages.Count > 5)
                            if (familyImages[5] != null)
                                virtualListingModel.Image6Family = Path.Combine(dir, familyImages[5].Image);
                    }

                    List<ListingImage> VideoImages = returnedListing.images.FindAll(image => (image.ListingImageCategory == ListingImageCategoryNames.VIDEOUPLOAD));

                    if (VideoImages != null && VideoImages.Count > 0)
                    {
                        if (VideoImages[0] != null && VideoImages[0].Image != "Video Place-Holder")
                            virtualListingModel.VideoLink = Path.Combine(dir, VideoImages[0].Image);
                        else
                            virtualListingModel.VideoLink = "No Video";
                    }

                    List<ListingImage> bathroomImages = returnedListing.images.FindAll(image => (image.ListingImageCategory == ListingImageCategoryNames.BATHROOM));

                    if (bathroomImages != null && bathroomImages.Count > 0)
                    {
                        if (bathroomImages[0] != null)
                            virtualListingModel.Image1Bathroom = Path.Combine(dir, bathroomImages[0].Image);

                        if (bathroomImages.Count > 1)
                            if (bathroomImages[1] != null)
                                virtualListingModel.Image2Bathroom = Path.Combine(dir, bathroomImages[1].Image);

                        if (bathroomImages.Count > 2)
                            if (bathroomImages[2] != null)
                                virtualListingModel.Image3Bathroom = Path.Combine(dir, bathroomImages[2].Image);

                        if (bathroomImages.Count > 3)
                            if (bathroomImages[3] != null)
                                virtualListingModel.Image4Bathroom = Path.Combine(dir, bathroomImages[3].Image);

                        if (bathroomImages.Count > 4)
                            if (bathroomImages[4] != null)
                                virtualListingModel.Image5Bathroom = Path.Combine(dir, bathroomImages[4].Image);
                       
                        if (bathroomImages.Count > 5)
                            if (bathroomImages[5] != null)
                                virtualListingModel.Image6Bathroom = Path.Combine(dir, bathroomImages[5].Image);
                    }

                    List<ListingImage> otherSection1Images = returnedListing.images.FindAll(image => (image.ListingImageCategory == ListingImageCategoryNames.OTHERSECTION1));

                    if (otherSection1Images != null && otherSection1Images.Count > 0)
                    {
                        if (otherSection1Images[0] != null)
                            virtualListingModel.Image1OtherSection1= Path.Combine(dir, otherSection1Images[0].Image);

                        if (otherSection1Images.Count > 1)
                            if (otherSection1Images[1] != null)
                                virtualListingModel.Image2OtherSection1 = Path.Combine(dir, otherSection1Images[1].Image);

                        if (otherSection1Images.Count > 2)
                            if (otherSection1Images[2] != null)
                                virtualListingModel.Image3OtherSection1 = Path.Combine(dir, otherSection1Images[2].Image);

                        if (otherSection1Images.Count > 3)
                            if (otherSection1Images[3] != null)
                                virtualListingModel.Image4OtherSection1 = Path.Combine(dir, otherSection1Images[3].Image);

                        if (otherSection1Images.Count > 4)
                            if (otherSection1Images[4] != null)
                                virtualListingModel.Image5OtherSection1 = Path.Combine(dir, otherSection1Images[4].Image);

                        if (otherSection1Images.Count > 5)
                            if (otherSection1Images[5] != null)
                                virtualListingModel.Image6OtherSection1 = Path.Combine(dir, otherSection1Images[5].Image);
                    }

                    List<ListingImage> otherSection2Images = returnedListing.images.FindAll(image => (image.ListingImageCategory == ListingImageCategoryNames.OTHERSECTION2));

                    if (otherSection2Images != null && otherSection2Images.Count > 0)
                    {
                        if (otherSection2Images[0] != null)
                            virtualListingModel.Image1OtherSection2 = Path.Combine(dir, otherSection2Images[0].Image);

                        if (otherSection2Images.Count > 1)
                            if (otherSection2Images[1] != null)
                                virtualListingModel.Image2OtherSection2 = Path.Combine(dir, otherSection2Images[1].Image);

                        if (otherSection2Images.Count > 2)
                            if (otherSection2Images[2] != null)
                                virtualListingModel.Image3OtherSection2 = Path.Combine(dir, otherSection2Images[2].Image);

                        if (otherSection2Images.Count > 3)
                            if (otherSection2Images[3] != null)
                                virtualListingModel.Image4OtherSection2 = Path.Combine(dir, otherSection2Images[3].Image);

                        if (otherSection2Images.Count > 4)
                            if (otherSection2Images[4] != null)
                                virtualListingModel.Image5OtherSection2 = Path.Combine(dir, otherSection2Images[4].Image);

                        if (otherSection2Images.Count > 5)
                            if (otherSection2Images[5] != null)
                                virtualListingModel.Image6OtherSection2 = Path.Combine(dir, otherSection2Images[5].Image);
                    }

                    List<ListingImage> otherSection3Images = returnedListing.images.FindAll(image => (image.ListingImageCategory == ListingImageCategoryNames.OTHERSECTION3));

                    if (otherSection3Images != null && otherSection3Images.Count > 0)
                    {
                        if (otherSection3Images[0] != null)
                            virtualListingModel.Image1OtherSection3 = Path.Combine(dir, otherSection3Images[0].Image);

                        if (otherSection3Images.Count > 1)
                            if (otherSection3Images[1] != null)
                                virtualListingModel.Image2OtherSection3 = Path.Combine(dir, otherSection3Images[1].Image);

                        if (otherSection3Images.Count > 2)
                            if (otherSection3Images[2] != null)
                                virtualListingModel.Image3OtherSection3 = Path.Combine(dir, otherSection3Images[2].Image);

                        if (otherSection3Images.Count > 3)
                            if (otherSection3Images[3] != null)
                                virtualListingModel.Image4OtherSection3 = Path.Combine(dir, otherSection3Images[3].Image);

                        if (otherSection3Images.Count > 4)
                            if (otherSection3Images[4] != null)
                                virtualListingModel.Image5OtherSection3 = Path.Combine(dir, otherSection3Images[4].Image);

                        if (otherSection3Images.Count > 5)
                            if (otherSection3Images[5] != null)
                                virtualListingModel.Image6OtherSection3 = Path.Combine(dir, otherSection3Images[5].Image);
                    }

                    List<ListingImage> otherSection4Images = returnedListing.images.FindAll(image => (image.ListingImageCategory == ListingImageCategoryNames.OTHERSECTION4));

                    if (otherSection4Images != null && otherSection4Images.Count > 0)
                    {
                        if (otherSection4Images[0] != null)
                            virtualListingModel.Image1OtherSection4 = Path.Combine(dir, otherSection4Images[0].Image);

                        if (otherSection4Images.Count > 1)
                            if (otherSection4Images[1] != null)
                                virtualListingModel.Image2OtherSection4 = Path.Combine(dir, otherSection4Images[1].Image);

                        if (otherSection4Images.Count > 2)
                            if (otherSection4Images[2] != null)
                                virtualListingModel.Image3OtherSection4 = Path.Combine(dir, otherSection4Images[2].Image);

                        if (otherSection4Images.Count > 3)
                            if (otherSection4Images[3] != null)
                                virtualListingModel.Image4OtherSection4 = Path.Combine(dir, otherSection4Images[3].Image);

                        if (otherSection4Images.Count > 4)
                            if (otherSection4Images[4] != null)
                                virtualListingModel.Image5OtherSection4 = Path.Combine(dir, otherSection4Images[4].Image);

                        if (otherSection4Images.Count > 5)
                            if (otherSection4Images[5] != null)
                                virtualListingModel.Image6OtherSection4 = Path.Combine(dir, otherSection4Images[5].Image);
                    }
                }
            }

            ViewBag.RootUrl =  System.Configuration.ConfigurationManager.AppSettings["applicationRoot"];
            ViewBag.ImageUrl = System.Configuration.ConfigurationManager.AppSettings["AWSS3BucketURL"];

            return View("ListingTemplate", virtualListingModel);
        }

        public ActionResult Poster(int? id)
        {

            if (id == null)
            {
                return View("Index");
            }

            VirtualListing virtualListingModel = null;

            if (id.Value > 0)
            {
                Listing returnedListing = null;

                try
                {
                    returnedListing = ListingManager.GetListingData(id.Value);
                }
                catch (MessageManagerExceptions.InvalidListingRequestException ex)
                {
                    return new HttpNotFoundResult(string.Format("Request resource with id {0} could not be found", id.Value.ToString()));
                }

                var dir = System.Configuration.ConfigurationManager.AppSettings["AWSS3BucketURL"];

                string listingId = "VUE" + id.Value.ToString("D5");

                virtualListingModel = new VirtualListing()
                {
                    UserId = returnedListing.UserId,
                    CientUrl = returnedListing.ClientUrl,
                    ContactPerson = returnedListing.ContactPerson,
                    DontShowListPrice = returnedListing.DontShowListPrice,
                    DontShowPhoneNo = returnedListing.DontShowPhoneNo,
                    DontShowSquareFootage = returnedListing.DontShowSquareFootage,
                    ListPrice = returnedListing.DontShowListPrice == true ? 0 : returnedListing.ListPrice,
                    TollFreeNumber = dbManager.GetConfigurationByName("TOLLFREENUMBER").CongfigValue,
                    PhoneNo = returnedListing.DontShowPhoneNo == true ? dbManager.GetConfigurationByName("TOLLFREENUMBER").CongfigValue : returnedListing.PhoneNo,//"<Phone number hidden>"
                    SquareFootage = returnedListing.DontShowSquareFootage == true ? 0 : returnedListing.SquareFootage,
                    NoOfBedrooms = returnedListing.NoOfBedrooms,
                    NoOfBathrooms = returnedListing.NoOfBathrooms,
                    PropertyType = returnedListing.PropertyType,
                    LocationDesc = returnedListing.LocationDesc,
                    KitchenDesc = returnedListing.KitchenDesc,
                    ListingPerson = returnedListing.ContactPerson,//returnedListing.ListingPerson,
                    MainSellingDesc = returnedListing.MainSellingDesc,
                    ListingId = listingId,
                    PrivateSellerOption = returnedListing.isPrivateSeller,
                     
                    Address = returnedListing.Address,
                    City = returnedListing.City,
                    Province = returnedListing.Province,
                    PostalCode = returnedListing.PostalCode
                };

                Session.Add("currListingId", id);
                
                
                if (returnedListing.images != null)
                {
                    List<ListingImage> listingImages = returnedListing.images.FindAll(image => (image.ListingImageCategory == ListingImageCategoryNames.VIRTUALSIGN));

                    if (listingImages != null && listingImages.Count > 0)
                    {
                        if (listingImages[0] != null)
                            virtualListingModel.Image1VirtualSign = Path.Combine(dir, listingImages[0].Image);

                        if (listingImages[1] != null)
                            virtualListingModel.Image2VirtualSign = Path.Combine(dir, listingImages[1].Image);

                        if (listingImages[2] != null)
                            virtualListingModel.Image3VirtualSign = Path.Combine(dir, listingImages[2].Image);

                        if (listingImages[3] != null)
                            virtualListingModel.Image4VirtualSign = Path.Combine(dir, listingImages[3].Image);
                    }

                    List<ListingImage> locationImages = returnedListing.images.FindAll(image => (image.ListingImageCategory == ListingImageCategoryNames.LOCATION));

                    if (locationImages != null && locationImages.Count > 0)
                    {
                        if (locationImages[0] != null)
                            virtualListingModel.Image1Location = Path.Combine(dir, locationImages[0].Image);

                        if (locationImages[1] != null)
                            virtualListingModel.Image2Location = Path.Combine(dir, locationImages[1].Image);

                        if (locationImages[2] != null)
                            virtualListingModel.Image3Location = Path.Combine(dir, locationImages[2].Image);

                        if (locationImages[3] != null)
                            virtualListingModel.Image4Location = Path.Combine(dir, locationImages[3].Image);
                    }                
                }

                ViewBag.RootUrl = System.Configuration.ConfigurationManager.AppSettings["applicationRoot"];
                ViewBag.ImageUrl = System.Configuration.ConfigurationManager.AppSettings["AWSS3BucketURL"];
                ViewBag.ListingOwnerUserId = returnedListing.UserId;

                return View("Poster", virtualListingModel);
            }
            return View("ListingTemplate");

        }

        //[ListingOwnerAttribute]
        [Authorize()]
        public ActionResult Edit(int id)
        {
            VirtualListing model = new VirtualListing();
            string imagesFolder = ApplicationUtility.ImageStoreURL;

            if (id != null)
            {

                List<ListingImage> images = ListingManager.GetListingImageByListing(id).Where(x => x.ListingImageCategory == ListingImageCategoryNames.VIRTUALSIGN).ToList();

                var listing = ListingManager.GetListingData(id);

                User user = dbManager.GetUserByUserName(User.Identity.Name);
                UserProfile listingOwner;


                using (UsersContext db = new UsersContext())
                {
                   listingOwner = db.UserProfiles.Where(x => x.UserId == listing.UserId).FirstOrDefault();
                }

                int currentAgent = 0;

                if (listingOwner != null){
                    currentAgent = listingOwner.AgentId;
                }
              

                if ((currentAgent == WebSecurity.CurrentUserId) || (listing.UserId == user.UserId)){
                    model.UserId = listing.UserId;
                    model.Address = listing.Address;
                    model.CientUrl = listing.ClientUrl;
                    model.City = listing.City;
                    model.ContactPerson = listing.ContactPerson;
                    model.DontShowListPrice = listing.DontShowListPrice;
                    model.DontShowPhoneNo = listing.DontShowPhoneNo;
                    model.DontShowSquareFootage = listing.DontShowSquareFootage;
                    model.PropertyType = listing.PropertyType;
                    model.Province = listing.Province;
                    model.PostalCode = listing.PostalCode;
                    model.ListPrice = listing.ListPrice;
                    model.ListingPerson = listing.ListingPerson;
                    model.SquareFootage = listing.SquareFootage;
                    model.NoOfBathrooms = listing.NoOfBathrooms;
                    model.NoOfBedrooms = listing.NoOfBedrooms;
                    model.MainSellingDesc = listing.MainSellingDesc;
                    model.ListingPerson = listing.ListingPerson;
                    model.PhoneNo = listing.PhoneNo;
                    model.PrivateSellerOption = listing.isPrivateSeller;
                    model.ListingId = id.ToString();
                    model.Neighbourhood = listing.NeighbourHood;
                    model.TollFreeNumber = dbManager.GetConfigurationByName("TOLLFREENUMBER").CongfigValue;
                    //model.Image1VirtualSign = Path.Combine(imagesFolder, images[0].Image);
                    //model.Image2VirtualSign = Path.Combine(imagesFolder, images[1].Image);
                    //model.Image3VirtualSign = Path.Combine(imagesFolder, images[2].Image);
                    //model.Image4VirtualSign = Path.Combine(imagesFolder, images[3].Image);
                }
                else
                {
                    return RedirectToAction("Index", "Home", routeValues: new { id = UrlParameter.Optional });
                }
            }
            return View("Update", model);
        }

      //  [ListingOwnerAttribute]
        public ActionResult Update(VirtualListing model)
        {
            return View("Update", model);
        }

        //[EnableCompression]
        public ActionResult ViewFullListing(string id)
        {
            MessageManagerUtility mmu = new MessageManagerUtility();

            ListingRequest lr;

            if (id.ToLower().Contains("vue")){
                lr = mmu.ParseRequest(id);
            }
            else {
                lr = new ListingRequest();
                lr.ListingId = int.Parse(id);
            }

            if (id == null)
            {
                return View("Index");
            }

            Session["currListingId"] = lr.ListingId;
            Listing returnedListing;

            try {
                returnedListing = ListingManager.GetListingData(lr.ListingId);
            } catch (ApplicationException appEx)  {
                ViewBag.Error = appEx.Message;
                return View("Error");                
            }
            catch (Exception ex)      {
                this.LogException(ex);
                ViewBag.Error = "Error processing request";
                return View("Error");                
            }
                
                VirtualListing virtualListingModel = new VirtualListing()
                {
                    UserId = returnedListing.UserId,
                    CientUrl = returnedListing.ClientUrl,
                    ContactPerson = returnedListing.ContactPerson,
                    DontShowListPrice = returnedListing.DontShowListPrice,
                    DontShowPhoneNo = returnedListing.DontShowPhoneNo,
                    DontShowSquareFootage = returnedListing.DontShowSquareFootage,
                    ListPrice = returnedListing.ListPrice,
                    Neighbourhood = returnedListing.NeighbourHood,
                    PhoneNo = returnedListing.DontShowListPrice ? dbManager.GetConfigurationByName("TOLLFREENUMBER").CongfigValue:returnedListing.PhoneNo,
                    SquareFootage = returnedListing.SquareFootage,
                    NoOfBedrooms = returnedListing.NoOfBedrooms,
                    NoOfBathrooms = returnedListing.NoOfBathrooms,
                    PropertyType = returnedListing.PropertyType,
                    heatType = returnedListing.HeatType,
                    AgeOfHome = returnedListing.AgeOfHome,
                    NoOfFloors = returnedListing.NoOfFloors,
                    hasBasement = returnedListing.hasBasement,
                    hasFinishedBasement = returnedListing.hasFinishedBasement,
                    hasGarage = returnedListing.hasGarage,
                    hasAC = returnedListing.hasAC,
                    annualTaxes = returnedListing.AnnualTaxes,
                    DiningDesc = returnedListing.DiningRoomDesc,
                    FamilyDesc = returnedListing.FamilyRoomDesc,
                    OBedroomDesc = returnedListing.BedroomDesc,
                    LocationDesc = returnedListing.LocationDesc,
                    BathroomDesc = returnedListing.BathroomDesc,
                    BasementDesc = returnedListing.BasementDesc,
                    KitchenDesc = returnedListing.KitchenDesc,
                    MainSellingDesc = returnedListing.MainSellingDesc,
                    ListingPerson = returnedListing.ContactPerson,//returnedListing.ListingPerson,
                    ListingId = id,
                    PrivateSellerOption = returnedListing.isPrivateSeller,
                    Address = returnedListing.Address,
                    City = returnedListing.City,
                    Province = returnedListing.Province,
                    PostalCode = returnedListing.PostalCode,
                    TollFreeNumber = dbManager.GetConfigurationByName("TOLLFREENUMBER").CongfigValue,
                    //Image1VirtualSign = returnedListing.images[0].Image ?? string.Empty,
                    //Image2VirtualSign = returnedListing.images[1].Image ?? string.Empty,
                    //Image3VirtualSign = returnedListing.images[2].Image ?? string.Empty,
                    //Image4VirtualSign = returnedListing.images[3].Image ?? string.Empty,
                    //Image5VirtualSign = returnedListing.images[4].Image ?? string.Empty,
                    //Image6VirtualSign = returnedListing.images[5].Image ?? string.Empty,
                    VideoDesc = returnedListing.VideoDesc,
                    isBasementPublic = returnedListing.isBasementPublic,
                    isBathroomPublic = returnedListing.isBathroomPublic,
                    isBedroomPublic = returnedListing.isBedroomPublic,
                    isDiningPublic = returnedListing.isDiningPublic,
                    isFamilyRoomPublic = returnedListing.isFamilyRoomPublic,
                    isKitchenPublic = returnedListing.isKitchenPublic,
                    isLocationPublic = returnedListing.isLocationPublic,
                    isVideoPublic = returnedListing.isVideoPublic,
                    isOtherSection1Public = returnedListing.isOtherSection1Public,
                    isOtherSection2Public = returnedListing.isOtherSection2Public,
                    isOtherSection3Public = returnedListing.isOtherSection3Public,
                    isOtherSection4Public = returnedListing.isOtherSection4Public,
                    OtherSection1Desc = returnedListing.OtherSection1Desc,
                    OtherSection2Desc = returnedListing.OtherSection2Desc,
                    OtherSection3Desc = returnedListing.OtherSection3Desc,
                    OtherSection4Desc = returnedListing.OtherSection4Desc
                };

            string dir = ApplicationUtility.ImageStoreURL;

            returnedListing.images.RemoveAll(x => x.Image.ToLower() == "nohouse.png");
            List<ListingImage> listingImages = returnedListing.images.FindAll(image => (image.ListingImageCategory == ListingImageCategoryNames.VIRTUALSIGN));
            //listingImages.RemoveAll( x => x.Image.ToLower() == "nohouse.png")   

            string noImagePath =  Path.Combine(dir, "no_houseicon.png");
            if (listingImages != null && listingImages.Count > 0)
            {
                if (listingImages[0].Image != null)
                    virtualListingModel.Image1VirtualSign = Path.Combine(dir, listingImages[0].Image);

                if (listingImages.Count > 1)
                    if (listingImages[1].Image != null)
                        virtualListingModel.Image2VirtualSign = Path.Combine(dir, listingImages[1].Image);

                if (listingImages.Count > 2)
                    if (listingImages[2].Image != null)
                        virtualListingModel.Image3VirtualSign = Path.Combine(dir, listingImages[2].Image);

                if (listingImages.Count > 3)
                    if (listingImages[3].Image != null)
                        virtualListingModel.Image4VirtualSign = Path.Combine(dir, listingImages[3].Image);

                if (listingImages.Count > 4)
                    if (listingImages[4].Image != null)
                        virtualListingModel.Image5VirtualSign = Path.Combine(dir, listingImages[4].Image);

                if (listingImages.Count > 5)
                    if (listingImages[5].Image != null)
                        virtualListingModel.Image6VirtualSign = Path.Combine(dir, listingImages[5].Image);    
            }

            List<ListingImage> Location = returnedListing.images.FindAll(image => (image.ListingImageCategory == ListingImageCategoryNames.LOCATION));

            if (Location != null && Location.Count > 0)
            {
                if (Location[0] != null)
                    virtualListingModel.Image1Location = Path.Combine(dir, Location[0].Image);

                if (Location.Count > 1)
                    if (Location[1] != null)
                        virtualListingModel.Image2Location = Path.Combine(dir, Location[1].Image);

                if (Location.Count > 2)
                    if (Location[2] != null)
                        virtualListingModel.Image3Location = Path.Combine(dir, Location[2].Image);

                if (Location.Count > 3)
                    if (Location[3] != null)
                        virtualListingModel.Image4Location = Path.Combine(dir, Location[3].Image);

                if (Location.Count > 4)
                    if (Location[4] != null)
                        virtualListingModel.Image5Location = Path.Combine(dir, Location[4].Image);

                if (Location.Count > 5)
                    if (Location[5] != null)
                        virtualListingModel.Image6Location = Path.Combine(dir, Location[5].Image);
            }

            List<ListingImage> Kitchen = returnedListing.images.FindAll(image => (image.ListingImageCategory == ListingImageCategoryNames.KITCHEN));
            
            if (Kitchen != null && Kitchen.Count > 0)
            {
                virtualListingModel.KitchenImages = Kitchen.Where(m => m.Image != ApplicationUtility.DefaultNoImageFileName).Select(m => dir + m.Image).ToList();

                if (Kitchen[0] != null)
                    virtualListingModel.Image1Kitchen = Path.Combine(dir, Kitchen[0].Image);

                if (Kitchen.Count > 1)
                    if (Kitchen[1] != null)
                        virtualListingModel.Image2Kitchen = Path.Combine(dir, Kitchen[1].Image);

                if (Kitchen.Count > 2)
                    if (Kitchen[2] != null)
                        virtualListingModel.Image3Kitchen = Path.Combine(dir, Kitchen[2].Image);

                if (Kitchen.Count > 3)
                    if (Kitchen[3] != null)
                        virtualListingModel.Image4Kitchen = Path.Combine(dir, Kitchen[3].Image);

                if (Kitchen.Count > 4)
                    if (Kitchen[4] != null)
                        virtualListingModel.Image5Kitchen = Path.Combine(dir, Kitchen[4].Image);

                if (Kitchen.Count > 5)
                    if (Kitchen[5] != null)
                        virtualListingModel.Image6Kitchen = Path.Combine(dir, Kitchen[5].Image);
            }

            List<ListingImage> FamilyRoomImages = returnedListing.images.FindAll(image => (image.ListingImageCategory == ListingImageCategoryNames.FAMILYROOM));

            if (FamilyRoomImages != null && FamilyRoomImages.Count > 0)
            {
                virtualListingModel.FamilyImages = FamilyRoomImages.Where(m => m.Image != ApplicationUtility.DefaultNoImageFileName).Select(m => dir + m.Image).ToList();
                if (FamilyRoomImages[0] != null)
                    virtualListingModel.Image1Family = Path.Combine(dir, FamilyRoomImages[0].Image);

                if (FamilyRoomImages.Count > 1)
                    if (FamilyRoomImages[1] != null)
                        virtualListingModel.Image2Family = Path.Combine(dir, FamilyRoomImages[1].Image);

                if (FamilyRoomImages.Count > 2)
                    if (FamilyRoomImages[2] != null)
                        virtualListingModel.Image3Family = Path.Combine(dir, FamilyRoomImages[2].Image);

                if (FamilyRoomImages.Count > 3)
                    if (FamilyRoomImages[3] != null)
                        virtualListingModel.Image4Family = Path.Combine(dir, FamilyRoomImages[3].Image);

                if (FamilyRoomImages.Count > 4)
                    if (FamilyRoomImages[4] != null)
                        virtualListingModel.Image5Family = Path.Combine(dir, FamilyRoomImages[4].Image);

                if (FamilyRoomImages.Count > 5)
                    if (FamilyRoomImages[5] != null)
                        virtualListingModel.Image6Family = Path.Combine(dir, FamilyRoomImages[5].Image);
            }

            List<ListingImage> bathroomImages = returnedListing.images.FindAll(image => (image.ListingImageCategory == ListingImageCategoryNames.BATHROOM));

            if (bathroomImages != null && bathroomImages.Count > 0)
            {
                virtualListingModel.BathroomImages = bathroomImages.Where(m => m.Image != ApplicationUtility.DefaultNoImageFileName).Select(m => dir + m.Image).ToList();
                if (bathroomImages[0] != null)
                    virtualListingModel.Image1Bathroom = Path.Combine(dir, bathroomImages[0].Image);

                if (bathroomImages.Count > 1)
                    if (bathroomImages[1] != null)
                        virtualListingModel.Image2Bathroom = Path.Combine(dir, bathroomImages[1].Image);

                if (bathroomImages.Count > 2)
                    if (bathroomImages[2] != null)
                        virtualListingModel.Image3Bathroom = Path.Combine(dir, bathroomImages[2].Image);

                if (bathroomImages.Count > 3)
                    if (bathroomImages[3] != null)
                        virtualListingModel.Image4Bathroom = Path.Combine(dir, bathroomImages[3].Image);

                if (bathroomImages.Count > 4)
                    if (bathroomImages[4] != null)
                        virtualListingModel.Image5Bathroom = Path.Combine(dir, bathroomImages[4].Image);

                if (bathroomImages.Count > 5)
                    if (bathroomImages[5] != null)
                        virtualListingModel.Image6Bathroom = Path.Combine(dir, bathroomImages[5].Image);
            }

            List<ListingImage> basementImages = returnedListing.images.FindAll(image => (image.ListingImageCategory == ListingImageCategoryNames.BASEMENT));

            if (basementImages != null && basementImages.Count > 0)
            {
                virtualListingModel.BasementImages = basementImages.Where(m => m.Image != ApplicationUtility.DefaultNoImageFileName).Select(m => dir + m.Image).ToList();
                if (basementImages[0] != null)
                    virtualListingModel.Image1Basement = Path.Combine(dir, basementImages[0].Image);

                if (basementImages.Count > 1)
                    if (basementImages[1] != null)
                        virtualListingModel.Image2Basement = Path.Combine(dir, basementImages[1].Image);

                if (basementImages.Count > 2)
                    if (basementImages[2] != null)
                        virtualListingModel.Image3Basement = Path.Combine(dir, basementImages[2].Image);

                if (basementImages.Count > 3)
                    if (basementImages[3] != null)
                        virtualListingModel.Image4Basement = Path.Combine(dir, basementImages[3].Image);

                if (basementImages.Count > 4)
                    if (basementImages[4] != null)
                        virtualListingModel.Image5Basement = Path.Combine(dir, basementImages[4].Image);

                if (basementImages.Count > 5)
                    if (basementImages[5] != null)
                        virtualListingModel.Image6Basement = Path.Combine(dir, basementImages[5].Image);
            }

            List<ListingImage> OBedroom = returnedListing.images.FindAll(image => (image.ListingImageCategory == ListingImageCategoryNames.OBEDROOM));

            if (OBedroom != null && OBedroom.Count > 0)
            {
                virtualListingModel.OBedroomImages = OBedroom.Where(m => m.Image != ApplicationUtility.DefaultNoImageFileName).Select(m => dir + m.Image).ToList();
                if (OBedroom[0] != null)
                    virtualListingModel.Image1OBedroom = Path.Combine(dir, OBedroom[0].Image);

                if (OBedroom.Count > 1)
                    if (OBedroom[1] != null)
                        virtualListingModel.Image2OBedroom = Path.Combine(dir, OBedroom[1].Image);

                if (OBedroom.Count > 2)
                    if (OBedroom[2] != null)
                        virtualListingModel.Image3OBedroom = Path.Combine(dir, OBedroom[2].Image);

                if (OBedroom.Count > 3)
                    if (OBedroom[3] != null)
                        virtualListingModel.Image4OBedroom = Path.Combine(dir, OBedroom[3].Image);

                if (OBedroom.Count > 4)
                    if (OBedroom[4] != null)
                        virtualListingModel.Image5OBedroom = Path.Combine(dir, OBedroom[4].Image);

                if (basementImages.Count > 5)
                    if (OBedroom[5] != null)
                        virtualListingModel.Image6OBedroom = Path.Combine(dir, OBedroom[5].Image);
            }

            List<ListingImage> diningImages = returnedListing.images.FindAll(image => (image.ListingImageCategory == ListingImageCategoryNames.DININGROOM));

            if (diningImages != null && diningImages.Count > 0)
            {
                virtualListingModel.DiningImages = diningImages.Where(m => m.Image != ApplicationUtility.DefaultNoImageFileName).Select(m => dir + m.Image).ToList();
                if (diningImages[0] != null)
                    virtualListingModel.Image1Dining = Path.Combine(dir, diningImages[0].Image);

                if (diningImages.Count > 1)
                    if (diningImages[1] != null)
                        virtualListingModel.Image2Dining = Path.Combine(dir, diningImages[1].Image);

                if (diningImages.Count > 2)
                    if (diningImages[2] != null)
                        virtualListingModel.Image3Dining = Path.Combine(dir, diningImages[2].Image);

                if (diningImages.Count > 3)
                    if (diningImages[3] != null)
                        virtualListingModel.Image4Dining = Path.Combine(dir, diningImages[3].Image);

                if (diningImages.Count > 4)
                    if (diningImages[4] != null)
                        virtualListingModel.Image5Dining = Path.Combine(dir, diningImages[4].Image);

                if (diningImages.Count > 5)
                    if (diningImages[5] != null)
                        virtualListingModel.Image6Dining = Path.Combine(dir, diningImages[5].Image);
            }


            List<ListingImage> otherSection1Images = returnedListing.images.FindAll(image => (image.ListingImageCategory == ListingImageCategoryNames.OTHERSECTION1));

            if (otherSection1Images != null && otherSection1Images.Count > 0)
            {
                virtualListingModel.OtherSection1Images = otherSection1Images.Where(m => m.Image != ApplicationUtility.DefaultNoImageFileName).Select(m => dir + m.Image).ToList();
                if (otherSection1Images[0] != null)
                    virtualListingModel.Image1OtherSection1 = Path.Combine(dir, otherSection1Images[0].Image);

                if (otherSection1Images.Count > 1)
                    if (otherSection1Images[1] != null)
                        virtualListingModel.Image2OtherSection1 = Path.Combine(dir, otherSection1Images[1].Image);

                if (otherSection1Images.Count > 2)
                    if (otherSection1Images[2] != null)
                        virtualListingModel.Image3OtherSection1 = Path.Combine(dir, otherSection1Images[2].Image);

                if (otherSection1Images.Count > 3)
                    if (otherSection1Images[3] != null)
                        virtualListingModel.Image4OtherSection1 = Path.Combine(dir, otherSection1Images[3].Image);

                if (otherSection1Images.Count > 4)
                    if (otherSection1Images[4] != null)
                        virtualListingModel.Image5OtherSection1 = Path.Combine(dir, otherSection1Images[4].Image);

                if (otherSection1Images.Count > 5)
                    if (otherSection1Images[5] != null)
                        virtualListingModel.Image6OtherSection1 = Path.Combine(dir, otherSection1Images[5].Image);
            }


            List<ListingImage> otherSection2Images = returnedListing.images.FindAll(image => (image.ListingImageCategory == ListingImageCategoryNames.OTHERSECTION2));

            if (otherSection2Images != null && otherSection2Images.Count > 0)
            {
                virtualListingModel.OtherSection2Images = otherSection2Images.Where(m => m.Image != ApplicationUtility.DefaultNoImageFileName).Select(m => dir + m.Image).ToList();
                if (otherSection2Images[0] != null)
                    virtualListingModel.Image1OtherSection2 = Path.Combine(dir, otherSection2Images[0].Image);

                if (otherSection2Images.Count > 1)
                    if (otherSection2Images[1] != null)
                        virtualListingModel.Image2OtherSection2 = Path.Combine(dir, otherSection2Images[1].Image);

                if (otherSection2Images.Count > 2)
                    if (otherSection2Images[2] != null)
                        virtualListingModel.Image3OtherSection2 = Path.Combine(dir, otherSection2Images[2].Image);

                if (otherSection2Images.Count > 3)
                    if (otherSection2Images[3] != null)
                        virtualListingModel.Image4OtherSection2 = Path.Combine(dir, otherSection2Images[3].Image);

                if (otherSection2Images.Count > 4)
                    if (otherSection2Images[4] != null)
                        virtualListingModel.Image5OtherSection2 = Path.Combine(dir, otherSection2Images[4].Image);

                if (otherSection2Images.Count > 5)
                    if (otherSection2Images[5] != null)
                        virtualListingModel.Image6OtherSection2 = Path.Combine(dir, otherSection2Images[5].Image);
            }

            List<ListingImage> otherSection3Images = returnedListing.images.FindAll(image => (image.ListingImageCategory == ListingImageCategoryNames.OTHERSECTION3));

            if (otherSection3Images != null && otherSection3Images.Count > 0)
            {
                virtualListingModel.OtherSection3Images = otherSection3Images.Where(m => m.Image != ApplicationUtility.DefaultNoImageFileName).Select(m => dir + m.Image).ToList();
                if (otherSection3Images[0] != null)
                    virtualListingModel.Image1OtherSection3 = Path.Combine(dir, otherSection3Images[0].Image);

                if (otherSection3Images.Count > 1)
                    if (otherSection3Images[1] != null)
                        virtualListingModel.Image2OtherSection3 = Path.Combine(dir, otherSection3Images[1].Image);

                if (otherSection3Images.Count > 2)
                    if (otherSection3Images[2] != null)
                        virtualListingModel.Image3OtherSection3 = Path.Combine(dir, otherSection3Images[2].Image);

                if (otherSection3Images.Count > 3)
                    if (otherSection3Images[3] != null)
                        virtualListingModel.Image4OtherSection3 = Path.Combine(dir, otherSection3Images[3].Image);

                if (otherSection3Images.Count > 4)
                    if (otherSection3Images[4] != null)
                        virtualListingModel.Image5OtherSection3 = Path.Combine(dir, otherSection3Images[4].Image);

                if (otherSection3Images.Count > 5)
                    if (otherSection3Images[5] != null)
                        virtualListingModel.Image6OtherSection3 = Path.Combine(dir, otherSection3Images[5].Image);
            }


            List<ListingImage> otherSection4Images = returnedListing.images.FindAll(image => (image.ListingImageCategory == ListingImageCategoryNames.OTHERSECTION4));

            if (otherSection4Images != null && otherSection4Images.Count > 0)
            {
                virtualListingModel.OtherSection4Images = otherSection4Images.Where(m => m.Image != ApplicationUtility.DefaultNoImageFileName).Select(m => dir + m.Image).ToList();
                if (otherSection4Images[0] != null)
                    virtualListingModel.Image1OtherSection4 = Path.Combine(dir, otherSection4Images[0].Image);

                if (otherSection4Images.Count > 1)
                    if (otherSection4Images[1] != null)
                        virtualListingModel.Image2OtherSection4 = Path.Combine(dir, otherSection4Images[1].Image);

                if (otherSection4Images.Count > 2)
                    if (otherSection4Images[2] != null)
                        virtualListingModel.Image3OtherSection4 = Path.Combine(dir, otherSection4Images[2].Image);

                if (otherSection4Images.Count > 3)
                    if (otherSection4Images[3] != null)
                        virtualListingModel.Image4OtherSection4 = Path.Combine(dir, otherSection4Images[3].Image);

                if (otherSection4Images.Count > 4)
                    if (otherSection4Images[4] != null)
                        virtualListingModel.Image5OtherSection4 = Path.Combine(dir, otherSection4Images[4].Image);

                if (otherSection4Images.Count > 5)
                    if (otherSection4Images[5] != null)
                        virtualListingModel.Image6OtherSection4 = Path.Combine(dir, otherSection4Images[5].Image);
            }

            List<ListingImage> VideoImages = returnedListing.images.FindAll(image => (image.ListingImageCategory == ListingImageCategoryNames.VIDEOUPLOAD));

            if (VideoImages != null && VideoImages.Count > 0)
            {
                if (VideoImages[0] != null && VideoImages[0].Image != "Video Place-Holder")
                    virtualListingModel.VideoLink = Path.Combine(dir, VideoImages[0].Image);
                else
                    virtualListingModel.VideoLink = "No Video";
            }

            ListingMessageModel listingMessageModel = new ListingMessageModel();
            listingMessageModel.Listing = virtualListingModel;
            listingMessageModel.Images = returnedListing.images;

            listingMessageModel.Message = new MessageModel();
            listingMessageModel.Message.to = virtualListingModel.UserId.Value;

            if (WebSecurity.IsAuthenticated)
            {
                using (UsersContext db = new UsersContext())
                {
                    UserProfile user = db.UserProfiles.Find(WebSecurity.CurrentUserId);
                    listingMessageModel.Message.UserId = user.FirstName + user.UserId;
                }
            }

            //Generate Public Listing Image

            //TODO: Obviously this method shouldn't be in here, it needs to be in a utility by itself
            ExternalServiceController extController = new ExternalServiceController();
            string lawnSignFileNameTemplate = "VUE{0}Lawnsign.jpg";
            string lawnSignFileName = string.Format(lawnSignFileNameTemplate, lr.ListingId);
            string imagePath = "~/AgentImageResources/";

            virtualListingModel.PublicLawnSignImageUrl = extController.generateImage(lr.ListingId, Server.MapPath(imagePath), lawnSignFileName);
            
            ExternalServiceController.AmazonVideoUploader imageUploader = new ExternalServiceController.AmazonVideoUploader();
           // imageUploader.UploadFile(Server.MapPath(imagePath) + virtualListingModel.PublicLawnSignImageUrl, virtualListingModel.PublicLawnSignImageUrl, true);

            TempData["RedirectUrl"] = System.Configuration.ConfigurationManager.AppSettings["applicationRoot"] + "listing/viewFullListing/" + listingMessageModel.Listing.ListingId;
            TempData["UniqueListId"] = Session["currListingId"];
            ViewBag.RootUrl = System.Configuration.ConfigurationManager.AppSettings["applicationRoot"];
            ViewBag.ImageUrl = System.Configuration.ConfigurationManager.AppSettings["AWSS3BucketURL"];

            return View("FullListing", listingMessageModel);
        }


        public ActionResult FullListing(VirtualListing model)
        {
            int ListingId = Int32.Parse(Session["currListingId"].ToString());
            
            List<ListingImage> PrevlocationImages = ListingManager.GetListingImageByListing(ListingId).Where(x => x.ListingImageCategory == ListingImageCategoryNames.LOCATION).ToList();
            var BuildImageList = BuildEditImageList(null,PrevlocationImages,ListingImageCategoryNames.LOCATION);
            
            try
            {   
                int Id = ListingManager.AddListingDescription(ListingId, model.LocationDesc, ListingImageCategoryNames.LOCATION, BuildImageList);                
            }
            catch (Exception ex)
            {
                var theException = ex;
                return View();
            }
           
            
            VirtualListingDescription desc = new VirtualListingDescription()
            {
                ListingId = model.ListingId,
                Desc = model.LocationDesc,
            };

            var dir = System.Configuration.ConfigurationManager.AppSettings["applicationRoot"] + "ListingImgs/";

            //desc.Image1 = Path.Combine(dir, BuildImageList[0].Image);
            //desc.Image2 = Path.Combine(dir, BuildImageList[1].Image);
            //desc.Image3 = Path.Combine(dir, BuildImageList[2].Image);
            //desc.Image4 = Path.Combine(dir, BuildImageList[3].Image);

            return Json(desc, "json");

        }

        private List<ListingImage> BuildEditImageList(List<HttpPostedFileBase> Images, List<ListingImage> prevImages, string imageCategory)
        {
            VirtualListing emptyListingWithDefaults = new VirtualListing();

            string image1FilePath = emptyListingWithDefaults.Image1Location;
            string image2FilePath = emptyListingWithDefaults.Image1Location;
            string image3FilePath = emptyListingWithDefaults.Image1Location;
            string image4FilePath = emptyListingWithDefaults.Image1Location;
            string image5FilePath = emptyListingWithDefaults.Image1Location;
            string image6FilePath = emptyListingWithDefaults.Image1Location;

            List<ListingImage> NewBuildImages = new List<ListingImage>();
            ListingImageCategory imgCategoryType = ListingManager.GetListingImageCategory(imageCategory);

            if (imageCategory == "Video Upload")//Remove previous video from server
            {
                if (prevImages != null)
                    if (prevImages.Count > 0)
                        if (prevImages[0].Image != "Video Place-Holder" && prevImages[0].Image != "" && prevImages[0].Image != ApplicationUtility.DefaultNoImageFileName)
                        {
                            HOD.Controllers.ExternalServiceController.AmazonVideoUploader deleteFile = new ExternalServiceController.AmazonVideoUploader();
                            deleteFile.DeleteFile(prevImages[0].Image);
                        }
            }
            for (int count = 0; count < 6; count++)
            {
                HttpPostedFileBase currentImage;

                if (Images.Count >= count + 1)
                {
                    currentImage = Images[count];
                }
                else
                {
                    currentImage = null;
                }

                if (currentImage != null)
                {
                    if (currentImage.FileName != null)
                    {
                        if (!(prevImages[count].Image.Contains(ApplicationUtility.DefaultNoImageFileName)))
                        {
                            HOD.Controllers.ExternalServiceController.AmazonVideoUploader deleteFile = new ExternalServiceController.AmazonVideoUploader();
                            deleteFile.DeleteFile(prevImages[count].Image);
                        }
                        ListingImage theImage = SaveAndCreateImage(imgCategoryType, currentImage);

                        if (count == 0) image1FilePath = theImage.Image;
                        else if (count == 1) image2FilePath = theImage.Image;
                        else if (count == 2) image3FilePath = theImage.Image;
                        else if (count == 3) image4FilePath = theImage.Image;
                        else if (count == 4) image5FilePath = theImage.Image;
                        else if (count == 5) image6FilePath = theImage.Image;
                    }
                }
                else
                {
                    if (prevImages.Count > 0)
                    {
                        if (count == 0)
                            image1FilePath = prevImages[0].Image;
                        if (count == 1)
                            image2FilePath = prevImages[1].Image;
                        if (count == 2)
                            image3FilePath = prevImages[2].Image;
                        if (count == 3)
                            image4FilePath = prevImages[3].Image;
                        if (count == 4)
                            image5FilePath = prevImages[4].Image;
                        if (count == 5)
                            image6FilePath = prevImages[5].Image;
                    }
                    else
                    {
                        if (count == 0)
                            image1FilePath = "Video Place-Holder";
                        if (count == 1)
                            image2FilePath = "Video Place-Holder";
                        if (count == 2)
                            image3FilePath = "Video Place-Holder";
                        if (count == 3)
                            image4FilePath = "Video Place-Holder";
                        if (count == 4)
                            image5FilePath = "Video Place-Holder";
                        if (count == 5)
                            image6FilePath = "Video Place-Holder";
                    }
                }
            }
                        
            NewBuildImages.Add(new ListingImage { Image = image1FilePath, ListingImageCategory = imageCategory });
            NewBuildImages.Add(new ListingImage { Image = image2FilePath, ListingImageCategory = imageCategory });
            NewBuildImages.Add(new ListingImage { Image = image3FilePath, ListingImageCategory = imageCategory });
            NewBuildImages.Add(new ListingImage { Image = image4FilePath, ListingImageCategory = imageCategory });
            NewBuildImages.Add(new ListingImage { Image = image5FilePath, ListingImageCategory = imageCategory });
            NewBuildImages.Add(new ListingImage { Image = image6FilePath, ListingImageCategory = imageCategory });                   

            return NewBuildImages;
        }

        [HttpPost]
        [Authorize()]
        public ActionResult SaveMainSummary(List<HttpPostedFileBase> ImagesVirtualSign, VirtualListing model)
        {
            int ageOfHome = 0;
            model.UserId = WebSecurity.CurrentUserId;

            if (!int.TryParse(model.AgeOfHome.ToString(), out ageOfHome))
            {
                ModelState.AddModelError("AgeOfHome", "Age of home must be a number and is required");
            }

            if (String.IsNullOrEmpty(model.MainSellingDesc))
            {
                ModelState.AddModelError("MainSellingDesc", "Main Selling description must be entered");
            }
            
            if (ModelState.IsValid == false)
            {
                foreach (ModelState modelState in ViewData.ModelState.Values)
                {
                    foreach (ModelError error in modelState.Errors)
                    {
                        String errr = error.ErrorMessage;
                        Exception exxx = error.Exception;
                    }
                }

                return View("ListingTemplate",model);
            }
            
            try
            {
                ListingImage ImagePlaceHolder = new ListingImage();

                string image1FilePath = string.Empty;
                string image2FilePath = string.Empty;
                string image3FilePath = string.Empty;
                string image4FilePath = string.Empty;
                string image5FilePath = string.Empty;
                string image6FilePath = string.Empty;
                string tollFreeNumber = string.Empty;

                ListingImageCategory imageCategory = ListingManager.GetListingImageCategory(ListingImageCategoryNames.VIRTUALSIGN);
                Listing VirtualEditedList = new Listing();

                string listingId = model.ListingId.ToString();
                string vueCodeText = "vue";
                int startPoint = listingId.IndexOf(vueCodeText, StringComparison.OrdinalIgnoreCase);
                listingId = listingId.Substring(startPoint + vueCodeText.Length, listingId.Length - (startPoint + vueCodeText.Length));

                List<ListingImage> images = null;// CreateListingUploads(model, imageCategory);
                 

                VirtualEditedList.ListingId = Convert.ToInt32(listingId);
                VirtualEditedList.ListPrice = model.ListPrice;
                VirtualEditedList.LocationDesc = model.LocationDesc;
                VirtualEditedList.MainSellingDesc = model.MainSellingDesc;
                VirtualEditedList.NoOfBathrooms = model.NoOfBathrooms;
                VirtualEditedList.NoOfBedrooms = model.NoOfBedrooms;
                VirtualEditedList.NoOfFloors = model.NoOfFloors;
                VirtualEditedList.hasBasement = model.hasBasement;
                VirtualEditedList.hasFinishedBasement = model.hasFinishedBasement;
                VirtualEditedList.hasGarage = model.hasGarage;
                VirtualEditedList.hasAC = model.hasAC;
                VirtualEditedList.AnnualTaxes = model.annualTaxes;
                VirtualEditedList.PostalCode = model.PostalCode;
                VirtualEditedList.PropertyType = model.PropertyType;
                VirtualEditedList.PropertyStatus = model.PropertyStatus;
                VirtualEditedList.Province = model.Province;
                VirtualEditedList.SquareFootage = model.SquareFootage;
                VirtualEditedList.AgeOfHome = model.AgeOfHome;
                VirtualEditedList.HeatType = model.heatType; 

                var db = new HOD.Data.Entities.HODContext();
                //db.Entry(VirtualEditedList).State = System.Data.EntityState.Modified;  
                db.SaveChanges();

                //ListingManager.UpdateMainSummarySection(VirtualEditedList, images);

                return RedirectToAction("edit", routeValues: new { id = model.ListingId });
            }
            catch (Exception ex)
            {
                var theException = ex;
                //FIXME: Change to return the error page view.
                return View("error", ViewBag.Error = ex.Message + " " + ex.StackTrace);
            }
        }

        private string RetrieveImageId(string imgURL){

            string img = imgURL;
            string textToRemove = "ListingImgs/";
            int startPoint = img.IndexOf(textToRemove, StringComparison.OrdinalIgnoreCase) + textToRemove.Length;
            string imageId = img.Substring(startPoint, img.Length - (startPoint));

            return imageId;
        }

        [HttpPost]
        [Authorize]
        public ActionResult SaveListingImages(FormCollection frm)
        {
            bool isSavedSuccessfully = true;
            string fileNameAWS = "";
            string aviaryImagePath = "";
            int listingImageId = 0;
            //int ImageCategoryId = Convert.ToInt32(frm["ImageCategoryId"]);

            //ListingImageContext 
            int existingImageCount = this.GetListingImagesCount(Convert.ToInt32(frm["ImageListingId"]));
            if (frm["maxFileExceed"] == "yes")
            {
                return Json(new { Message = "Max file limit exceeded. Please upgrade you account to upload more images" });
            }

            var dbconn = new HOD.Managers.DbManager();
            try
            {
                foreach (string fileName in Request.Files)
                {
                    HttpPostedFileBase file = Request.Files[fileName];
                    //Save file content goes here
                    //fName = file.FileName;
                    if (file != null && file.ContentLength > 0)
                    {

                        var originalDirectory = new DirectoryInfo(string.Format("{0}Images", Server.MapPath(@"\")));

                        string pathString = System.IO.Path.Combine(originalDirectory.ToString(), "ListingImages");

                        var fileName1 = Path.GetFileName(file.FileName);

                        bool isExists = System.IO.Directory.Exists(pathString);

                        if (!isExists)
                            System.IO.Directory.CreateDirectory(pathString);

                        var path = string.Format("{0}\\{1}", pathString, file.FileName);
                        file.SaveAs(path);

                        //Save to amazon repo
                        HOD.Controllers.ExternalServiceController.AmazonVideoUploader listingImageAWS = new ExternalServiceController.AmazonVideoUploader();

                        string CurrentPath = Server.MapPath("~/Images/ListingImages/") + fileName1;

                        fileNameAWS = listingImageAWS.UploadFile(CurrentPath, fileName1);

                        var li = new HOD.Data.Entities.ListingImage()
                        {
                            Image = fileName1,
                            ListingId = Convert.ToInt32(frm["ImageListingId"]),
                            ListingImageCategoryId = Convert.ToInt32(frm["ImageCategoryId"])
                            //MessageDirection = messageDirection                    
                        };

                        listingImageId = dbconn.saveListingImage(li);

                    }

                }

            }
            catch (Exception ex)
            {
                isSavedSuccessfully = false;
            }


            if (isSavedSuccessfully)
            {
                return Json(new { name = fileNameAWS, imgID = "img" + listingImageId, ListingId = listingImageId });
            }
            else
            {
                return Json(new { Message = "Error in saving file" });
            }
        }

        //Save IMage from Aviary Update
        [HttpPost]
        [Authorize]
        public JsonResult  SaveAndUpdateListingImage()
        {
            string postdata = Request["postdata"]; // an encoded reference to the file that you're updating perhaps
            string url = Request["url"]; // the url of the saved file
            string currentImageName = Request["currentImageName"];

            
           
            
            string localFileDest = Server.MapPath("~/Images/ListingImages/"); // file save destination
            System.IO.File.Delete(localFileDest + currentImageName);
            string remoteImgPath = Request["url"];
            Uri remoteImgPathUri = new Uri(remoteImgPath);
            string remoteImgPathWithoutQuery = remoteImgPathUri.GetLeftPart(UriPartial.Path);
            string fileName = Path.GetFileName(remoteImgPathWithoutQuery);
            string localPath = localFileDest + currentImageName;
            WebClient webClient = new WebClient();
            webClient.DownloadFile(remoteImgPath, localPath);
            
            String updatedImageUrl = localFileDest;

            HOD.Controllers.ExternalServiceController.AmazonVideoUploader videoAndImageUpdate = new ExternalServiceController.AmazonVideoUploader();
            if (!(currentImageName.Contains(ApplicationUtility.DefaultNoImageFileName)))
            {
                videoAndImageUpdate.DeleteFile(currentImageName);
            }
           videoAndImageUpdate.UploadFile(localPath, currentImageName);
            
            return Json(new { Message = currentImageName });

        }

        //get all listing in dropzone
        [Authorize]
        public JsonResult GetListingImages(int ListingId)
        {
            ListingRequest lr = new ListingRequest();
            lr.ListingId = ListingId;

            Listing returnedListing;
            returnedListing = ListingManager.GetListingData(lr.ListingId);

            List<ListingImage> listingImages = returnedListing.images.FindAll(image => (image.ListingImageCategory == ListingImageCategoryNames.LOCATION || image.ListingImageCategory == ListingImageCategoryNames.VIRTUALSIGN));
           
            return Json(new { Data = listingImages }, JsonRequestBehavior.AllowGet);
            
        }


        //get all listing in dropzone
        [Authorize]
        private int GetListingImagesCount(int ListingId)
        {
            ListingRequest lr = new ListingRequest();
            lr.ListingId = ListingId;

            Listing returnedListing;
            returnedListing = ListingManager.GetListingData(lr.ListingId);

            List<ListingImage> listingImages = returnedListing.images.FindAll(image => (image.ListingImageCategory == ListingImageCategoryNames.LOCATION || image.ListingImageCategory == ListingImageCategoryNames.VIRTUALSIGN));

            return listingImages.Count;

        }

        //Remove Listing Image
        [HttpPost]
        public JsonResult RemoveListingImage()
        {
            int listingImageId = Convert.ToInt32(Request["ListingImageId"]); // an encoded reference to the file that you're updating perhaps

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["HODContext2"].ConnectionString);

            SqlCommand cmd = new SqlCommand("DELETE FROM ListingImages WHERE ListingImageId="+listingImageId,conn);
            conn.Open();
            cmd.ExecuteNonQuery();
            
            conn.Close();
            conn.Dispose();

            return Json(new { Msg = "DONE" }, JsonRequestBehavior.AllowGet);

        }

        //[HttpPost]
        //public ActionResult Edit(List<HttpPostedFileBase> ImagesVirtualSign, VirtualListing model)
        //{

        //    #region Check Model Validity
        //    //if (ModelState.IsValid == false)
        //    //{
        //    //    foreach (ModelState modelState in ViewData.ModelState.Values)
        //    //    {
        //    //        foreach (ModelError error in modelState.Errors)
        //    //        {
        //    //            String errr = error.ErrorMessage;
        //    //            Exception exxx = error.Exception;
        //    //        }
        //    //    }

        //    //    return View("Update", model);
        //    //}
        //    #endregion

        //   //string image1FilePath = string.Empty;
        //   //string image2FilePath = string.Empty;
        //   //string image3FilePath = string.Empty;
        //   //string image4FilePath = string.Empty;

        //    ListingImageCategory imageCategory = ListingManager.GetListingImageCategory(ListingImageCategoryNames.VIRTUALSIGN);
        //   // List<ListingImage> images = new List<ListingImage>();
        //    //VirtualListing defaultVirtualList = new VirtualListing();

        //    ////List<ListingImage> images = CreateListingUploads(model, imageCategory);
        //    //List<ListingImage> images = null;
        //    try
        //    {
        //        Listing VirtualEditedList = new Listing();
        //        var listingPerson = WebSecurity.CurrentUserId;

        //        VirtualEditedList.Address = model.Address;
        //        VirtualEditedList.City = model.City;
        //        VirtualEditedList.ClientUrl = model.CientUrl;
        //        VirtualEditedList.ContactPerson = model.ContactPerson;
        //        VirtualEditedList.DontShowListPrice = model.DontShowListPrice;
        //        VirtualEditedList.DontShowPhoneNo = model.DontShowPhoneNo;
        //        VirtualEditedList.DontShowSquareFootage = model.DontShowSquareFootage;
        //        VirtualEditedList.isPrivateSeller = model.PrivateSellerOption;
        //        VirtualEditedList.KitchenDesc = model.KitchenDesc;
        //        VirtualEditedList.ListingId = Convert.ToInt32(model.ListingId);
        //        VirtualEditedList.ListingPerson = listingPerson.ToString();
        //        VirtualEditedList.ListPrice = model.ListPrice;
        //        VirtualEditedList.LocationDesc = model.LocationDesc;
        //        VirtualEditedList.MainSellingDesc = model.MainSellingDesc;
        //        VirtualEditedList.NoOfBathrooms = model.NoOfBathrooms;
        //        VirtualEditedList.NoOfBedrooms = model.NoOfBedrooms;
        //        VirtualEditedList.PhoneNo = model.PhoneNo;
        //        VirtualEditedList.PostalCode = model.PostalCode;
        //        VirtualEditedList.NeighbourHood = model.Neighbourhood;
        //        VirtualEditedList.PropertyType = model.PropertyType;
        //        VirtualEditedList.PropertyStatus = model.PropertyStatus;
        //        VirtualEditedList.Province = model.Province;
        //        VirtualEditedList.SquareFootage = model.SquareFootage;
        //        VirtualEditedList.ListingId = Convert.ToInt32(model.ListingId);
                

        //        string propertyDetailsSelected = String.Join(",", Request.Form["propertydetails[]"]);

        //        if (propertyDetailsSelected == null)
        //            propertyDetailsSelected = "None";
        //        VirtualEditedList.MainSellingDesc = model.MainSellingDesc+"<=>"+propertyDetailsSelected;

        //        //ListingManager.UpdateVirtualSign(VirtualEditedList, images);

        //        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["HODContext2"].ConnectionString);

        //        SqlCommand cmd = new SqlCommand("UPDATE Listings SET Address='" + model.Address + "' WHERE ListingId=" + VirtualEditedList.ListingId, conn);
        //        conn.Open();
        //        cmd.ExecuteNonQuery();
        //        conn.Close();
        //        conn.Dispose();



        //        return RedirectToAction("ListingTemplate", routeValues: new { id = model.ListingId });
        //    }
        //    catch (Exception ex)
        //    {
        //        var theException = ex;
        //        //FIXME: Change to return the error page view.
        //        return View("error", ViewBag.Error = ex.Message + " " + ex.StackTrace);
        //    }
        //}

        [HttpPost]
        public ActionResult Edit(FormCollection form)
        {

            try
            { 
                var listingPerson = WebSecurity.CurrentUserId;
                int ListingId = Convert.ToInt32(form["ListingId"].ToString());
                string propertyDetailsSelected = String.Join(",", Request.Form["propertydetails[]"]);

                if (propertyDetailsSelected == null)
                    propertyDetailsSelected = "None";
                string MainSellingDesc = form["MainSellingDesc"].ToString() + "<=>" + propertyDetailsSelected;

                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["HODContext2"].ConnectionString);

                SqlCommand cmd = new SqlCommand("UPDATE Listings SET  " + "Address='" + form["Address"].ToString() + "', City='" + form["City"].ToString() + "',  ListPrice='" + form["ListPrice"].ToString() + "', NoOfBathrooms='" + form["NoOfBathrooms"].ToString() + "', NoOfBedrooms='" + form["NoOfBedrooms"].ToString() + "', PhoneNo='" + form["PhoneNo"].ToString() + "', PostalCode='" + form["PostalCode"].ToString() + "', PropertyType='" + form["PropertyType"].ToString() + "', PropertyStatus='" + form["PropertyStatus"].ToString() + "', SquareFootage='" + form["SquareFootage"].ToString() + "', MainSellingDesc='" + MainSellingDesc + "'" + " WHERE ListingId=" + ListingId, conn);
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
                conn.Dispose();

                return RedirectToAction("viewFullListing/vue0" + ListingId, "Listing");
            }
            catch (Exception ex)
            {
                var theException = ex;
                //FIXME: Change to return the error page view.
                return View("error", ViewBag.Error = ex.Message + " " + ex.StackTrace);
            }
        }

        [HttpPost]
        public ActionResult SetSectionVisible(string section, string value)
        {
            //insert into ListingCatSection Table
            if (!Request.IsAjaxRequest())
                return Json(new {response ="error" });

            if (Session["currListingId"] == null)
                return Json(new {response ="error" });

            int listingId = int.Parse(Session["currListingId"].ToString());
            
            try
            {
                ListingManager.AddListingSection(listingId, section, Convert.ToBoolean(value), WebSecurity.CurrentUserId);
            }
            catch (Exception ex)
            {
                this.LogException(ex);
                return Json(new { response = "error" });
            }

            return Json(new { response="success"});
        }

        //[ListingOwnerAttribute]
        [HttpPost]
        public ActionResult EditSection(List<HttpPostedFileBase> Images, VirtualListing model)
        {

            try
            {

                ListingRequest lr;

                IMessageManagerUtility mmu = new MessageManagerUtility();

                if (model.ListingId.ToLower().Contains("vue"))
                {
                    lr = mmu.ParseRequest(model.ListingId);
                }
                else
                {
                    lr = new ListingRequest(); 
                    lr.ListingId = int.Parse(model.ListingId);
                }

                int ListingId = lr.ListingId;

                string section = model.CurrentSection;
                string description = string.Empty;
                List<ListingImage> SectionImages = new List<ListingImage>();
                List<ListingImage> BuildImageList = new List<ListingImage>();
                int imgCounter = 0;

                List<HttpPostedFileBase> uploadedImages = new List<HttpPostedFileBase>();
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    uploadedImages.Add(Request.Files[i]);
                    imgCounter++;
                }

                switch (model.CurrentSection)
                {
                    case "kitchen": section = ListingImageCategoryNames.KITCHEN; description = String.IsNullOrEmpty(model.KitchenDesc) ? "" : model.KitchenDesc  ; SectionImages = BuildSectionImages(model.Image1Kitchen, model.Image2Kitchen, model.Image3Kitchen, model.Image4Kitchen, model.Image5Kitchen, model.Image6Kitchen, section);
                        break;
                    case "location": section = ListingImageCategoryNames.LOCATION; description = String.IsNullOrEmpty(model.LocationDesc) ? "" : model.LocationDesc; SectionImages = BuildSectionImages(model.Image1Location, model.Image2Location, model.Image3Location, model.Image4Location, model.Image5Location, model.Image6Location, section);
                        break;
                    case "diningroom": section = ListingImageCategoryNames.DININGROOM; description = String.IsNullOrEmpty(model.DiningDesc) ? "" : model.DiningDesc; SectionImages = BuildSectionImages(model.Image1Dining, model.Image2Dining, model.Image3Dining, model.Image4Dining, model.Image5Dining, model.Image6Dining, section);
                        break;
                    case "familyroom": section = ListingImageCategoryNames.FAMILYROOM; description = String.IsNullOrEmpty(model.FamilyDesc) ? "" : model.FamilyDesc; SectionImages = BuildSectionImages(model.Image1Family, model.Image2Family, model.Image3Family, model.Image4Family, model.Image5Family, model.Image6Family, section);
                        break;
                    case "otherbedroom": section = ListingImageCategoryNames.OBEDROOM; description = String.IsNullOrEmpty(model.OBedroomDesc) ? "" : model.OBedroomDesc; SectionImages = BuildSectionImages(model.Image1OBedroom, model.Image2OBedroom, model.Image3OBedroom, model.Image4OBedroom, model.Image5OBedroom, model.Image6OBedroom, section);
                        break;
                    case "basement": section = ListingImageCategoryNames.BASEMENT; description = String.IsNullOrEmpty(model.BasementDesc) ? "" : model.BasementDesc; SectionImages = BuildSectionImages(model.Image1Basement, model.Image2Basement, model.Image3Basement, model.Image4Basement, model.Image5Basement, model.Image6Basement, section);
                        break;
                    case "bathroom": section = ListingImageCategoryNames.BATHROOM; description = String.IsNullOrEmpty(model.BathroomDesc) ? "" : model.BathroomDesc; SectionImages = BuildSectionImages(model.Image1Bathroom, model.Image2Bathroom, model.Image3Bathroom, model.Image4Bathroom, model.Image5Bathroom, model.Image6Bathroom, section);
                        break;
                    case "othersection1": section = ListingImageCategoryNames.OTHERSECTION1; description = String.IsNullOrEmpty(model.OtherSection1Desc) ? "" : model.OtherSection1Desc; SectionImages = BuildSectionImages(model.Image1OtherSection1, model.Image2OtherSection1, model.Image3OtherSection1, model.Image4OtherSection1, model.Image5OtherSection1, model.Image6OtherSection1, section);
                        break;
                    case "othersection2": section = ListingImageCategoryNames.OTHERSECTION2; description = String.IsNullOrEmpty(model.OtherSection2Desc) ? "" : model.OtherSection2Desc; SectionImages = BuildSectionImages(model.Image1OtherSection2, model.Image2OtherSection2, model.Image3OtherSection2, model.Image4OtherSection2, model.Image5OtherSection2, model.Image6OtherSection2, section);
                        break;
                    case "othersection3": section = ListingImageCategoryNames.OTHERSECTION3; description = String.IsNullOrEmpty(model.OtherSection3Desc) ? "" : model.OtherSection3Desc; SectionImages = BuildSectionImages(model.Image1OtherSection3, model.Image2OtherSection3, model.Image3OtherSection3, model.Image4OtherSection3, model.Image5OtherSection3, model.Image6OtherSection3, section);
                        break;
                    case "othersection4": section = ListingImageCategoryNames.OTHERSECTION4; description = String.IsNullOrEmpty(model.OtherSection4Desc) ? "" : model.OtherSection4Desc; SectionImages = BuildSectionImages(model.Image1OtherSection4, model.Image2OtherSection4, model.Image3OtherSection4, model.Image4OtherSection4, model.Image5OtherSection4, model.Image6OtherSection4, section);
                        break;
                    case "VideoUpload": section = ListingImageCategoryNames.VIDEOUPLOAD; description = String.IsNullOrEmpty(model.VideoDesc) ? "" : model.VideoDesc;
                        break;
                }


                List<ListingImage> PrevImages = ListingManager.GetListingImageByListing(ListingId).Where(x => x.ListingImageCategory == section).ToList();

                if (section == "Video Upload")
                {
                    BuildImageList = BuildEditImageList(uploadedImages, PrevImages, section);
                }
                else
                {
                    BuildImageList = BuildEditSectionImageList(SectionImages, PrevImages, section);
                }
                int Id = ListingManager.AddListingDescription(ListingId, description, section, BuildImageList);

                VirtualListingDescription desc = new VirtualListingDescription()
                {
                    ListingId = model.ListingId,
                    Desc = description,
                };

                string imagePath = System.Configuration.ConfigurationManager.AppSettings["AWSS3BucketURL"];
                if (section == "Video Upload")
                {
                    desc.Image1 = imagePath + BuildImageList[0].Image;
                    //Video Placeholder

                }
                else
                {
                    desc.Image1 = imagePath + BuildImageList[0].Image;
                    desc.Image2 = imagePath + BuildImageList[1].Image;
                    desc.Image3 = imagePath + BuildImageList[2].Image;
                    desc.Image4 = imagePath + BuildImageList[3].Image;
                    desc.Image5 = imagePath + BuildImageList[4].Image;
                    desc.Image6 = imagePath + BuildImageList[5].Image;
                }

                System.Threading.Thread.Sleep(1000);
                return Json(desc, "json");

            } catch (Exception ex){

                this.LogException(ex);
                Response.StatusCode = 500;
                return Json(new object());
            }
        }

        private List<ListingImage> BuildEditSectionImageList(List<ListingImage> CurrentImages, List<ListingImage> PrevImages, string section)
        {
            List<ListingImage> newList = new List<ListingImage>();

            if (CurrentImages.Count == PrevImages.Count)
            {
                for (int i = 0; i < PrevImages.Count; i++)
                {
                    if (PrevImages[i].Image != CurrentImages[i].Image)
                    {
                       newList.Add(SaveAndUpdateImageListing(section, CurrentImages[i].Image, PrevImages[i].Image));
                    }
                    else
                    {
                        newList.Add(CurrentImages[i]);
                    }
                }
            }

            return newList;
        }
        private ListingImage SaveAndUpdateImageListing(string imageCategory, string currentImageName, string PreviousFileName)
        {
            HOD.Controllers.ExternalServiceController.AmazonVideoUploader videoAndImageUpdate = new ExternalServiceController.AmazonVideoUploader();
            string fileName = "";
            string CurrentPath = Server.MapPath("~/Images/") + currentImageName;
            if (!(PreviousFileName.Contains(ApplicationUtility.DefaultNoImageFileName)))
            {
                videoAndImageUpdate.DeleteFile(PreviousFileName);
            }

            fileName = videoAndImageUpdate.UploadFile(CurrentPath, currentImageName);

            ListingImage theImage = new ListingImage();
            theImage.ListingImageCategory = imageCategory;
            theImage.Image = fileName;

            return theImage;
        }
        private List<ListingImage> BuildSectionImages(string image1, string image2, string image3, string image4, string image5, string image6, string section)
        {
            List<ListingImage> imageList = new List<ListingImage>();
            ListingImage image = null;

            image = new ListingImage();
            image.Image= Path.GetFileName(image1);
            image.ListingImageCategory = section;
            imageList.Add(image);

            image = new ListingImage();
            image.Image = Path.GetFileName(image2);
            image.ListingImageCategory = section;
            imageList.Add(image);

            image = new ListingImage();
            image.Image = Path.GetFileName(image3);
            image.ListingImageCategory = section;
            imageList.Add(image);

            image = new ListingImage();
            image.Image = Path.GetFileName(image4);
            image.ListingImageCategory = section;
            imageList.Add(image);

            image = new ListingImage();
            image.Image = Path.GetFileName(image5);
            image.ListingImageCategory = section;
            imageList.Add(image);

            image = new ListingImage();
            image.Image = Path.GetFileName(image6);
            image.ListingImageCategory = section;
            imageList.Add(image);

            return imageList;
        }

        private VirtualListing populateListingModel(int? id)
        {

            VirtualListing virtualListingModel = null;
            Listing returnedListing = null;

                try
                {
                    returnedListing = ListingManager.GetListingData(id.Value);

                    if(returnedListing == null)
                        throw new ApplicationException("Listing does not exist");
                }
                catch (MessageManagerExceptions.InvalidListingRequestException mEx)
                {
                     throw mEx;
                }

                var dir = System.Configuration.ConfigurationManager.AppSettings["applicationRoot"] + "ListingImgs/";

                string listingId = "vue" + id.Value.ToString("D5");

                virtualListingModel = new VirtualListing()
                {
                    CientUrl = returnedListing.ClientUrl,
                    ContactPerson = returnedListing.ContactPerson,
                    DontShowListPrice = returnedListing.DontShowListPrice,
                    DontShowPhoneNo = returnedListing.DontShowPhoneNo,
                    DontShowSquareFootage = returnedListing.DontShowSquareFootage,
                    ListPrice = returnedListing.DontShowListPrice == true ? 0 : returnedListing.ListPrice,
                    PhoneNo = (returnedListing.DontShowPhoneNo == true) || (returnedListing.isPrivateSeller == true) ? "844-CASAVUE" : returnedListing.PhoneNo,//"<Phone number hidden>"
                    SquareFootage = returnedListing.DontShowSquareFootage == true ? 0 : returnedListing.SquareFootage,
                    NoOfBedrooms = returnedListing.NoOfBedrooms,
                    NoOfBathrooms = returnedListing.NoOfBathrooms,
                    PropertyType = returnedListing.PropertyType,
                    LocationDesc = returnedListing.LocationDesc,
                    KitchenDesc = returnedListing.KitchenDesc,
                    DiningDesc = returnedListing.DiningRoomDesc,
                    BedroomDesc = returnedListing.BedroomDesc,
                    FamilyDesc = returnedListing.FamilyRoomDesc,
                    BasementDesc = returnedListing.BasementDesc,
                    BathroomDesc = returnedListing.BathroomDesc,

                    ListingPerson = returnedListing.ContactPerson,//returnedListing.ListingPerson,
                    MainSellingDesc = returnedListing.MainSellingDesc,
                    ListingId = listingId,
                    PrivateSellerOption = returnedListing.isPrivateSeller,

                    Address = returnedListing.Address,
                    City = returnedListing.City,
                    Province = returnedListing.Province,
                    PostalCode = returnedListing.PostalCode,

                    isBathroomPublic = ListingManager.GetListingSection((int)id, ListingImageCategoryNames.BATHROOM).Value,
                    isKitchenPublic = ListingManager.GetListingSection((int)id, ListingImageCategoryNames.KITCHEN).Value,
                    isLocationPublic = ListingManager.GetListingSection((int)id, ListingImageCategoryNames.LOCATION).Value,
                    isBedroomPublic = ListingManager.GetListingSection((int)id, ListingImageCategoryNames.OBEDROOM).Value,
                    isFamilyRoomPublic =ListingManager.GetListingSection((int)id, ListingImageCategoryNames.FAMILYROOM).Value,
                    isBasementPublic = ListingManager.GetListingSection((int)id, ListingImageCategoryNames.BASEMENT).Value
                };

                Session.Add("currListingId", id);
                if (returnedListing.images != null)
                {
                    List<ListingImage> listingImages = returnedListing.images.FindAll(image => (image.ListingImageCategory == ListingImageCategoryNames.VIRTUALSIGN));

                    if (listingImages != null && listingImages.Count > 0)
                    {
                        if (listingImages[0].Image != null)
                            virtualListingModel.Image1VirtualSign = Path.Combine(dir, listingImages[0].Image);

                        if (listingImages.Count > 1)
                            if (listingImages[1].Image != null)
                                virtualListingModel.Image2VirtualSign = Path.Combine(dir, listingImages[1].Image);

                        if (listingImages.Count > 2)
                            if (listingImages[2].Image != null)
                                virtualListingModel.Image3VirtualSign = Path.Combine(dir, listingImages[2].Image);

                        if (listingImages.Count > 3)
                            if (listingImages[3].Image != null)
                                virtualListingModel.Image4VirtualSign = Path.Combine(dir, listingImages[3].Image);
                    }

                    List<ListingImage> locationImages = returnedListing.images.FindAll(image => (image.ListingImageCategory == ListingImageCategoryNames.LOCATION));

                    if (locationImages != null && locationImages.Count > 0)
                    {
                        if (locationImages[0] != null)
                            virtualListingModel.Image1Location = Path.Combine(dir, locationImages[0].Image);

                        if (locationImages.Count > 1)
                            if (locationImages[1] != null)
                                virtualListingModel.Image2Location = Path.Combine(dir, locationImages[1].Image);

                        if (locationImages.Count > 2)
                            if (locationImages[2] != null)
                                virtualListingModel.Image3Location = Path.Combine(dir, locationImages[2].Image);

                        if (locationImages.Count > 3)
                            if (locationImages[3] != null)
                                virtualListingModel.Image4Location = Path.Combine(dir, locationImages[3].Image);
                    }

                    List<ListingImage> kitchenImages = returnedListing.images.FindAll(image => (image.ListingImageCategory == ListingImageCategoryNames.KITCHEN));

                    if (kitchenImages != null && kitchenImages.Count > 0)
                    {
                        if (kitchenImages[0] != null)
                            virtualListingModel.Image1Kitchen = Path.Combine(dir, kitchenImages[0].Image);

                        if (kitchenImages.Count > 1)
                            if (kitchenImages[1] != null)
                                virtualListingModel.Image2Kitchen = Path.Combine(dir, kitchenImages[1].Image);

                        if (kitchenImages.Count > 2)
                            if (kitchenImages[2] != null)
                                virtualListingModel.Image3Kitchen = Path.Combine(dir, kitchenImages[2].Image);

                        if (kitchenImages.Count > 3)
                            if (kitchenImages[3] != null)
                                virtualListingModel.Image4Kitchen = Path.Combine(dir, kitchenImages[3].Image);
                    }

                    List<ListingImage> bedroomImages = returnedListing.images.FindAll(image => (image.ListingImageCategory == ListingImageCategoryNames.OBEDROOM));

                    if (bedroomImages != null && bedroomImages.Count > 0)
                    {
                        if (bedroomImages[0] != null)
                            virtualListingModel.Image1Bedroom = Path.Combine(dir, bedroomImages[0].Image);

                        if (bedroomImages.Count > 1)
                            if (bedroomImages[1] != null)
                                virtualListingModel.Image2Bedroom = Path.Combine(dir, bedroomImages[1].Image);

                        if (bedroomImages.Count > 2)
                            if (bedroomImages[2] != null)
                                virtualListingModel.Image3Bedroom = Path.Combine(dir, bedroomImages[2].Image);

                        if (bedroomImages.Count > 3)
                            if (bedroomImages[3] != null)
                                virtualListingModel.Image4Bedroom = Path.Combine(dir, bedroomImages[3].Image);
                    }

                    List<ListingImage> basementImages = returnedListing.images.FindAll(image => (image.ListingImageCategory == ListingImageCategoryNames.BASEMENT));

                    if (basementImages != null && basementImages.Count > 0)
                    {
                        if (basementImages[0] != null)
                            virtualListingModel.Image1Basement = Path.Combine(dir, basementImages[0].Image);

                        if (basementImages.Count > 1)
                            if (basementImages[1] != null)
                                virtualListingModel.Image2Basement = Path.Combine(dir, basementImages[1].Image);

                        if (basementImages.Count > 2)
                            if (basementImages[2] != null)
                                virtualListingModel.Image3Basement = Path.Combine(dir, basementImages[2].Image);

                        if (basementImages.Count > 3)
                            if (basementImages[3] != null)
                                virtualListingModel.Image4Basement = Path.Combine(dir, basementImages[3].Image);
                    }

                    List<ListingImage> diningImages = returnedListing.images.FindAll(image => (image.ListingImageCategory == ListingImageCategoryNames.DININGROOM));

                    if (diningImages != null && diningImages.Count > 0)
                    {
                        if (diningImages[0] != null)
                            virtualListingModel.Image1Dining = Path.Combine(dir, diningImages[0].Image);

                        if (diningImages.Count > 1)
                            if (diningImages[1] != null)
                                virtualListingModel.Image2Dining = Path.Combine(dir, diningImages[1].Image);

                        if (diningImages.Count > 2)
                            if (diningImages[2] != null)
                                virtualListingModel.Image3Dining = Path.Combine(dir, diningImages[2].Image);

                        if (diningImages.Count > 3)
                            if (diningImages[3] != null)
                                virtualListingModel.Image4Dining = Path.Combine(dir, diningImages[3].Image);
                    }

                    List<ListingImage> familyImages = returnedListing.images.FindAll(image => (image.ListingImageCategory == ListingImageCategoryNames.FAMILYROOM));

                    if (familyImages != null && familyImages.Count > 0)
                    {
                        if (familyImages[0] != null)
                            virtualListingModel.Image1Family = Path.Combine(dir, familyImages[0].Image);

                        if (familyImages.Count > 1)
                            if (familyImages[1] != null)
                                virtualListingModel.Image2Family = Path.Combine(dir, familyImages[1].Image);

                        if (familyImages.Count > 2)
                            if (familyImages[2] != null)
                                virtualListingModel.Image3Family = Path.Combine(dir, familyImages[2].Image);

                        if (familyImages.Count > 3)
                            if (familyImages[3] != null)
                                virtualListingModel.Image4Family = Path.Combine(dir, familyImages[3].Image);
                    }

                    List<ListingImage> bathroomImages = returnedListing.images.FindAll(image => (image.ListingImageCategory == ListingImageCategoryNames.BATHROOM));

                    if (bathroomImages != null && bathroomImages.Count > 0)
                    {
                        if (bathroomImages[0] != null)
                            virtualListingModel.Image1Bathroom = Path.Combine(dir, bathroomImages[0].Image);

                        if (bathroomImages.Count > 1)
                            if (bathroomImages[1] != null)
                                virtualListingModel.Image2Bathroom = Path.Combine(dir, bathroomImages[1].Image);

                        if (bathroomImages.Count > 2)
                            if (bathroomImages[2] != null)
                                virtualListingModel.Image3Bathroom = Path.Combine(dir, bathroomImages[2].Image);

                        if (bathroomImages.Count > 3)
                            if (bathroomImages[3] != null)
                                virtualListingModel.Image4Bathroom = Path.Combine(dir, bathroomImages[3].Image);

                        if (bathroomImages.Count > 4)
                            if (bathroomImages[4] != null)
                                virtualListingModel.Image5Bathroom = Path.Combine(dir, bathroomImages[4].Image);
                       
                        if (bathroomImages.Count > 5)
                            if (bathroomImages[5] != null)
                                virtualListingModel.Image6Bathroom = Path.Combine(dir, bathroomImages[5].Image);
                    }
                }

                return virtualListingModel;
            }
        [HttpPost]
        public ActionResult CropSave()
        {

            try{

            string path = Server.MapPath("~/Images/");
            string Name = "";

            respmessage CropperResponse = new respmessage();
            HttpFileCollectionBase test = this.HttpContext.Request.Files;
            Name = test[0].FileName;
            test[0].SaveAs(path + Name);

            FileStream Fs = new FileStream(path + Name, FileMode.Open, FileAccess.Read, FileShare.Read);

            System.Drawing.Image image = System.Drawing.Image.FromStream(Fs);
            CropperResponse.height = image.Height;
            CropperResponse.width = image.Width;
            Fs.Close();
            CropperResponse.url = Url.Content("~/Images/" + Name);
            CropperResponse.status = "success";
            var resul = Json(CropperResponse, "json");

            return Json(CropperResponse);
        } catch (Exception ex) {

                this.LogException(ex);

                respmessage cropperResponse = new respmessage();
                cropperResponse.status = "failure";

                return Json(cropperResponse);
            }
        }

        public ActionResult CropResponse(string imgUrl, double? imgInitW, double? imgInitH, double? imgW, double? imgH, double? imgY1, double? imgX1, double? cropW, double? cropH)
       // public ActionResult CropResponse(string imgUrl, string? imgInitW, string? imgInitH, string? imgW, string? imgH, string? imgY1, string? imgX1, string? cropW, string? cropH)
        {
            int _imgInitW = Convert.ToInt32(Math.Truncate(Convert.ToDouble(imgInitW)));
            int _imgInitH = Convert.ToInt32(Math.Truncate(Convert.ToDouble(imgInitH)));
            int _imgW = Convert.ToInt32(Math.Truncate(Convert.ToDouble(imgW)));
            int _imgH = Convert.ToInt32(Math.Truncate(Convert.ToDouble(imgH)));
            int _imgY1 = Convert.ToInt32(Math.Truncate(Convert.ToDouble(imgY1)));
            int _imgX1 = Convert.ToInt32(Math.Truncate(Convert.ToDouble(imgX1)));
            int _cropW = Convert.ToInt32(Math.Truncate(Convert.ToDouble(cropW)));
            int _cropH = Convert.ToInt32(Math.Truncate(Convert.ToDouble(cropH)));

            int _imgWP=0;
            int _imgHP = 0;
            decimal _imgWidthCropRatio = 0;
            decimal _imgHeightCropRatio = 0;
            Point _CropPoint = new Point();
            Image cropped=null;

            string Name = "";
            string filePath = Server.MapPath(imgUrl);
            Name = Guid.NewGuid() + Path.GetFileName(filePath);

            _imgWidthCropRatio = decimal.Divide(_cropW,_imgW);
            _imgHeightCropRatio = decimal.Divide(_cropH, _imgH);

            _CropPoint.X = Convert.ToInt32(decimal.Divide((_imgX1 * _imgInitW), _imgW));
            _CropPoint.Y = Convert.ToInt32(decimal.Divide((_imgY1 * _imgInitH), _imgH));

            _imgWP = Convert.ToInt32(Math.Truncate(_imgWidthCropRatio * _imgInitW));
            _imgHP = Convert.ToInt32(Math.Truncate(_imgHeightCropRatio * _imgInitH));

            respmessage response = new respmessage();
            string path = Server.MapPath("~/Images/");
            try
            {
                Image localImage = Image.FromFile(Server.MapPath(imgUrl));
                Rectangle rect = new Rectangle(_CropPoint.X, _CropPoint.Y, Convert.ToInt32(_imgWP), Convert.ToInt32(_imgHP));

                //Update Image Quality
                using (Graphics g = Graphics.FromImage(localImage))
                {
                    g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.Bicubic;//HighQualityBicubic;
                    g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                    g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
                    g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;

                    g.Save();
                    g.Dispose();
                } 

                //crop image
                if ((_imgInitW + _imgInitH) > ((_cropW + _cropH)))
                {
                    if (_imgInitH >= _cropH && _imgInitW >= _cropW)
                    {
                        int totalWidth = rect.Left + rect.Width; //think this is the same as Right property
                        
                        int allowableWidth = localImage.Width - rect.Left;
                        int finalWidth = 0;

                        if (totalWidth > allowableWidth){
                            finalWidth = allowableWidth;
                        } else {
                            finalWidth = totalWidth;
                        }

                        rect.Width = finalWidth;

                        int totalHeight = rect.Top + rect.Height; //think this is the same as Bottom property
                        int allowableHeight = localImage.Height - rect.Top;
                        int finalHeight = 0;

                        if (totalHeight > allowableHeight){
                            finalHeight = allowableHeight;
                        } else {
                            finalHeight = totalHeight;
                        }

                        rect.Height = finalHeight;

                        cropped = ((Bitmap)localImage).Clone(rect, System.Drawing.Imaging.PixelFormat.DontCare);
                    }
                    else
                    {
                        if (_imgInitW >= _cropW && _imgInitH <= _cropH)
                            cropped = ((Bitmap)localImage).Clone(new Rectangle(_CropPoint.X, 0, Convert.ToInt32(_imgWP), Convert.ToInt32(_imgH)), System.Drawing.Imaging.PixelFormat.DontCare);
                        if (_imgInitW <= _cropW && _imgInitH >= _cropH)
                            cropped = ((Bitmap)localImage).Clone(new Rectangle(0, _CropPoint.Y, Convert.ToInt32(_imgW), Convert.ToInt32(_imgHP)), System.Drawing.Imaging.PixelFormat.DontCare);
                    }
                }
                else
                    cropped = localImage;

                cropped.Save(path + Name,System.Drawing.Imaging.ImageFormat.Jpeg);
                cropped.Dispose();
                localImage.Dispose();
                
                System.IO.File.Delete(filePath);
                       
                response.status = "success";
                response.url = Url.Content("~/Images/" + Name);

                return Json(response);
            }
            catch (Exception ex)
            {
                this.LogException(ex);

                response.status = "failure";
                Response.StatusCode = 500;
                return Json(response);
            }
        }
        public struct respmessage
        {
            public string status;
            public string url;
            public int width;
            public int height;
        };

        }
    }    
