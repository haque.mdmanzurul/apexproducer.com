﻿using HOD.Data.Entities;
using HOD.Managers.Utilities;
using HOD.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;

namespace HOD.Controllers
{
    public class ListingLayoutController : Controller
    {
        


        [HttpGet]
        public ActionResult SaveSkinLayout()
        {
            return View();
        }
       

        [HttpPost]
        public ActionResult SaveSkinLayout(Int32 ListingId, String HeaderImage, String SkinColor, String BackgroundImage, String BackgroundColor)
        {
            String PreviousFileName = "";
            ListingLayout ObjLayout = new ListingLayout();
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
                using (ListingLayoutContext layoutContext = new ListingLayoutContext())
                {
                    ObjLayout = layoutContext.LLayout.Where(ll => ll.ListingId == ListingId).FirstOrDefault();
                    if (ObjLayout == null)
                    {
                        HeaderImage = SaveAndUpdateImageListing(HeaderImage, PreviousFileName);

                        ObjLayout = new ListingLayout();
                        ObjLayout.ListingId = ListingId;
                        ObjLayout.HeaderImage = HeaderImage;
                        ObjLayout.SkinColor = SkinColor;
                        ObjLayout.BackgroundImage = BackgroundImage;
                        ObjLayout.BackgroundColor = BackgroundColor;
                        layoutContext.LLayout.Add(ObjLayout);
                    }
                    else
                    {
                        if (HeaderImage != ObjLayout.HeaderImage)
                            PreviousFileName = ObjLayout.HeaderImage;

                        if (HeaderImage != "")
                            HeaderImage = SaveAndUpdateImageListing(HeaderImage, PreviousFileName);
                        else
                        {
                            if (PreviousFileName != "")
                                DeleteImageListing(PreviousFileName);
                        }

                        ObjLayout.ListingId = ListingId;
                        ObjLayout.HeaderImage = HeaderImage;
                        ObjLayout.SkinColor = SkinColor;
                        ObjLayout.BackgroundImage = BackgroundImage;
                        ObjLayout.BackgroundColor = BackgroundColor;
                    }

                    layoutContext.SaveChanges();
                }
                scope.Complete();
            }

            return Content("Thanks", "text/html");
        }

        private string SaveAndUpdateImageListing(string currentImageName, string PreviousFileName)
        {
            HOD.Controllers.ExternalServiceController.AmazonVideoUploader videoAndImageUpdate = new ExternalServiceController.AmazonVideoUploader();
            if (PreviousFileName != "")
            {
                if (!(PreviousFileName.Contains(ApplicationUtility.DefaultNoImageFileName)))
                {
                    videoAndImageUpdate.DeleteFile(PreviousFileName);
                }
            }
            string fileName = "";
            if (currentImageName != "")
            {
                string CurrentPath = Server.MapPath("~/temp_uploads/") + currentImageName;
                fileName = videoAndImageUpdate.UploadFile(CurrentPath, currentImageName);

                System.IO.File.Delete(CurrentPath);
            }

            return fileName;
        }

        private void DeleteImageListing(string PreviousFileName)
        {
            HOD.Controllers.ExternalServiceController.AmazonVideoUploader videoAndImageUpdate = new ExternalServiceController.AmazonVideoUploader();
            if (PreviousFileName != "")
            {
                if (!(PreviousFileName.Contains(ApplicationUtility.DefaultNoImageFileName)))
                {
                    videoAndImageUpdate.DeleteFile(PreviousFileName);
                }
            }
        }
    }


        //public void UpdateVirtualSign(Listing model, List<ListingImage> images)
        //{
        //    HODContext context = new HODContext();
        //    try
        //    {
        //        ListingImage image = new ListingImage();
        //        Listing listing = new Listing();
        //        listing = (from c in context.Listings
        //                   where c.ListingId == model.ListingId
        //                   select c).SingleOrDefault<Listing>();
        //        listing.Address = model.Address;
        //        listing.City = model.City;
        //        listing.clientUrl = model.ClientUrl;
        //        listing.ContactPerson = model.ContactPerson;
        //        listing.DontShowListPrice = new bool?(model.DontShowListPrice);
        //        listing.DontShowPhoneNo = new bool?(model.DontShowPhoneNo);
        //        listing.DontShowSquareFootage = new bool?(model.DontShowSquareFootage);
        //        listing.IsPrivateSeller = new bool?(model.isPrivateSeller);
        //        listing.KitchenDesc = model.KitchenDesc;
        //        listing.ListingPerson = Convert.ToInt32(model.ListingPerson);
        //        listing.ListPrice = new decimal?(model.ListPrice);
        //        listing.MainSellingDesc = model.MainSellingDesc;
        //        listing.NoOfBathrooms = new decimal?(model.NoOfBathrooms);
        //        listing.NoOfBedrooms = new decimal?(model.NoOfBedrooms);
        //        listing.PhoneNo = model.PhoneNo;
        //        listing.PostalCode = model.PostalCode;
        //        listing.Neighbourhood = model.NeighbourHood;
        //        listing.PropertyType = model.PropertyType;
        //        listing.Province = model.Province;
        //        listing.SquareFootage = new decimal?(model.SquareFootage);
        //        listing.KitchenDesc = model.KitchenDesc;
        //        listing.PropertyType = model.PropertyType;
        //        context.SaveChanges();
        //        List<ListingImage> list = (from c in context.ListingImages
        //                                   where (c.ListingId == model.ListingId) && (c.ListingImageCategory.Name == "VirtualSign")
        //                                   select c).ToList<ListingImage>();
        //        List<ListingImage> list2 = (from img in images
        //                                    where img.Image != null
        //                                    select img).ToList<ListingImage>();
        //        list[0].Image = images[0].Image;
        //        list[1].Image = images[1].Image;
        //        list[2].Image = images[2].Image;
        //        list[3].Image = images[3].Image;
        //        context.SaveChanges();
        //    }
        //    catch (DbEntityValidationException exception)
        //    {
        //        foreach (DbEntityValidationResult result in exception.EntityValidationErrors)
        //        {
        //            foreach (DbValidationError error in result.ValidationErrors)
        //            {
        //                Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"", error.PropertyName, error.ErrorMessage);
        //            }
        //        }
        //        throw;
        //    }
        //}


    
}
