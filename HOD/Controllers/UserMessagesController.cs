﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HOD.Constants;
using HOD.Entities;
using HOD.Managers;
using HOD.Managers.Utilities;
using HOD.Models;
using WebMatrix.WebData;

namespace HOD.Controllers
{
    public class UserMessagesController : Controller
    {


        IMessageManager messageManager = new MessageManager();
        IMessageManagerUtility messageManagerUtility = new MessageManagerUtility();

        //
        // GET: /UserMessages/
        [Authorize]
        public ActionResult Index()
        {
            return View();
        }

        [Authorize]
        public ActionResult Messages(int id)
        {
            if (id == WebSecurity.CurrentUserId)
            {
                HOD.Models.UserMessageModel userMessage = new UserMessageModel();

                List<Message> messages = messageManager.GetMessagesForUser(id, null);

                if (messages != null && messages.Count > 0)
                {
                    userMessage.Messages = PrivateMessageModel.Translate(messages);
                    userMessage.Messages.GroupBy(m => m.ConversationId);
                }
                return View(userMessage);
            }
            else
                return View("Error");
        }

        [Authorize]
        public ActionResult Conversations(int id)
        {
            UserProfile user = null;
            using (UsersContext db = new UsersContext())
            {
                user = db.UserProfiles.Find(WebSecurity.CurrentUserId);
            }

            
            MessageType messageType = messageManager.GetMessageTypeByName(MessageTypes.REPLY);
            if (messageType == null)
                throw new ArgumentNullException("Message Type does not exist.");
            
            if (id == WebSecurity.CurrentUserId)
            {
                UserMessageModel userMessageModel = new UserMessageModel();
                userMessageModel.Conversations = new List<UserConversationModel>();
                List<Message> messages = new List<Message>();
                
                List<Guid> conversations = messageManager.GetConversationsForUser(id, null);
                int listingID = messageManager.GetListingIDForUser(id, null);

                //If there are messages for the user
                if (messageManager.GetMessagesForUser(id, null).Count != 0)
                {
                    int fromUser = messageManager.GetMessagesForUser(id, null).First().FromUser;
                    messages = messageManager.GetMessagesByConversation(listingID, null, id, fromUser, true);
                }
                else { return View(userMessageModel); }

                //foreach (Guid conversationId in conversations)
                //{
                    //List<Message> messages = messageManager.GetMessagesByConversation(conversationId, null);
                    

                    //messages = messages.Where(m => m.ToUser == id || (m.FromUser == id && m.MessageTypeId == messageType.MessageTypeId)).ToList();
                foreach (Message m in messages)
                {
                    if (m != null && messages.Count > 0)
                    {
                        if ((m.ToUser == WebSecurity.CurrentUserId || (m.MessageTypeId == messageType.MessageTypeId && m.FromUser == id)) || (m.FromUser == WebSecurity.CurrentUserId || (m.MessageTypeId == messageType.MessageTypeId && m.ToUser == id)))
                        {
                            UserConversationModel conversationModel = new UserConversationModel();
                            conversationModel.MessageDate = m.MessageDate;
                            conversationModel.ConversationId = m.ConversationId;
                            conversationModel.FromUser = m.FromUser;
                            conversationModel.MessageRead = m.MessageRead;
                            conversationModel.MessageTypeId = m.MessageTypeId;
                            conversationModel.Subject = m.Subject;
                            conversationModel.ToUser = m.ToUser;
                            conversationModel.MessageId = m.MessageId;
                            conversationModel.FromUserName = user != null ? user.FirstName + conversationModel.FromUser : "";

                            userMessageModel.Conversations.Add(conversationModel);
                        }
                    }
                    //}
                }
                return View(userMessageModel);
            }
            else
                return View("Error");
            
        }

        [Authorize]
        public ActionResult Details(int id)
        {
            Message message = messageManager.ReadReturnMessage(id);

            if (message != null && message.ToUser == WebSecurity.CurrentUserId)
            {
                //PrivateMessageModel userMessage = PrivateMessageModel.Translate(message);
                //userMessage.ReplySubject = message.Subject;

                //return View(userMessage);

                PrivateMessageModel userMessage = PrivateMessageModel.Translate(message);
                userMessage.ReplySubject = message.Subject;

                return View(userMessage);
            }
            else
                return View("Error");
        }

        [Authorize]
        public ActionResult Conversation(int id)
        {
            UserConversationModel conversationModel = null;
            Message message = messageManager.GetMessagesById(id);

            if (message != null)
            {
                List<Message> conversationMessages = null;
                int listingID = messageManager.GetListingIDForUser(message.ToUser, null);
                int fromUser = messageManager.GetMessagesById(id).FromUser;

                if (message.ToUser == WebSecurity.CurrentUserId)
                {
                    //conversationMessages  = messageManager.ReadReturnMessagesByConversation(id, message.ConversationId, null);
                    conversationMessages = messageManager.ReadReturnMessagesByConversation(id, listingID, null, message.ToUser, fromUser);
                }
                else
                    conversationMessages = messageManager.GetMessagesByConversation(listingID, null, message.ToUser, fromUser, false);

                if (conversationMessages != null && conversationMessages.Count > 0)
                {
                    conversationModel = new UserConversationModel();
                    conversationModel.ConversationId = message.ConversationId;
                    conversationModel.Subject = message.Subject;
                    conversationModel.ToUser = message.ToUser;
                    conversationModel.FromUser = message.FromUser;
                    conversationModel.ReplySubject = message.Subject;
                    foreach (Message conMessage in conversationMessages)
                    {
                        conversationModel.Messages = PrivateMessageModel.Translate(conversationMessages);
                    }
                    return View(conversationModel);
                }
                else
                    return View("Error");
            }
            else
                return View("Error");
        }

        [Authorize]
        [HttpPost]
        public ActionResult Reply(UserConversationModel model)
        {
                MessageType messageType = messageManager.GetMessageTypeByName(MessageTypes.REPLY);
                int listingID = messageManager.GetListingIDForUser(model.ToUser, null);

                if (messageType == null)
                    throw new ArgumentNullException("Message Type does not exist.");

                Message userMessage = new Message();
                userMessage.Body = model.ReplyBody;
                userMessage.ConversationId = model.ConversationId;
                int toUserId = model.ToUser;

                userMessage.FromUser = model.ToUser;
                userMessage.MessageDate = DateTime.Now;
                userMessage.MessageRead = false;
                userMessage.MessageTypeId = messageType.MessageTypeId;
                userMessage.Subject = model.ReplySubject;
                userMessage.ToUser = model.FromUser;
                userMessage.ListingID = listingID;

                messageManager.SaveUserMessage(userMessage);

                UserProfile toUser = null;
                using (UsersContext db = new UsersContext())
                {
                    toUser = db.UserProfiles.Find(toUserId);
                }

                string conversationUrl = System.Configuration.ConfigurationManager.AppSettings["applicationRoot"] + "UserMessages/Conversations/" + userMessage.ToUser;

                EmailMessage emailMessage = new EmailMessage();
                emailMessage.subject = "CasaVue Private Message";
                emailMessage.sender = ApplicationUtility.EmailSettings.DefaultSenderEmailAddress;
                emailMessage.recipients.Add(toUser.UserName);

                //emailMessage.body = String.Format("Hi {0}, <P> you have received a message about your listing. Please see the message below.</P><P>{1}</P>. You may reply by clicking the link below. <BR>{2}", toUser.FirstName, model.ReplyBody, conversationUrl);

                emailMessage.body = MailMessageHelper.FormattedMail(model.Subject, toUser.FirstName, model.ReplyBody, conversationUrl);
                messageManagerUtility.SendEmailMessage(emailMessage);

            return RedirectToAction("Conversations", "UserMessages", routeValues: new { id = WebSecurity.CurrentUserId });
        }

        

    }
}
