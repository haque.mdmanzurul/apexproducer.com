﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.WebPages;
using HOD.Managers;
using HOD.Managers.Exceptions;

namespace HOD.Controllers
{

    public class ExternalMessageController : ApiController
    {
        private IMessageManager MessageManager  = new MessageManager();      

        public ExternalMessageController(IMessageManager messageManager)
        {
            MessageManager = messageManager;
        }
       
        public ExternalMessageController()
        {
        }

        [System.Web.Http.HttpGet]
        public HttpResponseMessage Send(HttpRequestMessage requestMessage, string phoneNumber, string message)
        {
            
            HttpContent requestContent = Request.Content;
            var responseMessage = new HttpResponseMessage();

            if ((message.IsEmpty() || phoneNumber.IsEmpty()) ){
                responseMessage.StatusCode = HttpStatusCode.BadRequest;
                return responseMessage;
            }

            string response = string.Empty;

            try
            {
                response = MessageManager.ProcessMessage(phoneNumber, message, Entities.MessageDirection.Incoming);

            } catch (MessageManagerExceptions.InvalidMessageRequestException exception){

                responseMessage.Content = new StringContent(exception.Message, Encoding.UTF8, "text/html");
                responseMessage.StatusCode = HttpStatusCode.BadRequest;
                return responseMessage;
            }
            catch (Exception e)
            {
                responseMessage.Content = new StringContent(e.Message, Encoding.UTF8, "text/html");
                responseMessage.StatusCode = HttpStatusCode.BadRequest;
                return responseMessage;
            }            
  

            responseMessage.Content = new StringContent(response, Encoding.UTF8, "text/html");
            responseMessage.StatusCode = HttpStatusCode.OK;

            return responseMessage;
        }

        [System.Web.Http.HttpGet]
        public HttpResponseMessage SendFromInbox(HttpRequestMessage requestMessage, string from, string message)
        {
            return this.Send(requestMessage, from, message);
        }

       /* [System.Web.Http.HttpGet]
        [System.Web.Http.ActionName("SendSms")]
        public HttpResponseMessage SendSMS(HttpRequestMessage requestMessage, string from, string message)
        {
            var responseMessage = new HttpResponseMessage();          

            try
            {
                MessageManager.SendSMS(from, message);
                responseMessage.Content = new StringContent("Success", Encoding.UTF8, "text/html");
                responseMessage.StatusCode = HttpStatusCode.OK  ;
            }
            catch (Exception e)
            {
                responseMessage.Content = new StringContent(e.Message, Encoding.UTF8, "text/html");
                responseMessage.StatusCode = HttpStatusCode.BadRequest;
            }

            return responseMessage;
        }*/

    }
}
