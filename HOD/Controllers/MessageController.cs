﻿using HOD.Entities;
using HOD.Managers;
using HOD.Managers.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HOD.Controllers
{
    public class MessageController : Controller
    {

        IMessageManager MessageManager = new MessageManager();

        public JsonResult SendSMS(string phoneNo, string message)
        {
            SmsResponse response = new SmsResponse();

            try {
                MessageManager.SendSMS(phoneNo, message);
                Response.StatusCode = 200;
                Response.ContentType = "application/json";
                response.code = 200;
                response.desc = "Success sending message";
            } catch (MessageManagerExceptions.InvalidPhoneNumberException mEx){
                Response.StatusCode = 400;
                Response.ContentType = "application/json";
                response.code = 400;
                response.desc = mEx.Message;                            
            } catch (MessageManagerExceptions.InvalidListingRequestException mEx2){
                Response.StatusCode = 400;
                Response.ContentType = "application/json";
                response.code = 400;
                response.desc = mEx2.Message;                
            } catch (Exception e){
                //TODO: Error logging in the database required here.
                Response.StatusCode = 500;
                response.code = 500;
                response.desc = "Failure Sending Message";
            }

            return Json(response, JsonRequestBehavior.AllowGet);                                 
        }



    }
}
