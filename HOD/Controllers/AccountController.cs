﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using DotNetOpenAuth.AspNet;
using Microsoft.Web.WebPages.OAuth;
using WebMatrix.WebData;
using HOD.Filters;
using HOD.Models;
using HOD.Payments.StripeChargingGatewayManager.Contracts.ServiceContracts;
using HOD.Payments.StripeChargingGatewayManager.Entities;
using HOD.Payments.StripeChargingGatewayManager.Operations;
using System.Collections;
using System.Web.UI;
using HOD.Managers; 
using HOD.Constants;
using HOD.Entities;
using HOD.Payments.StripeChargingGatewayManager.DataContracts;
using System.Text.RegularExpressions;
using System.Security;
using GooglePlusOAuthLogin;
using System.Globalization;

namespace HOD.Controllers
{
    [Authorize]
    [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
    public class AccountController : HODBaseController
    {

        const string freeTier = "FREE";

        public IListingManager ListingManager = new ListingManager();
        public IMessageManager messageManager = new MessageManager();
        public IPackageManager packageManager = new PackageManager();

        [HttpPost]
        [AllowAnonymous]
        public ActionResult AccountMenu()
        {
            string packageSelected="";
            UserProfile userX = null;
            HOD.Managers.IMenu menuManager = new HOD.Managers.ListingManager();
            List<HOD.Entities.MenuItems> MenuData = menuManager.GetMenu();
            List<Entities.MenuItems> Data = null;

            if (WebSecurity.IsAuthenticated)
            {
                using (UsersContext db = new UsersContext())
                {
                    if (WebSecurity.Initialized)
                    {
                        userX = db.UserProfiles.Find(WebSecurity.CurrentUserId);
                        UserPackages userPackages = db.UserPackages.Find(WebSecurity.CurrentUserId);
                        if (userPackages != null)
                        {
                            UserPackageTypes selectedPackage = db.UserPackageTypes.Find(userPackages.PackageTypeId);
                            packageSelected = selectedPackage.Name;
                        }
                    }
                }
                Data = MenuData.Where(a => a.menuRole == packageSelected).ToList<Entities.MenuItems>();
            }
            else
            {
                Data = MenuData.Where(a => a.menuRole == "default").ToList<Entities.MenuItems>();//default role 
            }

            return Json(Data, "json");
        }

        //
        // GET: /Account/TestPage

        [AllowAnonymous]
        public ActionResult Dashboard(string returnUrl)
        {
            return View();
        }
        //

        //
        // GET: /Account/TestPage

        [AllowAnonymous]
        public ActionResult TestPage(string returnUrl)
        {
            return View();
        }
        //
        // GET: /Account/Login

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            if (WebSecurity.IsAuthenticated == true){
                WebSecurity.Logout();
                Session.Abandon();
            }
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model, string returnUrl){

            string postLoginRedirectAction = string.Empty;
            string postLoginRedirectController = string.Empty;
            object routeValues = null;

            if (!ModelState.IsValid){                
                ModelState.AddModelError("", "The user name or password provided is incorrect.");
                return View(model);
            }
            
            if (!WebSecurity.Login(model.UserName, model.Password)){
                ModelState.AddModelError("", "The user name or password provided is incorrect.");
                return View(model);
            }

            Session["guestsession"] = false;
            int userId = WebSecurity.GetUserId(model.UserName);
                    
            List<HOD.Entities.Message> messages = messageManager.GetMessagesForUser(userId, false);
            var listingIds = ListingManager.GetListingIdsByUserId(userId);

            UserProfile user;

            using (UsersContext db = new UsersContext()){
                user = db.UserProfiles.Find(userId);
            }

            if (listingIds != null && listingIds.Count > 0){
                Session["currListingId"] = listingIds[0];
                Session.Add("ListingUserId", userId);
                Session.Add("TIER",user.Tier);
               // return RedirectToAction("ListingTemplate", "Listing", routeValues: new { id = listingIds[0] });
            }
                      
            TempData["RegisterAddress"] = user.BillingAddress;
            TempData["RegisterCity"] = user.BillingCity;
            TempData["RegisterProvince"] = user.BillingProvince;
            TempData["RegisterPostalCode"] = user.BillingPostalCode;
            TempData["RegisterContactPerson"] = user.BillingName;
            TempData["RegisterPhone"] = user.PhoneNumberCell;
            Session.Add("TIER", user.Tier);

            switch (user.UserTypeId){
                case 2:
                    Session.Add("ListingUserId", userId);
                    postLoginRedirectAction = "Create";
                    postLoginRedirectController = "Listing";                                    
                    break;

                case 3:
                    if (isFullyRegisteredAgent(userId)){
                        postLoginRedirectAction = "AgentPortal";
                        postLoginRedirectController = "Agent";
                    } else {
                        postLoginRedirectAction = "AccountCompleteSetup";
                        postLoginRedirectController = "Account";
                    }

                    Session["AgentId"] = userId;
                    Session["AgentContext"] = true;                                  
                    routeValues = new { id = userId };
                    break;

                default:
                    postLoginRedirectAction = "Index";
                    postLoginRedirectController = "Home";
                    break;
            }
         
            //return RedirectToAction(postLoginRedirectAction, postLoginRedirectController, routeValues);
            return  RedirectToAction("Dashboard", "Account");
        }



        //For Casavue Login
        [AllowAnonymous]
        public ActionResult CasavueLogin(LoginModel model, string returnUrl)
        {
            return View();
        }

        //For Casavue Login
        [HttpPost]
        [AllowAnonymous]
        public ActionResult CasavueLogin(FormCollection form, string returnUrl)
        {

            string postLoginRedirectAction = string.Empty;
            string postLoginRedirectController = string.Empty;
            object routeValues = null;

            LoginModel model = new LoginModel()
            {
                UserName = form["UserName"].ToString(),
                Password = form["Password"].ToString()
            };

            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "The user name or password provided is incorrect.");
                return View(model);
            }

            if (!WebSecurity.Login(model.UserName, model.Password))
            {
                ModelState.AddModelError("", "The user name or password provided is incorrect.");
                return View(model);
            }

            Session["guestsession"] = false;
            int userId = WebSecurity.GetUserId(model.UserName);

            List<HOD.Entities.Message> messages = messageManager.GetMessagesForUser(userId, false);
            var listingIds = ListingManager.GetListingIdsByUserId(userId);

            UserProfile user;

            using (UsersContext db = new UsersContext())
            {
                user = db.UserProfiles.Find(userId);
            }

            if (listingIds != null && listingIds.Count > 0)
            {
                Session["currListingId"] = listingIds[0];
                Session.Add("ListingUserId", userId);
                Session.Add("TIER", user.Tier);
                return RedirectToAction("ListingTemplate", "Listing", routeValues: new { id = listingIds[0] });
            }

            TempData["RegisterAddress"] = user.BillingAddress;
            TempData["RegisterCity"] = user.BillingCity;
            TempData["RegisterProvince"] = user.BillingProvince;
            TempData["RegisterPostalCode"] = user.BillingPostalCode;
            TempData["RegisterContactPerson"] = user.BillingName;
            TempData["RegisterPhone"] = user.PhoneNumberCell;
            Session.Add("TIER", user.Tier);

            switch (user.UserTypeId)
            {
                case 2:
                    Session.Add("ListingUserId", userId);
                    postLoginRedirectAction = "Create";
                    postLoginRedirectController = "Listing";
                    break;

                case 3:
                    if (isFullyRegisteredAgent(userId))
                    {
                        postLoginRedirectAction = "AgentPortal";
                        postLoginRedirectController = "Agent";
                    }
                    else
                    {
                        postLoginRedirectAction = "AccountCompleteSetup";
                        postLoginRedirectController = "Account";
                    }

                    Session["AgentId"] = userId;
                    Session["AgentContext"] = true;
                    routeValues = new { id = userId };
                    break;

                default:
                    postLoginRedirectAction = "Index";
                    postLoginRedirectController = "Home";
                    break;
            }

            return RedirectToAction(postLoginRedirectAction, postLoginRedirectController, routeValues);
        }

        private bool isFullyRegisteredAgent(int userId)
        {
            bool agentFullyRegistered = false;

            using (HOD.Data.Entities.HODContext hodContext = new HOD.Data.Entities.HODContext())
            {
                int agentCount = hodContext.AgentInformations.Where(agent => agent.AgentId == userId).Count();

                if (agentCount > 0){
                    agentFullyRegistered = true;
                }

                if (hodContext.UserProfiles.Where(x => x.UserId == userId && x.Tier.ToUpper() == "PAID").Count() > 0){
                    return true;
                }


                int profileCompleteCount = hodContext.UserProfiles.Where(userProfile => userProfile.NoOfEmployees != null && userProfile.UserId == userId).Count();

                if (profileCompleteCount > 0){
                    agentFullyRegistered = true;
                } else {
                    agentFullyRegistered = false;
                }
            }

            return agentFullyRegistered;
        }

        //
        // POST: /Account/LogOff

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            WebSecurity.Logout();

            return RedirectToAction("Login", "Account");
        }
        public ActionResult LogOut()
        {
            WebSecurity.Logout();
            Session.Abandon();
            return RedirectToAction("Login", "Account");
        }

        //
        // GET: /Account/Register

        //[AllowAnonymous]
        //public ActionResult Register()
        //{
        //    return View();
        //}

        [AllowAnonymous]
        public ActionResult Upgrade(String UserType, String RedirectUrl)
        {
            //Session["PayOnUserBehalf"] = true;// This will be moved to the agent Controller. its only for testing here.

            TempData["UserType"] = UserType;
            //Determine who is paying
            bool payOnUserBehalf = false;

            if (Session["PayOnUserBehalf"] != null)
            {
                payOnUserBehalf = (bool)Session["PayOnUserBehalf"];
            }
            else
            {
                Session["PayOnUserBehalf"] = payOnUserBehalf;
            }

            RegisterModel model = new RegisterModel();
            model.UserType = UserType;

            //Get current URL String to register 
            string uRL = Request.Url.Host;
            model.CopyBillingAddress = true;

            // Get Agent ID by URL

            if (Session["AGENTID"] == null)
            {
                IDbAgent DB = new DbManager();
                model.AgentID = DB.GetAgentIDByURL(uRL);
            }
            else
            {
                model.AgentID = int.Parse(Session["AGENTID"].ToString());
            }

            if (String.IsNullOrEmpty(RedirectUrl) != true)
            {
                ViewData.Add("redirectUrl", RedirectUrl);
            }

            bool isFreeTier = false;

            if (Session["TIER"] == null)
            {
                isFreeTier = false;
            }
            else
            {
                if (Session["TIER"].ToString().ToUpper() == "FREE")
                {
                    isFreeTier = true;
                }
                else
                {
                    isFreeTier = false;
                }
            }

            Data.Entities.UserProfile userProfile;

            if (WebSecurity.IsAuthenticated)
            {
                using (HOD.Data.Entities.HODContext hodContext = new HOD.Data.Entities.HODContext())
                {

                    userProfile = hodContext.UserProfiles.Where(user => user.UserId == WebSecurity.CurrentUserId).FirstOrDefault();

                    var listingDetails = hodContext.Listings.Where(x => x.UserId == WebSecurity.CurrentUserId).FirstOrDefault();

                    if (userProfile.Tier != null && userProfile.Tier.ToUpper() == "FREE")
                    {
                        isFreeTier = true;
                    }
                    else
                    {
                        isFreeTier = false;
                    }

                    if (userProfile.UserTypeId == 2)
                        model.UserType = "Seller";
                    else
                        model.UserType = "Agent";

                    model.UserName = userProfile.UserName;
                    model.Password = "12345678";// only to pass model validation
                    model.ConfirmPassword = "12345678";// only to pass model validation
                    model.FirstName = userProfile.FirstName;
                    model.LastName = userProfile.LastName;
                    model.PhoneNumberCell = userProfile.PhoneNumberCell;
                    model.PhoneNumberLandLine = userProfile.PhoneNumberLandLine;
                    model.Address = userProfile.Address;
                    model.City = userProfile.City;
                    model.PostalCode = userProfile.PostalCode;
                    model.Country = userProfile.Country;
                    model.CVC = "";
                    model.CardNumber = "";

                    if (model.BillingCity == null)
                    {
                        model.City = listingDetails.City;
                        model.BillingCity = listingDetails.City;
                    }
                    if (model.BillingAddress == null)
                    {
                        model.Address = listingDetails.Address;
                        model.BillingAddress = listingDetails.Address;
                    }
                    if (model.BillingProvince == null)
                    {
                        model.Province = listingDetails.Province;
                        model.BillingProvince = listingDetails.Province;
                    }
                    if (model.BillingPostalCode == null)
                    {
                        model.PostalCode = listingDetails.PostalCode;
                        model.BillingPostalCode = listingDetails.PostalCode;
                    }
                    if (model.BillingName == null)
                    {
                        string[] fullname = listingDetails.ContactPerson.Split(' ');
                        model.FirstName = fullname[0];
                        model.BillingName = listingDetails.ContactPerson;
                    }
                    
                }
            }

            
            //If the aagent is paying, Get agent previous info
            if (isFreeTier == false)
            {
                if (payOnUserBehalf && UserType.ToLower() == "seller" && WebSecurity.IsAuthenticated)
                {

                    string CustID = "";
                    StripeOperation PaymentTransaction = new StripeOperation();

                    Session["PayOnUserBehalf"] = true;
                    model.AgentPayOnUserBehalf = true;
                    model.CopyBillingAddress = false;

                    UsersContext info = new UsersContext();
                    CustID = info.UserProfiles.First(m => m.UserId == WebSecurity.CurrentUserId).StripeCustomerId;

                    GetStripeCustomerRequest CustInfoREQ = new GetStripeCustomerRequest() { CustomerId = CustID };
                    GetStripeCustomerResponse CustInfoRESP = PaymentTransaction.GetStripeCustomer(CustInfoREQ);

                    var StripeCardInfo = CustInfoRESP.Customer.StripeCardList.Cards.First(m => m.Id == CustInfoRESP.Customer.StripeDefaultCardId);


                    model.BillingAddress = StripeCardInfo.AddressLine1;
                    model.BillingCity = StripeCardInfo.AddressCity;
                    model.BillingName = StripeCardInfo.Name;
                    model.BillingPostalCode = StripeCardInfo.AddressZip;
                    model.BillingProvince = StripeCardInfo.AddressState;

                    if (StripeCardInfo.ExpirationMonth.Length == 1)
                    {
                        model.ExpirationMonth = "0" + StripeCardInfo.ExpirationMonth;
                    }
                    else
                    {
                        model.ExpirationMonth = StripeCardInfo.ExpirationMonth;
                    }

                    model.ExpirationYear = StripeCardInfo.ExpirationYear;
                    model.CardNumber = "************" + StripeCardInfo.Last4;
                    model.CVC = "***";

                    System.Diagnostics.Trace.Write(StripeCardInfo);
                    System.Diagnostics.Trace.Write(model);
                }
            }

            return View(model);
        }


        //account upgrade
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Upgrade(FormCollection form)
        {
            var userRegisterType = form["UserType"];

            bool isFreeTier = false;
            bool agentPayForCustomer = false;

            const string TIER_SESSIONKEY = "TIER";
            const string TIERS_FREE = "FREE";

            if (Session[TIER_SESSIONKEY] == null)
            {
                isFreeTier = false;
            }
            else
            {
                if (Session[TIER_SESSIONKEY].ToString().ToUpper() == TIERS_FREE)
                {
                    isFreeTier = true;
                }
                else
                {
                    isFreeTier = false;
                }
            }

            if (isFreeTier)
            {
                agentPayForCustomer = false;
            }

            #region BUYER
            if (userRegisterType == UserTypes.BUYER)
            {
                ActionResult buyerSaveResult = BuyerRegistration(form);
                if (buyerSaveResult != null)
                {
                    return buyerSaveResult;
                }

                RegisterBaseModel buyerModel = new RegisterBaseModel();
                TryUpdateModel(buyerModel, form.ToValueProvider());
                return View(buyerModel);
            }
            #endregion

            if (userRegisterType == UserTypes.SELLER)
            {

                RegisterModel model = new RegisterModel();
                RegisterFreeCustomer customerFreeModel = new RegisterFreeCustomer();

                string currentUserName = string.Empty;

                //if (isFreeTier)
                //{

                //    using (TransactionScope transScope = new TransactionScope(TransactionScopeOption.RequiresNew))
                //    {
                //        try
                //        {
                //            TryUpdateModel(customerFreeModel, form.ToValueProvider());
                //            currentUserName = customerFreeModel.UserName;
                //            if (!RegisterFreeSeller(customerFreeModel))
                //            {
                //                RegisterFreeCustomer faultedModelFree = new RegisterFreeCustomer();
                //                TryUpdateModel(faultedModelFree, form.ToValueProvider());
                //                return View(faultedModelFree);
                //            }

                //            //Logout Agent and login new user
                //            if (Session["AgentContext"] != null)
                //            {
                //                if ((bool)Session["AgentContext"] == false)
                //                {
                //                    WebSecurity.Login(currentUserName, customerFreeModel.Password);
                //                }
                //            }

                //            Session.Add("ListingUserId", WebSecurity.GetUserId(currentUserName));
                //            transScope.Complete();
                //        }
                //        catch (TransactionException ex)
                //        {
                //            ModelState.AddModelError("username", ex.Message);
                //            RegisterFreeCustomer faultedModelFree = new RegisterFreeCustomer();
                //            TryUpdateModel(faultedModelFree, form.ToValueProvider());
                //            return View(faultedModelFree);
                //        }
                //    }

                //    if (isFreeTier)
                //    {
                //        TempData["RegisterAddress"] = customerFreeModel.Address;
                //        TempData["RegisterCity"] = customerFreeModel.City;
                //        TempData["RegisterProvince"] = customerFreeModel.Province;
                //        TempData["RegisterPostalCode"] = customerFreeModel.PostalCode;
                //        TempData["RegisterContactPerson"] = customerFreeModel.FirstName + " " + customerFreeModel.LastName;
                //        TempData["RegisterPhone"] = customerFreeModel.PhoneNumberCell;
                //    }
                //    else
                //    {
                //        TempData["RegisterAddress"] = model.Address;
                //        TempData["RegisterCity"] = model.City;
                //        TempData["RegisterProvince"] = model.Province;
                //        TempData["RegisterPostalCode"] = model.PostalCode;
                //        TempData["RegisterContactPerson"] = model.FirstName + " " + model.LastName;
                //        TempData["RegisterPhone"] = model.PhoneNumberCell;
                //    }

                //    return RedirectToAction("Create", "Listing");
                //}


                TryUpdateModel(model, form.ToValueProvider());

                if (!model.AgentPayOnUserBehalf)
                {
                    if (ValidateCreditCard(model.CardNumber, model.CVC) == false)
                    {
                        RegisterModel faultedModel = new RegisterModel();
                        TryUpdateModel(faultedModel, form.ToValueProvider());
                        return View(faultedModel);
                    }
                }


                StripeOperation operation = new StripeOperation();
                CreateStripeCustomerResponse createStripeCustomerResponse = new CreateStripeCustomerResponse();

                try
                {
                    using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
                    {
                        using (UsersContext userContext = new UsersContext())
                        {
                            string currentUserType = isFreeTier == true ? customerFreeModel.UserType : model.UserType;
                            currentUserType = currentUserType.ToUpper();

                            var userType = userContext.UserTypes.Where(u => u.Name.ToUpper() == currentUserType).FirstOrDefault();

                            if (userType == null)
                                throw new ArgumentNullException("User Type not recognized.");

                            if (!model.AgentPayOnUserBehalf)
                            {
                                agentPayForCustomer = false;
                            }
                            else
                            {
                                agentPayForCustomer = true;
                            }

                            if (agentPayForCustomer == false)
                            {
                                //Customer paying for product himself
                                createStripeCustomerResponse = CreateStripeCustomer(model, operation, createStripeCustomerResponse);
                            }

                            //WebSecurity.CreateUserAndAccount(model.UserName, model.Password, propertyValues: new
                            //{
                            //    UserName = model.UserName,
                            //    FirstName = model.FirstName,
                            //    LastName = model.LastName,
                            //    Address = model.Address,
                            //    City = model.City,
                            //    Province = model.Province,
                            //    PostalCode = model.PostalCode,
                            //    PhoneNumberCell = model.PhoneNumberCell,
                            //    PhoneNumberLandLine = model.PhoneNumberLandLine,
                            //    UserTypeId = userType.UserTypeId,
                            //    BillingName = model.BillingName,
                            //    BillingAddress = model.BillingAddress,
                            //    BillingCity = model.BillingCity,
                            //    BillingProvince = model.BillingProvince,
                            //    BillingPostalCode = model.BillingPostalCode,
                            //    isAgent = model.isAgent,
                            //    AgentID = model.AgentID,
                            //    StripeCustomerId = isFreeTier ? null : createStripeCustomerResponse.Customer.Id,
                            //    Tier = Session["TIER"]
                            //});

                            //Get Stripe User from database
                            if (agentPayForCustomer && (bool)Session["PayOnUserBehalf"] && WebSecurity.IsAuthenticated)
                            {
                                createStripeCustomerResponse = RetrieveStripeCustomerData(operation);
                            }

                            UserProfile user = userContext.UserProfiles.Where<UserProfile>(u => u.UserName == model.UserName).FirstOrDefault<UserProfile>();

                            UserPackage userPackage = null;
                            List<PackageFeature> packageFeatues = new List<PackageFeature>();

                            PackageType packageType = packageManager.GetPackageTypeByName(PackageTypes.GOLD);

                            //user selected package
                            PackageList userChosenPackage;
                            int pkgId = Convert.ToInt32(form["Package"].ToString());
                            using (HOD.Models.PackageListContext pl = new HOD.Models.PackageListContext())
                            {
                                 
                                 userChosenPackage = pl.PList.Where(x => x.PackageId == pkgId).FirstOrDefault();
                            }

                            //Add Package for user
                            userPackage = new UserPackage()
                            {
                                PackageTypeId = packageType.PackageTypeId, //packageType.PackageTypeId replaced with user chosen packageId,
                                UserId = user.UserId
                            };

                            packageType.BaseCost = userChosenPackage.PackageAmount;
                            //Create InvoiceItem
                            CreateStripeInvoiceItemRequest createStripeInvoiceItemGOLDRequest = CreateInvoiceItemRequest(createStripeCustomerResponse, packageType);

                            CreateStripeInvoiceItemResponse createStripeInvoiceGOLDResponse = operation.CreateStripeInvoiceItem(createStripeInvoiceItemGOLDRequest);
                            if (createStripeInvoiceGOLDResponse == null)
                                throw new ArgumentException("InvoiceItem '" + createStripeInvoiceGOLDResponse.InvoiceItem.Id + "' was not created.");
                            else if (createStripeInvoiceGOLDResponse.InvoiceItem == null)
                                throw new ArgumentException("InvoiceItem '" + createStripeInvoiceGOLDResponse.InvoiceItem.Id + "' was not created.");

                            //userPackage = packageManager.SaveUserPackage(userPackage, packageFeatues);

                            CreateAndPayStripeInvoiceRequest createAndPayStripeInvoice = new CreateAndPayStripeInvoiceRequest()
                            {
                                CustomerId = createStripeCustomerResponse.Customer.Id
                            };


                            //Doing a direct charge and not invoicing, this is one off. Description will aptly tell customer
                            /*CreateAndPayStripeInvoiceResponse createAndPayStripeInvoiceResponse = operation.CreateAndPayStripeInvoice(createAndPayStripeInvoice);*/

                            CreateStripeChargeRequest goldChargeRequest = new CreateStripeChargeRequest()
                            {
                                CustomerId = createStripeCustomerResponse.Customer.Id,
                                AmountInCents = (int)(packageType.BaseCost * 100),
                                Currency = "USD",
                                Description = packageType.Description
                            };


                            CreateStripeChargeResponse chargeResponse = operation.CreateStripeCharge(goldChargeRequest);

                            Charge transactionCharge = chargeResponse.Charge;

                            if (transactionCharge.Paid.Value == true)
                            {

                                user.Tier = "PAID";
                                user.FirstName = form["FirstName"].ToString();
                                user.LastName = form["LastName"].ToString();
                                user.PhoneNumberCell = form["PhoneNumberCell"].ToString();
                                user.PhoneNumberLandLine = form["PhoneNumberLandLine"].ToString();
                                user.Address = form["Address"].ToString();
                                user.City = form["City"].ToString();
                                user.Province = form["Province"].ToString();
                                user.BillingName = form["BillingName"].ToString();
                                user.BillingCity = form["BillingCity"].ToString();
                                user.BillingAddress = form["BillingAddress"].ToString();
                                user.BillingProvince = form["BillingProvince"].ToString();
                                user.BillingPostalCode = form["BillingPostalCode"].ToString();

                                user.MaxUploadCount = "6";

                                using (PackageListContext pkc = new PackageListContext())
                                {
                                    var pkg = pkc.PList.Where(x => x.PackageId == pkgId).FirstOrDefault();
                                    if (pkg != null)
                                        user.MaxUploadCount = pkg.UploadLimit.ToString();
                                } 


                                userContext.SaveChanges();

                                StripePayment stripePayment = new StripePayment()
                                {
                                    UserId = user.UserId,
                                    StripeCustomerId = createStripeCustomerResponse.Customer.Id,
                                    StripeChargeId = transactionCharge.Id,
                                    Amount = transactionCharge.AmountInCents.HasValue ? ((decimal)transactionCharge.AmountInCents.Value) / 100.00M : 0.00M,
                                    PaidByAgent = model.AgentPayOnUserBehalf
                                };

                                packageManager.CreateStripePayment(stripePayment);
                            }
                            else
                            {
                                ModelState.AddModelError("CardNumber", chargeResponse.Charge.FailureMessage);
                                return View(model);
                            }

                            //Logout Agent and login new user
                            //if (Session["AgentContext"] != null)
                            //{
                            //    if ((bool)Session["AgentContext"] == false)
                            //    {
                            //        WebSecurity.Login(model.UserName, model.Password);
                            //    }
                            //}

                            //Session.Add("ListingUserId", WebSecurity.GetUserId(model.UserName));

                            scope.Complete();

                            TempData["RegisterAddress"] = model.Address;
                            TempData["RegisterCity"] = model.City;
                            TempData["RegisterProvince"] = model.Province;
                            TempData["RegisterPostalCode"] = model.PostalCode;
                            TempData["RegisterContactPerson"] = model.FirstName + " " + model.LastName;
                            TempData["RegisterPhone"] = model.PhoneNumberCell;


                            return RedirectToLocal("/Listing/ListingTemplate/" + form["Session[currListingId]"]);

                            //return RedirectToAction("ListingTemplate", "Listing", routeValues: new {id = Session["currListingId"]});
                        }
                    }
                }
                catch (MembershipCreateUserException e)
                {
                    ModelState.AddModelError("", e.StatusCode.ToString());
                }
                catch (StripeException stripeException)
                {
                    try
                    {
                        using (UsersContext db = new UsersContext())
                        {
                            HOD.Models.RegistrationError registrationError = null;
                            if (stripeException.Error != null)
                            {
                                registrationError = new HOD.Models.RegistrationError()
                                {
                                    UserEmail = model.UserName,
                                    ErrorType = stripeException.Error.ErrorType,
                                    Code = stripeException.Error.Code,
                                    Charge = stripeException.Error.Charge,
                                    Message = stripeException.Error.Message,
                                    Parameter = stripeException.Error.Parameter,
                                    StripeError = stripeException.Error.StripeError,

                                    FirstName = model.FirstName,
                                    LastName = model.LastName,
                                    Address = model.Address,
                                    City = model.City,
                                    Province = model.Province,
                                    PostalCode = model.PostalCode,
                                    PhoneNumberCell = model.PhoneNumberCell,
                                    PhoneNumberLandLine = model.PhoneNumberLandLine,
                                    //Carrier = model.MobileCarrier,

                                    BillingName = model.BillingName,
                                    BillingAddress = model.BillingAddress,
                                    BillingCity = model.BillingCity,
                                    BillingProvince = model.BillingProvince,
                                    BillingPostalCode = model.BillingPostalCode,
                                };

                            }
                            else
                            {
                                registrationError = new HOD.Models.RegistrationError()
                                {
                                    UserEmail = model.UserName,
                                    Message = stripeException.Message,

                                    FirstName = model.FirstName,
                                    LastName = model.LastName,
                                    Address = model.Address,
                                    City = model.City,
                                    Province = model.Province,
                                    PostalCode = model.PostalCode,
                                    PhoneNumberCell = model.PhoneNumberCell,
                                    PhoneNumberLandLine = model.PhoneNumberLandLine,
                                    //Carrier = model.MobileCarrier,

                                    BillingName = model.BillingName,
                                    BillingAddress = model.BillingAddress,
                                    BillingCity = model.BillingCity,
                                    BillingProvince = model.BillingProvince,
                                    BillingPostalCode = model.BillingPostalCode,
                                };
                            }

                            db.RegistrationErrors.Add(registrationError);
                            db.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        String err = ex.Message;
                    }
                    finally
                    {
                        if (stripeException.Error != null)
                        {
                            System.Text.StringBuilder sb = new System.Text.StringBuilder("Credit Card Error: ");
                            sb.Append(stripeException.Message);
                            ModelState.AddModelError("CardNumber", sb.ToString());
                        }
                        else
                            ModelState.AddModelError("", stripeException.Message);
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                }
            }


            else if (userRegisterType == UserTypes.AGENT)
            {
                RegisterModel model = new RegisterModel();
                TryUpdateModel(model, form.ToValueProvider());

                if (!model.AgentPayOnUserBehalf)
                {
                    if (ValidateCreditCard(model.CardNumber, model.CVC) == false)
                    {
                        RegisterModel faultedModel = new RegisterModel();
                        TryUpdateModel(faultedModel, form.ToValueProvider());
                        return View(faultedModel);
                    }
                }

                if (model.isAgent == false)
                {
                    ModelState.AddModelError("isAgent", "Check-box is required");
                }
                if (!ModelState.IsValid)
                    return View(model);

                StripeOperation operation = new StripeOperation();
                CreateStripeCustomerResponse createStripeCustomerResponse = null;
                // Attempt to register the user
                try
                {
                    using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
                    {
                        using (UsersContext userContext = new UsersContext())
                        {
                            var userType = userContext.UserTypes.Where(u => u.Name == model.UserType).FirstOrDefault();

                            if (userType == null)
                                throw new ArgumentNullException("User Type not recognized.");
                            CreateStripeCustomerRequest createStripeCustomerRequest = new CreateStripeCustomerRequest()
                            {
                                Email = model.UserName,
                                Description = model.FirstName + " " + model.LastName + "(" + model.UserName + ")",
                                //CardAddressCountry = "CA",
                                CardAddressLine1 = model.BillingAddress,
                                //CardAddressLine2 = "",
                                CardAddressCity = model.BillingCity,
                                CardAddressState = model.BillingProvince,
                                CardAddressZip = model.BillingPostalCode,
                                CardCvc = model.CVC,
                                CardExpirationMonth = model.ExpirationMonth,
                                CardExpirationYear = model.ExpirationYear,
                                CardName = model.BillingName,
                                CardNumber = model.CardNumber,
                            };

                            createStripeCustomerResponse = operation.CreateStripeCustomer(createStripeCustomerRequest);
                            if (createStripeCustomerResponse == null)
                                throw new ArgumentException("Customer was not created");
                            else if (createStripeCustomerResponse.Customer == null)
                                throw new ArgumentException("Customer was not created");

                            //WebSecurity.CreateUserAndAccount(model.UserName, model.Password, propertyValues: new
                            //{
                            //    UserName = model.UserName,
                            //    FirstName = model.FirstName,
                            //    LastName = model.LastName,
                            //    Address = model.Address,
                            //    City = model.City,
                            //    Province = model.Province,
                            //    PostalCode = model.PostalCode,
                            //    PhoneNumberCell = model.PhoneNumberCell,
                            //    PhoneNumberLandLine = model.PhoneNumberLandLine,
                            //    UserTypeId = userType.UserTypeId,
                            //    BillingName = model.BillingName,
                            //    BillingAddress = model.BillingAddress,
                            //    BillingCity = model.BillingCity,
                            //    BillingProvince = model.BillingProvince,
                            //    BillingPostalCode = model.BillingPostalCode,
                            //    AgentURL = model.AgentURL.ToLower(),
                            //    isAgent = model.isAgent,
                            //    AgentID = model.AgentID,
                            //    StripeCustomerId = createStripeCustomerResponse.Customer.Id,
                            //    Tier = "PAID"
                            //});

                            TempData["RegisterAddress"] = model.Address;
                            TempData["RegisterCity"] = model.City;
                            TempData["RegisterProvince"] = model.Province;
                            TempData["RegisterPostalCode"] = model.PostalCode;
                            TempData["RegisterContactPerson"] = model.FirstName + " " + model.LastName;
                            TempData["RegisterPhone"] = model.PhoneNumberCell;
                            TempData["RegisterPhone2"] = model.PhoneNumberLandLine;
                            TempData["AgentUrl"] = model.AgentURL;

                            UserProfile user = userContext.UserProfiles.Where<UserProfile>(u => u.UserName == model.UserName).FirstOrDefault<UserProfile>();

                            //Add Package for user
                            PackageType packageType = packageManager.GetPackageTypeByName(PackageTypes.AGENTPRODUCER);
                            if (packageType == null)
                                throw new ArgumentException("Package '" + PackageTypes.AGENT + "' does not exists.");

                            GetStripePlanRequest AgentStripePlanRequest = new GetStripePlanRequest() { PlanId = packageType.Description };

                            GetStripePlanResponse StripeAgentPlanResponse = operation.GetStripePlan(AgentStripePlanRequest);
                            if (StripeAgentPlanResponse == null)
                                throw new ArgumentException("Payment Plan '" + packageType.Description + "' does not exists.");
                            else if (StripeAgentPlanResponse.Plan == null)
                                throw new ArgumentException("Payment Plan '" + packageType.Description + "' does not exists.");

                            CreateStripeSubscriptionRequest createStripeSubscriptionAGENTequest = new CreateStripeSubscriptionRequest()
                            {
                                CustomerId = createStripeCustomerResponse.Customer.Id,
                                Plan = StripeAgentPlanResponse.Plan.Id,
                                Quantity = 1
                            };

                            CreateStripeSubscriptionResponse createStripeSubscriptionAGENTResponse = operation.CreateStripeSubscription(createStripeSubscriptionAGENTequest);
                            if (createStripeSubscriptionAGENTResponse == null)
                                throw new ArgumentException("Subscription '" + StripeAgentPlanResponse.Plan.Id + "' was not created.");
                            else if (createStripeSubscriptionAGENTResponse.Subscription == null)
                                throw new ArgumentException("Subscription '" + StripeAgentPlanResponse.Plan.Id + "' was not created.");
                        }

                       // WebSecurity.Login(model.UserName, model.Password);

                        scope.Complete();

                        Session["TIER"] = "PAID";
                        //return RedirectToAction("ListingTemplate", "Listing", routeValues: new { id = Session["currListingId"] });
                        return RedirectToLocal("/Listing/ListingTemplate/" + form["Session[currListingId]"]);
                        
                        //return RedirectToAction("Create", "Listing");
                        //return RedirectToAction("Pages", "Agent");
                    }
                }
                catch (MembershipCreateUserException e)
                {
                    ModelState.AddModelError("", e.StatusCode.ToString());
                }
                catch (StripeException stripeException)
                {
                    try
                    {
                        using (UsersContext db = new UsersContext())
                        {
                            HOD.Models.RegistrationError registrationError = null;
                            if (stripeException.Error != null)
                            {
                                registrationError = new HOD.Models.RegistrationError()
                                {
                                    UserEmail = model.UserName,
                                    ErrorType = stripeException.Error.ErrorType,
                                    Code = stripeException.Error.Code,
                                    Charge = stripeException.Error.Charge,
                                    Message = stripeException.Error.Message,
                                    Parameter = stripeException.Error.Parameter,
                                    StripeError = stripeException.Error.StripeError,

                                    FirstName = model.FirstName,
                                    LastName = model.LastName,
                                    Address = model.Address,
                                    City = model.City,
                                    Province = model.Province,
                                    PostalCode = model.PostalCode,
                                    PhoneNumberCell = model.PhoneNumberCell,
                                    PhoneNumberLandLine = model.PhoneNumberLandLine,
                                    //Carrier = model.MobileCarrier,

                                    BillingName = model.BillingName,
                                    BillingAddress = model.BillingAddress,
                                    BillingCity = model.BillingCity,
                                    BillingProvince = model.BillingProvince,
                                    BillingPostalCode = model.BillingPostalCode,

                                    AgentID = model.AgentID,
                                    PayOnUserBehalf = model.AgentPayOnUserBehalf,
                                };

                            }
                            else
                            {
                                registrationError = new HOD.Models.RegistrationError()
                                {
                                    UserEmail = model.UserName,
                                    Message = stripeException.Message,

                                    FirstName = model.FirstName,
                                    LastName = model.LastName,
                                    Address = model.Address,
                                    City = model.City,
                                    Province = model.Province,
                                    PostalCode = model.PostalCode,
                                    PhoneNumberCell = model.PhoneNumberCell,
                                    PhoneNumberLandLine = model.PhoneNumberLandLine,
                                    //Carrier = model.MobileCarrier,
                                    AgentID = model.AgentID,
                                    PayOnUserBehalf = model.AgentPayOnUserBehalf,

                                    BillingName = model.BillingName,
                                    BillingAddress = model.BillingAddress,
                                    BillingCity = model.BillingCity,
                                    BillingProvince = model.BillingProvince,
                                    BillingPostalCode = model.BillingPostalCode,
                                };
                            }

                            db.RegistrationErrors.Add(registrationError);
                            db.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        String err = ex.Message;
                    }
                    finally
                    {
                        if (stripeException.Error != null)
                        {
                            System.Text.StringBuilder sb = new System.Text.StringBuilder("Credit Card Error: ");
                            sb.Append(stripeException.Message);
                            ModelState.AddModelError("CardNumber", sb.ToString());
                        }
                        else
                            ModelState.AddModelError("", stripeException.Message);
                    }

                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                }
            }

            // If we got this far, something failed, redisplay form


            RegisterModel faultedModelX = new RegisterModel();
            TryUpdateModel(faultedModelX, form.ToValueProvider());
            return View(faultedModelX);

        }


        [AllowAnonymous]
        public ActionResult Register(String UserType, String RedirectUrl)
        {
            //Session["PayOnUserBehalf"] = true;// This will be moved to the agent Controller. its only for testing here.

            TempData["UserType"] = UserType;
            //Determine who is paying
            bool payOnUserBehalf = false;

            if (Session["PayOnUserBehalf"] != null){
                payOnUserBehalf = (bool)Session["PayOnUserBehalf"];
            } else {
               Session["PayOnUserBehalf"] = payOnUserBehalf;
            }

            RegisterModel model = new RegisterModel();
            model.UserType = UserType;

            //Get current URL String to register 
            string uRL = Request.Url.Host;
            model.CopyBillingAddress = true;
            
            // Get Agent ID by URL

            if (Session["AGENTID"] == null){
                IDbAgent DB = new DbManager();
                model.AgentID = DB.GetAgentIDByURL(uRL);
            } else {
                model.AgentID = int.Parse(Session["AGENTID"].ToString());
            }

            if (String.IsNullOrEmpty(RedirectUrl) != true){
                ViewData.Add("redirectUrl", RedirectUrl);
            }

            bool isFreeTier = false;

            if (Session["TIER"] == null){
                isFreeTier = false;
            } else {
                if (Session["TIER"].ToString().ToUpper() == "FREE")
                {
                    isFreeTier = true;
                }
                else
                {
                    isFreeTier = false;
                }
            }

            Data.Entities.UserProfile userProfile;

            if (WebSecurity.IsAuthenticated){
               using (HOD.Data.Entities.HODContext hodContext = new HOD.Data.Entities.HODContext()){

                   userProfile = hodContext.UserProfiles.Where(user => user.UserId == WebSecurity.CurrentUserId).FirstOrDefault();
                   
                   if (userProfile.Tier.ToUpper() == "FREE"){
                       isFreeTier = true;
                   } else {
                       isFreeTier = false ;
                   }
                }
            }

            
            //If the aagent is paying, Get agent previous info
            if (isFreeTier == false)
            {
                if (payOnUserBehalf && UserType.ToLower() == "seller" && WebSecurity.IsAuthenticated)
                {

                    string CustID = "";
                    StripeOperation PaymentTransaction = new StripeOperation();

                    Session["PayOnUserBehalf"] = true;
                    model.AgentPayOnUserBehalf = true;
                    model.CopyBillingAddress = false;

                    UsersContext info = new UsersContext();
                    CustID = info.UserProfiles.First(m => m.UserId == WebSecurity.CurrentUserId).StripeCustomerId;

                    GetStripeCustomerRequest CustInfoREQ = new GetStripeCustomerRequest() { CustomerId = CustID };
                    GetStripeCustomerResponse CustInfoRESP = PaymentTransaction.GetStripeCustomer(CustInfoREQ);

                    var StripeCardInfo = CustInfoRESP.Customer.StripeCardList.Cards.First(m => m.Id == CustInfoRESP.Customer.StripeDefaultCardId);


                    model.BillingAddress = StripeCardInfo.AddressLine1;
                    model.BillingCity = StripeCardInfo.AddressCity;
                    model.BillingName = StripeCardInfo.Name;
                    model.BillingPostalCode = StripeCardInfo.AddressZip;
                    model.BillingProvince = StripeCardInfo.AddressState;

                    if (StripeCardInfo.ExpirationMonth.Length == 1)
                    {
                        model.ExpirationMonth = "0" + StripeCardInfo.ExpirationMonth;
                    }
                    else
                    {
                        model.ExpirationMonth = StripeCardInfo.ExpirationMonth;
                    }

                    model.ExpirationYear = StripeCardInfo.ExpirationYear;
                    model.CardNumber = "************" + StripeCardInfo.Last4;
                    model.CVC = "***";

                    System.Diagnostics.Trace.Write(StripeCardInfo);
                    System.Diagnostics.Trace.Write(model);
                }
            }

            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        public JsonResult doesUserEmailExist(string UserName)
        {
            var user = Membership.GetUser(UserName);

            return Json(user == null);
        }


        [HttpPost]
        [AllowAnonymous]
        public JsonResult AccountInformationUpgrade()
        {
            int id = WebSecurity.CurrentUserId;
            using (UsersContext userContext = new UsersContext())
            {
                var userProfile = userContext.UserProfiles.Where(x => x.UserId == id).FirstOrDefault();

                userProfile.FirstName = Request.Form["FirstName"].ToString();
                userProfile.LastName = Request.Form["LastName"].ToString();
                userProfile.PhoneNumberCell = Request.Form["PhoneNumberCell"].ToString();
                userProfile.PhoneNumberLandLine = Request.Form["PhoneNumberLandLine"].ToString();
                userProfile.PostalCode = Request.Form["PostalCode"].ToString();
                userProfile.City = Request.Form["City"].ToString();
                userProfile.Province = Request.Form["Province"].ToString();
                userProfile.Address = Request.Form["Address"].ToString();

                 
                userContext.SaveChanges();

            }


            
            return Json("Successfully updated");
        }

        public ActionResult MessageInbox()
        {
            return View();
        }

        public ActionResult MessageInbox(int userId)
        {
            return View();
        }

       [AllowAnonymous]
        public ActionResult RegisterFree(string userType)
        {
            RegisterBaseModel model = new RegisterBaseModel();

            model.UserType = userType;

            return View();
        }

       [HttpPost]
       [AllowAnonymous]
       public ActionResult RegisterExternal(FormCollection form)
       {

           string userType = "SELLER";
           string confirmationToken = string.Empty;
           bool generateConfirmationToken = false;

           RegisterFreeBaseModel registerModel = new RegisterFreeBaseModel()
           {
               UserName = form["Email"].ToString(),
               FirstName = form["First_Name"].ToString(),
               LastName = form["Last_Name"].ToString(),
               Password = form["Password"].ToString(),
               UserType = userType

           };
           try
           {
               CreateUser(registerModel.UserName, registerModel.FirstName, registerModel.LastName, registerModel.Password, generateConfirmationToken, registerModel.UserType);
               WebSecurity.Login(registerModel.UserName, registerModel.Password);
               Session["ListingUserId"] = WebSecurity.CurrentUserId;
           }
           catch (MembershipCreateUserException mex)
           {
               ModelState.AddModelError("username", mex.Message);
               return View("RegisterFree", registerModel);
           }

           return RedirectToAction("Create","Listing");
       }

       //[HttpPost]
       //[AllowAnonymous]
       //public ActionResult SellerRegistration(FormCollection form)
       //{
       //    string confirmationToken = string.Empty;
       //    bool generateConfirmationToken = false;

       //    RegisterFreeBaseModel registerModel = new RegisterFreeBaseModel()
       //    {
       //        UserName = form["UserName"],
       //        FirstName = form["FirstName"],
       //        LastName  = form["LastName"],
       //        Password  = form["Password"],
       //        UserType  = form["UserType"]

       //    };
       //    try
       //    {
       //        CreateUser(registerModel.UserName, registerModel.FirstName, registerModel.LastName, registerModel.Password, generateConfirmationToken, registerModel.UserType);
       //        WebSecurity.Login(registerModel.UserName, registerModel.Password);
       //        Session["ListingUserId"] = WebSecurity.CurrentUserId;
       //    }
       //    catch (MembershipCreateUserException mex)
       //    {
       //        ModelState.AddModelError("username", mex.Message);
       //        return View("RegisterFree", registerModel);
       //    }

       //    return RedirectToAction("Listing", "Create");

       //}


        [AllowAnonymous]
        public ActionResult RegisterFreeSave(RegisterFreeBaseModel registerModel)
        {
            string confirmationToken = string.Empty;
            bool generateConfirmationToken = false;

            if (!ModelState.IsValid){
                return View("RegisterFree", registerModel);
            }
            
            try  {
                CreateUser(registerModel.UserName, registerModel.FirstName, registerModel.LastName, registerModel.Password, generateConfirmationToken, registerModel.UserType);
                WebSecurity.Login(registerModel.UserName, registerModel.Password);

                
            }
            catch (MembershipCreateUserException mex){
                ModelState.AddModelError("username", mex.Message);
                return View("RegisterFree",registerModel);
            }

            AccountSetupModel returnModel = new AccountSetupModel();

            returnModel.FirstName = registerModel.FirstName;
            returnModel.LastName = registerModel.LastName;
            returnModel.UserType = registerModel.UserType;
            returnModel.UserName = registerModel.UserName;

            return View("AccountSetup", returnModel);
        }

        public ActionResult AccountSetup()
        {

            return View();
        }


        public ActionResult AccountCompleteSetup(int id)
        {
            UserProfile userProfile = new UserProfile();
            UserType userType = new UserType();

            using (UsersContext userContext = new UsersContext())
            {
                userProfile = userContext.UserProfiles.Where(x => x.UserId == id).FirstOrDefault();                
                userType = userContext.UserTypes.Where(x => x.UserTypeId == userProfile.UserTypeId).FirstOrDefault();
            }

            AccountSetupModel returnModel = new AccountSetupModel();

            returnModel.FirstName = userProfile.FirstName;
            returnModel.LastName = userProfile.LastName;
            returnModel.UserName = userProfile.UserName;
            returnModel.Password = "12345678"; //Fake password to pass the model
            returnModel.UserType = userType.Name.ToUpper();

            return View("AccountSetup", returnModel);
        }


        [HttpPost]
        public ActionResult AccountSetupSave(AccountSetupModel accountSetupModel)
        {
            UserProfile userProfile;

            if (!ModelState.IsValid)
            {
                return View("AccountSetup", accountSetupModel);
            }

            string redirectAction = string.Empty;
            string redirectController = string.Empty;
            bool isAgent = false;

            if (accountSetupModel.UserType.ToUpper() == "SELLER"){
                redirectAction = "Create";
                redirectController = "Listing";
                isAgent = false;
            } else {
                redirectAction = "Pages";
                redirectController = "Agent";
                isAgent = true;
                if (  String.IsNullOrEmpty(accountSetupModel.AgentSubDomain)){
                    ModelState.AddModelError("AgentSubDomain", "Agent Sub Domain is required");
                }
            }

            if (ModelState.IsValid)
            {
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
                {
                    using (UsersContext userContext = new UsersContext())
                    {
                        userProfile = userContext.UserProfiles.Where(user => user.UserId == WebSecurity.CurrentUserId).FirstOrDefault();
                        userProfile.FirstName = accountSetupModel.FirstName;
                        userProfile.LastName = accountSetupModel.LastName;
                        userProfile.PhoneNumberCell = accountSetupModel.PhoneNumber;
                        userProfile.Address = accountSetupModel.Address;
                        userProfile.City = accountSetupModel.City;
                        userProfile.Province = accountSetupModel.Province;
                        userProfile.PostalCode = accountSetupModel.PostalCode;
                        userProfile.AgentUrl = accountSetupModel.WebsiteUrl;
                        userProfile.NoOfEmployees = accountSetupModel.NoOfEmployees;
                        userProfile.BusinessName  = accountSetupModel.BusinessName;
                        userProfile.Country = accountSetupModel.Country;
                        userProfile.IsAgent = isAgent;
                        userProfile.AgentSubDomain = accountSetupModel.AgentSubDomain;
                        userProfile.AgentId = Convert.ToInt32(this.ConfigurationSettingValue("DefaultAgentId"));
                        userContext.SaveChanges();

                        if (accountSetupModel.UserType.ToUpper() == "SELLER") {
                            Session["ListingUserId"] = userProfile.UserId;
                            TempData["RegisterAddress"] = accountSetupModel.Address;
                            TempData["RegisterCity"] = accountSetupModel.City;
                            TempData["RegisterProvince"] = accountSetupModel.Province;
                            TempData["RegisterPostalCode"] = accountSetupModel.PostalCode;
                            TempData["RegisterContactPerson"] = accountSetupModel.FirstName + " " + accountSetupModel.LastName;
                            TempData["RegisterPhone"] = accountSetupModel.PhoneNumber;
                        }
                    }

                    scope.Complete();
                }

            }
            return RedirectToAction(redirectAction,redirectController);
        }

        private void CreateUser(string userName, string firstName, string lastName, string password, bool generateConfirmationToken, string userType){

            UserType userTypeX;

            using (UsersContext userContext = new UsersContext()){
                userTypeX = userContext.UserTypes.Where(x => x.Name == userType).FirstOrDefault();
            }

            bool isAgentType = false;
            int agentId = 0;
            agentId = Convert.ToInt32(ConfigurationSettingValue("DefaultAgentId"));

            if (userType.ToUpper() == "AGENT"){
                isAgentType = true;
            } else {
                isAgentType = false;
            }

            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
            {
                using (UsersContext userContext = new UsersContext())
                { 
                   WebSecurity.CreateUserAndAccount(userName, password, propertyValues: new
                    {
                        UserName = userName,
                        FirstName = firstName,
                        LastName = lastName,
                        Tier =   AccountTiers.FREE,
                        UserTypeId = userTypeX.UserTypeId,
                        isAgent = isAgentType,
                        AgentID = agentId
                    });
                }

                scope.Complete();
            }
        }


        //
        // POST: /Account/Register

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Register(FormCollection form)
        {
            var userRegisterType = form["UserType"];

            bool isFreeTier = false;
            bool agentPayForCustomer = false;

            const string TIER_SESSIONKEY = "TIER";
            const string TIERS_FREE = "FREE";

            if (Session[TIER_SESSIONKEY] == null){
                isFreeTier = false;
            } else {
                if (Session[TIER_SESSIONKEY].ToString().ToUpper() == TIERS_FREE){
                    isFreeTier = true;
                } else {
                    isFreeTier = false;
                }
            }    

            if (isFreeTier){
                agentPayForCustomer = false;
            }

            #region BUYER 
            if (userRegisterType == UserTypes.BUYER){
               ActionResult buyerSaveResult = BuyerRegistration(form);
               if (buyerSaveResult != null){
                   return buyerSaveResult;
               }

               RegisterBaseModel buyerModel = new RegisterBaseModel();
               TryUpdateModel(buyerModel, form.ToValueProvider());
               return View(buyerModel);
            }
            #endregion

            if (userRegisterType == UserTypes.SELLER){

                RegisterModel model = new RegisterModel();
                RegisterFreeCustomer customerFreeModel = new RegisterFreeCustomer();

                string currentUserName = string.Empty;

                if (isFreeTier){

                    using (TransactionScope transScope = new TransactionScope(TransactionScopeOption.RequiresNew))
                    {
                        try
                        {
                            TryUpdateModel(customerFreeModel, form.ToValueProvider());
                            currentUserName = customerFreeModel.UserName;
                            if (!RegisterFreeSeller(customerFreeModel))
                            {
                                RegisterFreeCustomer faultedModelFree = new RegisterFreeCustomer();
                                TryUpdateModel(faultedModelFree, form.ToValueProvider());
                                return View(faultedModelFree);
                            }

                            //Logout Agent and login new user
                            if (Session["AgentContext"] != null)
                            {
                                if ((bool)Session["AgentContext"] == false)
                                {
                                    WebSecurity.Login(currentUserName, customerFreeModel.Password);
                                }
                            }

                            Session.Add("ListingUserId", WebSecurity.GetUserId(currentUserName));
                            transScope.Complete();
                        }
                        catch (TransactionException ex)
                        {
                            ModelState.AddModelError("username", ex.Message);
                            RegisterFreeCustomer faultedModelFree = new RegisterFreeCustomer();
                            TryUpdateModel(faultedModelFree, form.ToValueProvider());
                            return View(faultedModelFree);
                        }
                    }

                    if (isFreeTier){
                        TempData["RegisterAddress"] = customerFreeModel.Address;
                        TempData["RegisterCity"] = customerFreeModel.City;
                        TempData["RegisterProvince"] = customerFreeModel.Province;
                        TempData["RegisterPostalCode"] = customerFreeModel.PostalCode;
                        TempData["RegisterContactPerson"] = customerFreeModel.FirstName + " " + customerFreeModel.LastName;
                        TempData["RegisterPhone"] = customerFreeModel.PhoneNumberCell;
                    }
                    else {
                        TempData["RegisterAddress"] = model.Address;
                        TempData["RegisterCity"] = model.City;
                        TempData["RegisterProvince"] = model.Province;
                        TempData["RegisterPostalCode"] = model.PostalCode;
                        TempData["RegisterContactPerson"] = model.FirstName + " " + model.LastName;
                        TempData["RegisterPhone"] = model.PhoneNumberCell;
                    }
                    if (!WebSecurity.IsAuthenticated)
                    {
                        WebSecurity.Login(form["UserName"].ToString(), form["Password"].ToString(), true);
                    }

                    return RedirectToAction("Create", "Listing");
                }


                TryUpdateModel(model, form.ToValueProvider());

                if (!model.AgentPayOnUserBehalf){
                    if (ValidateCreditCard(model.CardNumber, model.CVC) == false)
                    {
                        RegisterModel faultedModel = new RegisterModel();
                        TryUpdateModel(faultedModel, form.ToValueProvider());
                        return View(faultedModel);
                    }
                }


                StripeOperation operation = new StripeOperation();
                CreateStripeCustomerResponse createStripeCustomerResponse = new CreateStripeCustomerResponse();

                try  {            
                    using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
                    {
                        using (UsersContext userContext = new UsersContext())
                        {
                            string currentUserType = isFreeTier == true ? customerFreeModel.UserType : model.UserType;
                            currentUserType = currentUserType.ToUpper();

                            var userType = userContext.UserTypes.Where(u => u.Name.ToUpper() == currentUserType).FirstOrDefault();

                            if (userType == null)
                                throw new ArgumentNullException("User Type not recognized.");

                            if (!model.AgentPayOnUserBehalf){
                                agentPayForCustomer = false;
                            } else {
                                agentPayForCustomer = true;
                            }

                            if (agentPayForCustomer == false)
                            {
                                //Customer paying for product himself
                                createStripeCustomerResponse = CreateStripeCustomer(model, operation, createStripeCustomerResponse);
                            }

                            WebSecurity.CreateUserAndAccount(model.UserName, model.Password, propertyValues: new
                            {
                                UserName = model.UserName, FirstName = model.FirstName,  
                                LastName = model.LastName,
                                Address = model.Address, 
                                City = model.City,
                                Province = model.Province, 
                                PostalCode = model.PostalCode,
                                PhoneNumberCell = model.PhoneNumberCell, 
                                PhoneNumberLandLine = model.PhoneNumberLandLine,
                                UserTypeId = userType.UserTypeId,
                                BillingName = model.BillingName,
                                BillingAddress = model.BillingAddress,
                                BillingCity = model.BillingCity,
                                BillingProvince = model.BillingProvince,
                                BillingPostalCode = model.BillingPostalCode,
                                isAgent = model.isAgent,
                                AgentID = model.AgentID,
                                StripeCustomerId = isFreeTier ? null : createStripeCustomerResponse.Customer.Id,
                                Tier = Session["TIER"]
                            });

                            //Get Stripe User from database
                            if (agentPayForCustomer && (bool)Session["PayOnUserBehalf"] && WebSecurity.IsAuthenticated)
                            {
                                createStripeCustomerResponse = RetrieveStripeCustomerData(operation);
                            }

                            UserProfile user = userContext.UserProfiles.Where<UserProfile>(u => u.UserName == model.UserName).FirstOrDefault<UserProfile>();

                            UserPackage userPackage = null;
                            List<PackageFeature> packageFeatues = new List<PackageFeature>();                   

                            PackageType packageType = packageManager.GetPackageTypeByName(PackageTypes.GOLD);
                            //user selected package
                            PackageList userChosenPackage;
                            int pkgId = Convert.ToInt32(form["Package"].ToString());
                            using (HOD.Models.PackageListContext pl = new HOD.Models.PackageListContext())
                            {

                                userChosenPackage = pl.PList.Where(x => x.PackageId == pkgId).FirstOrDefault();
                            } 
                            //Add Package for user
                            userPackage = new UserPackage(){
                                PackageTypeId = packageType.PackageTypeId,
                                UserId = user.UserId
                            };

                            packageType.BaseCost = userChosenPackage.PackageAmount;
                            //Create InvoiceItem
                            CreateStripeInvoiceItemRequest createStripeInvoiceItemGOLDRequest = CreateInvoiceItemRequest(createStripeCustomerResponse, packageType);

                            CreateStripeInvoiceItemResponse createStripeInvoiceGOLDResponse = operation.CreateStripeInvoiceItem(createStripeInvoiceItemGOLDRequest);
                            if (createStripeInvoiceGOLDResponse == null)
                                throw new ArgumentException("InvoiceItem '" + createStripeInvoiceGOLDResponse.InvoiceItem.Id + "' was not created.");
                            else if (createStripeInvoiceGOLDResponse.InvoiceItem == null)
                                throw new ArgumentException("InvoiceItem '" + createStripeInvoiceGOLDResponse.InvoiceItem.Id + "' was not created.");

                            userPackage = packageManager.SaveUserPackage(userPackage, packageFeatues);

                            CreateAndPayStripeInvoiceRequest createAndPayStripeInvoice = new CreateAndPayStripeInvoiceRequest()
                            {
                                CustomerId = createStripeCustomerResponse.Customer.Id
                            };


                            //Doing a direct charge and not invoicing, this is one off. Description will aptly tell customer
                            /*CreateAndPayStripeInvoiceResponse createAndPayStripeInvoiceResponse = operation.CreateAndPayStripeInvoice(createAndPayStripeInvoice);*/
                            //Lawn sign order

                            double packageprice =0;
                            String packagename = form["selected_package_name"].ToString();

                            if (packagename == "Basic")
                                packageprice = 0;
                            else if(packagename == "Standard")
                                packageprice = 29.45;
                            else
                                packageprice = 149.45;

                            int lawnprice = (int)(packageprice*100);

                            int total_price = (int)(packageType.BaseCost * 100);
                            if (form["order_later"].ToString() == "1")
                            {
                               total_price = (int)(packageType.BaseCost * 100) + (int)(Convert.ToDouble(packageprice)*100);

                               using (UserOrderContext uoc = new UserOrderContext())
                               {
                                   UserOrder uo = new UserOrder(){
                                       UserId = user.UserId,
                                       PackageName = packagename,
                                       PackagePrice = lawnprice                                  
                                   };

                                   uoc.SaveChanges();
                                    
                               }
                            }
                           

                            CreateStripeChargeRequest goldChargeRequest = new CreateStripeChargeRequest()
                            {
                                CustomerId = createStripeCustomerResponse.Customer.Id,
                                AmountInCents = total_price,
                                Currency = "USD",
                                Description = packageType.Description
                            };


                            CreateStripeChargeResponse chargeResponse = operation.CreateStripeCharge(goldChargeRequest);

                            Charge transactionCharge = chargeResponse.Charge;

                            if (transactionCharge.Paid.Value == true)
                            {
                                if (userRegisterType != null)
                                {
                                    user.Tier = "PAID";
                                    user.FirstName = form["FirstName"].ToString();
                                    user.LastName = form["LastName"].ToString();
                                    user.PhoneNumberCell = form["PhoneNumberCell"].ToString();
                                    user.PhoneNumberLandLine = form["PhoneNumberLandLine"].ToString();
                                    user.Address = form["Address"].ToString();
                                    user.City = form["City"].ToString();
                                    user.Province = form["Province"].ToString();
                                    user.BillingName = form["BillingName"].ToString();
                                    user.BillingCity = form["BillingCity"].ToString();
                                    user.BillingAddress = form["BillingAddress"].ToString();
                                    user.BillingProvince = form["BillingProvince"].ToString();
                                    user.BillingPostalCode = form["BillingPostalCode"].ToString();

                                    user.MaxUploadCount = "6";

                                    using(PackageListContext pkc = new PackageListContext()){
                                        var pkg = pkc.PList.Where(x => x.PackageId == pkgId).FirstOrDefault();
                                        if(pkg != null)
                                        user.MaxUploadCount = pkg.UploadLimit.ToString();
                                    } 
                                    
                                }
                                userContext.SaveChanges();
                                

                                StripePayment stripePayment = new StripePayment()
                                {
                                    UserId = user.UserId,
                                    StripeCustomerId = createStripeCustomerResponse.Customer.Id,
                                    StripeChargeId = transactionCharge.Id,
                                    Amount = transactionCharge.AmountInCents.HasValue ? ((decimal)transactionCharge.AmountInCents.Value) / 100.00M : 0.00M,
                                    PaidByAgent = model.AgentPayOnUserBehalf
                                };

                                packageManager.CreateStripePayment(stripePayment);
                            }
                            else
                            {
                                ModelState.AddModelError("CardNumber", chargeResponse.Charge.FailureMessage);
                                return View(model);
                            }

                            //Logout Agent and login new user
                            if (Session["AgentContext"] != null)
                            {
                                if ((bool)Session["AgentContext"] == false)
                                {
                                    WebSecurity.Login(model.UserName, model.Password);
                                }
                            }

                            if (!WebSecurity.IsAuthenticated)
                            {
                                WebSecurity.Login(model.UserName, model.Password);
                            }

                            Session.Add("ListingUserId", WebSecurity.GetUserId(model.UserName));

                            scope.Complete();

                            TempData["RegisterAddress"] = model.Address;
                            TempData["RegisterCity"] = model.City;
                            TempData["RegisterProvince"] = model.Province;
                            TempData["RegisterPostalCode"] = model.PostalCode;
                            TempData["RegisterContactPerson"] = model.FirstName + " " + model.LastName;
                            TempData["RegisterPhone"] = model.PhoneNumberCell;

                            return RedirectToAction("Create", "Listing");
                        }
                    }
                }
            catch (MembershipCreateUserException e)
            {              
                ModelState.AddModelError("", e.StatusCode.ToString());
            }
            catch (StripeException stripeException)
            {
                try
                {
                    using (UsersContext db = new UsersContext())
                    {
                        HOD.Models.RegistrationError registrationError = null;
                        if (stripeException.Error != null)
                        {
                            registrationError = new HOD.Models.RegistrationError()
                            {
                                UserEmail = model.UserName,
                                ErrorType = stripeException.Error.ErrorType,
                                Code = stripeException.Error.Code,
                                Charge = stripeException.Error.Charge,
                                Message = stripeException.Error.Message,
                                Parameter = stripeException.Error.Parameter,
                                StripeError = stripeException.Error.StripeError,

                                FirstName = model.FirstName,
                                LastName = model.LastName,
                                Address = model.Address,
                                City = model.City,
                                Province = model.Province,
                                PostalCode = model.PostalCode,
                                PhoneNumberCell = model.PhoneNumberCell,
                                PhoneNumberLandLine = model.PhoneNumberLandLine,
                                //Carrier = model.MobileCarrier,

                                BillingName = model.BillingName,
                                BillingAddress = model.BillingAddress,
                                BillingCity = model.BillingCity,
                                BillingProvince = model.BillingProvince,
                                BillingPostalCode = model.BillingPostalCode,
                            };

                        }
                        else
                        {
                            registrationError = new HOD.Models.RegistrationError()
                            {
                                UserEmail = model.UserName,
                                Message = stripeException.Message,

                                FirstName = model.FirstName,
                                LastName = model.LastName,
                                Address = model.Address,
                                City = model.City,
                                Province = model.Province,
                                PostalCode = model.PostalCode,
                                PhoneNumberCell = model.PhoneNumberCell,
                                PhoneNumberLandLine = model.PhoneNumberLandLine,
                                //Carrier = model.MobileCarrier,

                                BillingName = model.BillingName,
                                BillingAddress = model.BillingAddress,
                                BillingCity = model.BillingCity,
                                BillingProvince = model.BillingProvince,
                                BillingPostalCode = model.BillingPostalCode,
                            };
                        }

                        db.RegistrationErrors.Add(registrationError);
                        db.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    String err = ex.Message;
                }
                finally
                {
                    if (stripeException.Error != null)
                    {
                        System.Text.StringBuilder sb = new System.Text.StringBuilder("Credit Card Error: ");
                        sb.Append(stripeException.Message);
                        ModelState.AddModelError("CardNumber", sb.ToString());
                    }
                    else
                        ModelState.AddModelError("", stripeException.Message);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
            }
    }
                    

            else if (userRegisterType == UserTypes.AGENT)
            {
                RegisterModel model = new RegisterModel();
                TryUpdateModel(model, form.ToValueProvider());

                if (!model.AgentPayOnUserBehalf){
                    if (ValidateCreditCard(model.CardNumber, model.CVC) == false)
                    {
                        RegisterModel faultedModel = new RegisterModel();
                        TryUpdateModel(faultedModel, form.ToValueProvider());
                        return View(faultedModel);
                    }
                }

                if (model.isAgent == false)
                {
                    ModelState.AddModelError("isAgent", "Check-box is required");
                }
                if (!ModelState.IsValid)
                    return View(model);

                StripeOperation operation = new StripeOperation();
                CreateStripeCustomerResponse createStripeCustomerResponse = null;
                // Attempt to register the user
                try
                {
                    using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
                    {
                        using (UsersContext userContext = new UsersContext())
                        {
                            var userType = userContext.UserTypes.Where(u => u.Name == model.UserType).FirstOrDefault();

                            if (userType == null)
                                throw new ArgumentNullException("User Type not recognized.");
                            CreateStripeCustomerRequest createStripeCustomerRequest = new CreateStripeCustomerRequest()
                            {
                                Email = model.UserName,
                                Description = model.FirstName + " " + model.LastName + "(" + model.UserName + ")",
                                //CardAddressCountry = "CA",
                                CardAddressLine1 = model.BillingAddress,
                                //CardAddressLine2 = "",
                                CardAddressCity = model.BillingCity,
                                CardAddressState = model.BillingProvince,
                                CardAddressZip = model.BillingPostalCode,
                                CardCvc = model.CVC,
                                CardExpirationMonth = model.ExpirationMonth,
                                CardExpirationYear = model.ExpirationYear,
                                CardName = model.BillingName,
                                CardNumber = model.CardNumber,
                            };

                            createStripeCustomerResponse = operation.CreateStripeCustomer(createStripeCustomerRequest);
                            if (createStripeCustomerResponse == null)
                                throw new ArgumentException("Customer was not created");
                            else if (createStripeCustomerResponse.Customer == null)
                                throw new ArgumentException("Customer was not created");

                            WebSecurity.CreateUserAndAccount(model.UserName, model.Password, propertyValues: new
                                {
                                    UserName = model.UserName,
                                    FirstName = model.FirstName,
                                    LastName = model.LastName,
                                    Address = model.Address,
                                    City = model.City,
                                    Province = model.Province,
                                    PostalCode = model.PostalCode,
                                    PhoneNumberCell = model.PhoneNumberCell,
                                    PhoneNumberLandLine = model.PhoneNumberLandLine,
                                    UserTypeId = userType.UserTypeId,
                                    BillingName = model.BillingName,
                                    BillingAddress = model.BillingAddress,
                                    BillingCity = model.BillingCity,
                                    BillingProvince = model.BillingProvince,
                                    BillingPostalCode = model.BillingPostalCode,
                                    AgentURL = model.AgentURL.ToLower(),
                                    isAgent = model.isAgent,
                                    AgentID = model.AgentID,
                                    StripeCustomerId = createStripeCustomerResponse.Customer.Id,
                                    Tier = "PAID"
                                });

                            TempData["RegisterAddress"] = model.Address;
                            TempData["RegisterCity"] = model.City;
                            TempData["RegisterProvince"] = model.Province;
                            TempData["RegisterPostalCode"] = model.PostalCode;
                            TempData["RegisterContactPerson"] = model.FirstName + " " + model.LastName;
                            TempData["RegisterPhone"] = model.PhoneNumberCell;
                            TempData["RegisterPhone2"] = model.PhoneNumberLandLine;
                            TempData["AgentUrl"] = model.AgentURL;

                            UserProfile user = userContext.UserProfiles.Where<UserProfile>(u => u.UserName == model.UserName).FirstOrDefault<UserProfile>();

                            //Add Package for user
                            PackageType packageType = packageManager.GetPackageTypeByName(PackageTypes.AGENTPRODUCER);
                            if (packageType == null)
                                throw new ArgumentException("Package '" + PackageTypes.AGENT + "' does not exists.");

                            GetStripePlanRequest AgentStripePlanRequest = new GetStripePlanRequest() {PlanId=packageType.Description};

                            GetStripePlanResponse StripeAgentPlanResponse = operation.GetStripePlan(AgentStripePlanRequest);
                            if (StripeAgentPlanResponse == null)
                                throw new ArgumentException("Payment Plan '" + packageType.Description + "' does not exists.");
                            else if (StripeAgentPlanResponse.Plan == null)
                                throw new ArgumentException("Payment Plan '" + packageType.Description + "' does not exists.");

                            CreateStripeSubscriptionRequest createStripeSubscriptionAGENTequest = new CreateStripeSubscriptionRequest()
                            {
                                CustomerId = createStripeCustomerResponse.Customer.Id,
                                Plan = StripeAgentPlanResponse.Plan.Id,
                              Quantity = 1
                            };

                            CreateStripeSubscriptionResponse createStripeSubscriptionAGENTResponse = operation.CreateStripeSubscription(createStripeSubscriptionAGENTequest);
                            if (createStripeSubscriptionAGENTResponse == null)
                                throw new ArgumentException("Subscription '" + StripeAgentPlanResponse.Plan.Id + "' was not created.");
                            else if (createStripeSubscriptionAGENTResponse.Subscription == null)
                                throw new ArgumentException("Subscription '" + StripeAgentPlanResponse.Plan.Id + "' was not created.");
                         }

                        WebSecurity.Login(model.UserName, model.Password);

                        scope.Complete();

                        Session["TIER"] = "PAID";
                        return RedirectToAction("Create", "Listing");
                        //return RedirectToAction("Pages", "Agent");
                    }
                }
                catch (MembershipCreateUserException e)
                {
                    ModelState.AddModelError("", e.StatusCode.ToString());
                }
                catch (StripeException stripeException)
                {
                    try
                    {
                        using (UsersContext db = new UsersContext())
                        {
                            HOD.Models.RegistrationError registrationError = null;
                            if (stripeException.Error != null)
                            {
                                registrationError = new HOD.Models.RegistrationError()
                                {
                                    UserEmail = model.UserName,
                                    ErrorType = stripeException.Error.ErrorType,
                                    Code = stripeException.Error.Code,
                                    Charge = stripeException.Error.Charge,
                                    Message = stripeException.Error.Message,
                                    Parameter = stripeException.Error.Parameter,
                                    StripeError = stripeException.Error.StripeError,

                                    FirstName = model.FirstName,
                                    LastName = model.LastName,
                                    Address = model.Address,
                                    City = model.City,
                                    Province = model.Province,
                                    PostalCode = model.PostalCode,
                                    PhoneNumberCell = model.PhoneNumberCell,
                                    PhoneNumberLandLine = model.PhoneNumberLandLine,
                                    //Carrier = model.MobileCarrier,

                                    BillingName = model.BillingName,
                                    BillingAddress = model.BillingAddress,
                                    BillingCity = model.BillingCity,
                                    BillingProvince = model.BillingProvince,
                                    BillingPostalCode = model.BillingPostalCode,

                                    AgentID = model.AgentID,
                                    PayOnUserBehalf = model.AgentPayOnUserBehalf,
                                };

                            }
                            else
                            {
                                registrationError = new HOD.Models.RegistrationError()
                                {
                                    UserEmail = model.UserName,
                                    Message = stripeException.Message,

                                    FirstName = model.FirstName,
                                    LastName = model.LastName,
                                    Address = model.Address,
                                    City = model.City,
                                    Province = model.Province,
                                    PostalCode = model.PostalCode,
                                    PhoneNumberCell = model.PhoneNumberCell,
                                    PhoneNumberLandLine = model.PhoneNumberLandLine,
                                    //Carrier = model.MobileCarrier,
                                    AgentID = model.AgentID,
                                    PayOnUserBehalf = model.AgentPayOnUserBehalf,

                                    BillingName = model.BillingName,
                                    BillingAddress = model.BillingAddress,
                                    BillingCity = model.BillingCity,
                                    BillingProvince = model.BillingProvince,
                                    BillingPostalCode = model.BillingPostalCode,
                                };
                            }

                            db.RegistrationErrors.Add(registrationError);
                            db.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        String err = ex.Message;
                    }
                    finally
                    {
                        if (stripeException.Error != null)
                        {
                            System.Text.StringBuilder sb = new System.Text.StringBuilder("Credit Card Error: ");
                            sb.Append(stripeException.Message);
                            ModelState.AddModelError("CardNumber", sb.ToString());
                        }
                        else
                            ModelState.AddModelError("", stripeException.Message);
                    }

                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                }
            }

            // If we got this far, something failed, redisplay form


            RegisterModel faultedModelX = new RegisterModel();
            TryUpdateModel(faultedModelX, form.ToValueProvider());
            return View(faultedModelX);           

        }

        private static CreateStripeInvoiceItemRequest CreateInvoiceItemRequest(CreateStripeCustomerResponse createStripeCustomerResponse, PackageType packageType)
        {
            CreateStripeInvoiceItemRequest invoiceItemRequest = new CreateStripeInvoiceItemRequest()
            {
                CustomerId = createStripeCustomerResponse.Customer.Id,
                Currency = "usd",
                AmountInCents = (int)(packageType.BaseCost * 100),
                Description = packageType.Description
            };
            return invoiceItemRequest;
        }

        private bool ValidateCreditCard(string creditCardNo, string cvv){

            bool isValidCreditCard = false;
            UInt16 errorCount = 0;

            if (String.IsNullOrEmpty(cvv)){
                ModelState.AddModelError("CVC", "No CVC entered");
                errorCount++;
                return false;
            }

            if (String.IsNullOrEmpty(creditCardNo))
            {
                ModelState.AddModelError("CardNumber", "No Card number entered");
                errorCount++;
                return false;
            }

            if (!Regex.Match(cvv, @"^(?!000)\d{3,4}$").Success){
                ModelState.AddModelError("CVC", "Invalid CVC");
                errorCount++;
            }

            if (!Regex.Match(creditCardNo, @"^((4\d{3})|(5[1-5]\d{2}))(-?|\040?)(\d{4}(-?|\040?)){3}|^(3[4,7]\d{2})(-?|\040?)\d{6}(-?|\040?)\d{5}").Success) {
                ModelState.AddModelError("CardNumber", "Invalid Card number");
                errorCount++;
            }

            if (errorCount == 0){
                isValidCreditCard = true;
            }

            return isValidCreditCard;
        }

        private bool RegisterFreeSeller(RegisterFreeCustomer customerFreeModel)
        {
            bool isSuccess = false;
            if (ModelState.IsValid)
            {
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
                {
                    using (UsersContext userContext = new UsersContext())
                    {

                        string currentUserType = customerFreeModel.UserType;
                        currentUserType = currentUserType.ToUpper();

                        var userType = userContext.UserTypes.Where(u => u.Name.ToUpper() == currentUserType).FirstOrDefault();

                        if (userType == null)
                            throw new ArgumentNullException("User Type not recognized.");

                        WebSecurity.CreateUserAndAccount(customerFreeModel.UserName, customerFreeModel.Password, propertyValues: new
                        {
                            UserName = customerFreeModel.UserName,
                            FirstName = customerFreeModel.FirstName,
                            LastName = customerFreeModel.LastName,
                            Address = customerFreeModel.Address,
                            City = customerFreeModel.City,
                            Province = customerFreeModel.Province,
                            PostalCode = customerFreeModel.PostalCode,
                            PhoneNumberCell = customerFreeModel.PhoneNumberCell,
                            PhoneNumberLandLine = customerFreeModel.PhoneNumberLandLine,
                            UserTypeId = userType.UserTypeId,
                            BillingName = string.Empty,
                            BillingAddress = string.Empty,
                            BillingCity = string.Empty,
                            BillingProvince = string.Empty,
                            BillingPostalCode = string.Empty,
                            isAgent = false,
                            AgentID = Session["AgentId"].ToString(),
                            Tier = Session["TIER"]
                        });

                    }
                    scope.Complete();
                    isSuccess = true;
                }
            }
            return isSuccess;
        }

        private ActionResult BuyerRegistration(FormCollection form)
        {
            RegisterBaseModel model = new RegisterBaseModel();

           
            TryUpdateModel(model, form.ToValueProvider());


            ActionResult returnAddress = null;
            try
            {
                if (ModelState.IsValid) {
                    string postRegInstructions;

                    postRegInstructions = SaveAndCreateBuyerAccount(model, form);
                    if (postRegInstructions == String.Empty){
                       returnAddress = RedirectToAction("Index", "Home");

                    } else {
                       returnAddress = Redirect(postRegInstructions);
                    }
                }
            }
            catch (MembershipCreateUserException e)
            {
                ModelState.AddModelError("", ErrorCodeToString(e.StatusCode));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
            }

            return returnAddress;
        }


        private string SaveAndCreateBuyerAccount(RegisterBaseModel model, FormCollection form){

                 using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
                    {
                        using (UsersContext userContext = new UsersContext())
                        {
                            var userType = userContext.UserTypes.Where(u => u.Name == UserTypes.BUYER).FirstOrDefault();

                            if (userType == null)
                                throw new ArgumentNullException("User Type not recognized.");

                            WebSecurity.CreateUserAndAccount(model.UserName, model.Password, propertyValues: new
                            {
                                UserName = model.UserName,
                                FirstName = model.FirstName,
                                LastName = model.LastName,
                                Address = model.Address,
                                City = model.City,
                                Province = model.Province,
                                PostalCode = model.PostalCode,
                                PhoneNumberCell = model.PhoneNumberCell,
                                PhoneNumberLandLine = model.PhoneNumberLandLine,
                                //Carrier = model.MobileCarrier,
                                UserTypeId = userType.UserTypeId,
                                //,PrivateMessageFeature = false
                                isAgent = false,
                                AgentID = -1
                            });
                        }
                        WebSecurity.Login(model.UserName, model.Password);
                        scope.Complete();
                    }

                    if (form["redirectUrl"].ToString().Length > 0)
                    {
                        return form["redirectUrl"].ToString();
                    }
                    else
                    {
                        return String.Empty; ;
                    }
        }

        private static CreateStripeCustomerResponse CreateStripeCustomer(RegisterModel model, StripeOperation operation, CreateStripeCustomerResponse createStripeCustomerResponse)
        {
            CreateStripeCustomerRequest createStripeCustomerRequest = new CreateStripeCustomerRequest()
            {
                Email = model.UserName,
                Description = model.FirstName + " " + model.LastName + "(" + model.UserName + ")",
                //CardAddressCountry = "CA",
                CardAddressLine1 = model.BillingAddress,
                //CardAddressLine2 = "",
                CardAddressCity = model.BillingCity,
                CardAddressState = model.BillingProvince,
                CardAddressZip = model.BillingPostalCode,
                CardCvc = model.CVC,
                CardExpirationMonth = model.ExpirationMonth,
                CardExpirationYear = model.ExpirationYear,
                CardName = model.BillingName,
                CardNumber = model.CardNumber,
            };

            createStripeCustomerResponse = operation.CreateStripeCustomer(createStripeCustomerRequest);

            if (createStripeCustomerResponse == null)
                throw new ArgumentException("Customer was not created");
            else if (createStripeCustomerResponse.Customer == null)
                throw new ArgumentException("Customer was not created");
            
            return createStripeCustomerResponse;
        }

        private static CreateStripeCustomerResponse CreateStripeCustomerOnUpgrade(UpgradeModel model, StripeOperation operation, CreateStripeCustomerResponse createStripeCustomerResponse)
        {
            CreateStripeCustomerRequest createStripeCustomerRequest = new CreateStripeCustomerRequest()
            {
                Email = model.UserName,
                Description = model.FirstName + " " + model.LastName + "(" + model.UserName + ")",
                //CardAddressCountry = "CA",
                CardAddressLine1 = model.BillingAddress,
                //CardAddressLine2 = "",
                CardAddressCity = model.BillingCity,
                CardAddressState = model.BillingProvince,
                CardAddressZip = model.BillingPostalCode,
                CardCvc = model.CVC,
                CardExpirationMonth = model.ExpirationMonth,
                CardExpirationYear = model.ExpirationYear,
                CardName = model.BillingName,
                CardNumber = model.CardNumber,
            };

            createStripeCustomerResponse = operation.CreateStripeCustomer(createStripeCustomerRequest);

            if (createStripeCustomerResponse == null)
                throw new ArgumentException("Customer was not created");
            else if (createStripeCustomerResponse.Customer == null)
                throw new ArgumentException("Customer was not created");

            return createStripeCustomerResponse;
        }

        private CreateStripeCustomerResponse RetrieveStripeCustomerData(StripeOperation operation)
        {
            GetStripeCustomerRequest StripeCustRequest;
            GetStripeCustomerResponse CustomerInfo;

            using (UsersContext info = new UsersContext())
            {
                string StripeCustID = info.UserProfiles.First(m => m.UserId == WebSecurity.CurrentUserId).StripeCustomerId;
                StripeCustRequest = new GetStripeCustomerRequest() { CustomerId = StripeCustID };
                CustomerInfo = operation.GetStripeCustomer(StripeCustRequest);
            }

            return new CreateStripeCustomerResponse() { Customer = CustomerInfo.Customer };
        }

        //
        // POST: /Account/Disassociate

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Disassociate(string provider, string providerUserId)
        {
            string ownerAccount = OAuthWebSecurity.GetUserName(provider, providerUserId);
            ManageMessageId? message = null;

            // Only disassociate the account if the currently logged in user is the owner
            if (ownerAccount == User.Identity.Name)
            {
                // Use a transaction to prevent the user from deleting their last login credential
                using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Serializable }))
                {
                    bool hasLocalAccount = OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
                    if (hasLocalAccount || OAuthWebSecurity.GetAccountsFromUserName(User.Identity.Name).Count > 1)
                    {
                        OAuthWebSecurity.DeleteAccount(provider, providerUserId);
                        scope.Complete();
                        message = ManageMessageId.RemoveLoginSuccess;
                    }
                }
            }

            return RedirectToAction("Manage", new { Message = message });
        }

        //
        // GET: /Account/Manage

        public ActionResult Manage(ManageMessageId? message)
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.ChangePasswordSuccess ? "Your password has been changed."
                : message == ManageMessageId.SetPasswordSuccess ? "Your password has been set."
                : message == ManageMessageId.RemoveLoginSuccess ? "The external login was removed."
                : "";
            ViewBag.HasLocalPassword = OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
            ViewBag.ReturnUrl = Url.Action("Manage");
            return View();
        }

        //
        // POST: /Account/Manage

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Manage(LocalPasswordModel model)
        {
            bool hasLocalAccount = OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
            ViewBag.HasLocalPassword = hasLocalAccount;
            ViewBag.ReturnUrl = Url.Action("Manage");
            if (hasLocalAccount)
            {
                if (ModelState.IsValid)
                {
                    // ChangePassword will throw an exception rather than return false in certain failure scenarios.
                    bool changePasswordSucceeded;
                    try
                    {
                        changePasswordSucceeded = WebSecurity.ChangePassword(User.Identity.Name, model.OldPassword, model.NewPassword);
                    }
                    catch (Exception)
                    {
                        changePasswordSucceeded = false;
                    }

                    if (changePasswordSucceeded)
                    {
                        return RedirectToAction("Manage", new { Message = ManageMessageId.ChangePasswordSuccess });
                    }
                    else
                    {
                        ModelState.AddModelError("", "The current password is incorrect or the new password is invalid.");
                    }
                }
            }
            else
            {
                // User does not have a local password so remove any validation errors caused by a missing
                // OldPassword field
                ModelState state = ModelState["OldPassword"];
                if (state != null)
                {
                    state.Errors.Clear();
                }

                if (ModelState.IsValid)
                {
                    try
                    {
                        WebSecurity.CreateAccount(User.Identity.Name, model.NewPassword);
                        return RedirectToAction("Manage", new { Message = ManageMessageId.SetPasswordSuccess });
                    }
                    catch (Exception)
                    {
                        ModelState.AddModelError("", String.Format("Unable to create local account. An account with the name \"{0}\" may already exist.", User.Identity.Name));
                    }
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // POST: /Account/ExternalLogin

        [HttpPost]
        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            return new ExternalLoginResult(provider, Url.Action("ExternalLoginCallback", new { ReturnUrl = returnUrl }));
        }


        
        // GET: /Account/ExternalLoginCallback

        [AllowAnonymous]
        public ActionResult ExternalLoginCallback(string returnUrl)
        {
            UserProfile uf;
            GooglePlusClient.RewriteRequest();
            // Rewrite request before it gets passed on to the OAuth Web Security classes
            AuthenticationResult result = OAuthWebSecurity.VerifyAuthentication(Url.Action("ExternalLoginCallback", new { ReturnUrl = returnUrl }));
            if (!result.IsSuccessful)
            {
                return RedirectToAction("ExternalLoginFailure");
            }

            if (OAuthWebSecurity.Login(result.Provider, result.ProviderUserId, createPersistentCookie: false))
            {
                //return RedirectToLocal(returnUrl);
                using (UsersContext userContext = new UsersContext())
                {
                    uf = userContext.UserProfiles.Where(x => x.UserName == result.UserName).FirstOrDefault();
                }
                int listingId;
                try
                {
                    var listingIds = ListingManager.GetListingIdsByUserId(uf.UserId);

                    Session["ListingUserId"] = uf.UserId;
                    listingId = listingIds[0];

                }catch(NullReferenceException nfe){
                    listingId = 0;
                }
                
                //return RedirectToAction(returnUrl);
                if (listingId==0)
                    return RedirectToLocal("/Listing/Create");
                else
                return RedirectToLocal("/Listing/ListingTemplate/" + listingId);
            }

            if (User.Identity.IsAuthenticated)
            {
                // If the current user is logged in add the new account
                OAuthWebSecurity.CreateOrUpdateAccount(result.Provider, result.ProviderUserId, User.Identity.Name);

                using (UsersContext userContext = new UsersContext())
                {
                    uf = userContext.UserProfiles.Where(x => x.UserName == result.UserName).FirstOrDefault();
                }

                var listingIds = ListingManager.GetListingIdsByUserId(uf.UserId);

                Session["ListingUserId"] = uf.UserId;
                //return RedirectToAction(returnUrl);
                if (listingIds[0] == null)
                    return RedirectToLocal("/Listing/Create");
                else
                    return RedirectToLocal("/Listing/ListingTemplate/" + listingIds[0]);
                //return RedirectToLocal(returnUrl);
            }
            else
            {
                // User is new, ask for their desired membership name
                string loginData = OAuthWebSecurity.SerializeProviderUserId(result.Provider, result.ProviderUserId);
                ViewBag.ProviderDisplayName = OAuthWebSecurity.GetOAuthClientData(result.Provider).DisplayName;
                ViewBag.ReturnUrl = returnUrl;
                return View("ExternalLoginConfirmation", new RegisterExternalLoginModel { UserName = result.UserName, ExternalLoginData = loginData });
            }



            //AuthenticationResult result = OAuthWebSecurity.VerifyAuthentication(Url.Action("ExternalLoginCallback", new { ReturnUrl = returnUrl }));
            //if (!result.IsSuccessful)
            //{
                 
            //    return RedirectToAction("ExternalLoginFailure");
            //}

            //if (OAuthWebSecurity.Login(result.Provider, result.ProviderUserId, createPersistentCookie: false))
            //{

            //    //using (UsersContext userContext = new UsersContext())
            //    //{
            //    //    uf = userContext.UserProfiles.Where(x => x.UserName == result.UserName).FirstOrDefault(); 
            //    //}

            //    //var listingIds = ListingManager.GetListingIdsByUserId(uf.UserId);

            //    //Session["ListingUserId"] = uf.UserId;
            //    return RedirectToAction(returnUrl);
            //    //return RedirectToLocal("/Listing/ListingTemplate/" + listingIds[0]);
            //}

            //if (User.Identity.IsAuthenticated)
            //{
            //    // If the current user is logged in add the new account
            //    OAuthWebSecurity.CreateOrUpdateAccount(result.Provider, result.ProviderUserId, User.Identity.Name);

            //    //using (UsersContext userContext = new UsersContext())
            //    //{
            //    //    uf = userContext.UserProfiles.Where(x => x.UserName == result.UserName).FirstOrDefault();
            //    //}

            //    //Session["ListingUserId"] = uf.UserId;
            //    // var listingIds = ListingManager.GetListingIdsByUserId(uf.UserId);
            //    //return RedirectToLocal("/Listing/ListingTemplate/" + listingIds[0]);
            //    return RedirectToLocal(returnUrl);
            //}
            //else
            //{
            //    // User is new, ask for their desired membership name
            //    string loginData = OAuthWebSecurity.SerializeProviderUserId(result.Provider, result.ProviderUserId);
            //    ViewBag.ProviderDisplayName = OAuthWebSecurity.GetOAuthClientData(result.Provider).DisplayName;
            //    ViewBag.ReturnUrl = returnUrl;
            //    string[] name = result.ExtraData["name"].Split(' ');
            //    return View("ExternalLoginConfirmation", new RegisterExternalLoginModel { UserName = result.UserName,  ExternalLoginData = loginData });
            //}
        }

        //
        // POST: /Account/ExternalLoginConfirmation

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLoginConfirmation(RegisterExternalLoginModel model, FormCollection form, string returnUrl)
        {
            string provider = null;
            string providerUserId = null;

            if (User.Identity.IsAuthenticated || !OAuthWebSecurity.TryDeserializeProviderUserId(model.ExternalLoginData, out provider, out providerUserId))
            {
                return RedirectToAction("Manage");
            }

            if (ModelState.IsValid)
            {
                // Insert a new user into the database
                using (UsersContext db = new UsersContext())
                {
                    UserProfile user = db.UserProfiles.FirstOrDefault(u => u.UserName.ToLower() == model.UserName.ToLower());
                    // Check if user already exists
                    if (user == null)
                    {
                        UserProfile up = new UserProfile();
                        up.UserName = model.UserName;
                        up.MaxUploadCount = "6";

                        //if (form["FirstName"].ToString() != "")
                        //    up.FirstName = form["FirstName"].ToString();

                        //if (form["LastName"].ToString() != "")
                        //    up.LastName = form["LastName"].ToString();
                        
                        up.AgentId = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["DefaultAgentId"].ToString());

                        up.UserTypeId = Convert.ToInt32("2");
                        // Insert name into the profile table
                        db.UserProfiles.Add(up);
                        db.SaveChanges();

                        OAuthWebSecurity.CreateOrUpdateAccount(provider, providerUserId, model.UserName);
                        OAuthWebSecurity.Login(provider, providerUserId, createPersistentCookie: false);

                        Session["ListingUserId"] = up.UserId;
                        
                        return RedirectToAction("Create","Listing");
                        //return SocialProviderAccountCompleteSetup(new RegisterExternalLoginModel { UserName = model.UserName });
                        //return View("SocialProviderAccountCompleteSetup", new RegisterExternalLoginModel { UserName = model.UserName });
                    }
                    else
                    {
                        ModelState.AddModelError("UserName", "User already registered.");
                    }
                }
            }

            //ViewBag.ProviderDisplayName = OAuthWebSecurity.GetOAuthClientData(provider).DisplayName;
            //ViewBag.ReturnUrl = returnUrl;
            ViewBag.ShowRemoveButton = true;
            return View(model);

           
        }

        //
        // GET: /Account/ExternalLoginFailure

        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        [AllowAnonymous]
        [ChildActionOnly]
        [OutputCache(Duration = 1000)]
        public ActionResult ExternalLoginsList(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return PartialView("_ExternalLoginsListPartial", OAuthWebSecurity.RegisteredClientData);
        }

        [ChildActionOnly]
        [OutputCache(Duration = 1000)]
        //[OutputCache(CacheProfile = "long", Location = OutputCacheLocation.Server, VaryByHeader = "X-Requested-With", Duration = 60, VaryByParam = "*")]
        public ActionResult RemoveExternalLogins()
        {
            ICollection<OAuthAccount> accounts = OAuthWebSecurity.GetAccountsFromUserName(User.Identity.Name);
            List<ExternalLogin> externalLogins = new List<ExternalLogin>();
            foreach (OAuthAccount account in accounts)
            {
                AuthenticationClientData clientData = OAuthWebSecurity.GetOAuthClientData(account.Provider);

                externalLogins.Add(new ExternalLogin
                {
                    Provider = account.Provider,
                    ProviderDisplayName = clientData.DisplayName,
                    ProviderUserId = account.ProviderUserId,
                });
            }

            ViewBag.ShowRemoveButton = externalLogins.Count > 1 || OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
            return PartialView("_RemoveExternalLoginsPartial", externalLogins);
        }


        [AllowAnonymous]
        [HttpPost]
        public JsonResult GetSellerStepProcess()
        {
            if (!Request.IsAjaxRequest())
                return Json(new { step = "step1" });

            string steps = "step1";

            if (WebSecurity.CurrentUserId > 0)
            {
                int UserId = WebSecurity.CurrentUserId;

                //get quick listing
                var result = ListingManager.GetListingsByUserId(UserId);
                if (result == null)
                    return Json(new { step = "step2" });
                else //check if the quick listing is complete 
                {
                    foreach (var item in result)
                    {
                        if (item.images.Find(x => x.ListingImageCategory == ListingImageCategoryNames.VIRTUALSIGN) != null && item.images.Count > 1 &&
                            item.images.Find(x => x.ListingImageCategory == ListingImageCategoryNames.KITCHEN) == null)
                        {
                            return Json(new { step = "step3" });
                        }
                        else if (item.images.Find(x => x.ListingImageCategory == ListingImageCategoryNames.VIRTUALSIGN) != null && item.images.Count > 1 &&
                            item.images.Find(x => x.ListingImageCategory == ListingImageCategoryNames.KITCHEN) != null &&
                            item.images.Find(x => x.ListingImageCategory == ListingImageCategoryNames.LOCATION) != null
                            )
                            return Json(new { step = "step4" });
                    }
                }


            }
            else
            {
                return Json(new { step = "step1" });
            }


            return Json(new { step = steps });
        }

        
        #region Helpers
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }


        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
        }

        internal class ExternalLoginResult : ActionResult
        {
            public ExternalLoginResult(string provider, string returnUrl)
            {
                Provider = provider;
                ReturnUrl = returnUrl;
            }

            public string Provider { get; private set; }
            public string ReturnUrl { get; private set; }

            public override void ExecuteResult(ControllerContext context)
            {
                OAuthWebSecurity.RequestAuthentication(Provider, ReturnUrl);
            }
        }

        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }
        #endregion
    }
}
