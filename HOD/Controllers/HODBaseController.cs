﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HOD.Controllers
{
    public class HODBaseController : Controller
    {
        //
        // GET: /HODBase/
        public string ImageBucketLocationKeyName
        {
            get
            {
                return "AWSS3BucketURL";
            }
        }

        public string BlankLogoIconKeyName
        {
            get
            {
                return "blank-logo-icon.png";
            }
        }

        public string ConfigurationSettingValue(string key)
        {
            return System.Configuration.ConfigurationManager.AppSettings[key];
        }

        public ActionResult Index()
        {
            return View();
        }

        public void LogException(Exception ex)
        {
            string logFile = "/App_Data/ErrorLog.txt";
            logFile = System.Web.HttpContext.Current.Server.MapPath(logFile);

            // Open the log file for append and write the log
            StreamWriter sw = new StreamWriter(logFile, true);
            sw.WriteLine("********** {0} **********", DateTime.Now);
            sw.Write("Execption: {0}", ex.ToString());
            sw.Write("StackTrace: {0}", ex.StackTrace);
            sw.Close();
        }

    }
}
