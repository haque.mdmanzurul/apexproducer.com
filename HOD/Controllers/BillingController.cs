﻿using HOD.Constants;
using HOD.Entities;
using HOD.Managers;
using HOD.Models;
using HOD.Payments.StripeChargingGatewayManager.Contracts.ServiceContracts;
using HOD.Payments.StripeChargingGatewayManager.DataContracts;
using HOD.Payments.StripeChargingGatewayManager.Operations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMatrix.WebData;
using System.Transactions;

namespace HOD.Controllers
{
    public class BillingController : Controller
    {
        //
        // GET: /Billing/
        public IPackageManager packageManager = new PackageManager();

        public ActionResult Billing(){

            RetrieveCustomerData(WebSecurity.CurrentUserId);

            return View(RetrieveCustomerData(WebSecurity.CurrentUserId));
        }

        public ActionResult BillingSave(BillingBaseModel model)
        {
            string selectedPackage = model.Package;

            if (PayForPlan(model)){
                return RedirectToAction("AgentPortal", "Agent");
            } else {
                return View("Billing", model);
            }            
        }

        private bool PayForPlan(BillingBaseModel model)
        {
            const bool SUCCESS = true;

            if (!ModelState.IsValid)
            {
                return !SUCCESS;
            }

            try
            {
                int currentUserId = WebSecurity.CurrentUserId;

                StripeOperation operation = new StripeOperation();
                CreateStripeCustomerResponse createStripeCustomerResponse = new CreateStripeCustomerResponse();

                createStripeCustomerResponse = CreateStripeCustomer(model, operation, createStripeCustomerResponse);
                PackageType packageType = packageManager.GetPackageTypeByName(PackageTypes.GOLD);

                //Add Package for user
                UserPackage userPackage = new UserPackage()
                {
                    PackageTypeId = packageType.PackageTypeId,
                    UserId = currentUserId
                };

                //Create InvoiceItem
                CreateStripeInvoiceItemRequest createStripeInvoiceItemGOLDRequest = new CreateStripeInvoiceItemRequest()
                {
                    CustomerId = createStripeCustomerResponse.Customer.Id,
                    Currency = "USD",
                    AmountInCents = (int)(packageType.BaseCost * 100),
                    Description = packageType.Description
                };

                CreateStripeInvoiceItemResponse createStripeInvoiceGOLDResponse = operation.CreateStripeInvoiceItem(createStripeInvoiceItemGOLDRequest);
                if (createStripeInvoiceGOLDResponse == null)
                    throw new ArgumentException("InvoiceItem '" + createStripeInvoiceGOLDResponse.InvoiceItem.Id + "' was not created.");
                else if (createStripeInvoiceGOLDResponse.InvoiceItem == null)
                    throw new ArgumentException("InvoiceItem '" + createStripeInvoiceGOLDResponse.InvoiceItem.Id + "' was not created.");

                //userPackage = packageManager.SaveUserPackage(userPackage, packageFeatues);

                CreateAndPayStripeInvoiceRequest createAndPayStripeInvoice = new CreateAndPayStripeInvoiceRequest()
                {
                    CustomerId = createStripeCustomerResponse.Customer.Id
                };


                //Doing a direct charge and not invoicing, this is one off. Description will aptly tell customer
                /*CreateAndPayStripeInvoiceResponse createAndPayStripeInvoiceResponse = operation.CreateAndPayStripeInvoice(createAndPayStripeInvoice);*/

                CreateStripeChargeRequest chargeRequest = new CreateStripeChargeRequest()
                {
                    CustomerId = createStripeCustomerResponse.Customer.Id,
                    AmountInCents = (int)(packageType.BaseCost * 100),
                    Currency = "USD",
                    Description = packageType.Description
                };


                CreateStripeChargeResponse chargeResponse = operation.CreateStripeCharge(chargeRequest);

                Charge transactionCharge = chargeResponse.Charge;

                if (transactionCharge.Paid.Value == true)
                {
                    StripePayment stripePayment = new StripePayment()
                    {
                        UserId = currentUserId,
                        StripeCustomerId = createStripeCustomerResponse.Customer.Id,
                        StripeChargeId = transactionCharge.Id,
                        Amount = transactionCharge.AmountInCents.HasValue ? ((decimal)transactionCharge.AmountInCents.Value) / 100.00M : 0.00M,
                        PaidByAgent = model.AgentPayOnUserBehalf
                    };

                    packageManager.CreateStripePayment(stripePayment);
                }

                using (UsersContext userContext = new UsersContext())
                {
                    UserProfile user = userContext.UserProfiles.Where<UserProfile>(u => u.UserId == currentUserId).FirstOrDefault<UserProfile>();
                    user.Tier = "PAID";
                    user.StripeCustomerId = createStripeCustomerResponse.Customer.Id;
                    userContext.SaveChanges();
                    Session["TIER"] = "PAID";
                }               
            }
            catch (TransactionException transEx)
            {
                Console.WriteLine(transEx.Message);
                return !SUCCESS;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return !SUCCESS;
            }
            
            return SUCCESS;
        }



        private static CreateStripeCustomerResponse CreateStripeCustomer(BillingBaseModel model, StripeOperation operation, CreateStripeCustomerResponse createStripeCustomerResponse)
        {
            CreateStripeCustomerRequest createStripeCustomerRequest = new CreateStripeCustomerRequest()
            {
                Email = model.UserName,
                Description = model.FirstName + " " + model.LastName + "(" + model.UserName + ")",
                //CardAddressCountry = "CA",
                CardAddressLine1 = model.BillingAddress,
                //CardAddressLine2 = "",
                CardAddressCity = model.BillingCity,
                CardAddressState = model.BillingProvince,
                CardAddressZip = model.BillingPostalCode,
                CardCvc = model.CVC,
                CardExpirationMonth = model.ExpirationMonth,
                CardExpirationYear = model.ExpirationYear,
                CardName = model.BillingName,
                CardNumber = model.CardNumber,
            };

            createStripeCustomerResponse = operation.CreateStripeCustomer(createStripeCustomerRequest);

            if (createStripeCustomerResponse == null)
                throw new ArgumentException("Customer was not created");
            else if (createStripeCustomerResponse.Customer == null)
                throw new ArgumentException("Customer was not created");

            return createStripeCustomerResponse;
        }

        private BillingBaseModel RetrieveCustomerData(int currentUserId)
        {
            BillingBaseModel registerModel = new BillingBaseModel();
            

            using (UsersContext userContext = new UsersContext()){

                UserProfile user = userContext.UserProfiles.Where<UserProfile>(u => u.UserId == currentUserId).FirstOrDefault<UserProfile>();

                UserType userType = userContext.UserTypes.Where(x => x.UserTypeId == user.UserTypeId).FirstOrDefault<UserType>();

                registerModel.UserName = user.UserName;
                registerModel.FirstName = user.FirstName;
                registerModel.LastName = user.LastName;
                registerModel.BillingName = string.Format("{0} {1}", user.FirstName, user.LastName);
                registerModel.BillingAddress = user.Address;
                registerModel.BillingCity = user.City;
                registerModel.BillingProvince = user.Province;
                registerModel.BillingPostalCode = user.PostalCode;
                registerModel.UserType = userType.Name;
            }

            return registerModel;
        }
    }
}
