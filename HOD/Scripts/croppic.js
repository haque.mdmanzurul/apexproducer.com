/*
 * CROP
 * dependancy: jQuery
 * author: Ognjen "Zmaj Džedaj" Božičković
 */

(function (window, document) {

	Croppic = function (id, options) {

		var that = this;
		that.id = id;
		that.obj = $('#' + id);
		that.outputDiv = that.obj;

		// DEFAULT OPTIONS
		that.options = {
            btnPodClick:'',
			uploadUrl:'',
			uploadData:{},
			cropUrl:'',
			cropData:{},
			outputUrlId:'',
			//styles
			imgEyecandy:true,
			imgEyecandyOpacity:0.2,
			zoomFactor:10,
			doubleZoomControls:true,
			modal:false,
			customUploadButtonId:'',
			loaderHtml:'',
			//callbacks
			onBeforeImgUpload: null,
			onAfterImgUpload: null,
			onImgDrag: null,
			onImgZoom: null,
			onBeforeImgCrop: null,
			onAfterImgCrop: null
		};

		// OVERWRITE DEFAULT OPTIONS
		for (i in options) that.options[i] = options[i];

		// INIT THE WHOLE DAMN THING!!!
		that.init();
		
	};

	Croppic.prototype = {
		id:'',
		imgInitW:0,
		imgInitH:0,
		imgW:0,
		imgH:0,
		objW:0,
		objH:0,
		windowW:0,
		windowH:$(window).height(),
		obj:{},
		outputDiv:{},
		outputUrlObj:{},
		img:{},
		defaultImg:{},
		croppedImg:{},
		imgEyecandy:{},
		form:{},
		cropControlsUpload:{},
		cropControlsCrop:{},
		cropControlZoomMuchIn:{},
		cropControlZoomMuchOut:{},
		cropControlZoomIn:{},
		cropControlZoomOut:{},
		cropControlCrop:{},
		cropControlReset:{},	
		cropControlRemoveCroppedImage:{},	
		modal:{},
		loader:{},
		
		init: function () {
			var that = this;
			
			that.objW = CropperWidth;//that.obj.width();
			that.objH = CropperHeight;//that.obj.height();
			
			if( $.isEmptyObject(that.defaultImg)){ that.defaultImg = that.obj.find('img'); }
			
			that.createImgUploadControls();
			that.bindImgUploadControl();
			
		},
		createImgUploadControls: function(){
			var that = this;
			
			var cropControlUpload = '';
			if(that.options.customUploadButtonId ===''){ cropControlUpload = '<i class="cropControlUpload"></i>'; }
			var cropControlRemoveCroppedImage = '<i class="cropControlRemoveCroppedImage"></i>';
			
			if( $.isEmptyObject(that.croppedImg)){ cropControlRemoveCroppedImage=''; }

			var html =    '<div class="cropControls cropControlsUpload"> ' + cropControlUpload + cropControlRemoveCroppedImage + ' </div>';
			that.outputDiv.append(html);
			
			that.cropControlsUpload = that.outputDiv.find('.cropControlsUpload');
			
			if(that.options.customUploadButtonId ===''){ that.imgUploadControl = that.outputDiv.find('.cropControlUpload'); }
			else{	that.imgUploadControl = $('#'+that.options.customUploadButtonId); that.imgUploadControl.show();	}

			if( !$.isEmptyObject(that.croppedImg)){
				that.cropControlRemoveCroppedImage = that.outputDiv.find('.cropControlRemoveCroppedImage');
			}
			
		},
		bindImgUploadControl: function(){
			
			var that = this;
			
			// CREATE UPLOAD IMG FORM
			var formHtml = '<form class="' + that.id + '_imgUploadForm" style="display: none; visibility: hidden;">  <input type="file" name="img" accept="image/*">  </form>';

			that.outputDiv.append(formHtml);
			that.form = that.outputDiv.find('.'+that.id+'_imgUploadForm');
			
			that.imgUploadControl.off('click');
			that.imgUploadControl.on('click',function(){ 
				that.form.find('input[type="file"]').trigger('click');										
			});						
			
			if( !$.isEmptyObject(that.croppedImg)){
			
				that.cropControlRemoveCroppedImage.on('click',function(){ 
					that.croppedImg.remove();
					$(this).hide();
					
					if( !$.isEmptyObject(that.defaultImg)){ 
						that.obj.append(that.defaultImg);
					}
					
					if(that.options.outputUrlId !== ''){	$('#'+that.options.outputUrlId).val('');	}
				
				});	
			
			}
											
			that.form.find('input[type="file"]').change(function(){
				
				if (that.options.onBeforeImgUpload) that.options.onBeforeImgUpload.call(that);
				
				that.showLoader();
				that.imgUploadControl.hide();
			
				var formData = new FormData(that.form[0]);
			
				for (var key in that.options.uploadData) {
					if( that.options.uploadData.hasOwnProperty(key) ) {
						formData.append( key , that.options.uploadData[key] );	
					}
				}
				
				$.ajax({
                    url: that.options.uploadUrl,
                    data: formData,
                    context: document.body,
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success: function (response) {
                        if (response.status == 'success') {
                            console.log(response);

                            that.imgInitW = that.imgW = response.width;
                            that.imgInitH = that.imgH = response.height;

                            if (that.options.modal) { that.createModal(); }
                            if (!$.isEmptyObject(that.croppedImg)) { that.croppedImg.remove(); }

                            that.imgUrl = response.url;

                            that.obj.append('<img src="' + response.url + '">');
                            that.initCropper();

                            that.hideLoader();

                            if (that.options.onAfterImgUpload) that.options.onAfterImgUpload.call(that);

                        }
                        if (response.status == 'error') {
                            that.obj.append('<p style="width:100%; height:100%; text-align:center; line-height:' + that.objH + 'px;">' + response.message + '</p>');
                            that.hideLoader();
                            setTimeout(function () { that.reset(); }, 2000)
                        }
                        
                    },
				    error: function (responseText) {
				    console.log(responseText);
				}
				})

				
			});
		
		},
		createModal: function(){
			var that = this;
		
			var marginTop = that.windowH/2-that.objH/2;
			var modalHTML =  '<div id="croppicModal">'+'<div id="croppicModalObj" style="width:'+ that.objW +'px; height:'+ that.objH +'px; margin:0 auto; margin-top:'+ marginTop +'px; position: relative;"> </div>'+'</div>';

			$('body').append(modalHTML);
			
			that.modal = $('#croppicModal');
			
			that.obj = $('#croppicModalObj');
			
		},
		destroyModal: function(){
			var that = this;
			
			that.obj = that.outputDiv;
			that.modal.remove();
		},
		initCropper: function(){
			var that = this;
			
			/*SET UP SOME VARS*/
			that.img = that.obj.find('img');
			that.img.wrap('<div class="cropImgWrapper" style="overflow:hidden; z-index:1; position:absolute; width:'+that.objW+'px; height:'+that.objH+'px;"></div>');
	
			/*INIT DRAGGING*/
			that.createCropControls();
			
			if(that.options.imgEyecandy){ that.createEyecandy(); }
			that.initDrag();
			that.initialScaleImg();
		},
		createEyecandy: function(){
			var that = this;

			that.imgEyecandy = that.img.clone();
			that.imgEyecandy.css({'z-index':'0','opacity':that.options.imgEyecandyOpacity}).appendTo(that.obj);
		},
		destroyEyecandy: function(){
			var that = this;
			that.imgEyecandy.remove();
		},
		initialScaleImg:function(){
			var that = this;
			that.zoom(-that.imgInitW);
			that.zoom(40);
			
			// initial center image
			
			that.img.css({'left': -(that.imgW -that.objW)/2, 'top': -(that.imgH -that.objH)/2, 'position':'relative'});
			if(that.options.imgEyecandy){ that.imgEyecandy.css({'left': -(that.imgW -that.objW)/2, 'top': -(that.imgH -that.objH)/2, 'position':'relative'}); }
			
		},
		
		createCropControls: function(){
			var that = this;
			
			// CREATE CONTROLS
			var cropControlZoomMuchIn =      '<i class="cropControlZoomMuchIn"></i>';
			var cropControlZoomIn =          '<i class="cropControlZoomIn"></i>';
			var cropControlZoomOut =         '<i class="cropControlZoomOut"></i>';
			var cropControlZoomMuchOut =     '<i class="cropControlZoomMuchOut"></i>';
			var cropControlCrop =            '<i class="cropControlCrop"></i>';
			var cropControlReset =           '<i class="cropControlReset"></i>';
			
            var html;
            
			if(that.options.doubleZoomControls){ html =  '<div class="cropControls cropControlsCrop">'+ cropControlZoomMuchIn + cropControlZoomIn + cropControlZoomOut + cropControlZoomMuchOut + cropControlCrop + cropControlReset + '</div>'; }
			else{ html =  '<div class="cropControls cropControlsCrop">' + cropControlZoomIn + cropControlZoomOut + cropControlCrop + cropControlReset + '</div>'; }	
			
			that.obj.append(html);
			
			that.cropControlsCrop = that.obj.find('.cropControlsCrop');

			// CACHE AND BIND CONTROLS
			if(that.options.doubleZoomControls){
				that.cropControlZoomMuchIn = that.cropControlsCrop.find('.cropControlZoomMuchIn');
				that.cropControlZoomMuchIn.on('click',function(){ that.zoom( that.options.zoomFactor*10 ); });
			
				that.cropControlZoomMuchOut = that.cropControlsCrop.find('.cropControlZoomMuchOut');
				that.cropControlZoomMuchOut.on('click',function(){ that.zoom(-that.options.zoomFactor*10); });
			}
			
			that.cropControlZoomIn = that.cropControlsCrop.find('.cropControlZoomIn');
			that.cropControlZoomIn.on('click',function(){ that.zoom(that.options.zoomFactor); });

			that.cropControlZoomOut = that.cropControlsCrop.find('.cropControlZoomOut');
			that.cropControlZoomOut.on('click',function(){ that.zoom(-that.options.zoomFactor); });		

			that.cropControlCrop = that.cropControlsCrop.find('.cropControlCrop');
			that.cropControlCrop.on('click',function(){ that.crop(); });

			that.cropControlReset = that.cropControlsCrop.find('.cropControlReset');
			that.cropControlReset.on('click',function(){ that.reset(); });				
			
		},
		initDrag:function(){
			var that = this;
			
			that.img.on("mousedown", function(e) {
				
				e.preventDefault(); // disable selection

				var z_idx = that.img.css('z-index'),
                drg_h = that.img.outerHeight(),
                drg_w = that.img.outerWidth(),
                pos_y = that.img.offset().top + drg_h - e.pageY,
                pos_x = that.img.offset().left + drg_w - e.pageX;
				
				that.img.css('z-index', 1000).on("mousemove", function(e) {
					
					var imgTop = e.pageY + pos_y - drg_h;
					var imgLeft = e.pageX + pos_x - drg_w;
					
					that.img.offset({
						top:imgTop,
						left:imgLeft
					}).on("mouseup", function() {
						$(this).removeClass('draggable').css('z-index', z_idx);
					});
					
					if(that.options.imgEyecandy){ that.imgEyecandy.offset({ top:imgTop, left:imgLeft }); }
					
					if( parseInt( that.img.css('top')) > 0 ){ that.img.css('top',0);  if(that.options.imgEyecandy){ that.imgEyecandy.css('top', 0); } }
					var maxTop = -( that.imgH-that.objH); if( parseInt( that.img.css('top')) < maxTop){ that.img.css('top', maxTop); if(that.options.imgEyecandy){ that.imgEyecandy.css('top', maxTop); } }
					
					if( parseInt( that.img.css('left')) > 0 ){ that.img.css('left',0); if(that.options.imgEyecandy){ that.imgEyecandy.css('left', 0); }}
					var maxLeft = -( that.imgW-that.objW); if( parseInt( that.img.css('left')) < maxLeft){ that.img.css('left', maxLeft); if(that.options.imgEyecandy){ that.imgEyecandy.css('left', maxLeft); } }
					
					if (that.options.onImgDrag) that.options.onImgDrag.call(that);
					
				});
	
			}).on("mouseup", function() {
				that.img.off("mousemove");
			}).on("mouseout", function() {
				that.img.off("mousemove");
			});
			
		},
		zoom :function(x){
			var that = this;
			var ratio = that.imgW / that.imgH;
			var newWidth = that.imgW+x;
			var newHeight = newWidth/ratio;
			var doPositioning = true;
			
			if( newWidth < that.objW || newHeight < that.objH){
				
				if( newWidth - that.objW < newHeight - that.objH ){ 
					newWidth = that.objW;
					newHeight = newWidth/ratio;
				}else{
					newHeight = that.objH;
					newWidth = ratio * newHeight;
				}
				
				doPositioning = false;
				
			} 
			
			if( newWidth > that.imgInitW || newHeight > that.imgInitH){
				
				if( newWidth - that.imgInitW < newHeight - that.imgInitH ){ 
					newWidth = that.imgInitW;
					newHeight = newWidth/ratio;
				}else{
					newHeight = that.imgInitH;
					newWidth = ratio * newHeight;
				}
				
				doPositioning = false;
				
			}
			
			that.imgW = newWidth;
			that.img.width(newWidth); 
			
			that.imgH = newHeight;
			that.img.height(newHeight); 
	
			var newTop = parseInt( that.img.css('top') ) - x/2;
			var newLeft = parseInt( that.img.css('left') ) - x/2;
			
			if( newTop>0 ){ newTop=0;}
			if( newLeft>0 ){ newLeft=0;}
			
			var maxTop = -( newHeight-that.objH); if( newTop < maxTop){	newTop = maxTop;	}
			var maxLeft = -( newWidth-that.objW); if( newLeft < maxLeft){	newLeft = maxLeft;	}
			
			if( doPositioning ){
				that.img.css({'top':newTop, 'left':newLeft}); 
			}
			
			if(that.options.imgEyecandy){
				that.imgEyecandy.width(newWidth);
				that.imgEyecandy.height(newHeight);
				if( doPositioning ){
					that.imgEyecandy.css({'top':newTop, 'left':newLeft}); 
				}
			}	
			
			if (that.options.onImgZoom) that.options.onImgZoom.call(that);

		},
		crop:function(){
			var that = this;
			
			if (that.options.onBeforeImgCrop) that.options.onBeforeImgCrop.call(that);
			
			that.cropControlsCrop.hide();
			that.showLoader();
	
			var cropData = {
					imgUrl:that.imgUrl,
					imgInitW:that.imgInitW,
					imgInitH:that.imgInitH,
					imgW:that.imgW,
					imgH:that.imgH,
					imgY1:Math.abs( parseInt( that.img.css('top') ) ),
					imgX1:Math.abs( parseInt( that.img.css('left') ) ),
					cropH:that.objH,
					cropW:that.objW
				};
			
			var formData = new FormData();

			for (var key in cropData) {
				if( cropData.hasOwnProperty(key) ) {
						formData.append( key , cropData[key] );
				}
			}
			
			for (var key in that.options.cropData) {
				if( that.options.cropData.hasOwnProperty(key) ) {
						formData.append( key , that.options.cropData[key] );
				}
			}

			$.ajax({
			    url: that.options.cropUrl,
			    data: formData,
			    context: document.body,
			    cache: false,
			    contentType: false,
			    processData: false,
			    type: 'POST',
			    success: function (response) {
			        
			        // response = jQuery.parseJSON(test12);
			        console.log(response);
			        if (response.status == 'success') {

			            if (typeof window.theCurrentImage != "undefined") {
			                window.theCurrentImage.src = response.url;
			                //contentData.setCurrentImage = response.url;
			            }

			            if (SelectBtn == 'profilePic1') {
			                document.getElementById('profilePic1').src = response.url;
			                $("#_profilePic1Display").val(response.url);			                
			            }

			            if (SelectBtn == 'profilePic2') {
			                document.getElementById('profilePic2').src = response.url;
			                $("#_profilePic2Display").val(response.url);
			            }

			            if (SelectBtn == 'profilePic3') {
			                document.getElementById('profilePic3').src = response.url;
			                $("#_profilePic3Display").val(response.url);
			            }

			            if (SelectBtn == 'profileAgentLogoDisplay') {
			                document.getElementById('profileAgentLogoDisplay').src = response.url;
			                $("#_profileAgentLogoDisplay").val(response.url);
			            }

			            if (SelectBtn == 'profileBackgroundImage') {
			                document.getElementById('profileBackgroundImage').src = response.url;
			                $("#_profileBackgroundImage").val(response.url);
			            }

			            if (SelectBtn == 'profileWebsiteTopImage') {
			                document.getElementById('profileWebsiteTopImage').src = response.url;
			                $("#_profileWebsiteTopImage").val(response.url);
			            }

			            if (SelectBtn == 'otherAgentPhoto1') {
			                document.getElementById('otherAgentPhoto1').src = response.url;
			                $("#_otherAgentPhoto1").val(response.url);
			            }
			            
			            if (SelectBtn == 'otherAgentPhoto2') {
			                document.getElementById('otherAgentPhoto2').src = response.url;
			                $("#_otherAgentPhoto2").val(response.url);
			            }

			            if (SelectBtn == 'otherAgentPhoto3') {
			                document.getElementById('otherAgentPhoto3').src = response.url;
			                $("#_otherAgentPhoto3").val(response.url);
			            }

			            if (SelectBtn == 'otherAgentPhoto1') {
			                document.getElementById('otherAgentPhoto1').src = response.url;
			                $("#_otherAgentPhoto1").val(response.url);
			            }

			            if (SelectBtn == 'otherAgentPhoto4') {
			                document.getElementById('otherAgentPhoto4').src = response.url;
			                $("#_otherAgentPhoto4").val(response.url);
			            }

			            if (SelectBtn == 'otherAgentPhoto5') {
			                document.getElementById('otherAgentPhoto5').src = response.url;
			                $("#_otherAgentPhoto5").val(response.url);
			            }

			            if (SelectBtn == 'otherAgentPhoto6') {
			                document.getElementById('otherAgentPhoto6').src = response.url;
			                $("#_otherAgentPhoto6").val(response.url);
			            }

			            if (SelectBtn == 'otherAgentPhoto7') {
			                document.getElementById('otherAgentPhoto7').src = response.url;
			                $("#_otherAgentPhoto7").val(response.url);
			            }

			            if (SelectBtn == 'otherAgentPhoto8') {
			                document.getElementById('otherAgentPhoto8').src = response.url;
			                $("#_otherAgentPhoto8").val(response.url);
			            }

			            if (SelectBtn == 'otherAgentPhoto9') {
			                document.getElementById('otherAgentPhoto9').src = response.url;
			                $("#_otherAgentPhoto9").val(response.url);
			            }

			            if (SelectBtn == 'otherAgentPhoto10') {
			                document.getElementById('otherAgentPhoto10').src = response.url;
			                $("#_otherAgentPhoto10").val(response.url);
			            }
			            
			            if (SelectBtn == 'image1') {
			                document.getElementById('list1').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, '" width="50" />'].join('');
			                $("#_image1").val(response.url);
			            }
			            if (SelectBtn == 'image2') {
			                document.getElementById('list2').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, '" width="50" />'].join('');
			                $("#_image2").val(response.url);
			            }
			            if (SelectBtn == 'image3') {
			                document.getElementById('list3').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, '" width="50" />'].join('');
			                $("#_image3").val(response.url);
			            }
			            if (SelectBtn == 'image4') {
			                document.getElementById('list4').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, '" width="50" />'].join('');
			                $("#_image4").val(response.url);
			            }
			            //Property Summary and Highlights
			            if (SelectBtn == 'imageMain1') {
			                document.getElementById('summarylist1').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, ' height="100px" width="125px" />'].join('');
			                $("#_image1").val(response.url);
			            }
			            if (SelectBtn == 'imageMain2') {
			                document.getElementById('summarylist2').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, ' height="100px" width="125px" />'].join('');
			                $("#_image2").val(response.url);
			            }
			            if (SelectBtn == 'imageMain3') {
			                document.getElementById('summarylist3').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, ' height="100px" width="125px" />'].join('');
			                $("#_image3").val(response.url);
			            }
			            if (SelectBtn == 'imageMain4') {
			                document.getElementById('summarylist4').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, ' height="100px" width="125px" />'].join('');
			                $("#_image4").val(response.url);
			            }
			            if (SelectBtn == 'imageMain5') {
			                document.getElementById('summarylist5').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, ' height="100px" width="125px" />'].join('');
			                $("#_image5").val(response.url);
			            }
			            if (SelectBtn == 'imageMain6') {
			                document.getElementById('summarylist6').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, ' height="100px" width="125px" />'].join('');
			                $("#_image6").val(response.url);
			            }
			            //Kitchen
			            if (SelectBtn == 'imagekitchen1') {
			                document.getElementById('kitchenlist1').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, ' height="100px" width="125px" />'].join('');
			                $("#_kitchenlist1").val(response.url);
			            }
			            if (SelectBtn == 'imagekitchen2') {
			                document.getElementById('kitchenlist2').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, ' height="100px" width="125px" />'].join('');
			                $("#_kitchenlist2").val(response.url);
			            }
			            if (SelectBtn == 'imagekitchen3') {
			                document.getElementById('kitchenlist3').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, ' height="100px" width="125px" />'].join('');
			                $("#_kitchenlist3").val(response.url);
			            }
			            if (SelectBtn == 'imagekitchen4') {
			                document.getElementById('kitchenlist4').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, ' height="100px" width="125px" />'].join('');
			                $("#_kitchenlist4").val(response.url);
			            }
			            if (SelectBtn == 'imagekitchen5') {
			                document.getElementById('kitchenlist5').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, ' height="100px" width="125px" />'].join('');
			                $("#_kitchenlist5").val(response.url);
			            }
			            if (SelectBtn == 'imagekitchen6') {
			                document.getElementById('kitchenlist6').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, ' height="100px" width="125px" />'].join('');
			                $("#_kitchenlist6").val(response.url);
			            }
			            //Bathroom
			            if (SelectBtn == 'imagebathroom1') {
			                document.getElementById('bathroomlist1').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, ' height="100px" width="125px" />'].join('');
			                $("#_bathroomlist1").val(response.url);
			            }
			            if (SelectBtn == 'imagebathroom2') {
			                document.getElementById('bathroomlist2').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, ' height="100px" width="125px" />'].join('');
			                $("#_bathroomlist2").val(response.url);
			            }
			            if (SelectBtn == 'imagebathroom3') {
			                document.getElementById('bathroomlist3').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, ' height="100px" width="125px" />'].join('');
			                $("#_bathroomlist3").val(response.url);
			            }
			            if (SelectBtn == 'imagebathroom4') {
			                document.getElementById('bathroomlist4').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, ' height="100px" width="125px" />'].join('');
			                $("#_bathroomlist4").val(response.url);
			            }
			            if (SelectBtn == 'imagebathroom5') {
			                document.getElementById('bathroomlist5').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, ' height="100px" width="125px" />'].join('');
			                $("#_bathroomlist5").val(response.url);
			            }
			            if (SelectBtn == 'imagebathroom6') {
			                document.getElementById('bathroomlist6').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, ' height="100px" width="125px" />'].join('');
			                $("#_bathroomlist6").val(response.url);
			            }
			            //Basement
			            if (SelectBtn == 'imagebasement1') {
			                document.getElementById('basementlist1').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, ' height="100px" width="125px" />'].join('');
			                $("#_basementlist1").val(response.url);
			            }
			            if (SelectBtn == 'imagebasement2') {
			                document.getElementById('basementlist2').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, ' height="100px" width="125px" />'].join('');
			                $("#_basementlist2").val(response.url);
			            }
			            if (SelectBtn == 'imagebasement3') {
			                document.getElementById('basementlist3').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, ' height="100px" width="125px" />'].join('');
			                $("#_basementlist3").val(response.url);
			            }
			            if (SelectBtn == 'imagebasement4') {
			                document.getElementById('basementlist4').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, ' height="100px" width="125px" />'].join('');
			                $("#_basementlist4").val(response.url);
			            }
			            if (SelectBtn == 'imagebasement5') {
			                document.getElementById('basementlist5').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, ' height="100px" width="125px" />'].join('');
			                $("#_basementlist5").val(response.url);
			            }
			            if (SelectBtn == 'imagebasement6') {
			                document.getElementById('basementlist6').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, ' height="100px" width="125px" />'].join('');
			                $("#_basementlist6").val(response.url);
			            }
			            //Other Bedroom
			            if (SelectBtn == 'imageObedroom1') {
			                document.getElementById('Obedroomlist1').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, ' height="100px" width="125px" />'].join('');
			                $("#_Obedroomlist1").val(response.url);
			            }
			            if (SelectBtn == 'imageObedroom2') {
			                document.getElementById('Obedroomlist2').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, ' height="100px" width="125px" />'].join('');
			                $("#_Obedroomlist2").val(response.url);
			            }
			            if (SelectBtn == 'imageObedroom3') {
			                document.getElementById('Obedroomlist3').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, ' height="100px" width="125px" />'].join('');
			                $("#_Obedroomlist3").val(response.url);
			            }
			            if (SelectBtn == 'imageObedroom4') {
			                document.getElementById('Obedroomlist4').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, ' height="100px" width="125px" />'].join('');
			                $("#_Obedroomlist4").val(response.url);
			            }
			            if (SelectBtn == 'imageObedroom5') {
			                document.getElementById('Obedroomlist5').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, ' height="100px" width="125px" />'].join('');
			                $("#_Obedroomlist5").val(response.url);
			            }
			            if (SelectBtn == 'imageObedroom6') {
			                document.getElementById('Obedroomlist6').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, ' height="100px" width="125px" />'].join('');
			                $("#_Obedroomlist6").val(response.url);
			            }
			            //Dining Room
			            if (SelectBtn == 'imagedining1') {
			                document.getElementById('dininglist1').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, ' height="100px" width="125px" />'].join('');
			                $("#_dininglist1").val(response.url);
			            }
			            if (SelectBtn == 'imagedining2') {
			                document.getElementById('dininglist2').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, ' height="100px" width="125px" />'].join('');
			                $("#_dininglist2").val(response.url);
			            }
			            if (SelectBtn == 'imagedining3') {
			                document.getElementById('dininglist3').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, ' height="100px" width="125px" />'].join('');
			                $("#_dininglist3").val(response.url);
			            }
			            if (SelectBtn == 'imagedining4') {
			                document.getElementById('dininglist4').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, ' height="100px" width="125px" />'].join('');
			                $("#_dininglist4").val(response.url);
			            }
			            if (SelectBtn == 'imagedining5') {
			                document.getElementById('dininglist5').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, ' height="100px" width="125px" />'].join('');
			                $("#_dininglist5").val(response.url);
			            }
			            if (SelectBtn == 'imagedining6') {
			                document.getElementById('dininglist6').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, ' height="100px" width="125px" />'].join('');
			                $("#_dininglist6").val(response.url);
			            }
			            //Family Room / Den
			            if (SelectBtn == 'imagefamily1') {
			                document.getElementById('familylist1').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, ' height="100px" width="125px" />'].join('');
			                $("#_familylist1").val(response.url);
			            }
			            if (SelectBtn == 'imagefamily2') {
			                document.getElementById('familylist2').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, ' height="100px" width="125px" />'].join('');
			                $("#_familylist2").val(response.url);
			            }
			            if (SelectBtn == 'imagefamily3') {
			                document.getElementById('familylist3').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, ' height="100px" width="125px" />'].join('');
			                $("#_familylist3").val(response.url);
			            }
			            if (SelectBtn == 'imagefamily4') {
			                document.getElementById('familylist4').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, ' height="100px" width="125px" />'].join('');
			                $("#_familylist4").val(response.url);
			            }
			            if (SelectBtn == 'imagefamily5') {
			                document.getElementById('familylist5').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, ' height="100px" width="125px" />'].join('');
			                $("#_familylist5").val(response.url);
			            }
			            if (SelectBtn == 'imagefamily6') {
			                document.getElementById('familylist6').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, ' height="100px" width="125px" />'].join('');
			                $("#_familylist6").val(response.url);
			            }

			            //Other Section 1
			            if (SelectBtn == 'image1othersection1') {
			                document.getElementById('othersection1List1').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, ' height="100px" width="125px" />'].join('');
			                $("#_othersection1List1").val(response.url);
			            }
			            if (SelectBtn == 'image2othersection1') {
			                document.getElementById('othersection1List2').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, ' height="100px" width="125px" />'].join('');
			                $("#_othersection1List2").val(response.url);
			            }
			            if (SelectBtn == 'image3othersection1') {
			                document.getElementById('othersection1List3').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, ' height="100px" width="125px" />'].join('');
			                $("#_othersection1List3").val(response.url);
			            }
			            if (SelectBtn == 'image4othersection1') {
			                document.getElementById('othersection1List4').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, ' height="100px" width="125px" />'].join('');
			                $("#_othersection1List4").val(response.url);
			            }
			            if (SelectBtn == 'image5othersection1') {
			                document.getElementById('othersection1List5').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, ' height="100px" width="125px" />'].join('');
			                $("#_othersection1List5").val(response.url);
			            }
			            if (SelectBtn == 'image6othersection1') {
			                document.getElementById('othersection1List6').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, ' height="100px" width="125px" />'].join('');
			                $("#_othersection1List6").val(response.url);
			            }

			            //Other Section 2
			            if (SelectBtn == 'image1othersection2') {
			                document.getElementById('othersection2List1').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, ' height="100px" width="125px" />'].join('');
			                $("#_othersection2List1").val(response.url);
			            }
			            if (SelectBtn == 'image2othersection2') {
			                document.getElementById('othersection2List2').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, ' height="100px" width="125px" />'].join('');
			                $("#_othersection2List2").val(response.url);
			            }
			            if (SelectBtn == 'image3othersection2') {
			                document.getElementById('othersection2List3').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, ' height="100px" width="125px" />'].join('');
			                $("#_othersection2List3").val(response.url);
			            }
			            if (SelectBtn == 'image4othersection2') {
			                document.getElementById('othersection2List4').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, ' height="100px" width="125px" />'].join('');
			                $("#_othersection2List4").val(response.url);
			            }
			            if (SelectBtn == 'image5othersection2') {
			                document.getElementById('othersection2List5').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, ' height="100px" width="125px" />'].join('');
			                $("#_othersection2List5").val(response.url);
			            }
			            if (SelectBtn == 'image6othersection2') {
			                document.getElementById('othersection2List6').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, ' height="100px" width="125px" />'].join('');
			                $("#_othersection2List6").val(response.url);
			            }


			            //Other Section 3
			            if (SelectBtn == 'image1othersection3') {
			                document.getElementById('othersection3List1').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, ' height="100px" width="125px" />'].join('');
			                $("#_othersection3List1").val(response.url);
			            }
			            if (SelectBtn == 'image2othersection3') {
			                document.getElementById('othersection3List2').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, ' height="100px" width="125px" />'].join('');
			                $("#_othersection3List2").val(response.url);
			            }
			            if (SelectBtn == 'image3othersection3') {
			                document.getElementById('othersection3List3').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, ' height="100px" width="125px" />'].join('');
			                $("#_othersection3List3").val(response.url);
			            }
			            if (SelectBtn == 'image4othersection3') {
			                document.getElementById('othersection3List4').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, ' height="100px" width="125px" />'].join('');
			                $("#_othersection3List4").val(response.url);
			            }
			            if (SelectBtn == 'image5othersection3') {
			                document.getElementById('othersection3List5').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, ' height="100px" width="125px" />'].join('');
			                $("#_othersection3List5").val(response.url);
			            }
			            if (SelectBtn == 'image6othersection3') {
			                document.getElementById('othersection3List6').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, ' height="100px" width="125px" />'].join('');
			                $("#_othersection3List6").val(response.url);
			            }

			            //Other Section 4
			            if (SelectBtn == 'image1othersection4') {
			                document.getElementById('othersection4List1').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, ' height="100px" width="125px" />'].join('');
			                $("#_othersection4List1").val(response.url);
			            }
			            if (SelectBtn == 'image2othersection4') {
			                document.getElementById('othersection4List2').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, ' height="100px" width="125px" />'].join('');
			                $("#_othersection4List2").val(response.url);
			            }
			            if (SelectBtn == 'image3othersection4') {
			                document.getElementById('othersection4List3').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, ' height="100px" width="125px" />'].join('');
			                $("#_othersection4List3").val(response.url);
			            }
			            if (SelectBtn == 'image4othersection4') {
			                document.getElementById('othersection4List4').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, ' height="100px" width="125px" />'].join('');
			                $("#_othersection4List4").val(response.url);
			            }
			            if (SelectBtn == 'image5othersection4') {
			                document.getElementById('othersection4List5').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, ' height="100px" width="125px" />'].join('');
			                $("#_othersection4List5").val(response.url);
			            }
			            if (SelectBtn == 'image6othersection4') {
			                document.getElementById('othersection4List6').innerHTML = ['<img src="', response.url, '" title="', SelectBtn, ' height="100px" width="125px" />'].join('');
			                $("#_othersection4List6").val(response.url);
			            }

			            that.imgEyecandy.hide();

			            that.destroy();

			            that.obj.append('<img class="croppedImg" src="' + response.url + '">');
			            if (that.options.outputUrlId !== '') { $('#' + that.options.outputUrlId).val(response.url); }

			            that.croppedImg = that.obj.find('.croppedImg');

			            that.init();

			            that.hideLoader();

			        }
			        if (response.status == 'error') {
			            that.obj.append('<p style="width:100%; height:100%;>' + response.message + '</p>">');
			        }

			        if (that.options.onAfterImgCrop) {
			            that.options.onAfterImgCrop.call(that);
			        }
			    },
			
			    error: function (responseText) {
			        console.log(responseText);

			        that.hideLoader();
			    }
			});
		},
		showLoader:function(){
			var that = this;
			
			that.obj.append(that.options.loaderHtml);
			that.loader = that.obj.find('.loader');
			
		},
		hideLoader:function(){
			var that = this;
			that.loader.remove();	
		},
		reset:function(){
			var that = this;
			that.destroy();
			
			that.init();
			
			if( !$.isEmptyObject(that.croppedImg)){ 
				that.obj.append(that.croppedImg); 
				if(that.options.outputUrlId !== ''){	$('#'+that.options.outputUrlId).val(that.croppedImg.attr('url'));	}
			}
			
		},
		destroy:function(){
			var that = this;
			if(that.options.modal && !$.isEmptyObject(that.modal) ){ that.destroyModal(); }
			if(that.options.imgEyecandy && !$.isEmptyObject(that.imgEyecandy) ){  that.destroyEyecandy(); }
			if( !$.isEmptyObject( that.cropControlsUpload ) ){  that.cropControlsUpload.remove(); }
			if( !$.isEmptyObject( that.cropControlsCrop ) ){   that.cropControlsCrop.remove(); }
			if( !$.isEmptyObject( that.loader ) ){   that.loader.remove(); }
			if( !$.isEmptyObject( that.form ) ){   that.form.remove(); }
			that.obj.html('');
		}
	};
})(window, document);
