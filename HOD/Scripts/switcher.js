﻿/*(function ($) {
    $(document).ready(function () {
        $('#styleswitch_area .styleswitch').click(function () {
            switchStylestyle(this.getAttribute("data-src"));
            // console.log(this.getAttribute("rel"));
            return false;
        });
        var c = readCookie('style');
        if (c) switchStylestyle(c);
    });

    function switchStylestyle(styleName) {
        $('link[rel*=style][title]').each(function (i) {
            this.disabled = true;
            if (this.getAttribute('title') == styleName) this.disabled = false;
        });
        createCookie('style', styleName, 365);
    }
})(jQuery);*/


// Cookie functions
function createCookie(name, value, days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    } else var expires = "";
    document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name, "", -1);
}

// Switcher
jQuery('#demo_changer .demo-icon').click(function () {

    if (jQuery('.demo_changer').hasClass("active")) {
        jQuery('.demo_changer').animate({
            "left": "-230px"
        }, function () {
            jQuery('.demo_changer').toggleClass("active");
        });
    } else {
        jQuery('.demo_changer').animate({
            "left": "0px"
        }, function () {
            jQuery('.demo_changer').toggleClass("active");
        });
    }
});

var owlReinit = function () {
    var owlSlider = $('#owl-carousel-slider');
    var owl = $('#owl-carousel');
    if (owlSlider.length) {
        owlSlider.owlCarousel();
        var owlSliderInst = owlSlider.data('owlCarousel');
        owlSliderInst.reinit();
    }
    if (owl.length) {
        owl.owlCarousel();
        var owlInst = owl.data('owlCarousel');
        owlInst.reinit();
    }
}

var btnWide = $('#demo_changer #btn-wide');
var btnBoxed = $('#demo_changer #btn-boxed');

if ($('body').hasClass('boxed')) {
    btnBoxed.addClass('btn-primary');
} else {
    btnWide.addClass('btn-primary');
}

btnWide.click(function (event) {
    event.preventDefault();
    $('body').removeClass('boxed');
    $(this).addClass('btn-primary');
    btnBoxed.removeClass('btn-primary');
    owlReinit();
});

btnBoxed.click(function (event) {
    event.preventDefault();
    $('body').addClass('boxed');
    $(this).addClass('btn-primary');
    btnWide.removeClass('btn-primary');
    $('body').removeClass('bg-cover');
    $('body').css('background-image', 'url("img/patterns/binding_light.png")');
    owlReinit();
});

$('#styleswitch_area .styleswitch').click(function () {
    SetSkinColor($(this).attr('data-src'));
	return false;
});
function SetSkinColor(SC)
{
    $('.stripe').css('background-color', SC);
    /*$('.tabs nav ul li').css('border-color', SC);
	$('.tabs nav li.tab-current:before').css('background', SC);
	$('.tabs nav li.tab-current:after').css('background', SC);*/

    $('#qoute').css('background-color', SC);
    $('#cssmenu').css('background-color', SC);
    $('#contactButton').css('background-color', SC);

    $('h1 span').css('color', SC);
    //$('.tabs nav a').css('color', $(this).attr('data-src'));
    $('#footer').css('color', SC);
    SkinColorVal = SC;
}

$('#patternswitch_area .patternswitch').click(function (event) {
    SetBackgroundImage($(this).css('background-image'));
    BackgroundColorVal = "";
});

function SetBackgroundImage(BGI)
{
    $('body').css('background-image', BGI);
    BackgroundImageVal = BGI;
    if (BackgroundImageVal != "") {
        BackgroundImageVal = BackgroundImageVal.replace(/"/g, '');
        BackgroundImageVal = BackgroundImageVal.replace(/'/g, "");
        BackgroundImageVal = BackgroundImageVal.replace('ulr(', '');
        BackgroundImageVal = BackgroundImageVal.replace(')', '');
        //BackgroundImageVal = BackgroundImageVal.replace('http://', '');
        //BackgroundImageVal = BackgroundImageVal.replace('https' '');
        if (BackgroundImageVal.lastIndexOf("/") >= 0) {
            BackgroundImageVal = BackgroundImageVal.substring(BackgroundImageVal.lastIndexOf("/") + 1);
        }
    }
}

$('#bgcolorswitch_area .bgcolorswitch').click(function (event) {
    SetBackgroundColor($(this).attr('data-src'));
    /*BackgroundImageVal = */
});

function SetBackgroundColor(BGC)
{
    /*$('body').css('background-image', 'none');*/
    $('body').css('background-color', BGC);
    BackgroundColorVal = BGC;
}
//HeaderImageVal
function SetHeaderImage(HIV) {
    $('.header').css('background-image', 'url("' + HIV + '")');
    HeaderImageVal = HIV;
    if (HeaderImageVal != "") {
        HeaderImageVal = HeaderImageVal.replace(/"/g, '');
        HeaderImageVal = HeaderImageVal.replace(/'/g, "");
        HeaderImageVal = HeaderImageVal.replace('ulr(', '');
        HeaderImageVal = HeaderImageVal.replace(')', '');
        //HeaderImageVal = HeaderImageVal.replace('http://', '');
        //HeaderImageVal = HeaderImageVal.replace('https' '');
        if (HeaderImageVal.lastIndexOf("/") >= 0) {
            HeaderImageVal = HeaderImageVal.substring(HeaderImageVal.lastIndexOf("/") + 1);
        }
    }
}