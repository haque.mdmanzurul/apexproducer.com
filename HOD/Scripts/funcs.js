function finish(listing_id){
	// Save photo descriptions and video link
	var fields = '';
	for(i=0;i<10;i++){
		val = $('#photodesc'+i).val();
		fields += "photodesc"+i + "=" + encodeURIComponent(val) + "&";
	}
	fields += "video=" + encodeURIComponent($('#video').val());
	$.post("data/save_photo_step.php?listing_id="+listing_id, fields,
	   function(data){
	   	 if (!_adm){
	     	document.location.href='Addconfirm';
	     } else {
	     	document.location.href='_adm/index.php';
	     }
	   });	
}

function checkLength(e,textfield){
	var key;
	if (window.event)
		key = window.event.keyCode;
	else if (e)
		key = e.which
	else
		return true;

	if ((key==null) || (key==0) || (key==8) ||
    	(key==9) || (key==13) || (key==27) )
		return true;

	if (textfield.value.length > 500){
		return false;
	} else {
		return true;
	}
}

function checkLengthOnBlur(textfield){
	if (textfield.value.length > 500){
		textfield.value = textfield.value.substr(0,500);
		return false;
	} else {
		return true;
	}
}

function checkUser(submit){
	var fields = "username=" + encodeURIComponent($('#username').val());
	$.post("data/check_user.php", fields,
	   function(data){
	   	if (data == "OK") {
	   		if (submit){
		   		var form = document.getElementById("signup");
				form.submit();
			}
			return true;
	   	} else {
			$('#dialog').html('<p>The username <b>'+$('#username').val()+'</b> is already in use, please choose a different one</p>');
			$('#dialog').dialog('open');
			$('#username').val('');			
			return false;	    	
	    }
	   });	
}

function validEmailcutsheet(){
	var email = $('#email').val();
	if (email.lenght == 0 || !isEmail(email)){
		$('#dialog').html('<p>You must enter a valid email address</p>');
		$('#dialog').dialog('open');
	} else {
		$('#emailcutsheet').submit();
	}
}

function editListing(){
	var price = $('#price').val();
	var description = $('#description').val();
	if (price != ''){
		// Save photo descriptions and video link and price,description
		var fields = '';
		for(i=0;i<10;i++){
			val = $('#photodesc'+i).val();
			fields += "photodesc"+i + "=" + encodeURIComponent(val) + "&";
		}
		fields += "price=" + encodeURIComponent(price) + "&";
		fields += "description=" + encodeURIComponent(description);
		$.post("data/save_photo_step.php", fields,
		   function(data){
		     document.location.href='/Myaccount/Editlistingconfirm';
		   });		
	} else {
		$('#dialog').html('<p>Price is required</p>');
		$('#dialog').dialog('open');		
	}
}

function validAddListing(){
	var form = document.getElementById("addlisting");
	var errors = checkRequired(form);	
	if (errors == ''){	
		form.submit();
	} else {
		$('#dialog').html('<p>'+errors+'</p>');
		$('#dialog').dialog('open');
	}
}

function sold(){
	setConfirmText('Are you sure you want to deactivate your listing and mark it as sold?');
	$( "#dialog-confirm" ).dialog( "option", 
	"buttons", { 'Confirm': function() {
					$(this).dialog('close');
					document.location.href= "/Myaccount/Sold";
				},
				Cancel: function() {
					$(this).dialog('close');
				} } );
	$('#dialog-confirm').dialog('open');
}

function setConfirmText(text){
	var html = '<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>'+text+'</p>';
	$( "#dialog-confirm" ).html(text);
}

function validSignup() {
	var form = document.getElementById("signup");
	var errors = checkRequired(form);
	var passwd = document.getElementById("passwd");
	var cpasswd = document.getElementById("cpasswd");	

	if (passwd.value != cpasswd.value){
		errors += 'The password and password confirmation must match';
	} 
	if (document.getElementById("terms")) { 
		var terms = document.getElementById("terms");
		if (!terms.checked) errors += 'You must read and agree to the terms of service';
	}
	
	if (errors == ''){
		checkUser(true);
	} else {
		$('#dialog').html('<p>'+errors+'</p>');
		$('#dialog').dialog('open');
	}
}

function checkRequired(form) {
	var errors = '';
    for (i = 0, n = form.elements.length; i < n; i++) {
		var obj = form.elements[i];

		if (obj.getAttribute("required") == '1') {
			var title = obj.getAttribute("title");
			if (obj.type == "text" || obj.type == "password") {
				if (obj.value.length==0) {
					errors += title + " is required<br />";
				}				
			} else if (obj.type == "select-one") {				
				if (obj.value == "0" || obj.value == "-1") {
					errors += title + " is required<br />";
				}
			}
		}
		
		if (obj.value.length>0 && obj.name=="email" && !isEmail(obj.value)){
			errors += "The email address entered is not valid<br />";
		}		

	}
	return errors;	
}

function sendSampleText(shortcode){
	document.getElementById("sendSample").src = 'imgs/demo_bar_send_.gif'
	var phone = document.getElementById('phone').value;	
	var carrier_id = document.getElementById("carrier");
	var country = 'CA';
	var carrier_id = carrier_id.options[carrier_id.selectedIndex].value;

	if (phone.length > 5 && carrier_id > 0){	
		document.getElementById("sendSample").disabled = true;
		serverRequest('data/sendSample.php?shortcode='+shortcode+'&phone='+phone+'&carrier_id='+carrier_id+'&country_code='+country,'sendSampleArrival');	
	} else {		
		alert('You must choose a carrier and enter you mobile phone to send the sample');
		document.getElementById("sendSample").src = 'imgs/demo_bar_send.gif';
	}
}

function sendSampleArrival(data){
	alert('The sample listing has been sent to your phone');
	document.getElementById('phone').value = '';
	var carrier_id = document.getElementById('carrier');
	if (carrier_id)	carrier_id.selectedIndex = 0;
	document.getElementById("sendSample").disabled = false;
	document.getElementById("sendSample").src = 'imgs/demo_bar_send.gif';
}

var emailexp = /^[a-z0-9][a-z_0-9\-\.]+@[a-z_0-9\.\-]+\.[a-z]{2,7}$/i

function isEmail(str) {
	return emailexp.test(str);
}

function isPriceKey(e){

	var key;
	if (window.event)
		key = window.event.keyCode;
	else if (e)
		key = e.which 
	else
		return true;
   
	if ((key==null) || (key==0) || (key==8) || 
    	(key==9) || (key==13) || (key==27) )
		return true;   
		
	keychar = String.fromCharCode(key);
	var exp = /^[0-9\,]$/
	return exp.test(keychar);
}

function isNumberKey(e){

	var key;
	if (window.event)
		key = window.event.keyCode;
	else if (e)
		key = e.which 
	else
		return true;
   
	if ((key==null) || (key==0) || (key==8) || 
    	(key==9) || (key==13) || (key==27) )
		return true;   
		
	keychar = String.fromCharCode(key);
	var exp = /^[0-9\.]$/
	return exp.test(keychar);
}

function isAlphaNumKey(e){
	var key;
	if (window.event)
		key = window.event.keyCode;
	else if (e)
		key = e.which 
	else
		return true;
   
	if ((key==null) || (key==0) || (key==8) || 
    	(key==9) || (key==13) || (key==27) )
		return true;   
		
	keychar = String.fromCharCode(key);
	var exp = /^[0-9a-zA-Z]$/
	return exp.test(keychar);
}

var mailtoexp = /^mailto:[a-z0-9][a-z_0-9\-\.]+@[a-z_0-9\.\-]+\.[a-z]{2,7}$/i

function isMailto(str) {
	return mailtoexp.test(str);
}

function isUrl(str) {	
    var v = new RegExp();
    v.compile("^[A-Za-z]+://[A-Za-z0-9-_]+\\.[A-Za-z0-9-_%&\?\/.=]+$"); 
    return v.test(str);
}
///////////////////////////////////////////////////////////////
//FUNCIONES DE MANEJO DE COMBOS
///////////////////////////////////////////////////////////////
function selectInCombo(objCombo, paramerter){
    try{
        var count = objCombo.options.length;
        objCombo.selectedIndex = -1;
        
        for (i = 0; i < count; i++){
            if (objCombo.options[i].value == paramerter){
                objCombo.selectedIndex = i;
                return;
            }
        }
    }catch(e){

    }
}

function getComboText(objCombo, value){
    var count = objCombo.options.length;
    
    for (i = 0; i < count; i++){
        if (objCombo.options[i].value == value){
            return objCombo.options[i].text;
        }
    }
}

function getComboOption(objCombo, value){
    var count = objCombo.options.length;
    
    for (i = 0; i < count; i++){
        if (objCombo.options[i].value == value){
            return objCombo.options[i];
        }
    }
}

function removeAllOptions(objCombo) {
    for (i = objCombo.length; i >= 0; i--) {
        objCombo.remove(i);
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////

function validMessage() {
	var errores = "";

	var email = document.contact.email.value;
	if (!isEmail(email) || email.length < 8)
		errores+="* Type a valid e-mail address.\n";

	var name = document.contact.name.value;
	if (name.length < 3)
		errores+="* Type your Name.\n";

	var message = document.contact.message.value;
	if (message.length < 10)
		errores+="* Type your message.\n";

	if (errores!="") {
		alert(errores);
		return false;
	} else {
		return true;
	}
}

function bookmark(url,title){

	if (window.sidebar) { // Mozilla Firefox Bookmark

		window.sidebar.addPanel(title, url,"");

	} else if( window.external ) { // IE Favorite

		window.external.AddFavorite( url, title); 
		
	} else if(window.opera && window.print) { // Opera Hotlist

		return true; 
	}
	
}

function popup(url, name, height, width, scroll){
	window.open(url,name,"width="+width+",height="+height+",status=no,scrollbars="+scroll);
}

// States / Provinces
var provinces_ca = {BC:'British Columbia',
					YT:'Yukon',
					AB:'Alberta',
					NT:'Northwest Territories',
					SK:'Saskatchewan',
					MB:'Manitoba',
					ON:'Ontario',
					QC:'Quebec',
					NU:'Nunavut',
					NB:'New Brunswick',
					PE:'Prince Edward Island',
					NS:'Nova Scotia',
					NL:'NewFoundland and Labrador'};

var provinces_us = {AL:'Alabama',
					AK:'Alaska',
					AZ:'Arizona',
					AR:'Arkansas',
					CA:'California',
					CO:'Colorado',
					CT:'Connecticut',
					DE:'Delaware',
					DC:'District of Columbia',
					FL:'Florida',
					GA:'Georgia',
					HI:'Hawaii',
					ID:'Idaho',
					IL:'Illinois',
					IN:'Indiana',
					IA:'Iowa',
					KS:'Kansas',
					KY:'Kentucky',
					LA:'Louisiana',
					ME:'Maine',
					MD:'Maryland',
					MA:'Massachusetts',
					MI:'Michigan',
					MN:'Minnesota',
					MS:'Mississippi',
					MO:'Missouri',
					MT:'Montana',
					NE:'Nebraska',
					NV:'Nevada',
					NH:'New Hampshire',
					NJ:'New Jersey',
					NM:'New Mexico',
					NY:'New York',
					NC:'North Carolina',
					ND:'North Dakota',
					OH:'Ohio',
					OK:'Oklahoma',
					OR:'Oregon',
					PA:'Pennsylvania',
					RI:'Rhode Island',
					SC:'South Carolina',
					SD:'South Dakota',
					TN:'Tennessee',
					TX:'Texas',
					UT:'Utah',
					VT:'Vermont',
					VA:'Virginia',
					WA:'Washington',
					WV:'West Virginia',
					WI:'Wisconsin',
					WY:'Wyoming'};

function emptyProvinceSelect(){
	selectProv = document.getElementById('province');
	for(i=selectProv.options.length-1;i>=0;i--){
		selectProv.remove(i);
	}
}

function previewListing(max){
	var qs = '';
	$('#preview_button').attr('disabled',true);
	var form = document.getElementById('addlisting');
   	for (i = 0, n = form.elements.length; i < n; i++) {
		var obj = form.elements[i];
		if (obj.name.length > 0)
			qs += '&'+obj.name+'='+encodeURIComponent(obj.value);
	}	

	$.get(rel_path+"data/get_preview.php?"+qs, "",
  		function(data){
    		if (data.length > 0){	    
    			len = data.substring(0,data.indexOf("|"));
    			data = data.substring(data.indexOf("|")+1);			   
				$('#phonePreview').html(data);
				if (len > max){
					$('#dialog').html("Your listing exceeds the maximum character limit by "+(len-max)+" chars and will be cropped. Go back, shorten some fields and try again.");
					$('#dialog').dialog('open');
				}
    		}
    		$('#preview_button').attr('disabled',false);
  	});	
}
					
function loadProvinces(country){
	emptyProvinceSelect();
	provinces = provinces_ca;
	selectProv = document.getElementById('province');
	selectProv.options[0] = new Option('Select a Province','0');
	var carrier = document.getElementById('carrier');
	
	for (var code in provinces){
		selectProv.options[selectProv.options.length] = new Option(provinces[code],code); 
	}
}

function loadProvincesSelected(country,province){
	emptyProvinceSelect();
	if (province=='') province = '0';
	provinces = provinces_ca;
	selectProv = document.getElementById('province');
	selectProv.options[0] = new Option('Select a Province','0');
	
	for (var code in provinces){
		selectProv.options[selectProv.options.length] = new Option(provinces[code],code);
	}
	selectProv.value = province;
}

function serverRequest(url, returnFunctionName){  
    var xmlHttpReq = false;
    var self = this;
    if (window.XMLHttpRequest) { // Mozilla/Safari
        self.xmlHttpReq = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) { // IE
        self.xmlHttpReq = new ActiveXObject("Microsoft.XMLHTTP");
    }
    self.xmlHttpReq.open('GET', url, true);
    self.xmlHttpReq.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    self.xmlHttpReq.onreadystatechange = function() {
        if (self.xmlHttpReq.readyState == 4) {
            var response = self.xmlHttpReq.responseText;
            eval(returnFunctionName)(response);
        }
    }
    self.xmlHttpReq.send('');
}

function closeFollowup(){
	$('#followup').css('display','none');
}

function saveFollowup(){
	$.post("/data/followup.php", { phone: $('#followup_phone').html(), listing_id: $('#followup_listing_id').val(), x: "add", notes: $('#followup_notes').val() } );
	$('#followup').css('display','none');
}

function toggleFollowup(phone,listing_id){
	var img_id = 'followup_'+phone.replace('+','');
	var img_src = $('img[name='+img_id+']').attr('src');
	var action = 'unmark';
	var o = 'ok.gif';
	var n = 'ok_dim.gif';
	var alt = 'NO';
	if (img_src.indexOf('_dim') > 0) {
		action = 'mark';
		o = 'ok_dim.gif';
		n = 'ok.gif';
		var alt = 'YES';
	}
	img_src = img_src.replace(o,n); 
	$('img[name='+img_id+']').attr('src',img_src);
	$('img[name='+img_id+']').attr('alt',alt);
	$('img[name='+img_id+']').attr('title',alt);		
	$.post("/data/followup.php", { phone: phone, listing_id: listing_id, x: action } );
}

function addFollowup(phone,listing_id,date){
	$('#followup_phone').html(phone);
	$('#followup_listing_id').val(listing_id);
	$('#followup_date').html("Last updated: "+date);
	var top = $(window).scrollTop()+50;
	$('#followup').css('top',top);
	$.post("/data/followup.php", { phone: phone, listing_id: listing_id, x: "get" },
	function(data){
    	$('#followup_notes').val(data);		  	
		$('#followup').css('display','block');    		
  	} );	
}

function customRange(dateText, inst) {
	date = $.datepicker.parseDate('yy-mm-dd',dateText); 
	if (inst.id == 'end_date'){
		$('#start_date').datepicker('option','maxDate',date);
	} else if (inst.id == 'start_date'){					
		$('#end_date').datepicker('option','minDate',date);
	}
} 

function dateRangeFilter(request_uri){
	var url = request_uri+'start_date='+$("#start_date").val()+'&end_date='+$("#end_date").val();
	document.location.href = url;
}

function updateCounter(text_field,counter_span,max) {
	var obj = document.getElementById(counter_span);
	obj.innerHTML = max-(text_field.value).length;
}

function validBroadcast(){
	if ($("#message").val().length > 0 && $("#description").val().length > 0){
		return true;
	} else {
		$('#dialog').html("You must enter the message and description");
		$('#dialog').dialog('open');
		return false;
	}
}

function findListing(){
	var keyword = $('#search_keyword').val();
	var address = $('#search_address').val();
	$.post("/data/search.php", { keyword: keyword, address: address },
	function(data){
		if (data == "" || data == "ERROR"){
			$('#dialog').html("The requested listing was not found, revise your search and try again.");
			$('#dialog').dialog('open');
		} else {
			document.location.href = '/Listing/'+data;
		}  		
  	} );	
}

function validForm(theForm) {
	var errors = '';
    for (var i = 0, n = theForm.elements.length; i < n; i++) {
		var obj = theForm.elements[i];
		
		if (obj.type == "text" || obj.type == "password" || obj.type == "select-one" ||
		 	obj.type == "hidden" || obj.type == "textarea" || obj.type == "file") {
		
			if (obj.getAttribute("required") != null) {
				var title = obj.getAttribute("title");
				if (obj.type == "text" || obj.type == "password" || obj.type == "file") {
					if (obj.value.length==0) {
						errors += title + " is required\n";
					}				
				} else if (obj.type == "select-one") {
					if (obj.value == "0" || obj.value == "-1") {
						errors += title + " is required\n";
					}
				}
			}
			
			if (obj.value.length>0 && obj.name=="email" && !isEmail(obj.value)){
				errors += "The email address entered is not valid\n";
			}		
			
		}
	}
	if (errors.length==0){
		return true;
	} else {
		alert(errors);
		return false;
	}
}
