﻿<%@ WebHandler Language="C#" Class="Handler" %>

using System;
using System.Web;

public class Handler : IHttpHandler {

    public void ProcessRequest(HttpContext context)
    {
        var headers = context.Request.Headers;
        HttpPostedFile fileToUpload = context.Request.Files[0];
        string FileName = Guid.NewGuid() + System.IO.Path.GetExtension(fileToUpload.FileName);
        string pathToSave = HttpContext.Current.Server.MapPath("~/temp_uploads/phi_") + FileName;
        fileToUpload.SaveAs(pathToSave);
        context.Response.Write(FileName);
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

    //// Upload partial file
    //private void UploadPartialFile(string fileName, HttpContext context, List<FilesStatus> statuses)
    //{
    //    if (context.Request.Files.Count != 1) throw new HttpRequestValidationException("Attempt to upload chunked file containing more than one fragment per request");
    //    var inputStream = context.Request.Files[0].InputStream;
    //    var fullName = StorageRoot + Path.GetFileName(fileName);

    //    using (var fs = new FileStream(fullName, FileMode.Append, FileAccess.Write))
    //    {
    //        var buffer = new byte[1024];

    //        var l = inputStream.Read(buffer, 0, 1024);
    //        while (l > 0)
    //        {
    //            fs.Write(buffer, 0, l);
    //            l = inputStream.Read(buffer, 0, 1024);
    //        }
    //        fs.Flush();
    //        fs.Close();
    //    }
    //    statuses.Add(new FilesStatus(new FileInfo(fullName)));
    //}
}