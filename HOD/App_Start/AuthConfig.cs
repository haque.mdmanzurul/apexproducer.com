﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Web.WebPages.OAuth;
using HOD.Models;
using GooglePlusOAuthLogin;

namespace HOD
{
    public static class AuthConfig
    {
        public static void RegisterAuth()
        {
            // To let users of this site log in using their accounts from other sites such as Microsoft, Facebook, and Twitter,
            // you must update this site. For more information visit http://go.microsoft.com/fwlink/?LinkID=252166

            //OAuthWebSecurity.RegisterMicrosoftClient(
            //    clientId: "",
            //    clientSecret: "");

            //OAuthWebSecurity.RegisterTwitterClient(
            //    consumerKey: "",
            //    consumerSecret: "");
            //Local
            //OAuthWebSecurity.RegisterFacebookClient(
            //    appId: "991934317486534",
            //    appSecret: "c46135f3bbc72c62213c8c662e66f2c1");
            //Server
            OAuthWebSecurity.RegisterFacebookClient(
                appId: "1490468264508940",
                appSecret: "a86d478290b3db7f5027d321c1ad2dcc");

            ////SERVER 
            OAuthWebSecurity.RegisterClient(new GooglePlusClient("905858541434-1t2i6b60400eld980cn0k9gsd6rphchb.apps.googleusercontent.com", "WpdG0iS8XHk-jsiFYOlEJIrH"), "Google+", null);
            ////Local 
            //OAuthWebSecurity.RegisterClient(new GooglePlusClient("365959073434-4hvbk9lhk7p6btll4kmqpofchb0t7ku5.apps.googleusercontent.com", "Kno5CkljlQShLU5Mv3zP9Tfj"), "Google+", null);
            WebMatrix.WebData.WebSecurity.InitializeDatabaseConnection("HODContext2", "UserProfile", "UserId", "UserName", autoCreateTables: true);
        }
        /*
        public void ConfigureAuth(IAppBuilder app)
        {
            // Configure the db context and user manager to use a single instance per request
            app.CreatePerOwinContext(ApplicationDbContext.Create);
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);

            // Enable the application to use a cookie to store information for the signed in user
            // and to use a cookie to temporarily store information about a user logging in with a third party login provider
            // Configure the sign in cookie
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login"),
                Provider = new CookieAuthenticationProvider
                {
                    OnValidateIdentity = SecurityStampValidator.OnValidateIdentity<ApplicationUserManager, ApplicationUser>(
                        validateInterval: TimeSpan.FromMinutes(30),
                        regenerateIdentity: (manager, user) => user.GenerateUserIdentityAsync(manager))
                }
            });
    
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            // Uncomment the following lines to enable logging in with third party login providers
            //app.UseMicrosoftAccountAuthentication(
            //    clientId: "",
            //    clientSecret: "");

            //app.UseTwitterAuthentication(
            //   consumerKey: "",
            //   consumerSecret: "");

            //app.UseFacebookAuthentication(
            //   appId: "",
            //   appSecret: "");

            app.UseGoogleAuthentication(
                 clientId: "000-000.apps.googleusercontent.com",
                 clientSecret: "00000000000");
        } */
    }
}
