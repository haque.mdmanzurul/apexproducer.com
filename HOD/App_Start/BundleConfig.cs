﻿using System.Web;
using System.Web.Optimization;

namespace HOD
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-1.9.1.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/jquery.validate*"));

           /* bundles.Add(new ScriptBundle("~/bundles/fullListing").Include(
            "~/Scripts/listingres/jquery.easing.1.3.js",
            "~/Scripts/listingres/jquery.slide.js",
            "~/Scripts/listingres/jquery.tmpl.min.js", 
            "~/Scripts/listingres/gallery.js",
            "~/Scripts/listingres/cbpFWTabs.js",
            "~/Scripts/listingres/modernizr-2.5.3-min.js",
            "~/Scripts/listingres/responsivegridsystem.js",
            "~/Scripts/listingres/selectivizr-min.js")            
            );*/

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/scripts").Include(
                       "~/Scripts/jquery-ui-1.8.24.js",
                       "~/Scripts/jquery.form.js",
                       "~/Scripts/checkout.js",
                       "~/Scripts/bootstrap/js/bootstrap.js"
            ));


            bundles.Add(new StyleBundle("~/Content/css").Include(
                       // "~/Content/site.css",
                        "~/Content/bootstrap-theme.css",
                        "~/Content/bootstrap.css",
                        "~/Content/jquery-ui-1.8.2.custom.css",
                        "~/Content/responsive.css",
                        "~/Content/style.css"));

            bundles.Add(new StyleBundle("~/Content/fonts").Include(
                        "~/Content/fonts/glyphicons-halflings-regular.eot",
                        "~/Content/fonts/glyphicons-halflings-regular.svg",
                        "~/Content/fonts/glyphicons-halflings-regular.ttf",
                        "~/Content/fonts/glyphicons-halflings-regular.woff"));
                        /*"~/Content/fonts/opensans-bold-webfont.eot",
                        "~/Content/fonts/opensans-bold-webfont.svg",
                        "~/Content/fonts/opensans-bold-webfont.ttf",
                        "~/Content/fonts/opensans-bold-webfont.woff",
                        "~/Content/fonts/opensans-regular-webfont.eot",
                        "~/Content/fonts/opensans-regular-webfont.svg",
                        "~/Content/fonts/opensans-regular-webfont.ttf",
                        "~/Content/fonts/opensans-regular-webfont.woff"));
            */
            bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
                        "~/Content/themes/base/jquery.ui.core.css",
                        "~/Content/themes/base/jquery.ui.resizable.css",
                        "~/Content/themes/base/jquery.ui.selectable.css",
                        "~/Content/themes/base/jquery.ui.accordion.css",
                        "~/Content/themes/base/jquery.ui.autocomplete.css",
                        "~/Content/themes/base/jquery.ui.button.css",
                        "~/Content/themes/base/jquery.ui.dialog.css",
                        "~/Content/themes/base/jquery.ui.slider.css",
                        "~/Content/themes/base/jquery.ui.tabs.css",
                        "~/Content/themes/base/jquery.ui.datepicker.css",
                        "~/Content/themes/base/jquery.ui.progressbar.css",
                        "~/Content/themes/base/jquery.ui.theme.css"));
            /*
            bundles.Add(new StyleBundle("~/Content/listingresx/css").Include(
                        "~/Content/listingres/css/*.css"));

            bundles.Add(new StyleBundle("~/Content/agentresx/css").Include(
                 "~/Content/agentres/css/*.css")
                );*/

            bundles.Add(new StyleBundle("~/Content/fonts/bpicons").Include(
                        "~/Content/listingres/bpicons.eot",
                        "~/Content/listingres/bpicons.svg",
                        "~/Content/listingres/bpicons.tff",
                        "~/Content/listingres/bpicons.woff"
                ));

            bundles.Add(new StyleBundle("~/Content/fonts/icomoon").Include(
                        "~/Content/listingres/icomoon.eot",
                        "~/Content/listingres/icomoon.svg",
                        "~/Content/listingres/icomoon.tff",
                        "~/Content/listingres/icomoon.woff"
                ));

            //Added by Manzurul Haque

            bundles.Add(new ScriptBundle("~/bundles/dropzonescripts").Include(
                     "~/Scripts/dropzone/dropzone.js"));

            bundles.Add(new StyleBundle("~/Content/dropzonescss").Include(
                     "~/Scripts/dropzone/basic.css",
                     "~/Scripts/dropzone/dropzone.css"));

        }
    }
}