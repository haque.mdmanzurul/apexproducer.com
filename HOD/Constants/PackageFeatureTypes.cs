﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HOD.Constants
{
    public class PackageFeatureTypes
    {
        public const String Unique_Vue_Code = "Unique Vue Code";
        public const String Fully_Integrated_SMS_Marketing = "Fully Integrated SMS Marketing";
        public const String Virtual_Lawn_Sign = "Virtual Lawn Sign";
        public const String Fully_Customizable_Unique_Website = "Fully Customizable Unique Website";
        public const String Online_Buyer_Seller_Resources = "Online Buyer Sellers Resources";
        public const String Our_Verified_Network = "Our Verified Network";
        public const String Sellers_Privacy_Protection = "Sellers Privacy Protection";
        public const String Standard_Lawn_Sign = "Standard Lawn Sign";
        public const String Durable_Sign = "Durable Sign";
        public const String Ad_Agent = "Ad Agent";
        public const String Seller_Executive_Assistant = "Seller Executive Assistant";
        public const String Seller_Executive_Assistant_2_Weeks = "Seller Executive Assistant 2 Weeks";
    }
}