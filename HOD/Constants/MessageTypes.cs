﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HOD.Constants
{
    public static class MessageTypes
    {
        public const String MESSAGE = "Message";
        public const String REPLY = "Reply";
    }
}