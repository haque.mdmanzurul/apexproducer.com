﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HOD.Constants
{
    public class MailMessageHelper
    {
        public static string FormattedMail(string Subject, string FirstName, string Body, string ConversationUrl)
        {
            return
                String.Format(@"
                    <div style='min-height: 38px; font-family:Tahoma; font-size:15px; width: 800px; 
                    color: #4f4f4f; position: relative; border: 1px solid; padding: 15px; -webkit-border-radius: 3px; 
                    -moz-border-radius: 3px; border-radius: 3px; text-align: justify; margin: 0 0 30px 0; display: block; 
                    height: auto; clear: both; border-color: #d5e9f2; background-color: #dcf0fa; line-height:8px;'>
                    <p><b>Message from CasaVue<b></p><br/> 
                    <p>Hi <strong style='font-size:14px; font-family:Georgia; font-weight:bold; font-style:italic;'>{1}</strong>,  
                    you have received a message about your listing. Please see the message below:</p><br/>
                    <p><strong>Subject: {0}</strong></p><br/>
                    <blockquote style='display: block; color:black;'>
                    <p style='font-size:14px; font-family:Georgia; font-weight:bold; font-style:italic;'>
	                <span style='float: left; color: #903; font-size: 75px; line-height: 12px; padding-top: -19px; padding-right: 8px; padding-left: 3px; '>&ldquo;</span>
	                {2}</p></blockquote>
	                <br/>
	                <p>You may reply by clicking the button below.</p>
                    <br/>
                    <a style='color: #FFFFFF;text-decoration: none;' href={3}>
                    <input style='background-color: #8EA5B5; background-image: linear-gradient(#BBCBD8, #8EA5B5); border: 1px solid #929BA2; 
                    color: #57646D; text-shadow: 1px 1px 1px #C6D4DF; padding:8px; font-size:12px; 
                    font-family:Tahoma; font-weight:bold;' type=button value='Reply on CasaVue'/></a></div>",
                    Subject,
                    FirstName, Body, ConversationUrl);
        }

        public static string AgentMessage(string Subject, string firstName, string lastName, string email, string phoneNo, string contactPreference,
                string bio, string  occupation, string  income ,string  housetype ,string  affiliations, string  comments)
        {
            //first_name + last_name + email + phone + contact_preference + bio + occupation + income + housetype + affiliations + comments;
            return
                String.Format(@"
                        <div>
                                Subject: {0}, <p></p>
                                First Name: {1}
                                Last Name: {2}
                                Email: {3}
                                Phone No: {4}
                                Contact Preference: {5}
                                Bio: {6}
                                Occupation: {7}
                                Income: {8}
                                House Type: {9}
                                Affiliations: {10}
                                Comments: {11}
                        </div>
                    ",
                    Subject, firstName, lastName, email, phoneNo,contactPreference, bio, occupation, income, housetype, affiliations, comments);
        }
    }
}