﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using HOD.Entities;
using HOD.Models;
using HOD.Managers;
using System.Web.Routing;

namespace HOD.Constants
{
    public class ListingOwnerAttribute : AuthorizeAttribute
    {
        DbManager dbMgr = new DbManager();
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var isAuthorized = base.AuthorizeCore(httpContext);
            if (!isAuthorized)
            {
                return false;
            }

             Listing CurrentListing = (Listing)httpContext.Session["CurrentListing"];
             User user = dbMgr.GetUserByUserName(httpContext.User.Identity.Name);
           

            if (CurrentListing.UserId.HasValue)
            {
                if (CurrentListing.UserId == user.UserId)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            } 
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = new RedirectToRouteResult(
                   new RouteValueDictionary(
                       new
                       {
                           controller = "Home",
                           action = "Index",
                           id = UrlParameter.Optional
                       })
                   );
        }


    }
}