﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HOD.Constants
{
    public static class PackageTypes
    {
        public const String BRONZE = "Bronze";
        public const String SILVER = "Silver";
        public const String GOLD = "Gold";
        public const String AGENT = "AGENT";
        public const String AGENTPRODUCER = "ApexAgent";
        public const String BRANDED  = "BRANDED";
        public const String BRANDEDPRO  = "BRANDEDPRO";
        public const String PROTEAM = "PROTEAM";
        public const String BASIC  = "BASIC";
        public const String PREMIUM  = "PREMIUM";
        public const String PROFESSIONAL  = "PROFESSIONAL";
    }

}