﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HOD.Constants
{
    public static class ListingImageCategoryNames
    {
        public  const String VIRTUALSIGN = "VirtualSign";
        public  const String LOCATION = "Location";
        public  const String KITCHEN = "Kitchen";
        public  const String BATHROOM = "Bathroom";
        public const String BEDROOM = "Bedroom";
        public const String OBEDROOM = "Other Bedroom";
        public const String BASEMENT = "Basement";
        public const String FAMILYROOM = "Family Room";
        public const String DININGROOM = "Dining Room";
        public const String VIDEOUPLOAD = "Video Upload";
        public const String OTHERSECTION1 = "OtherSection1";
        public const String OTHERSECTION2 = "OtherSection2";
        public const String OTHERSECTION3 = "OtherSection3";
        public const String OTHERSECTION4 = "OtherSection4";
        
    }
}