﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HOD.Constants
{
    public static class UserTypes
    {
        public const String BUYER = "Buyer";
        public const String SELLER = "Seller";
        public const String AGENT = "Agent";
    }

}