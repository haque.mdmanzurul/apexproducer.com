﻿using HOD.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace HOD.Models
{
    class ListingImageContext : DbContext
    {
        public ListingImageContext()
            : base("HODContext2")
        {
        }
        public DbSet<ListingImage> image { get; set; }
    }
}
