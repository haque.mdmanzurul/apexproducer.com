﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HOD.Models
{
    public class AgentPagesViewModel
    {
        public List<AgentPageViewModel> Pages { get; set; }

        public string Name { get; set; }

        public AgentPagesViewModel()
        {
            this.Pages = new List<AgentPageViewModel>();
        }
    }

    public class AgentPageViewModel
    {
        public List<AgentPageItemViewModel> Items { get; set; }
        public int PageId { get; set; }
        public string PageType { get; set; }

        public int PageTypeId { get; set; }

        public AgentPageViewModel()
        {
            this.Items = new List<AgentPageItemViewModel>();
        }
    }
    public class AgentPageItemViewModel
    {

        public string PageContent { get; set; }
 //       public int PageId {get; set;}
        public int PageItemTypeId { get; set; }
        public string PageItemDescription { get; set; }
        public string PageItemLabel { get; set; }
    }

    public class AgentSetupModel
    {

        public string YourHomeContent { get; set; }
        public string YourAboutUsContent { get; set; }
        public string OtherPage1Label { get; set; }
        public string OtherPage1Text { get; set; }
        public string OtherPage2Label { get; set; }
        public string OtherPage2Text { get; set; }
        
        public string ProfilePic1 { get; set; }
        public string ProfilePic2 { get; set; }
        public string ProfilePic3 { get; set; }
        public string MainWebsitePhoto { get; set; }

        public string OtherAgentPhoto1 { get; set; }
        public string OtherAgentPhoto2 { get; set; }
        public string OtherAgentPhoto3 { get; set; }
        public string OtherAgentPhoto4 { get; set; }
        public string OtherAgentPhoto5 { get; set; }
        public string OtherAgentPhoto6 { get; set; }
        public string OtherAgentPhoto7 { get; set; }
        public string OtherAgentPhoto8 { get; set; }
        public string OtherAgentPhoto9 { get; set; }
        public string OtherAgentPhoto10 { get; set; }

        public string ProfileAgentLogoDisplay { get; set; }
        public string listingOrderCategoryTop { get; set; }
        public string listingOrderCategoryMiddle { get; set; }
        public string listingOrderCategoryBottom { get; set; }
        public string AgentTopBackground { get; set; }
        public string AgentBottomBackground { get; set; }
        public string AgentLogo { get; set; }
        public string BusinessName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Province { get; set; }
        public string PostalCode { get; set; }
        public string PhoneNumberCell { get; set; }
        public string PhoneNumberLandLine { get; set; }
        public string AgentFullName { get; set; }
        public string AgentUrl { get; set; }
        public int PageTypeId { get; set; }
        public string profileWebsiteTopImage { get; set; }

    }

    public class AgentDisplayModel
    {
        public string AgentTopBackground { get; set; }
        public string AgentBottomBackground { get; set; }
        public string AgentLogo { get; set; }
        public string AgentProfilePic { get; set; }
        public int AgentId { get; set; }
        public string AboutMe { get; set; }
        public string HomePageContent { get; set; }
        public string OtherPage1Label { get; set; }
        public string OtherPage2Label { get; set; }
        public string OtherPage1Content { get; set; }
        public string OtherPage2Content { get; set; }
        public string BusinessName { get; set; }

       // public List<string> ListingsImageUrls { get; set; }
        public List<ListingImageMetaData> listingImages {get; set;}

        public string BusinessAddress { get; set; }

        public string BusinessCity { get; set; }

        public string PostalCode { get; set; }

        public string Province { get; set; }

        public string Phone1 { get; set; }

        public string Phone2 { get; set; }
    }

    public class ListingImageMetaData : VirtualListing
    {
        public string ListingImageUrl { get; set; }
        public string Category { get; set; }
        public string EmailAddress { get; set; }
    }


}