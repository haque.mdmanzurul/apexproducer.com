﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using HOD.Entities;

namespace HOD.Models
{
        public class PrivateMessageModel
        {
            public long MessageId { get; set; }
            public int MessageTypeId { get; set; }
            public Guid ConversationId { get; set; }
            public int FromUser { get; set; }
            public int ToUser { get; set; }
            public string Subject { get; set; }
            public string Body { get; set; }
            public DateTime MessageDate { get; set; }
            public bool MessageRead { get; set; }

            public int ListingID { get; set; }

            public string MesssageTypeName { get; set; }

            public string ReplySubject { get; set; }
            [Required]
            public string ReplyBody { get; set; }

            public static PrivateMessageModel Translate(Message translate)
            {
                PrivateMessageModel translatedMessage = new PrivateMessageModel();
                translatedMessage.Body = translate.Body;
                translatedMessage.ConversationId = translate.ConversationId;
                translatedMessage.FromUser = translate.FromUser;
                translatedMessage.MessageDate = translate.MessageDate;
                translatedMessage.MessageId = translate.MessageId;
                translatedMessage.MessageTypeId = translate.MessageTypeId;
                translatedMessage.MesssageTypeName = translate.MessageTypeName;
                translatedMessage.MessageRead = translate.MessageRead;
                translatedMessage.Subject = translate.Subject;
                translatedMessage.ToUser = translate.ToUser;
                translatedMessage.ListingID = translate.ListingID;

                return translatedMessage;
            }

            public static Message Translate(PrivateMessageModel translate)
            {
                Message translatedMessage = new Message();
                translatedMessage.Body = translate.Body;
                translatedMessage.ConversationId = translate.ConversationId;
                translatedMessage.FromUser = translate.FromUser;
                translatedMessage.MessageDate = translate.MessageDate;
                translatedMessage.MessageId = translate.MessageId;
                translatedMessage.MessageTypeId = translate.MessageTypeId;
                translatedMessage.MessageRead = translate.MessageRead;
                translatedMessage.Subject = translate.Subject;
                translatedMessage.ToUser = translate.ToUser;
                translatedMessage.ListingID = translate.ListingID;
                return translatedMessage;
            }

            public static List<Message> Translate(List<PrivateMessageModel> translateList)
            {
                List<Message> translatedErrorList = new List<Message>();

                foreach (PrivateMessageModel translate in translateList)
                    translatedErrorList.Add(Translate(translate));

                return translatedErrorList;
            }

            public static List<PrivateMessageModel> Translate(List<Message> translateList)
            {
                List<PrivateMessageModel> translatedErrorList = new List<PrivateMessageModel>();

                foreach (Message translate in translateList)
                    translatedErrorList.Add(Translate(translate));

                return translatedErrorList;
            }
        }

        public class UserMessageModel
        {
            public UserProfile User { get; set; }
            public List<PrivateMessageModel> Messages { get; set; }
            public List<UserConversationModel> Conversations { get; set; }
        }

        public class UserConversationModel
        {
            public Guid ConversationId { get; set; }
            public string Subject { get; set; }
            public string ReplySubject { get; set; }
            [Required]
            public string ReplyBody { get; set; }
            public int ToUser { get; set; }
            public int FromUser { get; set; }
            public string FromUserName { get; set; }

            public long MessageId { get; set; }
            public int MessageTypeId { get; set; }
            public string Body { get; set; }
            public DateTime MessageDate { get; set; }
            public bool MessageRead { get; set; }

            public int ListingID { get; set; }

            public List<PrivateMessageModel> Messages { get; set; }
        }

}