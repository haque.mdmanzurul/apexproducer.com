﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Security;
using DataAnnotationsExtensions;
using HOD.Validation;

namespace HOD.Models
{
    public class UsersContext : DbContext
    {
        public UsersContext()
            : base("HODContext2")
        {
        }

        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<UserType> UserTypes { get; set; }
        public DbSet<RegistrationError> RegistrationErrors { get; set; }
        public DbSet<UserPackages> UserPackages { get; set; }
        public DbSet<UserPackageTypes> UserPackageTypes { get; set; }
    }

    [Table("UserProfile")]
    public class UserProfile
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }
        public int UserTypeId { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Province { get; set; }
        public string PostalCode { get; set; }
        public string PhoneNumberCell { get; set; }
        public string PhoneNumberLandLine { get; set; }
        public string Carrier { get; set; }

        public string BillingName { get; set; }
        public string BillingAddress { get; set; }
        public string BillingCity { get; set; }
        public string BillingProvince { get; set; }
        public string BillingPostalCode { get; set; }
        public string StripeCustomerId { get; set; }
        public bool IsAgent { get; set; }
        public string AgentUrl { get; set; }
        public int AgentId { get; set; }
        public string Tier { get; set; }
        public string AgentSubDomain { get; set; }
        //public string StripeChargeId { get; set; }
      //  public bool PrivateMessageFeature { get; set; }


        public string Country { get; set; }

        public string BusinessName { get; set; }

        public string NoOfEmployees { get; set; }

        public string MaxUploadCount { get; set; }
    }

    [Table("UserTypes")]
    public partial class UserType
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int UserTypeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }

    [Table("RegistrationErrors")]
    public class RegistrationError
    {
        public int AgentID;
        public bool PayOnUserBehalf;
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int RegistrationErrorId { get; set; }
        public string UserEmail { get; set; }
        public string ErrorType { get; set; }
        public string Message { get; set; }
        public string Code { get; set; }
        public string Parameter { get; set; }
        public string Charge { get; set; }
        public string StripeError { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Province { get; set; }
        public string PostalCode { get; set; }
        public string PhoneNumberCell { get; set; }
        public string PhoneNumberLandLine { get; set; }
        public string Carrier { get; set; }

        public string BillingName { get; set; }
        public string BillingAddress { get; set; }
        public string BillingCity { get; set; }
        public string BillingProvince { get; set; }
        public string BillingPostalCode { get; set; }
    }

    [Table("UserPackages")]
    public class UserPackages
    {
        public int PackageId { get; set; }
        [Key]   
        public int UserId { get; set; }
        public int PackageTypeId { get; set; }
    }

    [Table("PackageTypes")]
    public class UserPackageTypes
    {
        [Key]
        public int PackageTypeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int PackageChargeFrequencyId { get; set; }
        public decimal BaseCost { get; set; }
    }

    public class RegisterExternalLoginModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }


        //[Display(Name = "First name")]
        //public string FirstName { get; set; }


        //[Display(Name = "Last name")]
        //public string LastName { get; set; }
        
        public string ExternalLoginData { get; set; }
    }

    public class LocalPasswordModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

       // private Boolean PrivateMessageFeature { get; set; }
    }

    public class LoginModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterFreeBaseModel
    {

        [Display(Name = "Email address")]
        [Required(ErrorMessage = "The email address is required")]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        [StringLength(56, ErrorMessage = "Email must be less than 56 characters long.")]
        [System.Web.Mvc.Remote("doesUserEmailExist", "Account", HttpMethod = "POST", ErrorMessage = "Email already exists. Please enter a different email address.")]
        public string UserName { get; set; }

        [Required]
        [Display(Name = "Password")]
        [MembershipPassword(ErrorMessage = "Please ensure that your password is at least 8 characters long",
            MinRequiredNonAlphanumericCharacters = 0,
            //MinNonAlphanumericCharactersError = "Minimun of one non-alphanumeric character required",
            MinRequiredPasswordLength = 8,
            MinPasswordLengthError = "Minimum password lenght should be at least 8")]
        [StringLength(128, ErrorMessage = "Password must be less than 128 characters long.")]
        public string Password { get; set; }

        [Required]
        [Display(Name = "First Name")]
        [StringLength(56, ErrorMessage = "First Name must be less than 56 characters long.")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        [StringLength(56, ErrorMessage = "Last Name must be less than 56 characters long.")]
        public string LastName { get; set; }

        public string UserType { get; set; }

    }

    public class RegisterFreeCustomer:RegisterFreeBaseModel
    {
        [Required]
        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Phone Number Cell")]
        [StringLength(20, ErrorMessage = "Phone Number must be less than 20 characters long.")]
        public string PhoneNumberCell { get; set; }

        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Phone Number Land Line")]
        [StringLength(20, ErrorMessage = "Phone Number must be less than 20 characters long.")]
        public string PhoneNumberLandLine { get; set; }

        [Required]
        [Display(Name = "Address")]
        [StringLength(200, ErrorMessage = "Address must be up to 200 characters long.")]
        public string Address { get; set; }

        [Required]
        [Display(Name = "City")]
        [StringLength(56, ErrorMessage = "Cisty must be up to 56 characters long.")]
        public string City { get; set; }

        [Required]
        [Display(Name = "Province")]
        [StringLength(56, ErrorMessage = "Province must be up to 56 characters long.")]
        public string Province { get; set; }

        [Required]
        [Display(Name = "Country")]
        [StringLength(56, ErrorMessage = "Country must be up to 56 characters long.")]
        public string Country { get; set; }

        [Required]
        [Display(Name = "PostalCode")]
        [StringLength(20, ErrorMessage = "Postal Code must be up to 20 characters long.")]
        public string PostalCode { get; set; }

    }

    public class AccountSetupModel : RegisterFreeBaseModel
    {


        //[Required]
        //[Display(Name = "Mobile Carrier")]
        //[StringLength(56, ErrorMessage = "Mobile Carrier must be less than 56 characters long.")]
        //public string MobileCarrier { get; set; }

        [Required]
        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Phone Number")]
        [StringLength(20, ErrorMessage = "Phone Number must be less than 20 characters long.")]
        public string PhoneNumber { get; set; }

        [Required]
        [Display(Name = "Address")]
        [StringLength(200, ErrorMessage = "Address must be up to 200 characters long.")]
        public string Address { get; set; }

        [Required]
        [Display(Name = "City")]
        [StringLength(56, ErrorMessage = "Cisty must be up to 56 characters long.")]
        public string City { get; set; }

        [Required]
        [Display(Name = "Province")]
        [StringLength(56, ErrorMessage = "Province must be up to 56 characters long.")]
        public string Province { get; set; }

        [Required]
        [Display(Name = "Country")]
        [StringLength(56, ErrorMessage = "Country must be up to 56 characters long.")]
        public string Country { get; set; }

        [Required]
        [Display(Name = "PostalCode")]
        [StringLength(20, ErrorMessage = "Postal Code must be up to 20 characters long.")]
        public string PostalCode { get; set; }

        [Required]
        [Display(Name = "BusinessName")]
        [StringLength(100, ErrorMessage = "Business name must be up to 100 characters long.")]
        public string BusinessName { get; set; }

        [Required]
        [Display(Name = "WebsiteUrl")]
        [StringLength(200, ErrorMessage = "Website Address must be up to 200 characters long.")]
        public string WebsiteUrl { get; set; }

        [Display(Name = "AgentSubDomain")]
        [StringLength(50, ErrorMessage = "Agent Subdomain is required")]
        public string AgentSubDomain { get; set; }

        [Required]
        [Display(Name = "NoOfEmployees")]
        [StringLength(20, ErrorMessage = "Number of employees")]
        public string NoOfEmployees { get; set; }

        public string UserType { get; set; }

    }

    public class RegisterBaseModel
    {
        [Display(Name = "Email address")]
        [Required(ErrorMessage = "The email address is required")]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        [StringLength(56, ErrorMessage = "Email must be less than 56 characters long.")]
        [System.Web.Mvc.Remote("doesUserEmailExist", "Account", HttpMethod = "POST", ErrorMessage = "Email already exists. Please enter a different email address.")]
        public string UserName { get; set; }

        [Required]
        public string UserType { get; set; }

        [Required]
        [Display(Name = "Password")]
        [MembershipPassword(ErrorMessage = "Please ensure that your password is at least 8 characters long",
            MinRequiredNonAlphanumericCharacters = 0,
            //MinNonAlphanumericCharactersError = "Minimun of one non-alphanumeric character required",
            MinRequiredPasswordLength = 8,            
            MinPasswordLengthError = "Minimum password lenght should be at least 8")]
        [StringLength(128, ErrorMessage = "Password must be less than 128 characters long.")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [CompareAttribute("Password", ErrorMessage = "The password and confirmation password do not match.")]
        [StringLength(128, ErrorMessage = "Password must be less than 128 characters long.")]
        public string ConfirmPassword { get; set; }


        //Contact information
        [Required]
        [Display(Name = "First Name")]
        [StringLength(56, ErrorMessage = "First Name must be less than 56 characters long.")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        [StringLength(56, ErrorMessage = "Last Name must be less than 56 characters long.")]
        public string LastName { get; set; }

        [Required]
        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Phone Number Cell")]
        [StringLength(20, ErrorMessage = "Phone Number must be less than 20 characters long.")]
        public string PhoneNumberCell { get; set; }

        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Phone Number Land Line")]
        [StringLength(20, ErrorMessage = "Phone Number must be less than 20 characters long.")]
        public string PhoneNumberLandLine { get; set; }

        //[Required]
        //[Display(Name = "Mobile Carrier")]
        //[StringLength(56, ErrorMessage = "Mobile Carrier must be less than 56 characters long.")]
        //public string MobileCarrier { get; set; }

        [Required]
        [Display(Name = "Address")]
        [StringLength(200, ErrorMessage = "Address must be up to 200 characters long.")]
        public string Address { get; set; }

        [Required]
        [Display(Name = "City")]
        [StringLength(56, ErrorMessage = "Cisty must be up to 56 characters long.")]
        public string City { get; set; }

        [Required]
        [Display(Name = "Province")]
        [StringLength(56, ErrorMessage = "Province must be up to 56 characters long.")]
        public string Province { get; set; }

        [Required]
        [Display(Name = "PostalCode")]
        [StringLength(20, ErrorMessage = "Postal Code must be up to 20 characters long.")]
        public string PostalCode { get; set; }

        //[Display(Name = "Enable Private Messaging Feature")]  
        //public Boolean PrivateMessageFeature { get; set; }
    }

    public class BillingBaseModel
    {
        [Display(Name = "Email address")]
        [Required(ErrorMessage = "The email address is required")]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        [StringLength(56, ErrorMessage = "Email must be less than 56 characters long.")]
        [System.Web.Mvc.Remote("doesUserEmailExist", "Account", HttpMethod = "POST", ErrorMessage = "Email already exists. Please enter a different email address.")]
        public string UserName { get; set; }

        [Required]
        public string UserType { get; set; }

        //Contact information
        [Required]
        [Display(Name = "First Name")]
        [StringLength(56, ErrorMessage = "First Name must be less than 56 characters long.")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        [StringLength(56, ErrorMessage = "Last Name must be less than 56 characters long.")]
        public string LastName { get; set; }

        [Required]
        [Display(Name = "Billing Name")]
        [StringLength(120, ErrorMessage = "Billing Name must be up to 120 characters long.")]
        public string BillingName { get; set; }

        [Required]
        [Display(Name = "Billing Address")]
        [StringLength(200, ErrorMessage = "Billing Address must be up to 200 characters long.")]
        public string BillingAddress { get; set; }

        [Required]
        [Display(Name = "Billing City")]
        [StringLength(56, ErrorMessage = "Billing City must be up to 56 characters long.")]
        public string BillingCity { get; set; }

        [Required]
        [Display(Name = "Billing Province")]
        [StringLength(56, ErrorMessage = "Billing Province must be up to 56 characters long.")]
        public string BillingProvince { get; set; }

        [Required]
        [Display(Name = "Billing PostalCode")]
        [StringLength(20, ErrorMessage = "Billing Postal Code must be up to 20 characters long.")]
        public string BillingPostalCode { get; set; }

        //Credit card information
        [Required]
        [StringLength(16, ErrorMessage = "Credit card must be up to 16 characters long.")]
        public string CardNumber { get; set; }

        public string CVC { get; set; }

        [Required(ErrorMessage = "Expiration month is required")]
        [Numeric(ErrorMessage = "Invalid Month")]
        [MinLength(2, ErrorMessage = "Invalid month length")]
        public string ExpirationMonth { get; set; }

        [Required(ErrorMessage = "Expiration year is required")]
        [Year(ErrorMessage = "Invalid year")]
        [MinLength(4, ErrorMessage = "Invalid year length")]
        public string ExpirationYear { get; set; }

        [Mandatory(ErrorMessage = "You must agree to the Terms to register.")]
        public Boolean AgreedToTermsOfService { get; set; }


        [Required]
        public string Package { get; set; }
        [Display(Name = "Sellers Privacy Protection $9.95/Weekly")]
        public bool PrivacyBronze { get; set; }
        [Display(Name = "Standard Lawn Sign ($75+Shipping) - (One Time Cost)")]
        public bool LawnSignBronze { get; set; }
        [Display(Name = "Durable Sign $125+Shipping - (One Time Cost)")]
        public bool DurableBronze { get; set; }
        [Display(Name = "Durable Sign $125+Shipping - (One Time Cost)")]
        public bool DurableSilver { get; set; }
        [Display(Name = "Ad Agent $199.95 - (One Time Cost)")]
        public bool AdAgentBronze { get; set; }
        [Display(Name = "Ad Agent $199.95 - (One Time Cost)")]
        public bool AdAgentSilver { get; set; }
        [Display(Name = "Seller Executive Assistant 49.95/Weekly")]
        public bool AssistantBronze { get; set; }
        [Display(Name = "Seller Executive Assistant 49.95/Weekly")]
        public bool AssistantSilver { get; set; }

        [Required]
        [Display(Name = "Become an Agent and get your own customized site to sell on.")]
        public bool isAgent { get; set; }

        [Display(Name = "AgentID number")]
        public int AgentID { get; set; }

        //[System.ComponentModel.DataAnnotations.UrlAttribute]
        [Display(Name = "URL ")]
        [MaxLength(50, ErrorMessage = "Maximum length should be no more than 50 characters.")]
        public string AgentURL { get; set; }

        [Display(Name = "Pay with my info")]
        public Boolean AgentPayOnUserBehalf { get; set; }


        //[Display(Name = "Enable Private Messaging Feature")]  
        //public Boolean PrivateMessageFeature { get; set; }
    }

    public class RegisterModel : RegisterBaseModel
    {

        //Billing information
        [Display(Name = "Pre-Checked Billing Address Same as Above")]        
        public bool CopyBillingAddress { get; set; }

        [Required]
        [Display(Name = "Billing Name")]
        [StringLength(120, ErrorMessage = "Billing Name must be up to 120 characters long.")]
        public string BillingName { get; set; }

        [Required]
        [Display(Name = "Billing Address")]
        [StringLength(200, ErrorMessage = "Billing Address must be up to 200 characters long.")]
        public string BillingAddress { get; set; }

        [Required]
        [Display(Name = "Billing City")]
        [StringLength(56, ErrorMessage = "Billing City must be up to 56 characters long.")]
        public string BillingCity { get; set; }

        [Required]
        [Display(Name = "Billing Province")]
        [StringLength(56, ErrorMessage = "Billing Province must be up to 56 characters long.")]
        public string BillingProvince { get; set; }

        [Required]
        [Display(Name = "Billing PostalCode")]
        [StringLength(20, ErrorMessage = "Billing Postal Code must be up to 20 characters long.")]
        public string BillingPostalCode { get; set; }

        [Required]
        [Display(Name = "Country")]
        [StringLength(56, ErrorMessage = "Country must be up to 56 characters long.")]
        public string Country { get; set; }

        //Credit card information
    //    [Required(ErrorMessage = "Card Number is required")]
        [StringLength(16, ErrorMessage = "Credit card must be up to 16 characters long.")]
    //    [RegularExpression(@"^((4\d{3})|(5[1-5]\d{2}))(-?|\040?)(\d{4}(-?|\040?)){3}|^(3[4,7]\d{2})(-?|\040?)\d{6}(-?|\040?)\d{5}", ErrorMessage = "Invalid Card number")]
        public string CardNumber { get; set; }

    //    [Required(ErrorMessage = "CVC is required")]
    //    [RegularExpression(@"^(?!000)\d{3,4}$", ErrorMessage = "Invalid CVC")]
        public string CVC { get; set; }

        [Required(ErrorMessage = "Expiration month is required")]
        [Numeric(ErrorMessage="Invalid Month")]
        [MinLength(2, ErrorMessage = "Invalid month length")]
        public string ExpirationMonth { get; set; }

        [Required(ErrorMessage = "Expiration year is required")]
        [Year(ErrorMessage="Invalid year")]
        [MinLength(4, ErrorMessage = "Invalid year length")]
        public string ExpirationYear { get; set; }

        [Mandatory(ErrorMessage = "You must agree to the Terms to register.")]
        public Boolean AgreedToTermsOfService { get; set; }

        //public string PackageName { get; set; }
        //public bool? PackagePrivacy { get; set; }
        //public bool? PackageLawnSign { get; set; }
        //public bool? PackageDurable { get; set; }
        //public bool? PackageAssistant { get; set; }
        //public bool? PackageAdAgent { get; set; }

        [Required]
        public string Package { get; set; }
        [Display(Name = "Sellers Privacy Protection $9.95/Weekly")]
        public bool PrivacyBronze { get; set; }
        [Display(Name = "Standard Lawn Sign ($75+Shipping) - (One Time Cost)")]
        public bool LawnSignBronze { get; set; }
        [Display(Name = "Durable Sign $125+Shipping - (One Time Cost)")]
        public bool DurableBronze { get; set; }
        [Display(Name = "Durable Sign $125+Shipping - (One Time Cost)")]
        public bool DurableSilver { get; set; }
        [Display(Name = "Ad Agent $199.95 - (One Time Cost)")]
        public bool AdAgentBronze { get; set; }
        [Display(Name = "Ad Agent $199.95 - (One Time Cost)")]  
        public bool AdAgentSilver { get; set; }
        [Display(Name = "Seller Executive Assistant 49.95/Weekly")]
        public bool AssistantBronze { get; set; }
        [Display(Name = "Seller Executive Assistant 49.95/Weekly")]
        public bool AssistantSilver { get; set; }

        [Required]
        [Display(Name = "Become an Agent and get your own customized site to sell on.")]
        public bool isAgent { get; set; }

        [Display(Name = "AgentID number")]
        public int AgentID { get; set; }

        //[System.ComponentModel.DataAnnotations.UrlAttribute]
        [Display(Name = "URL ")]
        [MaxLength(50, ErrorMessage="Maximum length should be no more than 50 characters.")]
        public string AgentURL { get; set; }

        [Display(Name = "Pay with my info")]
        public Boolean AgentPayOnUserBehalf { get; set; }

        [Required]
        public string UserType { get; set; }
    }

    public class UpgradeBaseModel
    {
        [Display(Name = "Email address")]
        [Required(ErrorMessage = "The email address is required")]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        [StringLength(56, ErrorMessage = "Email must be less than 56 characters long.")]
        [System.Web.Mvc.Remote("doesUserEmailExist", "Account", HttpMethod = "POST", ErrorMessage = "Email already exists. Please enter a different email address.")]
        public string UserName { get; set; }

        [Required]
        public string UserType { get; set; }

        //[Required]
        //[Display(Name = "Password")]
        //[MembershipPassword(ErrorMessage = "Please ensure that your password is at least 8 characters long",
        //    MinRequiredNonAlphanumericCharacters = 0,
        //    //MinNonAlphanumericCharactersError = "Minimun of one non-alphanumeric character required",
        //    MinRequiredPasswordLength = 8,
        //    MinPasswordLengthError = "Minimum password lenght should be at least 8")]
        //[StringLength(128, ErrorMessage = "Password must be less than 128 characters long.")]
        //public string Password { get; set; }

        //[DataType(DataType.Password)]
        //[Display(Name = "Confirm password")]
        //[CompareAttribute("Password", ErrorMessage = "The password and confirmation password do not match.")]
        //[StringLength(128, ErrorMessage = "Password must be less than 128 characters long.")]
        //public string ConfirmPassword { get; set; }


        //Contact information
        [Required]
        [Display(Name = "First Name")]
        [StringLength(56, ErrorMessage = "First Name must be less than 56 characters long.")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        [StringLength(56, ErrorMessage = "Last Name must be less than 56 characters long.")]
        public string LastName { get; set; }

        [Required]
        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Phone Number Cell")]
        [StringLength(20, ErrorMessage = "Phone Number must be less than 20 characters long.")]
        public string PhoneNumberCell { get; set; }

        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Phone Number Land Line")]
        [StringLength(20, ErrorMessage = "Phone Number must be less than 20 characters long.")]
        public string PhoneNumberLandLine { get; set; }

        //[Required]
        //[Display(Name = "Mobile Carrier")]
        //[StringLength(56, ErrorMessage = "Mobile Carrier must be less than 56 characters long.")]
        //public string MobileCarrier { get; set; }

        [Required]
        [Display(Name = "Address")]
        [StringLength(200, ErrorMessage = "Address must be up to 200 characters long.")]
        public string Address { get; set; }

        [Required]
        [Display(Name = "City")]
        [StringLength(56, ErrorMessage = "Cisty must be up to 56 characters long.")]
        public string City { get; set; }

        [Required]
        [Display(Name = "Province")]
        [StringLength(56, ErrorMessage = "Province must be up to 56 characters long.")]
        public string Province { get; set; }

        [Required]
        [Display(Name = "PostalCode")]
        [StringLength(20, ErrorMessage = "Postal Code must be up to 20 characters long.")]
        public string PostalCode { get; set; }

        //[Display(Name = "Enable Private Messaging Feature")]  
        //public Boolean PrivateMessageFeature { get; set; }
    }

    public class UpgradeModel : UpgradeBaseModel
    {

        //Billing information
        [Display(Name = "Pre-Checked Billing Address Same as Above")]
        public bool CopyBillingAddress { get; set; }

        [Required]
        [Display(Name = "Billing Name")]
        [StringLength(120, ErrorMessage = "Billing Name must be up to 120 characters long.")]
        public string BillingName { get; set; }

        [Required]
        [Display(Name = "Billing Address")]
        [StringLength(200, ErrorMessage = "Billing Address must be up to 200 characters long.")]
        public string BillingAddress { get; set; }

        [Required]
        [Display(Name = "Billing City")]
        [StringLength(56, ErrorMessage = "Billing City must be up to 56 characters long.")]
        public string BillingCity { get; set; }

        [Required]
        [Display(Name = "Billing Province")]
        [StringLength(56, ErrorMessage = "Billing Province must be up to 56 characters long.")]
        public string BillingProvince { get; set; }

        [Required]
        [Display(Name = "Billing PostalCode")]
        [StringLength(20, ErrorMessage = "Billing Postal Code must be up to 20 characters long.")]
        public string BillingPostalCode { get; set; }

        [Required]
        [Display(Name = "Country")]
        [StringLength(56, ErrorMessage = "Country must be up to 56 characters long.")]
        public string Country { get; set; }

        //Credit card information
        //    [Required(ErrorMessage = "Card Number is required")]
        [StringLength(16, ErrorMessage = "Credit card must be up to 16 characters long.")]
        //    [RegularExpression(@"^((4\d{3})|(5[1-5]\d{2}))(-?|\040?)(\d{4}(-?|\040?)){3}|^(3[4,7]\d{2})(-?|\040?)\d{6}(-?|\040?)\d{5}", ErrorMessage = "Invalid Card number")]
        public string CardNumber { get; set; }

        //    [Required(ErrorMessage = "CVC is required")]
        //    [RegularExpression(@"^(?!000)\d{3,4}$", ErrorMessage = "Invalid CVC")]
        public string CVC { get; set; }

        [Required(ErrorMessage = "Expiration month is required")]
        [Numeric(ErrorMessage = "Invalid Month")]
        [MinLength(2, ErrorMessage = "Invalid month length")]
        public string ExpirationMonth { get; set; }

        [Required(ErrorMessage = "Expiration year is required")]
        [Year(ErrorMessage = "Invalid year")]
        [MinLength(4, ErrorMessage = "Invalid year length")]
        public string ExpirationYear { get; set; }

        [Mandatory(ErrorMessage = "You must agree to the Terms to register.")]
        public Boolean AgreedToTermsOfService { get; set; }

        //public string PackageName { get; set; }
        //public bool? PackagePrivacy { get; set; }
        //public bool? PackageLawnSign { get; set; }
        //public bool? PackageDurable { get; set; }
        //public bool? PackageAssistant { get; set; }
        //public bool? PackageAdAgent { get; set; }

        [Required]
        public string Package { get; set; }
        [Display(Name = "Sellers Privacy Protection $9.95/Weekly")]
        public bool PrivacyBronze { get; set; }
        [Display(Name = "Standard Lawn Sign ($75+Shipping) - (One Time Cost)")]
        public bool LawnSignBronze { get; set; }
        [Display(Name = "Durable Sign $125+Shipping - (One Time Cost)")]
        public bool DurableBronze { get; set; }
        [Display(Name = "Durable Sign $125+Shipping - (One Time Cost)")]
        public bool DurableSilver { get; set; }
        [Display(Name = "Ad Agent $199.95 - (One Time Cost)")]
        public bool AdAgentBronze { get; set; }
        [Display(Name = "Ad Agent $199.95 - (One Time Cost)")]
        public bool AdAgentSilver { get; set; }
        [Display(Name = "Seller Executive Assistant 49.95/Weekly")]
        public bool AssistantBronze { get; set; }
        [Display(Name = "Seller Executive Assistant 49.95/Weekly")]
        public bool AssistantSilver { get; set; }

        [Required]
        [Display(Name = "Become an Agent and get your own customized site to sell on.")]
        public bool isAgent { get; set; }

        [Display(Name = "AgentID number")]
        public int AgentID { get; set; }

        //[System.ComponentModel.DataAnnotations.UrlAttribute]
        [Display(Name = "URL ")]
        [MaxLength(50, ErrorMessage = "Maximum length should be no more than 50 characters.")]
        public string AgentURL { get; set; }

        [Display(Name = "Pay with my info")]
        public Boolean AgentPayOnUserBehalf { get; set; }

        [Required]
        public string UserType { get; set; }
    }

    public class ExternalLogin
    {
        public string Provider { get; set; }
        public string ProviderDisplayName { get; set; }
        public string ProviderUserId { get; set; }
    }
}
