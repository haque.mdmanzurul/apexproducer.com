﻿using HOD.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace HOD.Models
{
    public class ListingLayoutContext : DbContext
    {
        public ListingLayoutContext()
            : base("HODContext2")
        {
        }
        public DbSet<ListingLayout> LLayout { get; set; }
    }

    [Table("ListingLayout")]
    public class ListingLayout
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ListingLayoutId { get; set; }
        public int ListingId { get; set; }
        public string HeaderImage { get; set; }
        public string SkinColor { get; set; }
        public string BackgroundImage { get; set; }
        public string BackgroundColor { get; set; }
    }

    public class ListingDataContext : DbContext
    {
        public ListingDataContext()
            : base("HODContext2")
        {
        }
        public DbSet<ListingData> Listings { get; set; }
    }

    [Table("Listings")]
    public class ListingData
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)] 
        public int ListingId { get; set; }
        public int UserId { get; set; } 
        public string PropertyStatus { get; set; }
    }

    public class UserOrderContext : DbContext
    {
        public UserOrderContext()
            : base("HODContext2")
        {
        }
        public DbSet<UserOrder> UserOrders { get; set; }
    }

    [Table("UserOrder")]
    public class UserOrder
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int OrderId { get; set; }
        public int UserId { get; set; }
        public string PackageName { get; set; }
        public int PackagePrice { get; set; }
    }
}