﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web;
using System.Web.Helpers;
using System.Web.Security;
using DataAnnotationsExtensions;

namespace HOD.Models
{
    public class VirtualListing
    {

        private string ImageVirtualSignPlaceHolder = "nohouse.png";
        private string ImageLocationPlaceHolder = "nohouse.png";
        private string ImageKitchenPlaceHolder = "nohouse.png";
        private string ImageBedroomPlaceHolder = "nohouse.png";
        private string ImageOBedroomPlaceHolder = "nohouse.png";
        private string ImageBasementPlaceHolder = "nohouse.png";
        private string ImageDiningPlaceHolder = "nohouse.png";
        private string ImageFamilyPlaceHolder = "nohouse.png";
        private string ImageBathroomPlaceHolder = "nohouse.png";
        private string ImageOtherSection1PlaceHolder = "nohouse.png";
        private string ImageOtherSection2PlaceHolder = "nohouse.png";
        private string ImageOtherSection3PlaceHolder = "nohouse.png";
        private string ImageOtherSection4PlaceHolder = "nohouse.png";

        public string ListingId { get; set; }
        public int? UserId { get; set; }

        [Required]
        public string PropertyType { get; set; }

        public string PropertyStatus { get; set; }
        
        public string ListingPerson { get; set; }
        [Required]
        public string ContactPerson { get; set; }
        
        public bool PrivateSellerOption { get; set; }

        public string TollFreeNumber { get; set; }

        public string CientUrl { get; set; }

        [Required]
        public string PhoneNo { get; set; }

        public bool DontShowPhoneNo { get; set; }
        [Numeric]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:c0}")]
        [Required]
        [Range(1, (double)Decimal.MaxValue, ErrorMessage = "Please enter a value greater than {1} for List Price ")]
        public decimal ListPrice { get; set; }
        public bool DontShowListPrice { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:n0}")]
        [Required]
        public decimal SquareFootage { get; set; }
        public bool DontShowSquareFootage { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:F0}")]
        [Required]
        public decimal NoOfBathrooms { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:F0}")]
        [Required]
        public decimal NoOfBedrooms { get; set; }
        [Required(ErrorMessage="Sellers Description is a required field")]       
        public string MainSellingDesc { get; set; }
        public string PropetyDetails { get; set; }

        public string Image1VirtualSign { get; set; }
        public string Image2VirtualSign { get; set; }
        public string Image3VirtualSign { get; set; }
        public string Image4VirtualSign { get; set; }
        public string Image5VirtualSign { get; set; }
        public string Image6VirtualSign { get; set; }

        //[Required]
        public string LocationDesc { get; set; }
        //public List<string> locationImages { get; set; }

        public string Image1Location { get; set; }
        public string Image2Location { get; set; }
        public string Image3Location { get; set; }
        public string Image4Location { get; set; }
        public string Image5Location { get; set; }
        public string Image6Location { get; set; }

        [Required]
        public string Address { get; set; }
        [Required]
        public string City { get; set; }
        [Required]
        public string Province { get; set; }
        [Required]
        public string PostalCode { get; set; }

        public string Neighbourhood { get; set; }
        public string KitchenDesc { get; set; }
        public string Image1Kitchen { get; set; }
        public string Image2Kitchen { get; set; }
        public string Image3Kitchen { get; set; }
        public string Image4Kitchen { get; set; }
        public string Image5Kitchen { get; set; }
        public string Image6Kitchen { get; set; }

        public string BedroomDesc { get; set; }
        public string Image1Bedroom { get; set; }
        public string Image2Bedroom { get; set; }
        public string Image3Bedroom { get; set; }
        public string Image4Bedroom { get; set; }
        public string Image5Bedroom { get; set; }
        public string Image6Bedroom { get; set; }

        public string OBedroomDesc { get; set; }
        public string Image1OBedroom { get; set; }
        public string Image2OBedroom { get; set; }
        public string Image3OBedroom { get; set; }
        public string Image4OBedroom { get; set; }
        public string Image5OBedroom { get; set; }
        public string Image6OBedroom { get; set; }

        public string BasementDesc { get; set; }
        public string Image1Basement { get; set; }
        public string Image2Basement { get; set; }
        public string Image3Basement { get; set; }
        public string Image4Basement { get; set; }
        public string Image5Basement { get; set; }
        public string Image6Basement { get; set; }


        public string DiningDesc { get; set; }
        public string Image1Dining { get; set; }
        public string Image2Dining { get; set; }
        public string Image3Dining { get; set; }
        public string Image4Dining { get; set; }
        public string Image5Dining { get; set; }
        public string Image6Dining { get; set; }

        public string OtherSection1Desc { get; set; }
        public string Image1OtherSection1 { get; set; }
        public string Image2OtherSection1 { get; set; }
        public string Image3OtherSection1 { get; set; }
        public string Image4OtherSection1 { get; set; }
        public string Image5OtherSection1 { get; set; }
        public string Image6OtherSection1 { get; set; }

        public string OtherSection2Desc { get; set; }
        public string Image1OtherSection2 { get; set; }
        public string Image2OtherSection2 { get; set; }
        public string Image3OtherSection2 { get; set; }
        public string Image4OtherSection2 { get; set; }
        public string Image5OtherSection2 { get; set; }
        public string Image6OtherSection2 { get; set; }

        public string OtherSection3Desc { get; set; }
        public string Image1OtherSection3 { get; set; }
        public string Image2OtherSection3 { get; set; }
        public string Image3OtherSection3 { get; set; }
        public string Image4OtherSection3 { get; set; }
        public string Image5OtherSection3 { get; set; }
        public string Image6OtherSection3 { get; set; }

        public string OtherSection4Desc { get; set; }
        public string Image1OtherSection4 { get; set; }
        public string Image2OtherSection4 { get; set; }
        public string Image3OtherSection4 { get; set; }
        public string Image4OtherSection4 { get; set; }
        public string Image5OtherSection4 { get; set; }
        public string Image6OtherSection4 { get; set; }

        public string BathroomDesc { get; set; }
        public string Image1Bathroom { get; set; }
        public string Image2Bathroom { get; set; }
        public string Image3Bathroom { get; set; }
        public string Image4Bathroom { get; set; }
        public string Image5Bathroom { get; set; }
        public string Image6Bathroom { get; set; }

        public string FamilyDesc { get; set; }
        public string Image1Family { get; set; }
        public string Image2Family { get; set; }
        public string Image3Family { get; set; }
        public string Image4Family { get; set; }
        public string Image5Family { get; set; }
        public string Image6Family { get; set; }

        public bool isLocationPublic { get; set; }
        public bool isKitchenPublic { get; set; }
        public bool isBathroomPublic { get; set; }
        public bool isBedroomPublic { get; set; }
        public bool isBasementPublic  { get; set; }
        public bool isDiningPublic { get; set; }
        public bool isFamilyRoomPublic { get; set; }
        public bool isVideoPublic { get; set; }
        public bool isOtherSection1Public { get; set; }
        public bool isOtherSection2Public { get; set; }
        public bool isOtherSection3Public { get; set; }
        public bool isOtherSection4Public { get; set; }

        public List<string> KitchenImages { get; set; }
        public List<string> BathroomImages { get; set; }
        public List<string> BasementImages { get; set; }
        public List<string> OBedroomImages { get; set; }
        public List<string> DiningImages { get; set; }
        public List<string> FamilyImages { get; set; }
        public List<string> OtherSection1Images { get; set; }
        public List<string> OtherSection2Images { get; set; }
        public List<string> OtherSection3Images { get; set; }
        public List<string> OtherSection4Images { get; set; }

        public HttpPostedFileBase ImagesVirtualSign { get; set; }
       
        public List<HttpPostedFileBase> Images { get; set; }
       
        public string CurrentSection { get; set; }

        public int AgeOfHome { get; set; }
        public int NoOfFloors { get; set; }
        public bool hasBasement { get; set; }
        public bool hasFinishedBasement { get; set; }
        public bool hasGarage { get; set; }
        public bool hasAC { get; set; }
        public string heatType { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:c2}")]
        public decimal annualTaxes { get; set; }

        public string VideoDesc { get; set; }
        public string VideoID { get; set; }
        public string VideoLink { get; set; }

        public string PublicLawnSignImageUrl { get; set; }


        //Constructor
        public VirtualListing()
        {
            

            ListingId="";
            UserId=null;

       
            PropertyType ="";

            PropertyStatus = "";
        
            ListingPerson ="";
        
            ContactPerson="";
        
            PrivateSellerOption =false;

            TollFreeNumber="";

            CientUrl="";
        
            PhoneNo="";
            DontShowPhoneNo = false;
      
            ListPrice =0;
            DontShowListPrice=false;
       
            SquareFootage =0;
            DontShowSquareFootage =false;
        
            NoOfBathrooms=0;
            NoOfBedrooms =0;
        
            MainSellingDesc="";

            PropetyDetails = "None";

            Image1VirtualSign = ImageVirtualSignPlaceHolder;
            Image2VirtualSign = ImageVirtualSignPlaceHolder;
            Image3VirtualSign = ImageVirtualSignPlaceHolder;
            Image4VirtualSign = ImageVirtualSignPlaceHolder;
            Image5VirtualSign = ImageVirtualSignPlaceHolder;
            Image6VirtualSign = ImageVirtualSignPlaceHolder;

            LocationDesc="";

            Image1Location = ImageLocationPlaceHolder;
            Image2Location = ImageLocationPlaceHolder;
            Image3Location = ImageLocationPlaceHolder;
            Image4Location = ImageLocationPlaceHolder;
            Image5Location = ImageLocationPlaceHolder;
            Image6Location = ImageLocationPlaceHolder;

            Address="";        
            City="";
            Province="";
            PostalCode="";
            Neighbourhood = "";

            KitchenDesc="";
            Image1Kitchen = ImageKitchenPlaceHolder;
            Image2Kitchen = ImageKitchenPlaceHolder;
            Image3Kitchen = ImageKitchenPlaceHolder;
            Image4Kitchen = ImageKitchenPlaceHolder;
            Image5Kitchen = ImageKitchenPlaceHolder;
            Image6Kitchen = ImageKitchenPlaceHolder;

            BedroomDesc="";
            Image1Bedroom = ImageBedroomPlaceHolder;
            Image2Bedroom = ImageBedroomPlaceHolder;
            Image3Bedroom = ImageBedroomPlaceHolder;
            Image4Bedroom = ImageBedroomPlaceHolder;
            Image5Bedroom = ImageBedroomPlaceHolder;
            Image6Bedroom = ImageBedroomPlaceHolder;

            OBedroomDesc = "";
            Image1OBedroom = ImageOBedroomPlaceHolder;
            Image2OBedroom = ImageOBedroomPlaceHolder;
            Image3OBedroom = ImageOBedroomPlaceHolder;
            Image4OBedroom = ImageOBedroomPlaceHolder;
            Image5OBedroom = ImageOBedroomPlaceHolder;
            Image6OBedroom = ImageOBedroomPlaceHolder;

            BasementDesc="";
            Image1Basement = ImageBasementPlaceHolder;
            Image2Basement = ImageBasementPlaceHolder;
            Image3Basement = ImageBasementPlaceHolder;
            Image4Basement = ImageBasementPlaceHolder;
            Image5Basement = ImageBasementPlaceHolder;
            Image6Basement = ImageBasementPlaceHolder;


            DiningDesc="";
            Image1Dining = ImageDiningPlaceHolder;
            Image2Dining = ImageDiningPlaceHolder;
            Image3Dining = ImageDiningPlaceHolder;
            Image4Dining = ImageDiningPlaceHolder;
            Image5Dining = ImageDiningPlaceHolder;
            Image6Dining = ImageDiningPlaceHolder;

            FamilyDesc="";
            Image1Family = ImageFamilyPlaceHolder;
            Image2Family = ImageFamilyPlaceHolder;
            Image3Family = ImageFamilyPlaceHolder;
            Image4Family = ImageFamilyPlaceHolder;
            Image5Family = ImageFamilyPlaceHolder;
            Image6Family = ImageFamilyPlaceHolder;

            BathroomDesc="";
            Image1Bathroom = ImageBathroomPlaceHolder;
            Image2Bathroom = ImageBathroomPlaceHolder;
            Image3Bathroom = ImageBathroomPlaceHolder;
            Image4Bathroom = ImageBathroomPlaceHolder;
            Image5Bathroom = ImageBathroomPlaceHolder;
            Image6Bathroom = ImageBathroomPlaceHolder;

            OtherSection1Desc = "";
            Image1OtherSection1 = Image1OtherSection1;
            Image2OtherSection1 = Image2OtherSection1;
            Image3OtherSection1 = Image3OtherSection1;
            Image4OtherSection1 = Image4OtherSection1;
            Image5OtherSection1 = Image5OtherSection1;
            Image6OtherSection1 = Image6OtherSection1;

            OtherSection2Desc = "";
            Image1OtherSection2 = Image1OtherSection2;
            Image2OtherSection2 = Image2OtherSection2;
            Image3OtherSection2 = Image3OtherSection2;
            Image4OtherSection2 = Image4OtherSection2;
            Image5OtherSection2 = Image5OtherSection2;
            Image6OtherSection2 = Image6OtherSection2;

            OtherSection3Desc = "";
            Image1OtherSection3 = Image1OtherSection3;
            Image2OtherSection3 = Image2OtherSection3;
            Image3OtherSection3 = Image3OtherSection3;
            Image4OtherSection3 = Image4OtherSection3;
            Image5OtherSection3 = Image5OtherSection3;
            Image6OtherSection3 = Image6OtherSection3;

            OtherSection4Desc = "";
            Image1OtherSection4 = Image1OtherSection3;
            Image2OtherSection4 = Image2OtherSection3;
            Image3OtherSection4 = Image3OtherSection3;
            Image4OtherSection4 = Image4OtherSection3;
            Image5OtherSection4 = Image5OtherSection3;
            Image6OtherSection4 = Image6OtherSection3;   

            isLocationPublic=true;
            isKitchenPublic=true;
            isBathroomPublic=true;
            isBedroomPublic=true;
            isBasementPublic=true;
            isDiningPublic=true;
            isFamilyRoomPublic=true;
            isVideoPublic = true;
            isOtherSection1Public = true;
            isOtherSection2Public = true;
            isOtherSection3Public = true;
            isOtherSection4Public = true;

            ImagesVirtualSign=null;
       
            Images=null;
       
            CurrentSection="";

            AgeOfHome=0;
            NoOfFloors=0;
            hasBasement=false;
            hasFinishedBasement=false;
            hasGarage=false;
            hasAC=false;
            heatType = "";

            annualTaxes=0;

            VideoDesc="";
            VideoID="";
            VideoLink = "//www.youtube.com/embed/9eISx4Z4Jic?rel=0";
        }

        
    }

    public class VirtualListingDescription
    {
        public string ListingId { get; set; }

        //[Required]
        public string Desc { get; set; }
        //public List<string> locationImages { get; set; }

        public string Image1 { get; set; }
        public string Image2 { get; set; }
        public string Image3 { get; set; }
        public string Image4 { get; set; }
        public string Image5 { get; set; }
        public string Image6 { get; set; }
    }

    public class MessageModel
    {
        public string ListingId { get; set; }

        [Required]
        public int to { get; set; }
        public string UserId { get; set; }
        public int from { get; set; }
        [Required]
        public string Subject { get; set; }
        [Required]
        public string Body { get; set; }
    }

    public class ListingMessageModel
    {
        public MessageModel Message { get; set; }
        public VirtualListing Listing { get; set; }
        public List<HOD.Entities.ListingImage> Images { get; set; }
    }
    public class MenuItems
    {
        public string menuHeading { get; set; }
        public int menuId { get; set; }
        public List<MenuLinks> Links;
        public string menuRole { get; set; }
        public MenuItems()
        {
            menuHeading = "";
            menuId = 0;
            List<MenuLinks> Links = new List<MenuLinks>();
            menuRole = "";
        }
    }

    public struct MenuLinks
    {
        public string Name;
        public string Hyperlink;
        public int Position;
    }




}
