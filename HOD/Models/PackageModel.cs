﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HOD.Models
{
    public class PackageModel
    {
        public string Name { get; set; }
        public List<PackageFeatureModel> Features { get; set; }
    }

    public class PackageFeatureModel
    {
        public string Name { get; set; }
        public bool Optional { get; set; }
        public bool Shipping { get; set; }
    }

    public class PackageFeatureSelectionModel
    {
        public string Package { get; set; }
        public bool PrivacyBronze { get; set; }
        public bool LawnSignBronze { get; set; }
        public bool DurableBronze { get; set; }
        public bool DurableSilver  { get; set; }
        public bool AdAgentBronze { get; set; }
        public bool AdAgentSilver { get; set; }
        public bool AssistantBronze { get; set; }
        public bool AssistantSilver { get; set; }
    }
}