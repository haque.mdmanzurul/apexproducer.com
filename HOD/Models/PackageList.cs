﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace HOD.Models
{
    public class PackageListContext : DbContext
    {
        public PackageListContext()
            : base("HODContext2")
        {
        }
        public DbSet<PackageList> PList { get; set; }
    }

    [Table("PackageList")]
    public class PackageList
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int PackageId { get; set; }
        public string PackageName { get; set; }
        public string PackageDescription { get; set; }
        public int PackageAmount { get; set; }
        public int PackageTypeId { get; set; }
        public int UploadLimit { get; set; } 
    }
}
